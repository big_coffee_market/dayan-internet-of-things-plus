/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50740
 Source Host           : localhost:3306
 Source Schema         : ry-cloud

 Target Server Type    : MySQL
 Target Server Version : 50740
 File Encoding         : 65001

 Date: 06/01/2023 16:14:48
*/

DROP DATABASE IF EXISTS `ry-cloud`;

CREATE DATABASE  `ry-cloud` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

USE `ry-cloud`;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'Blob类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '日历信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'Cron类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Etc/UTC');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Etc/UTC');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Etc/UTC');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '已触发的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '任务详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.ruoyi.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001B636F6D2E72756F79692E6A6F622E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E00097872002B636F6D2E72756F79692E636F6D6D6F6E2E636F72652E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000184E3CB3BD078707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.ruoyi.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001B636F6D2E72756F79692E6A6F622E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E00097872002B636F6D2E72756F79692E636F6D6D6F6E2E636F72652E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000184E3CB3BD078707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.ruoyi.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001B636F6D2E72756F79692E6A6F622E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E00097872002B636F6D2E72756F79692E636F6D6D6F6E2E636F72652E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000184E3CB3BD078707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '存储的悲观锁信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '暂停的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '调度器状态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', '70b161bcc6c81672991900377', 1672992879976, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '简单触发器的信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '同步机制的行锁表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) NULL DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(2) NULL DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '触发器详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1672884910000, -1, 5, 'PAUSED', 'CRON', 1672884902000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1672884915000, -1, 5, 'PAUSED', 'CRON', 1672884903000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1672884920000, -1, 5, 'PAUSED', 'CRON', 1672884903000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-12-06 03:37:38', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-12-06 03:37:38', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-12-06 03:37:38', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2022-12-06 03:37:38', '', NULL, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-12-06 03:37:36', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-12-06 03:37:36', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-12-06 03:37:36', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-12-06 03:37:36', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-12-06 03:37:36', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-12-06 03:37:36', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-12-06 03:37:36', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-12-06 03:37:36', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-12-06 03:37:36', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-12-06 03:37:36', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 78 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-12-06 03:37:38', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (30, 1, 'tcp', '1', 'iot_protocol_suite', NULL, 'default', 'Y', '0', 'admin', '2022-08-25 01:04:40', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (31, 2, 'udp', '2', 'iot_protocol_suite', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:04:52', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (32, 3, 'mqtt', '3', 'iot_protocol_suite', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:05:04', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (39, 1, 'Int', '1', 'iot_field_type', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:28:04', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (40, 2, 'Bytes', '2', 'iot_field_type', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:28:23', '', NULL, '数组');
INSERT INTO `sys_dict_data` VALUES (41, 3, 'Long', '3', 'iot_field_type', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:28:42', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (42, 4, 'String', '4', 'iot_field_type', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:29:06', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (43, 5, 'Boolean', '5', 'iot_field_type', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:29:30', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (44, 6, 'Float', '6', 'iot_field_type', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:30:00', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (45, 7, 'Double', '7', 'iot_field_type', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:30:11', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (46, 8, 'Hex', '8', 'iot_field_type', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:30:43', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (47, 9, 'Byte', '9', 'iot_field_type', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:31:05', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (48, 1, '长度x位', '1', 'iot_trim_type', '', '', 'Y', '0', 'admin', '2022-08-25 01:35:44', 'admin', '2022-09-19 14:20:15', '顺序解析到第几位');
INSERT INTO `sys_dict_data` VALUES (49, 2, '到倒数x位', '2', 'iot_trim_type', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:36:17', 'admin', '2022-09-19 14:20:28', '一直到倒数第几位');
INSERT INTO `sys_dict_data` VALUES (50, 3, '直到x字符', '3', 'iot_trim_type', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:38:50', 'admin', '2022-09-19 14:20:35', '顺序结构，直到某某字符结束，例如 AA CC BB DD。参数是BB，所得结果是AA CC，参数是 CC ，所得结果是AA。');
INSERT INTO `sys_dict_data` VALUES (51, 1, '设备', '1', 'iot_message_by', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:57:08', 'admin', '2022-09-15 11:19:00', '设备发起');
INSERT INTO `sys_dict_data` VALUES (52, 2, '平台', '2', 'iot_message_by', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 01:57:28', 'admin', '2022-09-15 11:19:08', '平台发起');
INSERT INTO `sys_dict_data` VALUES (53, 1, '请求', '1', 'iot_message_type', '', '', 'Y', '0', 'admin', '2022-08-25 01:59:58', 'admin', '2022-09-15 11:22:02', '指令消息，需要回复');
INSERT INTO `sys_dict_data` VALUES (54, 2, '通知', '2', 'iot_message_type', '', '', 'Y', '0', 'admin', '2022-08-25 02:00:50', 'admin', '2022-09-15 14:00:14', '通知消息，不需要回复');
INSERT INTO `sys_dict_data` VALUES (55, 3, '回复', '3', 'iot_message_type', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 02:02:22', 'admin', '2022-09-15 11:22:14', '回复消息，与指令消息相对应');
INSERT INTO `sys_dict_data` VALUES (56, 1, '正常', '1', 'iot_device_state', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 06:15:16', 'admin', '2022-09-13 16:02:32', NULL);
INSERT INTO `sys_dict_data` VALUES (57, 2, '故障', '2', 'iot_device_state', NULL, NULL, 'Y', '0', 'admin', '2022-08-25 06:15:31', 'admin', '2022-09-13 16:02:40', NULL);
INSERT INTO `sys_dict_data` VALUES (60, 1, '准备', '1', 'iot_product_state', NULL, NULL, 'Y', '0', 'admin', '2022-08-29 06:47:30', 'admin', '2022-09-19 16:21:04', '准备');
INSERT INTO `sys_dict_data` VALUES (61, 2, '运行', '2', 'iot_product_state', NULL, NULL, 'Y', '0', 'admin', '2022-08-29 06:48:03', 'admin', '2022-09-15 10:53:16', '运行');
INSERT INTO `sys_dict_data` VALUES (62, 3, '停止', '3', 'iot_product_state', NULL, NULL, 'Y', '0', 'admin', '2022-08-29 06:48:18', 'admin', '2022-09-15 10:53:24', '停止');
INSERT INTO `sys_dict_data` VALUES (64, 10, 'CP56Time2a', '10', 'iot_field_type', NULL, NULL, 'Y', '0', 'admin', '2022-08-30 02:03:50', '', NULL, 'IEC时间格式');
INSERT INTO `sys_dict_data` VALUES (65, 1, '立项', '1', 'iot_project_state', NULL, 'default', 'N', '0', 'admin', '2022-09-09 14:22:54', 'admin', '2022-09-15 09:16:55', '立项');
INSERT INTO `sys_dict_data` VALUES (66, 2, '施工', '2', 'iot_project_state', NULL, 'default', 'N', '0', 'admin', '2022-09-09 14:30:30', 'admin', '2022-09-15 09:17:02', '施工');
INSERT INTO `sys_dict_data` VALUES (67, 4, '验收', '4', 'iot_project_state', NULL, 'default', 'N', '0', 'admin', '2022-09-09 14:31:43', 'admin', '2022-09-15 09:17:31', '验收');
INSERT INTO `sys_dict_data` VALUES (68, 3, '竣工', '3', 'iot_project_state', NULL, 'default', 'N', '0', 'admin', '2022-09-09 14:37:57', 'admin', '2022-09-15 09:17:07', '竣工');
INSERT INTO `sys_dict_data` VALUES (69, 5, '完工', '5', 'iot_project_state', NULL, 'default', 'N', '0', 'admin', '2022-09-09 14:38:57', 'admin', '2022-09-15 09:17:37', '完工');
INSERT INTO `sys_dict_data` VALUES (71, 1, '在线', '1', 'iot_communication', NULL, 'default', 'N', '0', 'admin', '2022-09-13 16:04:32', 'admin', '2022-09-16 10:08:17', '在线');
INSERT INTO `sys_dict_data` VALUES (72, 2, '离线', '2', 'iot_communication', NULL, 'default', 'N', '0', 'admin', '2022-09-13 16:04:46', 'admin', '2022-09-16 10:08:24', '离线');
INSERT INTO `sys_dict_data` VALUES (73, 4, '通知 | 回复', '4', 'iot_message_type', NULL, 'default', 'N', '0', 'admin', '2022-09-15 13:55:32', 'admin', '2022-09-28 17:00:42', '有时候是通知消息，有时候是回复消息');
INSERT INTO `sys_dict_data` VALUES (74, 1, '注册帧', '1', 'dict_frame', NULL, 'default', 'N', '0', 'admin', '2022-12-12 13:53:55', '', NULL, '注册帧');
INSERT INTO `sys_dict_data` VALUES (75, 2, '心跳帧', '2', 'dict_frame', NULL, 'default', 'N', '0', 'admin', '2022-12-12 13:55:34', '', NULL, '心跳帧');
INSERT INTO `sys_dict_data` VALUES (76, 3, 'hex-数据帧', '3', 'dict_frame', NULL, 'default', 'N', '0', 'admin', '2022-12-12 13:55:46', 'admin', '2022-12-19 13:33:06', 'hex编码的数据帧');
INSERT INTO `sys_dict_data` VALUES (77, 4, 'ascii-数据帧', '4', 'dict_frame', NULL, 'default', 'N', '0', 'admin', '2022-12-19 13:25:58', 'admin', '2022-12-19 13:32:46', 'ascii编码的数据帧');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2022-12-06 03:37:37', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2022-12-06 03:37:37', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2022-12-06 03:37:37', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2022-12-06 03:37:37', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2022-12-06 03:37:37', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2022-12-06 03:37:37', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2022-12-06 03:37:37', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2022-12-06 03:37:37', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2022-12-06 03:37:37', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2022-12-06 03:37:37', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (11, '协议簇', 'iot_protocol_suite', '0', 'admin', '2022-08-25 00:52:30', 'admin', '2022-08-25 00:55:41', '传输协议列表');
INSERT INTO `sys_dict_type` VALUES (14, '字段类型', 'iot_field_type', '0', 'admin', '2022-08-25 01:25:24', 'admin', '2022-08-25 01:25:34', '字段类型');
INSERT INTO `sys_dict_type` VALUES (15, '截获类型', 'iot_trim_type', '0', 'admin', '2022-08-25 01:32:24', '', NULL, '数组截获方式');
INSERT INTO `sys_dict_type` VALUES (16, '消息发起方', 'iot_message_by', '0', 'admin', '2022-08-25 01:56:18', '', NULL, '消息发起方');
INSERT INTO `sys_dict_type` VALUES (17, '消息类型', 'iot_message_type', '0', 'admin', '2022-08-25 01:58:09', '', NULL, '消息类型');
INSERT INTO `sys_dict_type` VALUES (18, '设备状态', 'iot_device_state', '0', 'admin', '2022-08-25 06:14:53', '', NULL, '设备状态');
INSERT INTO `sys_dict_type` VALUES (19, '产品状态', 'iot_product_state', '0', 'admin', '2022-08-29 06:46:10', '', NULL, '产品状态');
INSERT INTO `sys_dict_type` VALUES (20, '项目状态', 'iot_project_state', '0', 'admin', '2022-09-09 14:20:39', '', NULL, '项目状态');
INSERT INTO `sys_dict_type` VALUES (21, '通讯状态', 'iot_communication', '0', 'admin', '2022-09-13 16:03:42', '', NULL, '设备通讯状态');
INSERT INTO `sys_dict_type` VALUES (22, '帧类型', 'dict_frame', '0', 'admin', '2022-12-12 13:52:08', '', NULL, '帧类型');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-12-06 03:37:38', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-12-06 03:37:38', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-12-06 03:37:38', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提示信息',
  `access_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 166 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (1, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-06 11:40:19');
INSERT INTO `sys_logininfor` VALUES (2, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 08:37:15');
INSERT INTO `sys_logininfor` VALUES (3, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 08:41:38');
INSERT INTO `sys_logininfor` VALUES (4, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 08:41:48');
INSERT INTO `sys_logininfor` VALUES (5, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 08:42:05');
INSERT INTO `sys_logininfor` VALUES (6, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 08:42:35');
INSERT INTO `sys_logininfor` VALUES (7, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 08:45:55');
INSERT INTO `sys_logininfor` VALUES (8, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 08:55:02');
INSERT INTO `sys_logininfor` VALUES (9, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 08:58:06');
INSERT INTO `sys_logininfor` VALUES (10, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 09:08:47');
INSERT INTO `sys_logininfor` VALUES (11, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 09:11:14');
INSERT INTO `sys_logininfor` VALUES (12, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 09:12:20');
INSERT INTO `sys_logininfor` VALUES (13, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 09:13:07');
INSERT INTO `sys_logininfor` VALUES (14, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 09:14:40');
INSERT INTO `sys_logininfor` VALUES (15, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 09:35:09');
INSERT INTO `sys_logininfor` VALUES (16, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 09:46:31');
INSERT INTO `sys_logininfor` VALUES (17, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-07 10:04:34');
INSERT INTO `sys_logininfor` VALUES (18, 'admin', '172.21.0.1', '0', '退出成功', '2022-12-07 13:14:37');
INSERT INTO `sys_logininfor` VALUES (19, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-07 13:14:41');
INSERT INTO `sys_logininfor` VALUES (20, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-07 14:36:00');
INSERT INTO `sys_logininfor` VALUES (21, 'admin', '172.21.0.1', '0', '退出成功', '2022-12-07 14:48:04');
INSERT INTO `sys_logininfor` VALUES (22, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-07 14:48:12');
INSERT INTO `sys_logininfor` VALUES (23, 'admin', '172.22.0.1', '0', '登录成功', '2022-12-07 15:02:24');
INSERT INTO `sys_logininfor` VALUES (24, 'admin', '172.22.0.1', '0', '退出成功', '2022-12-07 16:43:35');
INSERT INTO `sys_logininfor` VALUES (25, 'admin', '172.22.0.1', '0', '登录成功', '2022-12-07 16:43:47');
INSERT INTO `sys_logininfor` VALUES (26, 'admin', '172.23.0.1', '0', '登录成功', '2022-12-07 17:11:29');
INSERT INTO `sys_logininfor` VALUES (27, 'admin', '172.23.0.1', '0', '登录成功', '2022-12-07 17:18:48');
INSERT INTO `sys_logininfor` VALUES (28, 'admin', '172.25.0.1', '0', '登录成功', '2022-12-07 17:37:15');
INSERT INTO `sys_logininfor` VALUES (29, 'admin', '172.26.0.1', '0', '退出成功', '2022-12-07 17:50:44');
INSERT INTO `sys_logininfor` VALUES (30, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-07 17:51:19');
INSERT INTO `sys_logininfor` VALUES (31, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 08:41:28');
INSERT INTO `sys_logininfor` VALUES (32, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 10:43:19');
INSERT INTO `sys_logininfor` VALUES (33, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 10:47:26');
INSERT INTO `sys_logininfor` VALUES (34, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 10:49:15');
INSERT INTO `sys_logininfor` VALUES (35, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 10:50:50');
INSERT INTO `sys_logininfor` VALUES (36, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 10:51:42');
INSERT INTO `sys_logininfor` VALUES (37, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 10:56:38');
INSERT INTO `sys_logininfor` VALUES (38, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 11:10:10');
INSERT INTO `sys_logininfor` VALUES (39, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 11:11:04');
INSERT INTO `sys_logininfor` VALUES (40, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 11:11:51');
INSERT INTO `sys_logininfor` VALUES (41, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 11:21:15');
INSERT INTO `sys_logininfor` VALUES (42, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 11:25:12');
INSERT INTO `sys_logininfor` VALUES (43, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 11:43:59');
INSERT INTO `sys_logininfor` VALUES (44, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 13:08:49');
INSERT INTO `sys_logininfor` VALUES (45, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 13:26:59');
INSERT INTO `sys_logininfor` VALUES (46, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 13:27:31');
INSERT INTO `sys_logininfor` VALUES (47, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 13:30:37');
INSERT INTO `sys_logininfor` VALUES (48, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 13:34:11');
INSERT INTO `sys_logininfor` VALUES (49, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 13:35:10');
INSERT INTO `sys_logininfor` VALUES (50, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 13:38:26');
INSERT INTO `sys_logininfor` VALUES (51, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 13:39:12');
INSERT INTO `sys_logininfor` VALUES (52, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 13:39:26');
INSERT INTO `sys_logininfor` VALUES (53, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 13:39:54');
INSERT INTO `sys_logininfor` VALUES (54, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 13:52:11');
INSERT INTO `sys_logininfor` VALUES (55, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 14:10:15');
INSERT INTO `sys_logininfor` VALUES (56, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 14:11:04');
INSERT INTO `sys_logininfor` VALUES (57, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 14:13:27');
INSERT INTO `sys_logininfor` VALUES (58, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 14:14:53');
INSERT INTO `sys_logininfor` VALUES (59, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 14:25:45');
INSERT INTO `sys_logininfor` VALUES (60, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-08 18:31:06');
INSERT INTO `sys_logininfor` VALUES (61, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-09 13:40:24');
INSERT INTO `sys_logininfor` VALUES (62, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-09 15:23:22');
INSERT INTO `sys_logininfor` VALUES (63, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-09 15:53:48');
INSERT INTO `sys_logininfor` VALUES (64, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-09 16:07:41');
INSERT INTO `sys_logininfor` VALUES (65, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-09 16:17:03');
INSERT INTO `sys_logininfor` VALUES (66, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-09 16:18:48');
INSERT INTO `sys_logininfor` VALUES (67, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-09 16:21:29');
INSERT INTO `sys_logininfor` VALUES (68, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-09 16:24:02');
INSERT INTO `sys_logininfor` VALUES (69, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-09 17:09:00');
INSERT INTO `sys_logininfor` VALUES (70, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-09 17:39:38');
INSERT INTO `sys_logininfor` VALUES (71, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-12 08:27:52');
INSERT INTO `sys_logininfor` VALUES (72, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-12 08:35:42');
INSERT INTO `sys_logininfor` VALUES (73, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-12 08:37:01');
INSERT INTO `sys_logininfor` VALUES (74, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-12 09:26:14');
INSERT INTO `sys_logininfor` VALUES (75, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-12 09:26:46');
INSERT INTO `sys_logininfor` VALUES (76, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-12 09:32:40');
INSERT INTO `sys_logininfor` VALUES (77, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-12 09:35:57');
INSERT INTO `sys_logininfor` VALUES (78, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-12 09:37:29');
INSERT INTO `sys_logininfor` VALUES (79, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-12 09:48:26');
INSERT INTO `sys_logininfor` VALUES (80, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-12 10:05:07');
INSERT INTO `sys_logininfor` VALUES (81, 'admin', '172.26.0.1', '0', '登录成功', '2022-12-12 10:08:50');
INSERT INTO `sys_logininfor` VALUES (82, 'admin', '172.18.0.1', '0', '退出成功', '2022-12-12 10:31:42');
INSERT INTO `sys_logininfor` VALUES (83, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-12 10:41:50');
INSERT INTO `sys_logininfor` VALUES (84, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-12 10:42:02');
INSERT INTO `sys_logininfor` VALUES (85, 'admin', '172.19.0.1', '0', '登录成功', '2022-12-12 11:24:04');
INSERT INTO `sys_logininfor` VALUES (86, 'admin', '172.19.0.1', '0', '登录成功', '2022-12-12 11:27:14');
INSERT INTO `sys_logininfor` VALUES (87, 'admin', '172.19.0.1', '0', '登录成功', '2022-12-12 11:45:17');
INSERT INTO `sys_logininfor` VALUES (88, 'admin', '172.19.0.1', '0', '登录成功', '2022-12-12 11:46:38');
INSERT INTO `sys_logininfor` VALUES (89, 'admin', '172.19.0.1', '0', '登录成功', '2022-12-12 12:00:12');
INSERT INTO `sys_logininfor` VALUES (90, 'admin', '172.19.0.1', '0', '登录成功', '2022-12-12 12:00:40');
INSERT INTO `sys_logininfor` VALUES (91, 'admin', '172.19.0.1', '0', '登录成功', '2022-12-12 12:01:45');
INSERT INTO `sys_logininfor` VALUES (92, 'admin', '172.19.0.1', '0', '登录成功', '2022-12-12 13:05:53');
INSERT INTO `sys_logininfor` VALUES (93, 'admin', '172.19.0.1', '0', '登录成功', '2022-12-12 13:08:01');
INSERT INTO `sys_logininfor` VALUES (94, 'admin', '172.20.0.1', '0', '退出成功', '2022-12-12 13:27:50');
INSERT INTO `sys_logininfor` VALUES (95, 'admin', '172.20.0.1', '0', '登录成功', '2022-12-12 13:29:34');
INSERT INTO `sys_logininfor` VALUES (96, 'admin', '172.20.0.1', '0', '退出成功', '2022-12-12 13:30:04');
INSERT INTO `sys_logininfor` VALUES (97, 'admin', '172.20.0.1', '0', '登录成功', '2022-12-12 13:39:41');
INSERT INTO `sys_logininfor` VALUES (98, 'admin', '172.20.0.1', '0', '登录成功', '2022-12-12 13:40:25');
INSERT INTO `sys_logininfor` VALUES (99, 'admin', '172.20.0.1', '0', '登录成功', '2022-12-12 13:41:10');
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '172.20.0.1', '0', '登录成功', '2022-12-12 13:45:07');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '172.20.0.1', '0', '登录成功', '2022-12-12 13:47:58');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '172.20.0.1', '0', '登录成功', '2022-12-12 13:56:22');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '172.20.0.1', '0', '登录成功', '2022-12-12 14:00:16');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 14:42:17');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 15:05:25');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 15:05:44');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '172.21.0.1', '0', '退出成功', '2022-12-12 15:06:07');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 15:06:12');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 15:06:42');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 15:51:06');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 15:55:30');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 16:23:11');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 16:31:03');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 16:32:49');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 16:33:50');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 16:45:23');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 16:45:31');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 16:48:01');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '172.21.0.1', '0', '登录成功', '2022-12-12 16:50:08');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-13 15:52:55');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '172.19.0.1', '0', '退出成功', '2022-12-13 16:23:21');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '172.19.0.1', '0', '登录成功', '2022-12-13 16:23:25');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '172.20.0.1', '0', '退出成功', '2022-12-13 16:48:20');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '172.20.0.1', '0', '登录成功', '2022-12-13 16:55:31');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-14 10:23:25');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-14 10:36:20');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '172.23.0.1', '0', '登录成功', '2022-12-16 16:36:05');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '172.19.0.1', '0', '登录成功', '2022-12-19 13:22:37');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '172.20.0.1', '0', '登录成功', '2022-12-19 14:13:49');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '172.20.0.1', '0', '登录成功', '2022-12-19 14:54:52');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '172.20.0.1', '0', '登录成功', '2022-12-27 09:37:57');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '172.18.0.1', '0', '登录成功', '2022-12-29 08:46:34');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '172.22.0.1', '0', '退出成功', '2022-12-29 09:20:39');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '172.22.0.1', '0', '登录成功', '2022-12-29 09:20:43');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '192.168.2.128', '0', '登录成功', '2022-12-29 10:28:30');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '192.168.2.128', '0', '登录成功', '2022-12-30 08:55:30');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '0', '登录成功', '2022-12-30 09:25:05');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '192.168.2.128', '0', '登录成功', '2022-12-30 13:40:31');
INSERT INTO `sys_logininfor` VALUES (139, 'admin', '127.0.0.1', '0', '登录成功', '2022-12-30 14:43:04');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '127.0.0.1', '0', '登录成功', '2023-01-03 14:37:34');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '0', '登录成功', '2023-01-03 15:00:10');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '192.168.3.144', '0', '登录成功', '2023-01-03 15:09:21');
INSERT INTO `sys_logininfor` VALUES (143, 'admin', '192.168.3.144', '0', '登录成功', '2023-01-04 08:35:26');
INSERT INTO `sys_logininfor` VALUES (144, 'admin', '127.0.0.1', '0', '登录成功', '2023-01-04 08:38:13');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '172.22.0.1', '0', '登录成功', '2023-01-04 09:19:48');
INSERT INTO `sys_logininfor` VALUES (146, 'admin', '192.168.3.144', '0', '登录成功', '2023-01-04 18:38:23');
INSERT INTO `sys_logininfor` VALUES (147, 'admin', '127.0.0.1', '0', '登录成功', '2023-01-04 18:38:58');
INSERT INTO `sys_logininfor` VALUES (148, 'admin', '172.19.0.1', '0', '登录成功', '2023-01-04 18:40:06');
INSERT INTO `sys_logininfor` VALUES (149, 'admin', '192.168.3.144', '0', '登录成功', '2023-01-04 18:45:00');
INSERT INTO `sys_logininfor` VALUES (150, 'admin', '172.19.0.1', '0', '登录成功', '2023-01-04 18:59:14');
INSERT INTO `sys_logininfor` VALUES (151, 'admin', '172.19.0.1', '0', '登录成功', '2023-01-04 19:00:41');
INSERT INTO `sys_logininfor` VALUES (152, 'admin', '192.168.3.144', '0', '登录成功', '2023-01-04 19:22:17');
INSERT INTO `sys_logininfor` VALUES (153, 'admin', '172.20.0.1', '0', '登录成功', '2023-01-04 19:24:07');
INSERT INTO `sys_logininfor` VALUES (154, 'admin', '172.20.0.1', '0', '登录成功', '2023-01-05 09:14:47');
INSERT INTO `sys_logininfor` VALUES (155, 'admin', '172.18.0.1', '0', '退出成功', '2023-01-05 09:27:12');
INSERT INTO `sys_logininfor` VALUES (156, 'admin', '172.18.0.1', '0', '登录成功', '2023-01-05 09:27:21');
INSERT INTO `sys_logininfor` VALUES (157, 'admin', '172.18.0.1', '0', '退出成功', '2023-01-05 09:27:56');
INSERT INTO `sys_logininfor` VALUES (158, 'admin', '172.18.0.1', '0', '登录成功', '2023-01-05 09:28:00');
INSERT INTO `sys_logininfor` VALUES (159, 'admin', '172.19.0.1', '0', '退出成功', '2023-01-05 09:35:44');
INSERT INTO `sys_logininfor` VALUES (160, 'admin', '172.19.0.1', '0', '登录成功', '2023-01-05 09:35:49');
INSERT INTO `sys_logininfor` VALUES (161, 'admin', '117.89.130.8', '0', '登录成功', '2023-01-05 10:15:39');
INSERT INTO `sys_logininfor` VALUES (162, 'admin', '117.89.130.8', '0', '登录成功', '2023-01-05 10:33:30');
INSERT INTO `sys_logininfor` VALUES (163, 'admin', '114.221.202.138', '0', '登录成功', '2023-01-06 15:10:03');
INSERT INTO `sys_logininfor` VALUES (164, 'admin', '192.168.3.144', '0', '退出成功', '2023-01-06 16:10:11');
INSERT INTO `sys_logininfor` VALUES (165, 'admin', '192.168.3.144', '0', '登录成功', '2023-01-06 16:10:14');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1191 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2022-12-06 03:37:36', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, 'monitor', NULL, '', 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2022-12-06 03:37:36', '', NULL, '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2022-12-06 03:37:36', '', NULL, '系统工具目录');
INSERT INTO `sys_menu` VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', NULL, '', 0, 0, 'M', '0', '0', '', 'guide', 'admin', '2022-12-06 03:37:36', '', NULL, '若依官网地址');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-12-06 03:37:36', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-12-06 03:37:36', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2022-12-06 03:37:37', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2022-12-06 03:37:37', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2022-12-06 03:37:37', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2022-12-06 03:37:37', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2022-12-06 03:37:37', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-12-06 03:37:37', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2022-12-06 03:37:37', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2022-12-06 03:37:37', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2022-12-06 03:37:37', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, 'Sentinel控制台', 2, 3, 'http://localhost:8718', '', '', 0, 0, 'C', '0', '0', 'monitor:sentinel:list', 'sentinel', 'admin', '2022-12-06 03:37:37', '', NULL, '流量控制菜单');
INSERT INTO `sys_menu` VALUES (112, 'Nacos控制台', 2, 4, 'http://localhost:8848/nacos', '', '', 0, 0, 'C', '0', '0', 'monitor:nacos:list', 'nacos', 'admin', '2022-12-06 03:37:37', '', NULL, '服务治理菜单');
INSERT INTO `sys_menu` VALUES (113, 'Admin控制台', 2, 5, 'http://localhost:9100/login', '', '', 0, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2022-12-06 03:37:37', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2022-12-06 03:37:37', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2022-12-06 03:37:37', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'http://localhost:8080/swagger-ui/index.html', '', '', 0, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2022-12-06 03:37:37', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'system/operlog/index', '', 1, 0, 'C', '0', '0', 'system:operlog:list', 'form', 'admin', '2022-12-06 03:37:37', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'system/logininfor/index', '', 1, 0, 'C', '0', '0', 'system:logininfor:list', 'logininfor', 'admin', '2022-12-06 03:37:37', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:operlog:query', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:operlog:remove', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '日志导出', 500, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:operlog:export', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:logininfor:query', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:logininfor:remove', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:logininfor:export', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '账户解锁', 501, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:logininfor:unlock', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 6, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2022-12-06 03:37:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1061, '物联管理', 0, 1, 'iot', NULL, NULL, 1, 0, 'M', '0', '0', '', 'online', 'admin', '2022-09-06 09:21:51', 'admin', '2022-09-06 09:22:44', '');
INSERT INTO `sys_menu` VALUES (1065, '指令解析', 1061, 52, 'cmdDecode', 'iot/cmdDecode/index', NULL, 1, 0, 'C', '0', '0', 'iot:cmdDecode:list', '#', 'admin', '2022-09-14 13:12:34', 'admin', '2022-12-29 13:28:40', '指令解析菜单');
INSERT INTO `sys_menu` VALUES (1066, '指令解析查询', 1065, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmdDecode:query', '#', 'admin', '2022-09-14 13:12:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1067, '指令解析新增', 1065, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmdDecode:add', '#', 'admin', '2022-09-14 13:12:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1068, '指令解析修改', 1065, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmdDecode:edit', '#', 'admin', '2022-09-14 13:12:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1069, '指令解析删除', 1065, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmdDecode:remove', '#', 'admin', '2022-09-14 13:12:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1070, '指令解析导出', 1065, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmdDecode:export', '#', 'admin', '2022-09-14 13:12:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1071, '指令编码', 1061, 51, 'cmdEncoder', 'iot/cmdEncoder/index', NULL, 1, 0, 'C', '0', '0', 'iot:cmdEncoder:list', '#', 'admin', '2022-09-14 13:12:51', 'admin', '2022-12-29 13:28:33', '指令编码菜单');
INSERT INTO `sys_menu` VALUES (1072, '指令编码查询', 1071, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmdEncoder:query', '#', 'admin', '2022-09-14 13:12:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1073, '指令编码新增', 1071, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmdEncoder:add', '#', 'admin', '2022-09-14 13:12:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1074, '指令编码修改', 1071, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmdEncoder:edit', '#', 'admin', '2022-09-14 13:12:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1075, '指令编码删除', 1071, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmdEncoder:remove', '#', 'admin', '2022-09-14 13:12:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1076, '指令编码导出', 1071, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmdEncoder:export', '#', 'admin', '2022-09-14 13:12:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1083, '指令', 1061, 50, 'cmd', 'iot/cmd/index', NULL, 1, 0, 'C', '0', '0', 'iot:cmd:list', '#', 'admin', '2022-09-14 13:13:34', 'admin', '2022-12-29 13:28:26', '指令菜单');
INSERT INTO `sys_menu` VALUES (1084, '指令查询', 1083, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmd:query', '#', 'admin', '2022-09-14 13:13:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1085, '指令新增', 1083, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmd:add', '#', 'admin', '2022-09-14 13:13:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1086, '指令修改', 1083, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmd:edit', '#', 'admin', '2022-09-14 13:13:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1087, '指令删除', 1083, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmd:remove', '#', 'admin', '2022-09-14 13:13:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1088, '指令导出', 1083, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:cmd:export', '#', 'admin', '2022-09-14 13:13:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1095, '厂家协议', 1061, 32, 'frame', 'iot/frame/index', NULL, 1, 0, 'C', '0', '0', 'iot:frame:list', '#', 'admin', '2022-09-14 13:13:55', 'admin', '2022-12-29 14:23:08', '协议帧类型菜单');
INSERT INTO `sys_menu` VALUES (1096, '协议帧查询', 1095, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:frame:query', '#', 'admin', '2022-09-14 13:13:55', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1097, '协议帧新增', 1095, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:frame:add', '#', 'admin', '2022-09-14 13:13:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1098, '协议帧修改', 1095, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:frame:edit', '#', 'admin', '2022-09-14 13:13:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1099, '协议帧删除', 1095, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:frame:remove', '#', 'admin', '2022-09-14 13:13:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1100, '协议帧导出', 1095, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:frame:export', '#', 'admin', '2022-09-14 13:13:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1125, '项目', 1061, 11, 'project', 'iot/project/index', NULL, 1, 0, 'C', '0', '0', 'iot:project:list', '#', 'admin', '2022-09-14 13:14:34', 'admin', '2022-09-26 16:51:11', '项目菜单');
INSERT INTO `sys_menu` VALUES (1126, '项目查询', 1125, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:project:query', '#', 'admin', '2022-09-14 13:14:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1127, '项目新增', 1125, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:project:add', '#', 'admin', '2022-09-14 13:14:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1128, '项目修改', 1125, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:project:edit', '#', 'admin', '2022-09-14 13:14:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1129, '项目删除', 1125, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:project:remove', '#', 'admin', '2022-09-14 13:14:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1130, '项目导出', 1125, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:project:export', '#', 'admin', '2022-09-14 13:14:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1137, '指令解释', 1061, 53, 'explain', 'iot/explain/index', NULL, 1, 0, 'C', '0', '0', 'iot:explain:list', '#', 'admin', '2022-09-26 15:03:54', 'admin', '2022-12-29 13:28:48', '指令解释菜单');
INSERT INTO `sys_menu` VALUES (1138, '指令解释查询', 1137, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:explain:query', '#', 'admin', '2022-09-26 15:03:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1139, '指令解释新增', 1137, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:explain:add', '#', 'admin', '2022-09-26 15:03:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1140, '指令解释修改', 1137, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:explain:edit', '#', 'admin', '2022-09-26 15:03:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1141, '指令解释删除', 1137, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:explain:remove', '#', 'admin', '2022-09-26 15:03:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1142, '指令解释导出', 1137, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:explain:export', '#', 'admin', '2022-09-26 15:03:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1143, '平台信息', 1061, 1, 'platform', 'iot/platform/index', NULL, 1, 0, 'C', '0', '0', 'iot:platform:list', '#', 'admin', '2022-12-08 10:13:34', '', NULL, '平台信息菜单');
INSERT INTO `sys_menu` VALUES (1144, '平台信息查询', 1143, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:platform:query', '#', 'admin', '2022-12-08 10:13:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1145, '平台信息新增', 1143, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:platform:add', '#', 'admin', '2022-12-08 10:13:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1146, '平台信息修改', 1143, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:platform:edit', '#', 'admin', '2022-12-08 10:13:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1147, '平台信息删除', 1143, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:platform:remove', '#', 'admin', '2022-12-08 10:13:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1148, '平台信息导出', 1143, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:platform:export', '#', 'admin', '2022-12-08 10:13:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1154, '项目导出', NULL, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:project:export', '#', 'admin', '2022-12-29 10:18:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1161, '设备', 1061, 20, 'device', 'iot/device/index', NULL, 1, 0, 'C', '0', '0', 'iot:device:list', '#', 'admin', '2022-12-29 10:21:35', 'admin', '2022-12-29 13:32:10', '设备菜单');
INSERT INTO `sys_menu` VALUES (1162, '设备查询', 1161, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:device:query', '#', 'admin', '2022-12-29 10:21:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1163, '设备新增', 1161, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:device:add', '#', 'admin', '2022-12-29 10:21:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1164, '设备修改', 1161, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:device:edit', '#', 'admin', '2022-12-29 10:21:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1165, '设备删除', 1161, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:device:remove', '#', 'admin', '2022-12-29 10:21:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1166, '设备导出', 1161, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:device:export', '#', 'admin', '2022-12-29 10:21:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1167, '产品-属性', 1061, 31, 'field', 'iot/field/index', NULL, 1, 0, 'C', '0', '0', 'iot:field:list', '#', 'admin', '2022-12-29 10:21:45', 'admin', '2022-12-29 13:29:59', '产品-属性菜单');
INSERT INTO `sys_menu` VALUES (1168, '产品-属性查询', 1167, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:field:query', '#', 'admin', '2022-12-29 10:21:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1169, '产品-属性新增', 1167, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:field:add', '#', 'admin', '2022-12-29 10:21:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1170, '产品-属性修改', 1167, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:field:edit', '#', 'admin', '2022-12-29 10:21:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1171, '产品-属性删除', 1167, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:field:remove', '#', 'admin', '2022-12-29 10:21:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1172, '产品-属性导出', 1167, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:field:export', '#', 'admin', '2022-12-29 10:21:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1173, '设备离在线', 1061, 21, 'line', 'iot/line/index', NULL, 1, 0, 'C', '0', '0', 'iot:line:list', '#', 'admin', '2022-12-29 10:21:52', 'admin', '2022-12-29 13:32:50', '设备离在线记录菜单');
INSERT INTO `sys_menu` VALUES (1174, '设备离在线记录查询', 1173, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:line:query', '#', 'admin', '2022-12-29 10:21:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1175, '设备离在线记录新增', 1173, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:line:add', '#', 'admin', '2022-12-29 10:21:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1176, '设备离在线记录修改', 1173, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:line:edit', '#', 'admin', '2022-12-29 10:21:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1177, '设备离在线记录删除', 1173, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:line:remove', '#', 'admin', '2022-12-29 10:21:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1178, '设备离在线记录导出', 1173, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:line:export', '#', 'admin', '2022-12-29 10:21:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1179, '产品-厂家协议', 1061, 33, 'product_frame', 'iot/product_frame/index', NULL, 1, 0, 'C', '0', '0', 'iot:product_frame:list', '#', 'admin', '2022-12-29 10:22:00', 'admin', '2022-12-29 14:23:22', '产品-数据帧菜单');
INSERT INTO `sys_menu` VALUES (1180, '产品-协议帧查询', 1179, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:product_frame:query', '#', 'admin', '2022-12-29 10:22:00', 'admin', '2022-12-29 14:23:59', '');
INSERT INTO `sys_menu` VALUES (1181, '产品-协议帧新增', 1179, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:product_frame:add', '#', 'admin', '2022-12-29 10:22:00', 'admin', '2022-12-29 14:24:07', '');
INSERT INTO `sys_menu` VALUES (1182, '产品-协议帧修改', 1179, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:product_frame:edit', '#', 'admin', '2022-12-29 10:22:00', 'admin', '2022-12-29 14:24:16', '');
INSERT INTO `sys_menu` VALUES (1183, '产品-协议帧删除', 1179, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:product_frame:remove', '#', 'admin', '2022-12-29 10:22:00', 'admin', '2022-12-29 14:24:24', '');
INSERT INTO `sys_menu` VALUES (1184, '产品-协议帧导出', 1179, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:product_frame:export', '#', 'admin', '2022-12-29 10:22:00', 'admin', '2022-12-29 14:24:33', '');
INSERT INTO `sys_menu` VALUES (1185, '产品', 1061, 30, 'product', 'iot/product/index', NULL, 1, 0, 'C', '0', '0', 'iot:product:list', '#', 'admin', '2022-12-29 10:22:08', 'admin', '2022-12-29 13:29:52', '产品菜单');
INSERT INTO `sys_menu` VALUES (1186, '产品查询', 1185, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:product:query', '#', 'admin', '2022-12-29 10:22:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1187, '产品新增', 1185, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:product:add', '#', 'admin', '2022-12-29 10:22:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1188, '产品修改', 1185, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:product:edit', '#', 'admin', '2022-12-29 10:22:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1189, '产品删除', 1185, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:product:remove', '#', 'admin', '2022-12-29 10:22:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1190, '产品导出', 1185, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'iot:product:export', '#', 'admin', '2022-12-29 10:22:08', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2022-12-06 03:37:38', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2022-12-06 03:37:38', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 232 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (1, '产品', 3, 'com.ruoyi.iot.controller.IotProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/2', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_device`, CONSTRAINT `iot_device_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))\n### The error may exist in class path resource [mapper/iot/IotProductMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotProductMapper.deleteIotProductByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_product where id in           (               ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_device`, CONSTRAINT `iot_device_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_device`, CONSTRAINT `iot_device_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_device`, CONSTRAINT `iot_device_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))', '2022-12-06 13:04:58');
INSERT INTO `sys_oper_log` VALUES (2, '设备', 3, 'com.ruoyi.iot.controller.IotDeviceController.remove()', 'DELETE', 1, 'admin', NULL, '/device/4', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:05:12');
INSERT INTO `sys_oper_log` VALUES (3, '设备', 3, 'com.ruoyi.iot.controller.IotDeviceController.remove()', 'DELETE', 1, 'admin', NULL, '/device/5', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:05:19');
INSERT INTO `sys_oper_log` VALUES (4, '产品', 3, 'com.ruoyi.iot.controller.IotProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/2', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_product_frame`, CONSTRAINT `iot_product_frame_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))\n### The error may exist in class path resource [mapper/iot/IotProductMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotProductMapper.deleteIotProductByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_product where id in           (               ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_product_frame`, CONSTRAINT `iot_product_frame_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_product_frame`, CONSTRAINT `iot_product_frame_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_product_frame`, CONSTRAINT `iot_product_frame_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))', '2022-12-06 13:05:56');
INSERT INTO `sys_oper_log` VALUES (5, '数据帧', 3, 'com.ruoyi.iot.controller.IotProductFrameController.remove()', 'DELETE', 1, 'admin', NULL, '/productFrame/10', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:06:19');
INSERT INTO `sys_oper_log` VALUES (6, '数据帧', 3, 'com.ruoyi.iot.controller.IotProductFrameController.remove()', 'DELETE', 1, 'admin', NULL, '/productFrame/11', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:06:21');
INSERT INTO `sys_oper_log` VALUES (7, '产品', 3, 'com.ruoyi.iot.controller.IotProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/3', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_product_frame`, CONSTRAINT `iot_product_frame_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))\n### The error may exist in class path resource [mapper/iot/IotProductMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotProductMapper.deleteIotProductByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_product where id in           (               ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_product_frame`, CONSTRAINT `iot_product_frame_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_product_frame`, CONSTRAINT `iot_product_frame_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_product_frame`, CONSTRAINT `iot_product_frame_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))', '2022-12-06 13:06:32');
INSERT INTO `sys_oper_log` VALUES (8, '产品', 3, 'com.ruoyi.iot.controller.IotProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/2', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_project_product`, CONSTRAINT `iot_project_product_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))\n### The error may exist in class path resource [mapper/iot/IotProductMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotProductMapper.deleteIotProductByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_product where id in           (               ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_project_product`, CONSTRAINT `iot_project_product_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_project_product`, CONSTRAINT `iot_project_product_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_project_product`, CONSTRAINT `iot_project_product_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))', '2022-12-06 13:06:35');
INSERT INTO `sys_oper_log` VALUES (9, '项目与产品关联', 3, 'com.ruoyi.iot.controller.IotProjectProductController.remove()', 'DELETE', 1, 'admin', NULL, '/projectProduct/1', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:06:49');
INSERT INTO `sys_oper_log` VALUES (10, '项目与产品关联', 3, 'com.ruoyi.iot.controller.IotProjectProductController.remove()', 'DELETE', 1, 'admin', NULL, '/projectProduct/3', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:06:51');
INSERT INTO `sys_oper_log` VALUES (11, '项目与产品关联', 3, 'com.ruoyi.iot.controller.IotProjectProductController.remove()', 'DELETE', 1, 'admin', NULL, '/projectProduct/2', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:06:53');
INSERT INTO `sys_oper_log` VALUES (12, '产品', 3, 'com.ruoyi.iot.controller.IotProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/2', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:07:03');
INSERT INTO `sys_oper_log` VALUES (13, '产品', 3, 'com.ruoyi.iot.controller.IotProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/3', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_product_frame`, CONSTRAINT `iot_product_frame_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))\n### The error may exist in class path resource [mapper/iot/IotProductMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotProductMapper.deleteIotProductByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_product where id in           (               ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_product_frame`, CONSTRAINT `iot_product_frame_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_product_frame`, CONSTRAINT `iot_product_frame_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_product_frame`, CONSTRAINT `iot_product_frame_iot_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`))', '2022-12-06 13:08:43');
INSERT INTO `sys_oper_log` VALUES (14, '数据帧', 3, 'com.ruoyi.iot.controller.IotProductFrameController.remove()', 'DELETE', 1, 'admin', NULL, '/productFrame/1', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:09:02');
INSERT INTO `sys_oper_log` VALUES (15, '数据帧', 3, 'com.ruoyi.iot.controller.IotProductFrameController.remove()', 'DELETE', 1, 'admin', NULL, '/productFrame/13', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:09:04');
INSERT INTO `sys_oper_log` VALUES (16, '数据帧', 3, 'com.ruoyi.iot.controller.IotProductFrameController.remove()', 'DELETE', 1, 'admin', NULL, '/productFrame/5', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:09:06');
INSERT INTO `sys_oper_log` VALUES (17, '数据帧', 3, 'com.ruoyi.iot.controller.IotProductFrameController.remove()', 'DELETE', 1, 'admin', NULL, '/productFrame/12', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:09:09');
INSERT INTO `sys_oper_log` VALUES (18, '数据帧', 3, 'com.ruoyi.iot.controller.IotProductFrameController.remove()', 'DELETE', 1, 'admin', NULL, '/productFrame/4', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:09:11');
INSERT INTO `sys_oper_log` VALUES (19, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/1', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:11:19');
INSERT INTO `sys_oper_log` VALUES (20, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/2', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:11:24');
INSERT INTO `sys_oper_log` VALUES (21, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/5', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:11:26');
INSERT INTO `sys_oper_log` VALUES (22, '指令编码', 3, 'com.ruoyi.iot.controller.IotCmdEncoderController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdEncoder/225,226,227,228', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:13:11');
INSERT INTO `sys_oper_log` VALUES (23, '指令解析', 3, 'com.ruoyi.iot.controller.IotCmdDecodeController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdDecode/223,224,225,226,227', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:13:26');
INSERT INTO `sys_oper_log` VALUES (24, '指令解释', 3, 'com.ruoyi.iot.controller.IotCmdExplainController.remove()', 'DELETE', 1, 'admin', NULL, '/explain/51,52,53,54,55,56,58,59,60,61', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:13:33');
INSERT INTO `sys_oper_log` VALUES (25, '指令解释', 3, 'com.ruoyi.iot.controller.IotCmdExplainController.remove()', 'DELETE', 1, 'admin', NULL, '/explain/62,63,77,78,79,80,81,82,83,84', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:13:36');
INSERT INTO `sys_oper_log` VALUES (26, '指令解释', 3, 'com.ruoyi.iot.controller.IotCmdExplainController.remove()', 'DELETE', 1, 'admin', NULL, '/explain/85,86,87,88,89,90,91,92,93,94', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:13:39');
INSERT INTO `sys_oper_log` VALUES (27, '指令解释', 3, 'com.ruoyi.iot.controller.IotCmdExplainController.remove()', 'DELETE', 1, 'admin', NULL, '/explain/96,99,100,101,102,103,104,105,107,108', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:13:42');
INSERT INTO `sys_oper_log` VALUES (28, '指令解释', 3, 'com.ruoyi.iot.controller.IotCmdExplainController.remove()', 'DELETE', 1, 'admin', NULL, '/explain/109,110,111,112', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:13:46');
INSERT INTO `sys_oper_log` VALUES (29, '指令', 3, 'com.ruoyi.iot.controller.IotCmdController.remove()', 'DELETE', 1, 'admin', NULL, '/cmd/80,81', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:13:59');
INSERT INTO `sys_oper_log` VALUES (30, '数据包-字段', 3, 'com.ruoyi.iot.controller.IotPacketFieldController.remove()', 'DELETE', 1, 'admin', NULL, '/packetField/128,129,130,131,132,133,134', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:14:07');
INSERT INTO `sys_oper_log` VALUES (31, '数据包-字段', 3, 'com.ruoyi.iot.controller.IotPacketFieldController.remove()', 'DELETE', 1, 'admin', NULL, '/packetField/93,94,95,96,97,98,99,100,101,102', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`))\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.deleteIotPacketFieldByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_packet_field where id in           (               ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`))', '2022-12-06 13:14:13');
INSERT INTO `sys_oper_log` VALUES (32, '数据包', 3, 'com.ruoyi.iot.controller.IotPacketController.remove()', 'DELETE', 1, 'admin', NULL, '/packet/2,3,4,5,6,7,8', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`))\n### The error may exist in class path resource [mapper/iot/IotPacketMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketMapper.deleteIotPacketByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_packet where id in           (               ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`))', '2022-12-06 13:14:21');
INSERT INTO `sys_oper_log` VALUES (33, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/4', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:14:32');
INSERT INTO `sys_oper_log` VALUES (34, '产品', 3, 'com.ruoyi.iot.controller.IotProductController.remove()', 'DELETE', 1, 'admin', NULL, '/product/3,4', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:14:40');
INSERT INTO `sys_oper_log` VALUES (35, '数据包', 3, 'com.ruoyi.iot.controller.IotPacketController.remove()', 'DELETE', 1, 'admin', NULL, '/packet/2,3,4,5,6,7,8', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`))\n### The error may exist in class path resource [mapper/iot/IotPacketMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketMapper.deleteIotPacketByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_packet where id in           (               ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`))', '2022-12-06 13:20:46');
INSERT INTO `sys_oper_log` VALUES (36, '数据包-字段', 3, 'com.ruoyi.iot.controller.IotPacketFieldController.remove()', 'DELETE', 1, 'admin', NULL, '/packetField/93,94,95,96,97,98,99,100,101,102', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`))\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.deleteIotPacketFieldByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_packet_field where id in           (               ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`))', '2022-12-06 13:20:51');
INSERT INTO `sys_oper_log` VALUES (37, '指令', 3, 'com.ruoyi.iot.controller.IotCmdController.remove()', 'DELETE', 1, 'admin', NULL, '/cmd/42,43,44,45,46,47,48,49,50,51', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_encoder`, CONSTRAINT `iot_cmd_encoder_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`))\n### The error may exist in class path resource [mapper/iot/IotCmdMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotCmdMapper.deleteIotCmdByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_cmd where id in           (               ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_encoder`, CONSTRAINT `iot_cmd_encoder_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_encoder`, CONSTRAINT `iot_cmd_encoder_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_encoder`, CONSTRAINT `iot_cmd_encoder_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`))', '2022-12-06 13:21:03');
INSERT INTO `sys_oper_log` VALUES (38, '指令编码', 3, 'com.ruoyi.iot.controller.IotCmdEncoderController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdEncoder/128,129,130,131,132,133,134,135,136,137', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:21:12');
INSERT INTO `sys_oper_log` VALUES (39, '指令编码', 3, 'com.ruoyi.iot.controller.IotCmdEncoderController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdEncoder/138,139,140,141,142,143,144,145,146,147', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:21:15');
INSERT INTO `sys_oper_log` VALUES (40, '指令编码', 3, 'com.ruoyi.iot.controller.IotCmdEncoderController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdEncoder/148,149,150,151,152,153,154,155,156,157', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:21:18');
INSERT INTO `sys_oper_log` VALUES (41, '指令编码', 3, 'com.ruoyi.iot.controller.IotCmdEncoderController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdEncoder/158,159,160,161,162,163,164,165,166,167', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:21:21');
INSERT INTO `sys_oper_log` VALUES (42, '指令编码', 3, 'com.ruoyi.iot.controller.IotCmdEncoderController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdEncoder/168,169,170,171,172,173,174,175,176,177', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:21:24');
INSERT INTO `sys_oper_log` VALUES (43, '指令编码', 3, 'com.ruoyi.iot.controller.IotCmdEncoderController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdEncoder/178,179,180,181,182,183,184,185,186,187', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:21:27');
INSERT INTO `sys_oper_log` VALUES (44, '指令编码', 3, 'com.ruoyi.iot.controller.IotCmdEncoderController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdEncoder/188,189,190,191,192,193,194,195,196,197', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:21:30');
INSERT INTO `sys_oper_log` VALUES (45, '指令编码', 3, 'com.ruoyi.iot.controller.IotCmdEncoderController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdEncoder/198,199,200,201,202,203,204,205,206,207', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:21:32');
INSERT INTO `sys_oper_log` VALUES (46, '指令编码', 3, 'com.ruoyi.iot.controller.IotCmdEncoderController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdEncoder/208,209,210,211,212,213,214,215,216,217', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:21:35');
INSERT INTO `sys_oper_log` VALUES (47, '指令编码', 3, 'com.ruoyi.iot.controller.IotCmdEncoderController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdEncoder/218,219,220,221,222,223,224,229,230,231', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:21:38');
INSERT INTO `sys_oper_log` VALUES (48, '指令编码', 3, 'com.ruoyi.iot.controller.IotCmdEncoderController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdEncoder/232,233,234,235,236,237,238,239,240,241', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:21:42');
INSERT INTO `sys_oper_log` VALUES (49, '指令编码', 3, 'com.ruoyi.iot.controller.IotCmdEncoderController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdEncoder/242,243,244,245,246,247', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:21:44');
INSERT INTO `sys_oper_log` VALUES (50, '指令', 3, 'com.ruoyi.iot.controller.IotCmdController.remove()', 'DELETE', 1, 'admin', NULL, '/cmd/42,43,44,45,46,47,48,49,50,51', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`))\n### The error may exist in class path resource [mapper/iot/IotCmdMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotCmdMapper.deleteIotCmdByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_cmd where id in           (               ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`))', '2022-12-06 13:21:57');
INSERT INTO `sys_oper_log` VALUES (51, '数据包-字段', 3, 'com.ruoyi.iot.controller.IotPacketFieldController.remove()', 'DELETE', 1, 'admin', NULL, '/packetField/93,94,95,96,97,98,99,100,101,102', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`))\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.deleteIotPacketFieldByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_packet_field where id in           (               ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`))', '2022-12-06 13:22:02');
INSERT INTO `sys_oper_log` VALUES (52, '数据包', 3, 'com.ruoyi.iot.controller.IotPacketController.remove()', 'DELETE', 1, 'admin', NULL, '/packet/2,3,4,5,6,7,8', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`))\n### The error may exist in class path resource [mapper/iot/IotPacketMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketMapper.deleteIotPacketByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_packet where id in           (               ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`))', '2022-12-06 13:22:05');
INSERT INTO `sys_oper_log` VALUES (53, '厂家协议', 3, 'com.ruoyi.iot.controller.IotFrameController.remove()', 'DELETE', 1, 'admin', NULL, '/frame/1,2', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:22:31');
INSERT INTO `sys_oper_log` VALUES (54, '数据包', 3, 'com.ruoyi.iot.controller.IotPacketController.remove()', 'DELETE', 1, 'admin', NULL, '/packet/2,3,4,5,6,7,8', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`))\n### The error may exist in class path resource [mapper/iot/IotPacketMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketMapper.deleteIotPacketByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_packet where id in           (               ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd`, CONSTRAINT `iot_cmd_iot_packet_id_fk` FOREIGN KEY (`packet_id`) REFERENCES `iot_packet` (`id`))', '2022-12-06 13:22:42');
INSERT INTO `sys_oper_log` VALUES (55, '数据包-字段', 3, 'com.ruoyi.iot.controller.IotPacketFieldController.remove()', 'DELETE', 1, 'admin', NULL, '/packetField/93,94,95,96,97,98,99,100,101,102', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`))\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.deleteIotPacketFieldByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_packet_field where id in           (               ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_packet_field` (`id`))', '2022-12-06 13:22:46');
INSERT INTO `sys_oper_log` VALUES (56, '指令', 3, 'com.ruoyi.iot.controller.IotCmdController.remove()', 'DELETE', 1, 'admin', NULL, '/cmd/42,43,44,45,46,47,48,49,50,51', '172.18.0.1', '', NULL, NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`))\n### The error may exist in class path resource [mapper/iot/IotCmdMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotCmdMapper.deleteIotCmdByIds-Inline\n### The error occurred while setting parameters\n### SQL: delete from iot_cmd where id in           (               ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          ,              ?          )\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`))\n; Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`)); nested exception is java.sql.SQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`iot_large_derivative`.`iot_cmd_decode`, CONSTRAINT `iot_cmd_decode_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`))', '2022-12-06 13:22:52');
INSERT INTO `sys_oper_log` VALUES (57, '指令解析', 3, 'com.ruoyi.iot.controller.IotCmdDecodeController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdDecode/128,129,130,131,132,133,160,161,162,163', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:23:10');
INSERT INTO `sys_oper_log` VALUES (58, '指令解析', 3, 'com.ruoyi.iot.controller.IotCmdDecodeController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdDecode/164,165,166,167,168,169,170,171,172,173', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:23:14');
INSERT INTO `sys_oper_log` VALUES (59, '指令解析', 3, 'com.ruoyi.iot.controller.IotCmdDecodeController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdDecode/174,175,176,177,178,179,180,181,182,183', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:23:17');
INSERT INTO `sys_oper_log` VALUES (60, '指令解析', 3, 'com.ruoyi.iot.controller.IotCmdDecodeController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdDecode/184,185,186,187,188,189,190,191,192,193', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:23:20');
INSERT INTO `sys_oper_log` VALUES (61, '指令解析', 3, 'com.ruoyi.iot.controller.IotCmdDecodeController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdDecode/194,195,196,203,204,205,206,213,214,215', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:23:23');
INSERT INTO `sys_oper_log` VALUES (62, '指令解析', 3, 'com.ruoyi.iot.controller.IotCmdDecodeController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdDecode/216,217,218,219,220,221,222,228,229,230', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:23:25');
INSERT INTO `sys_oper_log` VALUES (63, '指令解析', 3, 'com.ruoyi.iot.controller.IotCmdDecodeController.remove()', 'DELETE', 1, 'admin', NULL, '/cmdDecode/231,232,233,234,235,236,237,238,239,240', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:23:27');
INSERT INTO `sys_oper_log` VALUES (64, '指令', 3, 'com.ruoyi.iot.controller.IotCmdController.remove()', 'DELETE', 1, 'admin', NULL, '/cmd/42,43,44,45,46,47,48,49,50,51', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:23:42');
INSERT INTO `sys_oper_log` VALUES (65, '指令', 3, 'com.ruoyi.iot.controller.IotCmdController.remove()', 'DELETE', 1, 'admin', NULL, '/cmd/52,53,54,55,56,57,58,59,60,61', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:23:45');
INSERT INTO `sys_oper_log` VALUES (66, '指令', 3, 'com.ruoyi.iot.controller.IotCmdController.remove()', 'DELETE', 1, 'admin', NULL, '/cmd/62,63,64,65,66,67,68,69,70,71', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:23:48');
INSERT INTO `sys_oper_log` VALUES (67, '指令', 3, 'com.ruoyi.iot.controller.IotCmdController.remove()', 'DELETE', 1, 'admin', NULL, '/cmd/72,73,74,75,78,79,82,83,84,85', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:23:51');
INSERT INTO `sys_oper_log` VALUES (68, '指令', 3, 'com.ruoyi.iot.controller.IotCmdController.remove()', 'DELETE', 1, 'admin', NULL, '/cmd/86,87', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:23:55');
INSERT INTO `sys_oper_log` VALUES (69, '数据包-字段', 3, 'com.ruoyi.iot.controller.IotPacketFieldController.remove()', 'DELETE', 1, 'admin', NULL, '/packetField/93,94,95,96,97,98,99,100,101,102', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:23:59');
INSERT INTO `sys_oper_log` VALUES (70, '数据包-字段', 3, 'com.ruoyi.iot.controller.IotPacketFieldController.remove()', 'DELETE', 1, 'admin', NULL, '/packetField/103,104,105,106,107,108,109,110,111,112', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:24:02');
INSERT INTO `sys_oper_log` VALUES (71, '数据包-字段', 3, 'com.ruoyi.iot.controller.IotPacketFieldController.remove()', 'DELETE', 1, 'admin', NULL, '/packetField/113,114,115,116,117,118,119,120,121,122', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:24:04');
INSERT INTO `sys_oper_log` VALUES (72, '数据包-字段', 3, 'com.ruoyi.iot.controller.IotPacketFieldController.remove()', 'DELETE', 1, 'admin', NULL, '/packetField/123,124,125,126,127,135,136,137,138,139', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:24:08');
INSERT INTO `sys_oper_log` VALUES (73, '数据包-字段', 3, 'com.ruoyi.iot.controller.IotPacketFieldController.remove()', 'DELETE', 1, 'admin', NULL, '/packetField/140,141,142,143,144', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:24:10');
INSERT INTO `sys_oper_log` VALUES (74, '数据包', 3, 'com.ruoyi.iot.controller.IotPacketController.remove()', 'DELETE', 1, 'admin', NULL, '/packet/2,3,4,5,6,7,8', '172.18.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 13:24:14');
INSERT INTO `sys_oper_log` VALUES (75, '项目信息', 1, 'com.ruoyi.iot.controller.IotProjectController.add()', 'POST', 1, 'admin', NULL, '/project', '172.18.0.1', '', '{\"id\":6,\"latitude\":31.651133,\"longitude\":119.02828,\"name\":\"溧水和凤项目\",\"params\":{},\"place\":\"江苏溧水区\",\"principal\":\"赵晓强\",\"principalTel\":\"18010775624\",\"remark\":\"大衍物联-第一枪\",\"startupTime\":\"2022-12-06 00:00:00\",\"state\":2,\"warrantyYear\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 16:14:11');
INSERT INTO `sys_oper_log` VALUES (76, '数据包', 1, 'com.ruoyi.iot.controller.IotPacketController.add()', 'POST', 1, 'admin', NULL, '/packet', '172.18.0.1', '', '{\"id\":9,\"name\":\"仁大建科 -- 土壤温度水分变送器 485型\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 16:17:59');
INSERT INTO `sys_oper_log` VALUES (77, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"defVal\":\"1\",\"field\":\"dev_adress\",\"fieldType\":1,\"len\":1,\"name\":\"地址码\",\"packetId\":9,\"params\":{},\"remark\":\"设备地址，默认01\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.insertIotPacketField-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_packet_field          ( name,             packet_id,             field,             field_type,             defVal,             len,                          remark )           values ( ?,             ?,             ?,             ?,             ?,             ?,                          ? )\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'', '2022-12-06 16:22:15');
INSERT INTO `sys_oper_log` VALUES (78, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"defVal\":\"1\",\"field\":\"dev_adress\",\"fieldType\":1,\"len\":1,\"name\":\"地址码\",\"packetId\":9,\"params\":{},\"remark\":\"设备地址，默认01\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.insertIotPacketField-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_packet_field          ( name,             packet_id,             field,             field_type,             defVal,             len,                          remark )           values ( ?,             ?,             ?,             ?,             ?,             ?,                          ? )\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'', '2022-12-06 16:22:19');
INSERT INTO `sys_oper_log` VALUES (79, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"defVal\":\"1\",\"field\":\"dev_adress\",\"fieldType\":1,\"len\":1,\"name\":\"地址码\",\"packetId\":9,\"params\":{},\"remark\":\"设备地址，默认01\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.insertIotPacketField-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_packet_field          ( name,             packet_id,             field,             field_type,             defVal,             len,                          remark )           values ( ?,             ?,             ?,             ?,             ?,             ?,                          ? )\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'', '2022-12-06 16:22:22');
INSERT INTO `sys_oper_log` VALUES (80, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"defVal\":\"1\",\"field\":\"dev_adress\",\"fieldType\":1,\"len\":1,\"name\":\"地址码\",\"packetId\":9,\"params\":{},\"remark\":\"设备地址，默认01\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.insertIotPacketField-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_packet_field          ( name,             packet_id,             field,             field_type,             defVal,             len,                          remark )           values ( ?,             ?,             ?,             ?,             ?,             ?,                          ? )\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'', '2022-12-06 16:22:26');
INSERT INTO `sys_oper_log` VALUES (81, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"defVal\":\"1\",\"field\":\"dev_adress\",\"fieldType\":1,\"len\":1,\"name\":\"地址码\",\"packetId\":9,\"params\":{},\"remark\":\"设备地址，默认01\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.insertIotPacketField-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_packet_field          ( name,             packet_id,             field,             field_type,             defVal,             len,                          remark )           values ( ?,             ?,             ?,             ?,             ?,             ?,                          ? )\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'', '2022-12-06 16:22:29');
INSERT INTO `sys_oper_log` VALUES (82, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"defVal\":\"1\",\"field\":\"dev_adress\",\"fieldType\":1,\"len\":1,\"name\":\"地址码\",\"packetId\":9,\"params\":{},\"remark\":\"设备地址，默认01\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.insertIotPacketField-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_packet_field          ( name,             packet_id,             field,             field_type,             defVal,             len,                          remark )           values ( ?,             ?,             ?,             ?,             ?,             ?,                          ? )\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'', '2022-12-06 16:22:34');
INSERT INTO `sys_oper_log` VALUES (83, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"defVal\":\"1\",\"field\":\"dev_adress\",\"fieldType\":1,\"len\":1,\"name\":\"地址码\",\"packetId\":9,\"params\":{},\"remark\":\"设备地址，默认01\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.insertIotPacketField-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_packet_field          ( name,             packet_id,             field,             field_type,             defVal,             len,                          remark )           values ( ?,             ?,             ?,             ?,             ?,             ?,                          ? )\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'', '2022-12-06 16:22:37');
INSERT INTO `sys_oper_log` VALUES (84, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"defVal\":\"1\",\"field\":\"dev_adress\",\"fieldType\":1,\"len\":1,\"name\":\"地址码\",\"packetId\":9,\"params\":{},\"remark\":\"设备地址，默认01\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.insertIotPacketField-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_packet_field          ( name,             packet_id,             field,             field_type,             defVal,             len,                          remark )           values ( ?,             ?,             ?,             ?,             ?,             ?,                          ? )\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'', '2022-12-06 16:22:41');
INSERT INTO `sys_oper_log` VALUES (85, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"defVal\":\"1\",\"field\":\"dev_adress\",\"fieldType\":1,\"len\":1,\"name\":\"地址码\",\"packetId\":9,\"params\":{},\"remark\":\"设备地址，默认01\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.insertIotPacketField-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_packet_field          ( name,             packet_id,             field,             field_type,             defVal,             len,                          remark )           values ( ?,             ?,             ?,             ?,             ?,             ?,                          ? )\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'', '2022-12-06 16:22:45');
INSERT INTO `sys_oper_log` VALUES (86, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"defVal\":\"1\",\"field\":\"dev_adress\",\"fieldType\":1,\"len\":1,\"name\":\"地址码\",\"packetId\":9,\"params\":{},\"remark\":\"设备地址，默认01\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.insertIotPacketField-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_packet_field          ( name,             packet_id,             field,             field_type,             defVal,             len,                          remark )           values ( ?,             ?,             ?,             ?,             ?,             ?,                          ? )\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'', '2022-12-06 16:23:50');
INSERT INTO `sys_oper_log` VALUES (87, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"defVal\":\"1\",\"field\":\"dev_adress\",\"fieldType\":1,\"len\":1,\"name\":\"地址码\",\"packetId\":9,\"params\":{},\"remark\":\"设备地址，默认01\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n### The error may exist in class path resource [mapper/iot/IotPacketFieldMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPacketFieldMapper.insertIotPacketField-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_packet_field          ( name,             packet_id,             field,             field_type,             defVal,             len,                          remark )           values ( ?,             ?,             ?,             ?,             ?,             ?,                          ? )\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'defVal\' in \'field list\'', '2022-12-06 16:23:54');
INSERT INTO `sys_oper_log` VALUES (88, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"defVal\":\"1\",\"field\":\"dev_adress\",\"fieldType\":1,\"id\":145,\"len\":1,\"name\":\"地址码\",\"packetId\":9,\"params\":{},\"remark\":\"设备地址，默认01\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 16:29:28');
INSERT INTO `sys_oper_log` VALUES (89, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"defVal\":\"3\",\"field\":\"fun_code\",\"fieldType\":1,\"id\":146,\"len\":1,\"name\":\"功能码\",\"packetId\":9,\"params\":{},\"remark\":\"3读，6写\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 16:30:50');
INSERT INTO `sys_oper_log` VALUES (90, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"bigLittleEndian\":\"21\",\"defVal\":\"0\",\"field\":\"start_adress\",\"fieldType\":1,\"id\":147,\"len\":2,\"name\":\"起始地址\",\"packetId\":9,\"params\":{},\"remark\":\"准备读的寄存器指针地址\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 16:33:35');
INSERT INTO `sys_oper_log` VALUES (91, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"bigLittleEndian\":\"21\",\"defVal\":\"2\",\"field\":\"data_len\",\"fieldType\":1,\"id\":148,\"len\":2,\"name\":\"数据长度\",\"packetId\":9,\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 16:35:46');
INSERT INTO `sys_oper_log` VALUES (92, '数据包-字段', 2, 'com.ruoyi.iot.controller.IotPacketFieldController.edit()', 'PUT', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"bigLittleEndian\":\"21\",\"defVal\":\"2\",\"field\":\"data_len\",\"fieldType\":1,\"id\":148,\"len\":2,\"name\":\"数据长度\",\"packetId\":9,\"packetName\":\"仁大建科 -- 土壤温度水分变送器 485型\",\"params\":{},\"remark\":\"准备读取的数据长度\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 16:36:09');
INSERT INTO `sys_oper_log` VALUES (93, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"defVal\":\"1\",\"field\":\"read_data_len\",\"fieldType\":1,\"id\":149,\"len\":1,\"name\":\"返回字节数\",\"packetId\":9,\"params\":{},\"remark\":\"读指令后，返回的可读数据长度\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 16:38:22');
INSERT INTO `sys_oper_log` VALUES (94, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"bigLittleEndian\":\"21\",\"defVal\":\"0\",\"field\":\"moisture_value\",\"fieldType\":1,\"id\":150,\"len\":2,\"name\":\"水分值\",\"packetId\":9,\"params\":{},\"remark\":\"value*=0.1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 16:40:26');
INSERT INTO `sys_oper_log` VALUES (95, '数据包-字段', 1, 'com.ruoyi.iot.controller.IotPacketFieldController.add()', 'POST', 1, 'admin', NULL, '/packetField', '172.18.0.1', '', '{\"bigLittleEndian\":\"21\",\"defVal\":\"0\",\"field\":\"temperature\",\"fieldType\":1,\"id\":151,\"len\":2,\"name\":\"温度值\",\"packetId\":9,\"params\":{},\"remark\":\"当温度低于0时以补码的形式上传\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 16:41:42');
INSERT INTO `sys_oper_log` VALUES (96, '指令', 1, 'com.ruoyi.iot.controller.IotCmdController.add()', 'POST', 1, 'admin', NULL, '/cmd', '172.18.0.1', '', '{\"cmdType\":1,\"id\":88,\"name\":\"读取设备地址0x01的温度水分值\",\"packetId\":9,\"params\":{},\"remark\":\"读取温度水分值\",\"sponsor\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 16:46:39');
INSERT INTO `sys_oper_log` VALUES (97, '指令', 1, 'com.ruoyi.iot.controller.IotCmdController.add()', 'POST', 1, 'admin', NULL, '/cmd', '172.18.0.1', '', '{\"cmdHex\":\"010304\",\"cmdType\":3,\"id\":89,\"name\":\"回复 -&gt; 读取设备地址0x01的温度水分值 \",\"packetId\":9,\"params\":{},\"remark\":\"返回所读取数据\",\"sponsor\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 16:48:13');
INSERT INTO `sys_oper_log` VALUES (98, '指令', 2, 'com.ruoyi.iot.controller.IotCmdController.edit()', 'PUT', 1, 'admin', NULL, '/cmd', '172.18.0.1', '', '{\"cmdType\":1,\"id\":88,\"name\":\"读取设备地址0x01的温度水分值\",\"packetId\":9,\"packetName\":\"仁大建科 -- 土壤温度水分变送器 485型\",\"params\":{},\"remark\":\"读取温度水分值\",\"resultId\":89,\"sponsor\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 16:48:21');
INSERT INTO `sys_oper_log` VALUES (99, '厂家协议', 1, 'com.ruoyi.iot.controller.IotFrameController.add()', 'POST', 1, 'admin', NULL, '/frame', '172.18.0.1', '', '{\"fieldType\":8,\"id\":3,\"len\":7,\"markFlag\":\"FF\",\"markIndexList\":\"0\",\"name\":\"sf-register\",\"params\":{},\"preReadLen\":1,\"remark\":\"三丰注册帧，格式FF0000000001FF\",\"type\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 17:01:46');
INSERT INTO `sys_oper_log` VALUES (100, '厂家协议', 1, 'com.ruoyi.iot.controller.IotFrameController.add()', 'POST', 1, 'admin', NULL, '/frame', '172.18.0.1', '', '{\"dataLeftVector\":6,\"dataRightVector\":0,\"fieldType\":2,\"id\":4,\"len\":-1,\"lenIndexList\":\"5\",\"markFlag\":\"0000\",\"markIndexList\":\"0,1\",\"name\":\"tcp-modbus\",\"otherLen\":6,\"params\":{},\"preReadLen\":6,\"remark\":\"tcp-modbus协议帧\",\"type\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 17:05:32');
INSERT INTO `sys_oper_log` VALUES (101, '产品', 1, 'com.ruoyi.iot.controller.IotProductController.add()', 'POST', 1, 'admin', NULL, '/product', '172.18.0.1', '', '{\"id\":5,\"name\":\"土壤温度水分变送器 485型\",\"packetId\":9,\"params\":{},\"protocolType\":1,\"remark\":\"用于溧水和凤项目\",\"state\":1,\"transferPort\":10001}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 17:09:34');
INSERT INTO `sys_oper_log` VALUES (102, '数据帧', 1, 'com.ruoyi.iot.controller.IotProductFrameController.add()', 'POST', 1, 'admin', NULL, '/productFrame', '172.18.0.1', '', '{\"frameId\":3,\"id\":14,\"params\":{},\"productId\":5,\"remark\":\"支持注册帧协议\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 17:10:44');
INSERT INTO `sys_oper_log` VALUES (103, '数据帧', 1, 'com.ruoyi.iot.controller.IotProductFrameController.add()', 'POST', 1, 'admin', NULL, '/productFrame', '172.18.0.1', '', '{\"frameId\":4,\"id\":15,\"params\":{},\"productId\":5,\"remark\":\"支持tcp-modbus协议\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 17:10:55');
INSERT INTO `sys_oper_log` VALUES (104, '设备', 1, 'com.ruoyi.iot.controller.IotDeviceController.add()', 'POST', 1, 'admin', NULL, '/device', '172.18.0.1', '', '{\"devId\":\"0000000001\",\"frameId\":4,\"id\":6,\"isFault\":1,\"name\":\"土壤温湿度水分变松器01\",\"online\":2,\"params\":{},\"productId\":5,\"projectId\":6,\"remark\":\"读取土壤温湿度\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-06 17:13:18');
INSERT INTO `sys_oper_log` VALUES (105, '项目与产品关联', 1, 'com.ruoyi.iot.controller.IotProjectProductController.add()', 'POST', 1, 'admin', NULL, '/projectProduct', '172.18.0.1', '', '{\"id\":1,\"params\":{},\"productId\":5,\"projectId\":6,\"remark\":\"土壤温度水分变送器上报数据接口\",\"reportUrl\":\"127.0.0.1:9999\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-07 08:56:50');
INSERT INTO `sys_oper_log` VALUES (106, '厂家协议', 2, 'com.ruoyi.iot.controller.IotFrameController.edit()', 'PUT', 1, 'admin', NULL, '/frame', '172.21.0.1', '', '{\"dataLeftVector\":0,\"dataRightVector\":0,\"fieldType\":2,\"id\":4,\"len\":-1,\"lenIndexList\":\"5\",\"markFlag\":\"0000\",\"markIndexList\":\"0,1\",\"name\":\"tcp-modbus\",\"otherLen\":6,\"params\":{},\"preReadLen\":6,\"remark\":\"tcp-modbus协议帧\",\"type\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-07 14:48:35');
INSERT INTO `sys_oper_log` VALUES (107, '代码生成', 6, 'com.ruoyi.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/gen/importTable', '172.26.0.1', '', '\"iot_platform\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-08 09:23:57');
INSERT INTO `sys_oper_log` VALUES (108, '代码生成', 2, 'com.ruoyi.gen.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/gen', '172.26.0.1', '', '{\"businessName\":\"platform\",\"className\":\"IotPlatform\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"主键\",\"columnId\":35,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-08 01:23:57\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":false,\"insert\":false,\"isIncrement\":\"0\",\"isList\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"CompanyName\",\"columnComment\":\"公司名称\",\"columnId\":36,\"columnName\":\"company_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-08 01:23:57\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"companyName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"H5Url\",\"columnComment\":\"h5地址\",\"columnId\":37,\"columnName\":\"h5_url\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-08 01:23:57\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"javaField\":\"h5Url\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"WebUrl\",\"columnComment\":\"路径地址\",\"columnId\":38,\"columnName\":\"web_url\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-08 01:23:57\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"javaField\":\"webUrl\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"query', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-08 09:27:08');
INSERT INTO `sys_oper_log` VALUES (109, '代码生成', 2, 'com.ruoyi.gen.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/gen', '172.26.0.1', '', '{\"businessName\":\"platform\",\"className\":\"IotPlatform\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"主键\",\"columnId\":35,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-08 01:23:57\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":false,\"insert\":false,\"isIncrement\":\"0\",\"isList\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2022-12-08 01:27:08\",\"usableColumn\":false},{\"capJavaField\":\"CompanyName\",\"columnComment\":\"公司名称\",\"columnId\":36,\"columnName\":\"company_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-08 01:23:57\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"companyName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2022-12-08 01:27:08\",\"usableColumn\":false},{\"capJavaField\":\"H5Url\",\"columnComment\":\"h5地址\",\"columnId\":37,\"columnName\":\"h5_url\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-08 01:23:57\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"javaField\":\"h5Url\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2022-12-08 01:27:08\",\"usableColumn\":false},{\"capJavaField\":\"WebUrl\",\"columnComment\":\"路径地址\",\"columnId\":38,\"columnName\":\"web_url\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-08 01:23:57\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"i', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-08 09:27:53');
INSERT INTO `sys_oper_log` VALUES (110, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '172.26.0.1', '', NULL, NULL, 0, NULL, '2022-12-08 09:27:57');
INSERT INTO `sys_oper_log` VALUES (111, '平台信息', 1, 'com.ruoyi.iot.controller.IotPlatformController.add()', 'POST', 1, 'admin', NULL, '/platform', '172.26.0.1', '', '{\"companyName\":\"三丰智能--测试平台\",\"createDate\":\"2022-12-08 00:00:00\",\"h5Url\":\"http://192.168.2.125/h5/login\",\"params\":{},\"remark\":\"靓仔的电脑\",\"user\":\"admin\",\"webUrl\":\"http://192.168.2.125/web/login\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n### The error may exist in class path resource [mapper/iot/IotPlatformMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPlatformMapper.insertIotPlatform-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_platform          ( company_name,             h5_url,             web_url,             user,             password,             remark,             create_date )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ? )\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2022-12-08 10:27:44');
INSERT INTO `sys_oper_log` VALUES (112, '平台信息', 1, 'com.ruoyi.iot.controller.IotPlatformController.add()', 'POST', 1, 'admin', NULL, '/platform', '172.26.0.1', '', '{\"companyName\":\"三丰智能--测试平台\",\"createDate\":\"2022-12-08 00:00:00\",\"h5Url\":\"http://192.168.2.125/h5/login\",\"params\":{},\"remark\":\"靓仔的电脑\",\"user\":\"admin\",\"webUrl\":\"http://192.168.2.125/web/login\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n### The error may exist in class path resource [mapper/iot/IotPlatformMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPlatformMapper.insertIotPlatform-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_platform          ( company_name,             h5_url,             web_url,             user,             password,             remark,             create_date )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ? )\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2022-12-08 10:27:48');
INSERT INTO `sys_oper_log` VALUES (113, '平台信息', 1, 'com.ruoyi.iot.controller.IotPlatformController.add()', 'POST', 1, 'admin', NULL, '/platform', '172.26.0.1', '', '{\"companyName\":\"三丰智能--测试平台\",\"createDate\":\"2022-12-08 00:00:00\",\"h5Url\":\"http://192.168.2.125/h5/login\",\"params\":{},\"remark\":\"靓仔的电脑\",\"user\":\"admin\",\"webUrl\":\"http://192.168.2.125/web/login\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n### The error may exist in class path resource [mapper/iot/IotPlatformMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPlatformMapper.insertIotPlatform-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_platform          ( company_name,             h5_url,             web_url,             user,             password,             remark,             create_date )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ? )\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2022-12-08 10:27:52');
INSERT INTO `sys_oper_log` VALUES (114, '平台信息', 1, 'com.ruoyi.iot.controller.IotPlatformController.add()', 'POST', 1, 'admin', NULL, '/platform', '172.26.0.1', '', '{\"companyName\":\"三丰智能--测试平台\",\"createDate\":\"2022-12-08 00:00:00\",\"h5Url\":\"http://192.168.2.125/h5/login\",\"params\":{},\"remark\":\"靓仔的电脑\",\"user\":\"admin\",\"webUrl\":\"http://192.168.2.125/web/login\"}', NULL, 1, '\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n### The error may exist in class path resource [mapper/iot/IotPlatformMapper.xml]\n### The error may involve com.ruoyi.iot.mapper.IotPlatformMapper.insertIotPlatform-Inline\n### The error occurred while setting parameters\n### SQL: insert into iot_platform          ( company_name,             h5_url,             web_url,             user,             password,             remark,             create_date )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ? )\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2022-12-08 10:27:54');
INSERT INTO `sys_oper_log` VALUES (115, '平台信息', 1, 'com.ruoyi.iot.controller.IotPlatformController.add()', 'POST', 1, 'admin', NULL, '/platform', '172.26.0.1', '', '{\"companyName\":\"三丰智能--测试平台\",\"createDate\":\"2022-12-08 00:00:00\",\"h5Url\":\"http://192.168.2.125/h5/login\",\"params\":{},\"remark\":\"靓仔的电脑\",\"user\":\"admin\",\"webUrl\":\"http://192.168.2.125/web/login\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-08 10:28:34');
INSERT INTO `sys_oper_log` VALUES (116, '平台信息', 2, 'com.ruoyi.iot.controller.IotPlatformController.edit()', 'PUT', 1, 'admin', NULL, '/platform', '172.26.0.1', '', '{\"companyName\":\"三丰智能--本地测试平台\",\"createDate\":\"2022-12-08 00:00:00\",\"h5Url\":\"http://192.168.2.125/h5/login\",\"id\":1,\"params\":{},\"remark\":\"靓仔的电脑\",\"user\":\"admin\",\"webUrl\":\"http://192.168.2.125/web/login\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-08 10:28:50');
INSERT INTO `sys_oper_log` VALUES (117, '平台信息', 1, 'com.ruoyi.iot.controller.IotPlatformController.add()', 'POST', 1, 'admin', NULL, '/platform', '172.26.0.1', '', '{\"companyName\":\"三丰智能--展示平台\",\"createDate\":\"2022-12-08 00:00:00\",\"h5Url\":\"http://sf.zxqnywlw.cn/h5\",\"params\":{},\"remark\":\"用来钓鱼的\",\"user\":\"admin\",\"webUrl\":\"http://sf.zxqnywlw.cn/web\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-08 10:29:53');
INSERT INTO `sys_oper_log` VALUES (118, '平台信息', 2, 'com.ruoyi.iot.controller.IotPlatformController.edit()', 'PUT', 1, 'admin', NULL, '/platform', '172.26.0.1', '', '{\"companyName\":\"三丰智能--本地测试平台\",\"createDate\":\"2022-12-08 00:00:00\",\"h5Url\":\"http://192.168.2.125/h5/login\",\"id\":1,\"params\":{},\"remark\":\"靓仔的电脑\",\"user\":\"admin\",\"webUrl\":\"http://192.168.2.125\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-09 16:13:04');
INSERT INTO `sys_oper_log` VALUES (119, '平台信息', 2, 'com.ruoyi.iot.controller.IotPlatformController.edit()', 'PUT', 1, 'admin', NULL, '/platform', '172.26.0.1', '', '{\"companyName\":\"三丰智能--展示平台\",\"createDate\":\"2022-12-08 00:00:00\",\"h5Url\":\"http://sf.zxqnywlw.cn/h5\",\"id\":2,\"params\":{},\"remark\":\"用来钓鱼的\",\"user\":\"admin\",\"webUrl\":\"http://sf.zxqnywlw.cn\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-09 16:13:11');
INSERT INTO `sys_oper_log` VALUES (120, '项目信息', 2, 'com.ruoyi.iot.controller.IotProjectController.edit()', 'PUT', 1, 'admin', NULL, '/project', '172.19.0.1', '', '{\"id\":7,\"latitude\":31.977123,\"longitude\":118.779248,\"name\":\"大衍物联网测试\",\"params\":{},\"place\":\"中国江苏省南京市雨花台区铁心桥街道凤信路\",\"principal\":\"赵晓强\",\"principalTel\":\"18010775624\",\"state\":1,\"warrantyYear\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-12 11:45:57');
INSERT INTO `sys_oper_log` VALUES (121, '项目信息', 2, 'com.ruoyi.iot.controller.IotProjectController.edit()', 'PUT', 1, 'admin', NULL, '/project', '172.19.0.1', '', '{\"id\":8,\"latitude\":31.977161,\"longitude\":118.779271,\"name\":\"三丰测试\",\"params\":{},\"place\":\"中国江苏省南京市雨花台区铁心桥街道凤信路\",\"principal\":\"赵晓强\",\"principalTel\":\"18010775624\",\"state\":2,\"warrantyYear\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-12 11:46:02');
INSERT INTO `sys_oper_log` VALUES (122, '项目信息', 2, 'com.ruoyi.iot.controller.IotProjectController.edit()', 'PUT', 1, 'admin', NULL, '/project', '172.19.0.1', '', '{\"id\":9,\"latitude\":31.977126,\"longitude\":118.779233,\"name\":\"三丰测试\",\"params\":{},\"place\":\"中国江苏省南京市雨花台区铁心桥街道凤信路\",\"principal\":\"赵晓强\",\"principalTel\":\"18010775624\",\"state\":3,\"warrantyYear\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-12 11:46:06');
INSERT INTO `sys_oper_log` VALUES (123, '项目信息', 2, 'com.ruoyi.iot.controller.IotProjectController.edit()', 'PUT', 1, 'admin', NULL, '/project', '172.19.0.1', '', '{\"id\":11,\"latitude\":31.977122,\"longitude\":118.779241,\"name\":\"三丰大衍物联\",\"params\":{},\"place\":\"中国江苏省南京市雨花台区铁心桥街道凤信路\",\"principal\":\"赵晓强\",\"principalTel\":\"18010775624\",\"state\":4,\"warrantyYear\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-12 11:46:10');
INSERT INTO `sys_oper_log` VALUES (124, '项目信息', 2, 'com.ruoyi.iot.controller.IotProjectController.edit()', 'PUT', 1, 'admin', NULL, '/project', '172.19.0.1', '', '{\"id\":10,\"latitude\":31.97715,\"longitude\":118.779286,\"name\":\"大衍物联网\",\"params\":{},\"place\":\"中国江苏省南京市雨花台区铁心桥街道凤信路\",\"principal\":\"赵晓强\",\"principalTel\":\"18010775624\",\"state\":5,\"warrantyYear\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-12 11:46:13');
INSERT INTO `sys_oper_log` VALUES (125, '项目信息', 2, 'com.ruoyi.iot.controller.IotProjectController.edit()', 'PUT', 1, 'admin', NULL, '/project', '172.19.0.1', '', '{\"id\":12,\"latitude\":31.977114,\"longitude\":118.779233,\"name\":\"大衍物联网\",\"params\":{},\"place\":\"中国江苏省南京市雨花台区铁心桥街道凤信路\",\"principal\":\"赵晓强\",\"principalTel\":\"18010775624\",\"state\":1,\"warrantyYear\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-12 11:46:18');
INSERT INTO `sys_oper_log` VALUES (126, '项目信息', 2, 'com.ruoyi.iot.controller.IotProjectController.edit()', 'PUT', 1, 'admin', NULL, '/project', '172.19.0.1', '', '{\"id\":13,\"latitude\":31.977147,\"longitude\":118.779302,\"name\":\"大衍物联网\",\"params\":{},\"place\":\"中国江苏省南京市雨花台区铁心桥街道凤信路\",\"principal\":\"赵晓强\",\"principalTel\":\"18010775624\",\"state\":2,\"warrantyYear\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-12 11:46:21');
INSERT INTO `sys_oper_log` VALUES (127, '项目信息', 2, 'com.ruoyi.iot.controller.IotProjectController.edit()', 'PUT', 1, 'admin', NULL, '/project', '172.19.0.1', '', '{\"id\":14,\"latitude\":31.977146,\"longitude\":118.779271,\"name\":\"大衍物联网\",\"params\":{},\"place\":\"中国江苏省南京市雨花台区铁心桥街道凤信路\",\"principal\":\"赵晓强\",\"principalTel\":\"18010775624\",\"state\":1,\"warrantyYear\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-12 11:46:24');
INSERT INTO `sys_oper_log` VALUES (128, '项目信息', 2, 'com.ruoyi.iot.controller.IotProjectController.edit()', 'PUT', 1, 'admin', NULL, '/project', '172.19.0.1', '', '{\"id\":15,\"latitude\":31.977144,\"longitude\":118.779271,\"name\":\"大衍物联网\",\"params\":{},\"place\":\"中国江苏省南京市雨花台区铁心桥街道凤信路\",\"principal\":\"赵晓强\",\"principalTel\":\"18010775624\",\"remark\":\"开始\",\"state\":2,\"warrantyYear\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-12 12:01:30');
INSERT INTO `sys_oper_log` VALUES (129, '字典类型', 1, 'com.ruoyi.system.controller.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/dict/type', '172.20.0.1', '', '{\"createBy\":\"admin\",\"dictName\":\"帧类型\",\"dictType\":\"dict_frame\",\"params\":{},\"remark\":\"帧类型\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-12 13:52:08');
INSERT INTO `sys_oper_log` VALUES (130, '字典数据', 1, 'com.ruoyi.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '172.20.0.1', '', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"注册帧\",\"dictSort\":1,\"dictType\":\"dict_frame\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"remark\":\"注册帧\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-12 13:53:55');
INSERT INTO `sys_oper_log` VALUES (131, '字典数据', 1, 'com.ruoyi.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '172.20.0.1', '', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"心跳帧\",\"dictSort\":2,\"dictType\":\"dict_frame\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"remark\":\"心跳帧\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-12 13:55:34');
INSERT INTO `sys_oper_log` VALUES (132, '字典数据', 1, 'com.ruoyi.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '172.20.0.1', '', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"数据帧\",\"dictSort\":3,\"dictType\":\"dict_frame\",\"dictValue\":\"3\",\"listClass\":\"default\",\"params\":{},\"remark\":\"数据帧\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-12 13:55:46');
INSERT INTO `sys_oper_log` VALUES (133, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/7', '172.19.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:22:54');
INSERT INTO `sys_oper_log` VALUES (134, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/8', '172.19.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:22:56');
INSERT INTO `sys_oper_log` VALUES (135, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/9', '172.19.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:22:58');
INSERT INTO `sys_oper_log` VALUES (136, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/10', '172.19.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:23:00');
INSERT INTO `sys_oper_log` VALUES (137, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/11', '172.19.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:23:03');
INSERT INTO `sys_oper_log` VALUES (138, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/12', '172.19.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:23:05');
INSERT INTO `sys_oper_log` VALUES (139, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/13', '172.19.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:23:07');
INSERT INTO `sys_oper_log` VALUES (140, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/14', '172.19.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:23:09');
INSERT INTO `sys_oper_log` VALUES (141, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/15', '172.19.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:23:10');
INSERT INTO `sys_oper_log` VALUES (142, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/16', '172.19.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:23:12');
INSERT INTO `sys_oper_log` VALUES (143, '项目信息', 3, 'com.ruoyi.iot.controller.IotProjectController.remove()', 'DELETE', 1, 'admin', NULL, '/project/17', '172.19.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:23:14');
INSERT INTO `sys_oper_log` VALUES (144, '字典数据', 2, 'com.ruoyi.system.controller.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/dict/data', '172.19.0.1', '', '{\"createBy\":\"admin\",\"createTime\":\"2022-12-12 05:55:46\",\"default\":false,\"dictCode\":76,\"dictLabel\":\"Hex-数据帧\",\"dictSort\":3,\"dictType\":\"dict_frame\",\"dictValue\":\"3\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"remark\":\"数据帧\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:25:18');
INSERT INTO `sys_oper_log` VALUES (145, '字典数据', 1, 'com.ruoyi.system.controller.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/dict/data', '172.19.0.1', '', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"ASCII-注册帧\",\"dictSort\":4,\"dictType\":\"dict_frame\",\"dictValue\":\"4\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:25:58');
INSERT INTO `sys_oper_log` VALUES (146, '字典数据', 2, 'com.ruoyi.system.controller.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/dict/data', '172.19.0.1', '', '{\"createBy\":\"admin\",\"createTime\":\"2022-12-19 05:25:58\",\"default\":false,\"dictCode\":77,\"dictLabel\":\"ASCII-注册帧\",\"dictSort\":4,\"dictType\":\"dict_frame\",\"dictValue\":\"4\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"remark\":\"ascii编码的数据帧\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:26:15');
INSERT INTO `sys_oper_log` VALUES (147, '字典数据', 2, 'com.ruoyi.system.controller.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/dict/data', '172.19.0.1', '', '{\"createBy\":\"admin\",\"createTime\":\"2022-12-19 05:25:58\",\"default\":false,\"dictCode\":77,\"dictLabel\":\"ascii-数据帧\",\"dictSort\":4,\"dictType\":\"dict_frame\",\"dictValue\":\"4\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"remark\":\"ascii编码的数据帧\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:32:46');
INSERT INTO `sys_oper_log` VALUES (148, '字典数据', 2, 'com.ruoyi.system.controller.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/dict/data', '172.19.0.1', '', '{\"createBy\":\"admin\",\"createTime\":\"2022-12-12 05:55:46\",\"default\":false,\"dictCode\":76,\"dictLabel\":\"hex-数据帧\",\"dictSort\":3,\"dictType\":\"dict_frame\",\"dictValue\":\"3\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"remark\":\"数据帧\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:32:54');
INSERT INTO `sys_oper_log` VALUES (149, '字典数据', 2, 'com.ruoyi.system.controller.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/dict/data', '172.19.0.1', '', '{\"createBy\":\"admin\",\"createTime\":\"2022-12-12 05:55:46\",\"default\":false,\"dictCode\":76,\"dictLabel\":\"hex-数据帧\",\"dictSort\":3,\"dictType\":\"dict_frame\",\"dictValue\":\"3\",\"isDefault\":\"N\",\"listClass\":\"default\",\"params\":{},\"remark\":\"hex编码的数据帧\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 13:33:06');
INSERT INTO `sys_oper_log` VALUES (150, '厂家协议', 2, 'com.ruoyi.iot.controller.IotFrameController.edit()', 'PUT', 1, 'admin', NULL, '/frame', '172.20.0.1', '', '{\"dataLeftVector\":0,\"dataRightVector\":0,\"fieldType\":2,\"id\":4,\"len\":-1,\"lenIndexList\":\"5\",\"markFlag\":\"0000\",\"markIndexList\":\"0,1\",\"name\":\"tcp-modbus\",\"otherLen\":6,\"params\":{},\"preReadLen\":6,\"remark\":\"tcp-modbus协议帧\",\"type\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-19 14:14:05');
INSERT INTO `sys_oper_log` VALUES (151, '代码生成', 3, 'com.ruoyi.gen.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/gen/2', '172.20.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-27 09:39:31');
INSERT INTO `sys_oper_log` VALUES (152, '代码生成', 3, 'com.ruoyi.gen.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/gen/3', '172.20.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-27 09:39:33');
INSERT INTO `sys_oper_log` VALUES (153, '代码生成', 6, 'com.ruoyi.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/gen/importTable', '172.20.0.1', '', '\"iot_product_frame,iot_product_field,iot_product,iot_device_off_line,iot_project,iot_cmd,iot_device\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-27 09:47:07');
INSERT INTO `sys_oper_log` VALUES (154, '代码生成', 2, 'com.ruoyi.gen.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/gen', '172.20.0.1', '', '{\"businessName\":\"cmd\",\"className\":\"IotCmd\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"id\",\"columnId\":43,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isList\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ProductId\",\"columnComment\":\"产品id\",\"columnId\":44,\"columnName\":\"product_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"productId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ResultId\",\"columnComment\":\"回复的cmdid，用来配对消息收发\",\"columnId\":45,\"columnName\":\"result_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"resultId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"指令名称\",\"columnId\":46,\"columnName\":\"name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"que', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-27 09:49:09');
INSERT INTO `sys_oper_log` VALUES (155, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '172.20.0.1', '', NULL, NULL, 0, NULL, '2022-12-27 09:49:16');
INSERT INTO `sys_oper_log` VALUES (156, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '172.20.0.1', '', NULL, NULL, 0, NULL, '2022-12-27 13:19:47');
INSERT INTO `sys_oper_log` VALUES (157, '代码生成', 2, 'com.ruoyi.gen.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/gen', '172.20.0.1', '', '{\"businessName\":\"field\",\"className\":\"IotProductField\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"id\",\"columnId\":80,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isList\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"名称\",\"columnId\":81,\"columnName\":\"name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ProductId\",\"columnComment\":\"产品id\",\"columnId\":82,\"columnName\":\"product_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"productId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Field\",\"columnComment\":\"字段名\",\"columnId\":83,\"columnName\":\"field\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"javaField\":\"field\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-27 13:24:35');
INSERT INTO `sys_oper_log` VALUES (158, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '172.20.0.1', '', NULL, NULL, 0, NULL, '2022-12-27 13:24:40');
INSERT INTO `sys_oper_log` VALUES (159, '代码生成', 2, 'com.ruoyi.gen.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/gen', '172.20.0.1', '', '{\"businessName\":\"frame\",\"className\":\"IotProductFrame\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"id\",\"columnId\":93,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isList\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ProductId\",\"columnComment\":\"产品id\",\"columnId\":94,\"columnName\":\"product_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"productId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"FrameId\",\"columnComment\":\"协议id\",\"columnId\":95,\"columnName\":\"frame_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"frameId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Remark\",\"columnComment\":\"备注\",\"columnId\":96,\"columnName\":\"remark\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"javaField\":\"remark\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"que', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-27 13:30:33');
INSERT INTO `sys_oper_log` VALUES (160, '代码生成', 2, 'com.ruoyi.gen.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/gen', '172.20.0.1', '', '{\"businessName\":\"product_frame\",\"className\":\"IotProductFrame\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"id\",\"columnId\":93,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"updateTime\":\"2022-12-27 05:30:33\",\"usableColumn\":false},{\"capJavaField\":\"ProductId\",\"columnComment\":\"产品id\",\"columnId\":94,\"columnName\":\"product_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"productId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"updateTime\":\"2022-12-27 05:30:33\",\"usableColumn\":false},{\"capJavaField\":\"FrameId\",\"columnComment\":\"协议id\",\"columnId\":95,\"columnName\":\"frame_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"frameId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"updateTime\":\"2022-12-27 05:30:33\",\"usableColumn\":false},{\"capJavaField\":\"Remark\",\"columnComment\":\"备注\",\"columnId\":96,\"columnName\":\"remark\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isIns', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-27 13:39:55');
INSERT INTO `sys_oper_log` VALUES (161, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '172.20.0.1', '', NULL, NULL, 0, NULL, '2022-12-27 13:40:02');
INSERT INTO `sys_oper_log` VALUES (162, '代码生成', 2, 'com.ruoyi.gen.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/gen', '172.20.0.1', '', '{\"businessName\":\"line\",\"className\":\"IotDeviceOffLine\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"主键\",\"columnId\":68,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isList\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"DevId\",\"columnComment\":\"设备Id\",\"columnId\":69,\"columnName\":\"dev_id\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"devId\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ProjectId\",\"columnComment\":\"项目id\",\"columnId\":70,\"columnName\":\"project_id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"projectId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":6,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Status\",\"columnComment\":\"设备状态 0离线 1在线\",\"columnId\":71,\"columnName\":\"status\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"radio\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"status\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"quer', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-27 13:51:47');
INSERT INTO `sys_oper_log` VALUES (163, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '172.20.0.1', '', NULL, NULL, 0, NULL, '2022-12-27 13:51:51');
INSERT INTO `sys_oper_log` VALUES (164, '代码生成', 2, 'com.ruoyi.gen.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/gen', '172.20.0.1', '', '{\"businessName\":\"device\",\"className\":\"IotDevice\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"id\",\"columnId\":55,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isList\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"DevId\",\"columnComment\":\"编号\",\"columnId\":56,\"columnName\":\"dev_id\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"devId\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"名称\",\"columnId\":57,\"columnName\":\"name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":5,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Online\",\"columnComment\":\"是否在线\",\"columnId\":58,\"columnName\":\"online\",\"columnType\":\"tinyint(1)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"iot_communication\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"online\",\"javaType\":\"Integer\",\"list\":true,\"params\":{},\"pk\":false,\"query\":', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-27 13:59:01');
INSERT INTO `sys_oper_log` VALUES (165, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '172.20.0.1', '', NULL, NULL, 0, NULL, '2022-12-27 13:59:08');
INSERT INTO `sys_oper_log` VALUES (166, '代码生成', 2, 'com.ruoyi.gen.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/gen', '172.20.0.1', '', '{\"businessName\":\"product\",\"className\":\"IotProduct\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"id\",\"columnId\":73,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isList\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"名称\",\"columnId\":74,\"columnName\":\"name\",\"columnType\":\"text\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"textarea\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Remark\",\"columnComment\":\"备注\",\"columnId\":75,\"columnName\":\"remark\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"javaField\":\"remark\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":true,\"tableId\":7,\"updateBy\":\"\",\"usableColumn\":true},{\"capJavaField\":\"Creator\",\"columnComment\":\"创建者\",\"columnId\":76,\"columnName\":\"creator\",\"columnType\":\"bigint(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":false,\"insert\":false,\"isIncrement\":\"0\",\"isPk\":\"0\",\"javaField\":\"creator\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":4,\"superColumn\":false,\"tableId\":7,\"upda', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-27 14:16:57');
INSERT INTO `sys_oper_log` VALUES (167, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '172.20.0.1', '', NULL, NULL, 0, NULL, '2022-12-27 14:17:08');
INSERT INTO `sys_oper_log` VALUES (168, '代码生成', 2, 'com.ruoyi.gen.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/gen', '172.20.0.1', '', '{\"businessName\":\"project\",\"className\":\"IotProject\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"主键\",\"columnId\":101,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isList\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"名称\",\"columnId\":102,\"columnName\":\"name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Place\",\"columnComment\":\"地点\",\"columnId\":103,\"columnName\":\"place\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"place\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Longitude\",\"columnComment\":\"经度\",\"columnId\":104,\"columnName\":\"longitude\",\"columnType\":\"double\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"javaField\":\"longitude\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-27 14:58:36');
INSERT INTO `sys_oper_log` VALUES (169, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '172.20.0.1', '', NULL, NULL, 0, NULL, '2022-12-27 14:58:43');
INSERT INTO `sys_oper_log` VALUES (170, '代码生成', 2, 'com.ruoyi.gen.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/gen', '172.20.0.1', '', '{\"businessName\":\"project\",\"className\":\"IotProject\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"主键\",\"columnId\":101,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2022-12-27 06:58:36\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"名称\",\"columnId\":102,\"columnName\":\"name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2022-12-27 06:58:36\",\"usableColumn\":false},{\"capJavaField\":\"Place\",\"columnComment\":\"地点\",\"columnId\":103,\"columnName\":\"place\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"place\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2022-12-27 06:58:36\",\"usableColumn\":false},{\"capJavaField\":\"Longitude\",\"columnComment\":\"经度\",\"columnId\":104,\"columnName\":\"longitude\",\"columnType\":\"double\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-27 15:03:15');
INSERT INTO `sys_oper_log` VALUES (171, '代码生成', 2, 'com.ruoyi.gen.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/gen', '172.20.0.1', '', '{\"businessName\":\"project\",\"className\":\"IotProject\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"主键\",\"columnId\":101,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isList\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2022-12-27 07:03:15\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"名称\",\"columnId\":102,\"columnName\":\"name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2022-12-27 07:03:15\",\"usableColumn\":false},{\"capJavaField\":\"Place\",\"columnComment\":\"地点\",\"columnId\":103,\"columnName\":\"place\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"place\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":10,\"updateBy\":\"\",\"updateTime\":\"2022-12-27 07:03:15\",\"usableColumn\":false},{\"capJavaField\":\"Longitude\",\"columnComment\":\"经度\",\"columnId\":104,\"columnName\":\"longitude\",\"columnType\":\"double\",\"createBy\":\"admin\",\"createTime\":\"2022-12-27 01:47:07\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"j', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-27 15:05:25');
INSERT INTO `sys_oper_log` VALUES (172, '代码生成', 8, 'com.ruoyi.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/gen/batchGenCode', '172.20.0.1', '', NULL, NULL, 0, NULL, '2022-12-27 15:05:29');
INSERT INTO `sys_oper_log` VALUES (173, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1108', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:28:20');
INSERT INTO `sys_oper_log` VALUES (174, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1109', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:28:25');
INSERT INTO `sys_oper_log` VALUES (175, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1110', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:28:27');
INSERT INTO `sys_oper_log` VALUES (176, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1111', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:28:29');
INSERT INTO `sys_oper_log` VALUES (177, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1112', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:28:31');
INSERT INTO `sys_oper_log` VALUES (178, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1107', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:28:33');
INSERT INTO `sys_oper_log` VALUES (179, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1102', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:28:37');
INSERT INTO `sys_oper_log` VALUES (180, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1103', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:28:40');
INSERT INTO `sys_oper_log` VALUES (181, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1104', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:28:42');
INSERT INTO `sys_oper_log` VALUES (182, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1105', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:28:44');
INSERT INTO `sys_oper_log` VALUES (183, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1106', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:28:46');
INSERT INTO `sys_oper_log` VALUES (184, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1101', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:28:48');
INSERT INTO `sys_oper_log` VALUES (185, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1114', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:30:15');
INSERT INTO `sys_oper_log` VALUES (186, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1115', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:30:17');
INSERT INTO `sys_oper_log` VALUES (187, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1116', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:30:19');
INSERT INTO `sys_oper_log` VALUES (188, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1117', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:30:21');
INSERT INTO `sys_oper_log` VALUES (189, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1118', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:30:24');
INSERT INTO `sys_oper_log` VALUES (190, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1113', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:30:26');
INSERT INTO `sys_oper_log` VALUES (191, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1120', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:32:50');
INSERT INTO `sys_oper_log` VALUES (192, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1121', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:32:52');
INSERT INTO `sys_oper_log` VALUES (193, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1122', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:32:55');
INSERT INTO `sys_oper_log` VALUES (194, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1123', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:32:57');
INSERT INTO `sys_oper_log` VALUES (195, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1124', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:33:00');
INSERT INTO `sys_oper_log` VALUES (196, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1119', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:33:02');
INSERT INTO `sys_oper_log` VALUES (197, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1090', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:33:07');
INSERT INTO `sys_oper_log` VALUES (198, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1091', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:33:09');
INSERT INTO `sys_oper_log` VALUES (199, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1092', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:33:11');
INSERT INTO `sys_oper_log` VALUES (200, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1093', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:33:13');
INSERT INTO `sys_oper_log` VALUES (201, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1094', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:33:16');
INSERT INTO `sys_oper_log` VALUES (202, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1089', '172.22.0.1', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 09:33:18');
INSERT INTO `sys_oper_log` VALUES (203, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1149', '192.168.2.128', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:26:07');
INSERT INTO `sys_oper_log` VALUES (204, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1150', '192.168.2.128', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:26:09');
INSERT INTO `sys_oper_log` VALUES (205, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1151', '192.168.2.128', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:26:12');
INSERT INTO `sys_oper_log` VALUES (206, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1152', '192.168.2.128', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:26:15');
INSERT INTO `sys_oper_log` VALUES (207, '菜单管理', 3, 'com.ruoyi.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/menu/1153', '192.168.2.128', '', NULL, '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:26:17');
INSERT INTO `sys_oper_log` VALUES (208, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/device/index\",\"createTime\":\"2022-12-29 02:21:35\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1161,\"menuName\":\"设备\",\"menuType\":\"C\",\"orderNum\":20,\"params\":{},\"parentId\":1061,\"path\":\"device\",\"perms\":\"iot:device:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:27:01');
INSERT INTO `sys_oper_log` VALUES (209, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/line/index\",\"createTime\":\"2022-12-29 02:21:52\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1173,\"menuName\":\"设备离在线记录\",\"menuType\":\"C\",\"orderNum\":21,\"params\":{},\"parentId\":1061,\"path\":\"line\",\"perms\":\"iot:line:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:27:11');
INSERT INTO `sys_oper_log` VALUES (210, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/cmd/index\",\"createTime\":\"2022-09-14 05:13:34\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1083,\"menuName\":\"指令\",\"menuType\":\"C\",\"orderNum\":50,\"params\":{},\"parentId\":1061,\"path\":\"cmd\",\"perms\":\"iot:cmd:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:28:26');
INSERT INTO `sys_oper_log` VALUES (211, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/cmdEncoder/index\",\"createTime\":\"2022-09-14 05:12:51\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1071,\"menuName\":\"指令编码\",\"menuType\":\"C\",\"orderNum\":51,\"params\":{},\"parentId\":1061,\"path\":\"cmdEncoder\",\"perms\":\"iot:cmdEncoder:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:28:33');
INSERT INTO `sys_oper_log` VALUES (212, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/cmdDecode/index\",\"createTime\":\"2022-09-14 05:12:34\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1065,\"menuName\":\"指令解析\",\"menuType\":\"C\",\"orderNum\":52,\"params\":{},\"parentId\":1061,\"path\":\"cmdDecode\",\"perms\":\"iot:cmdDecode:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:28:40');
INSERT INTO `sys_oper_log` VALUES (213, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/explain/index\",\"createTime\":\"2022-09-26 07:03:54\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1137,\"menuName\":\"指令解释\",\"menuType\":\"C\",\"orderNum\":53,\"params\":{},\"parentId\":1061,\"path\":\"explain\",\"perms\":\"iot:explain:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:28:48');
INSERT INTO `sys_oper_log` VALUES (214, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/device/index\",\"createTime\":\"2022-12-29 02:21:35\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1161,\"menuName\":\"设备\",\"menuType\":\"C\",\"orderNum\":40,\"params\":{},\"parentId\":1061,\"path\":\"device\",\"perms\":\"iot:device:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:28:57');
INSERT INTO `sys_oper_log` VALUES (215, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/line/index\",\"createTime\":\"2022-12-29 02:21:52\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1173,\"menuName\":\"设备离在线记录\",\"menuType\":\"C\",\"orderNum\":41,\"params\":{},\"parentId\":1061,\"path\":\"line\",\"perms\":\"iot:line:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:29:03');
INSERT INTO `sys_oper_log` VALUES (216, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/product/index\",\"createTime\":\"2022-12-29 02:22:08\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1185,\"menuName\":\"产品\",\"menuType\":\"C\",\"orderNum\":30,\"params\":{},\"parentId\":1061,\"path\":\"product\",\"perms\":\"iot:product:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:29:52');
INSERT INTO `sys_oper_log` VALUES (217, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/field/index\",\"createTime\":\"2022-12-29 02:21:45\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1167,\"menuName\":\"产品-属性\",\"menuType\":\"C\",\"orderNum\":31,\"params\":{},\"parentId\":1061,\"path\":\"field\",\"perms\":\"iot:field:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:29:59');
INSERT INTO `sys_oper_log` VALUES (218, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/product_frame/index\",\"createTime\":\"2022-12-29 02:22:00\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1179,\"menuName\":\"产品-数据帧\",\"menuType\":\"C\",\"orderNum\":32,\"params\":{},\"parentId\":1061,\"path\":\"product_frame\",\"perms\":\"iot:product_frame:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:30:12');
INSERT INTO `sys_oper_log` VALUES (219, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/frame/index\",\"createTime\":\"2022-09-14 05:13:55\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1095,\"menuName\":\"厂家协议\",\"menuType\":\"C\",\"orderNum\":42,\"params\":{},\"parentId\":1061,\"path\":\"frame\",\"perms\":\"iot:frame:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:30:33');
INSERT INTO `sys_oper_log` VALUES (220, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/device/index\",\"createTime\":\"2022-12-29 02:21:35\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1161,\"menuName\":\"设备\",\"menuType\":\"C\",\"orderNum\":20,\"params\":{},\"parentId\":1061,\"path\":\"device\",\"perms\":\"iot:device:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:32:10');
INSERT INTO `sys_oper_log` VALUES (221, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/line/index\",\"createTime\":\"2022-12-29 02:21:52\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1173,\"menuName\":\"设备离在线记录\",\"menuType\":\"C\",\"orderNum\":21,\"params\":{},\"parentId\":1061,\"path\":\"line\",\"perms\":\"iot:line:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:32:21');
INSERT INTO `sys_oper_log` VALUES (222, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/frame/index\",\"createTime\":\"2022-09-14 05:13:55\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1095,\"menuName\":\"设备厂家协议\",\"menuType\":\"C\",\"orderNum\":22,\"params\":{},\"parentId\":1061,\"path\":\"frame\",\"perms\":\"iot:frame:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:32:40');
INSERT INTO `sys_oper_log` VALUES (223, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '192.168.2.128', '', '{\"children\":[],\"component\":\"iot/line/index\",\"createTime\":\"2022-12-29 02:21:52\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1173,\"menuName\":\"设备离在线\",\"menuType\":\"C\",\"orderNum\":21,\"params\":{},\"parentId\":1061,\"path\":\"line\",\"perms\":\"iot:line:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 13:32:50');
INSERT INTO `sys_oper_log` VALUES (224, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '172.22.0.1', '', '{\"children\":[],\"component\":\"iot/frame/index\",\"createTime\":\"2022-09-14 05:13:55\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1095,\"menuName\":\"产品-厂家协议\",\"menuType\":\"C\",\"orderNum\":22,\"params\":{},\"parentId\":1061,\"path\":\"frame\",\"perms\":\"iot:frame:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 14:22:23');
INSERT INTO `sys_oper_log` VALUES (225, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '172.22.0.1', '', '{\"children\":[],\"component\":\"iot/frame/index\",\"createTime\":\"2022-09-14 05:13:55\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1095,\"menuName\":\"厂家协议\",\"menuType\":\"C\",\"orderNum\":32,\"params\":{},\"parentId\":1061,\"path\":\"frame\",\"perms\":\"iot:frame:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 14:23:08');
INSERT INTO `sys_oper_log` VALUES (226, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '172.22.0.1', '', '{\"children\":[],\"component\":\"iot/product_frame/index\",\"createTime\":\"2022-12-29 02:22:00\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1179,\"menuName\":\"产品-厂家协议\",\"menuType\":\"C\",\"orderNum\":33,\"params\":{},\"parentId\":1061,\"path\":\"product_frame\",\"perms\":\"iot:product_frame:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 14:23:22');
INSERT INTO `sys_oper_log` VALUES (227, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '172.22.0.1', '', '{\"children\":[],\"component\":\"\",\"createTime\":\"2022-12-29 02:22:00\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1180,\"menuName\":\"产品-协议帧查询\",\"menuType\":\"F\",\"orderNum\":1,\"params\":{},\"parentId\":1179,\"path\":\"#\",\"perms\":\"iot:product_frame:query\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 14:23:59');
INSERT INTO `sys_oper_log` VALUES (228, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '172.22.0.1', '', '{\"children\":[],\"component\":\"\",\"createTime\":\"2022-12-29 02:22:00\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1181,\"menuName\":\"产品-协议帧新增\",\"menuType\":\"F\",\"orderNum\":2,\"params\":{},\"parentId\":1179,\"path\":\"#\",\"perms\":\"iot:product_frame:add\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 14:24:07');
INSERT INTO `sys_oper_log` VALUES (229, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '172.22.0.1', '', '{\"children\":[],\"component\":\"\",\"createTime\":\"2022-12-29 02:22:00\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1182,\"menuName\":\"产品-协议帧修改\",\"menuType\":\"F\",\"orderNum\":3,\"params\":{},\"parentId\":1179,\"path\":\"#\",\"perms\":\"iot:product_frame:edit\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 14:24:16');
INSERT INTO `sys_oper_log` VALUES (230, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '172.22.0.1', '', '{\"children\":[],\"component\":\"\",\"createTime\":\"2022-12-29 02:22:00\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1183,\"menuName\":\"产品-协议帧删除\",\"menuType\":\"F\",\"orderNum\":4,\"params\":{},\"parentId\":1179,\"path\":\"#\",\"perms\":\"iot:product_frame:remove\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 14:24:24');
INSERT INTO `sys_oper_log` VALUES (231, '菜单管理', 2, 'com.ruoyi.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/menu', '172.22.0.1', '', '{\"children\":[],\"component\":\"\",\"createTime\":\"2022-12-29 02:22:00\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1184,\"menuName\":\"产品-协议帧导出\",\"menuType\":\"F\",\"orderNum\":5,\"params\":{},\"parentId\":1179,\"path\":\"#\",\"perms\":\"iot:product_frame:export\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-12-29 14:24:33');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2022-12-06 03:37:36', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2022-12-06 03:37:36', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2022-12-06 03:37:36', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2022-12-06 03:37:36', '', NULL, '');
INSERT INTO `sys_post` VALUES (5, 'programer', '程序员', 5, '0', 'admin', '2022-09-16 16:34:21', 'admin', '2022-09-16 16:34:31', '程序员');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2022-12-06 03:37:36', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2022-12-06 03:37:36', '', NULL, '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-12-06 03:37:36', 'admin', '2022-12-06 03:37:36', '', NULL, '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-12-06 03:37:36', 'admin', '2022-12-06 03:37:36', '', NULL, '测试员');

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online`  (
  `sessionId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime(0) NULL DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime(0) NULL DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) NULL DEFAULT 0 COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '在线用户记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------
INSERT INTO `sys_user_online` VALUES ('ddd45b24-964a-4b85-8727-cea8712fbd6e', 'admin', '研发部门', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', 'on_line', '2022-08-30 18:06:35', '2022-08-30 19:31:33', 1800000);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

SET FOREIGN_KEY_CHECKS = 1;
