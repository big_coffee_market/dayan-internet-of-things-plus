/*
 Navicat Premium Data Transfer

 Source Server         : local-larglink
 Source Server Type    : MySQL
 Source Server Version : 50741
 Source Host           : localhost:3306
 Source Schema         : iot_large_derivative

 Target Server Type    : MySQL
 Target Server Version : 50741
 File Encoding         : 65001

 Date: 05/05/2023 15:32:22
*/

DROP DATABASE IF EXISTS `iot_large_derivative`;

CREATE DATABASE  `iot_large_derivative` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

USE `iot_large_derivative`;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (13, 'const_agricultural_stage', '农事阶段', NULL, NULL, 'ConstAgriculturalStage', 'crud', 'com.ruoyi.plant', 'plant', 'agricultural_stage', '农事阶段', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:00', NULL);
INSERT INTO `gen_table` VALUES (14, 'const_certificate_stage', '证书类型', NULL, NULL, 'ConstCertificateStage', 'crud', 'com.ruoyi.plant', 'plant', 'certificate_stage', '证书类型', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:14', NULL);
INSERT INTO `gen_table` VALUES (15, 'const_crop_stage', '生长阶段', NULL, NULL, 'ConstCropStage', 'crud', 'com.ruoyi.plant', 'plant', 'crop_stage', '生长阶段', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:47', NULL);
INSERT INTO `gen_table` VALUES (16, 'const_processing_stage', '加工类型', NULL, NULL, 'ConstProcessingStage', 'crud', 'com.ruoyi.plant', 'plant', 'processing_stage', '加工类型', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:57', NULL);
INSERT INTO `gen_table` VALUES (17, 'const_product_supplier', '供货商', NULL, NULL, 'ConstProductSupplier', 'crud', 'com.ruoyi.plant', 'plant', 'product_supplier', '供货商', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:07', NULL);
INSERT INTO `gen_table` VALUES (18, 'const_product_test_stage', '检测类型', NULL, NULL, 'ConstProductTestStage', 'crud', 'com.ruoyi.plant', 'plant', 'product_test_stage', '检测类型', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:18', NULL);
INSERT INTO `gen_table` VALUES (19, 'const_resource_stage', '农资类型', NULL, NULL, 'ConstResourceStage', 'crud', 'com.ruoyi.plant', 'plant', 'resource_stage', '农资类型', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:28', NULL);
INSERT INTO `gen_table` VALUES (20, 'product_certificate', '产品证书', NULL, NULL, 'ProductCertificate', 'crud', 'com.ruoyi.plant', 'plant', 'product_certificate', '产品证书', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:36', NULL);
INSERT INTO `gen_table` VALUES (21, 'product_processing', '产品加工', NULL, NULL, 'ProductProcessing', 'crud', 'com.ruoyi.plant', 'plant', 'product_processing', '产品加工', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:01', '', '2023-02-18 15:05:38', NULL);
INSERT INTO `gen_table` VALUES (22, 'product_testing', '产品检测', NULL, NULL, 'ProductTesting', 'crud', 'com.ruoyi.plant', 'plant', 'product_testing', '产品检测', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:54', NULL);
INSERT INTO `gen_table` VALUES (23, 'agricultural_resource', '农资管理', NULL, NULL, 'AgriculturalResource', 'crud', 'com.ruoyi.plant', 'plant', 'agricultural_resource', '农资管理', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:03', NULL);
INSERT INTO `gen_table` VALUES (24, 'agricultural_crop', '农作物', NULL, NULL, 'AgriculturalCrop', 'crud', 'com.ruoyi.plant', 'plant', 'agricultural_crop', '农作物', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:12', NULL);
INSERT INTO `gen_table` VALUES (25, 'agricultural_harvest', '采收管理', NULL, NULL, 'AgriculturalHarvest', 'crud', 'com.ruoyi.plant', 'plant', 'agricultural_harvest', '采收管理', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:20', NULL);
INSERT INTO `gen_table` VALUES (26, 'agricultural_plan', '农事计划', NULL, NULL, 'AgriculturalPlan', 'crud', 'com.ruoyi.plant', 'plant', 'agricultural_plan', '农事计划', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:28', NULL);
INSERT INTO `gen_table` VALUES (27, 'agricultural_planting_plot', '种植地块', NULL, NULL, 'AgriculturalPlantingPlot', 'crud', 'com.ruoyi.plant', 'plant', 'agricultural_planting_plot', '种植地块', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:50', NULL);
INSERT INTO `gen_table` VALUES (28, 'agricultural_record', '农事记录', NULL, NULL, 'AgriculturalRecord', 'crud', 'com.ruoyi.plant', 'plant', 'agricultural_record', '农事记录', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1191\"}', 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:33:00', NULL);
INSERT INTO `gen_table` VALUES (29, 'iot_product_filter', '数据筛选', NULL, NULL, 'IotProductFilter', 'crud', 'com.ruoyi.iot', 'iot', 'product_filter', '数据筛选', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1061\"}', 'admin', '2023-03-02 15:55:01', '', '2023-03-02 16:31:06', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 244 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (135, '13', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:00');
INSERT INTO `gen_table_column` VALUES (136, '13', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:00');
INSERT INTO `gen_table_column` VALUES (137, '13', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:00');
INSERT INTO `gen_table_column` VALUES (138, '14', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:14');
INSERT INTO `gen_table_column` VALUES (139, '14', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:14');
INSERT INTO `gen_table_column` VALUES (140, '14', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:14');
INSERT INTO `gen_table_column` VALUES (141, '15', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:47');
INSERT INTO `gen_table_column` VALUES (142, '15', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:47');
INSERT INTO `gen_table_column` VALUES (143, '15', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:47');
INSERT INTO `gen_table_column` VALUES (144, '16', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:57');
INSERT INTO `gen_table_column` VALUES (145, '16', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:57');
INSERT INTO `gen_table_column` VALUES (146, '16', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:30:57');
INSERT INTO `gen_table_column` VALUES (147, '17', 'id', '主键', 'bigint(11)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:07');
INSERT INTO `gen_table_column` VALUES (148, '17', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:07');
INSERT INTO `gen_table_column` VALUES (149, '17', 'phone_number', '手机号', 'varchar(11)', 'String', 'phoneNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:07');
INSERT INTO `gen_table_column` VALUES (150, '17', 'contact_person', '联系人', 'varchar(255)', 'String', 'contactPerson', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:07');
INSERT INTO `gen_table_column` VALUES (151, '17', 'time', '创建时间', 'datetime', 'Date', 'time', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 5, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:07');
INSERT INTO `gen_table_column` VALUES (152, '17', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:07');
INSERT INTO `gen_table_column` VALUES (153, '18', 'id', '主键', 'int(11)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:18');
INSERT INTO `gen_table_column` VALUES (154, '18', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:18');
INSERT INTO `gen_table_column` VALUES (155, '18', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:18');
INSERT INTO `gen_table_column` VALUES (156, '19', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:28');
INSERT INTO `gen_table_column` VALUES (157, '19', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:28');
INSERT INTO `gen_table_column` VALUES (158, '19', 'remarks', '记录', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:28');
INSERT INTO `gen_table_column` VALUES (159, '20', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:36');
INSERT INTO `gen_table_column` VALUES (160, '20', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:36');
INSERT INTO `gen_table_column` VALUES (161, '20', 'supplier_id', '供应商', 'bigint(20)', 'Long', 'supplierId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 3, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:36');
INSERT INTO `gen_table_column` VALUES (162, '20', 'certificate', '证书', 'varchar(255)', 'String', 'certificate', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:36');
INSERT INTO `gen_table_column` VALUES (163, '20', 'type', '类型', 'bigint(255)', 'Long', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 5, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:36');
INSERT INTO `gen_table_column` VALUES (164, '20', 'issuing_authority', '颁发机构', 'varchar(255)', 'String', 'issuingAuthority', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:36');
INSERT INTO `gen_table_column` VALUES (165, '20', 'issue_time', '颁发时间', 'datetime', 'Date', 'issueTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 7, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:36');
INSERT INTO `gen_table_column` VALUES (166, '20', 'certificate_uploading', '证书上传', 'varchar(255)', 'String', 'certificateUploading', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:36');
INSERT INTO `gen_table_column` VALUES (167, '20', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:36');
INSERT INTO `gen_table_column` VALUES (168, '21', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 15:05:38');
INSERT INTO `gen_table_column` VALUES (169, '21', 'harvest_id', '采收批次', 'bigint(20)', 'Long', 'harvestId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 2, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 15:05:38');
INSERT INTO `gen_table_column` VALUES (170, '21', 'type', '类型', 'bigint(255)', 'Long', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 3, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 15:05:38');
INSERT INTO `gen_table_column` VALUES (171, '21', 'quantity', '数量', 'int(255)', 'Long', 'quantity', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 15:05:38');
INSERT INTO `gen_table_column` VALUES (172, '21', 'time', '时间', 'datetime', 'Date', 'time', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 5, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 15:05:38');
INSERT INTO `gen_table_column` VALUES (173, '21', 'workshop', '车间', 'varchar(255)', 'String', 'workshop', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 15:05:38');
INSERT INTO `gen_table_column` VALUES (174, '21', 'equipment', '设备', 'varchar(255)', 'String', 'equipment', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 15:05:38');
INSERT INTO `gen_table_column` VALUES (175, '21', 'supplier_id', '供应商', 'bigint(20)', 'Long', 'supplierId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 8, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 15:05:38');
INSERT INTO `gen_table_column` VALUES (176, '21', 'picture', '图片', 'varchar(255)', 'String', 'picture', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 15:05:38');
INSERT INTO `gen_table_column` VALUES (177, '21', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 15:05:38');
INSERT INTO `gen_table_column` VALUES (178, '22', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:54');
INSERT INTO `gen_table_column` VALUES (179, '22', 'harvest_id', '采收批次', 'bigint(20)', 'Long', 'harvestId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 2, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:54');
INSERT INTO `gen_table_column` VALUES (180, '22', 'type', '类型', 'bigint(255)', 'Long', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 3, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:54');
INSERT INTO `gen_table_column` VALUES (181, '22', 'institution', '机构', 'varchar(255)', 'String', 'institution', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:54');
INSERT INTO `gen_table_column` VALUES (182, '22', 'time', '时间', 'datetime', 'Date', 'time', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 5, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:54');
INSERT INTO `gen_table_column` VALUES (183, '22', 'content', '内容', 'varchar(255)', 'String', 'content', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'editor', '', 6, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:54');
INSERT INTO `gen_table_column` VALUES (184, '22', 'conclusion', '结论', 'varchar(255)', 'String', 'conclusion', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:54');
INSERT INTO `gen_table_column` VALUES (185, '22', 'certificate_name', '合格证名称', 'varchar(255)', 'String', 'certificateName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 8, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:54');
INSERT INTO `gen_table_column` VALUES (186, '22', 'certificate_number', '证书编号', 'varchar(255)', 'String', 'certificateNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:54');
INSERT INTO `gen_table_column` VALUES (187, '22', 'certificate_picture', '证书图片', 'varchar(255)', 'String', 'certificatePicture', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:54');
INSERT INTO `gen_table_column` VALUES (188, '22', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2023-02-18 13:53:01', '', '2023-02-18 14:31:54');
INSERT INTO `gen_table_column` VALUES (189, '23', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:03');
INSERT INTO `gen_table_column` VALUES (190, '23', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:03');
INSERT INTO `gen_table_column` VALUES (191, '23', 'classification_id', '类别', 'bigint(20)', 'Long', 'classificationId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 3, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:03');
INSERT INTO `gen_table_column` VALUES (192, '23', 'packing_specification', '包装规格', 'varchar(255)', 'String', 'packingSpecification', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:03');
INSERT INTO `gen_table_column` VALUES (193, '23', 'inventory', '库存量', 'int(20)', 'Long', 'inventory', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:03');
INSERT INTO `gen_table_column` VALUES (194, '23', 'manufacturer', '生产厂家', 'varchar(255)', 'String', 'manufacturer', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:03');
INSERT INTO `gen_table_column` VALUES (195, '23', 'date_of_production', '生产日期', 'datetime', 'Date', 'dateOfProduction', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 7, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:03');
INSERT INTO `gen_table_column` VALUES (196, '23', 'shelf_life', '保质期', 'varchar(255)', 'String', 'shelfLife', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:03');
INSERT INTO `gen_table_column` VALUES (197, '23', 'approval_number', '批准文号', 'varchar(255)', 'String', 'approvalNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:03');
INSERT INTO `gen_table_column` VALUES (198, '23', 'production_license', '生产许可证', 'varchar(255)', 'String', 'productionLicense', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:03');
INSERT INTO `gen_table_column` VALUES (199, '23', 'selling_firm', '销售厂商', 'varchar(255)', 'String', 'sellingFirm', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:03');
INSERT INTO `gen_table_column` VALUES (200, '23', 'description', '描述', 'varchar(255)', 'String', 'description', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:03');
INSERT INTO `gen_table_column` VALUES (201, '23', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:03');
INSERT INTO `gen_table_column` VALUES (202, '24', 'id', '主键', 'bigint(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:12');
INSERT INTO `gen_table_column` VALUES (203, '24', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:12');
INSERT INTO `gen_table_column` VALUES (204, '24', 'resource_seed_id', '类别', 'bigint(20)', 'Long', 'resourceSeedId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 3, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:12');
INSERT INTO `gen_table_column` VALUES (205, '24', 'alias', '别名', 'varchar(255)', 'String', 'alias', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:12');
INSERT INTO `gen_table_column` VALUES (206, '24', 'image', '图像', 'varchar(255)', 'String', 'image', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'imageUpload', '', 5, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:12');
INSERT INTO `gen_table_column` VALUES (207, '24', 'stage', '所含阶段', 'varchar(255)', 'String', 'stage', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'checkbox', '', 6, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:12');
INSERT INTO `gen_table_column` VALUES (208, '24', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:12');
INSERT INTO `gen_table_column` VALUES (209, '25', 'id', '主键', 'bigint(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:20');
INSERT INTO `gen_table_column` VALUES (210, '25', 'plan_id', '农事计划', 'bigint(11)', 'Long', 'planId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 2, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:20');
INSERT INTO `gen_table_column` VALUES (211, '25', 'time', '时间', 'datetime', 'Date', 'time', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 3, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:20');
INSERT INTO `gen_table_column` VALUES (212, '25', 'quantity', '数量', 'int(20)', 'Long', 'quantity', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:20');
INSERT INTO `gen_table_column` VALUES (213, '25', 'source_code', '溯源码', 'varchar(255)', 'String', 'sourceCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:20');
INSERT INTO `gen_table_column` VALUES (214, '25', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:20');
INSERT INTO `gen_table_column` VALUES (215, '26', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:28');
INSERT INTO `gen_table_column` VALUES (216, '26', 'plot_id', '地块', 'bigint(20)', 'Long', 'plotId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 2, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:28');
INSERT INTO `gen_table_column` VALUES (217, '26', 'area', '面积', 'decimal(20,0)', 'Long', 'area', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:28');
INSERT INTO `gen_table_column` VALUES (218, '26', 'seed_id', '种子', 'bigint(20)', 'Long', 'seedId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 4, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:28');
INSERT INTO `gen_table_column` VALUES (219, '26', 'dosage', '用量', 'int(11)', 'Long', 'dosage', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:28');
INSERT INTO `gen_table_column` VALUES (220, '26', 'blockchain_key', '区块链', 'varchar(200)', 'String', 'blockchainKey', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:28');
INSERT INTO `gen_table_column` VALUES (221, '26', 'start_time', '开始时间', 'datetime', 'Date', 'startTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 7, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:28');
INSERT INTO `gen_table_column` VALUES (222, '26', 'end_time', '结束时间', 'datetime', 'Date', 'endTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 8, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:28');
INSERT INTO `gen_table_column` VALUES (223, '26', 'planting_condition', '种植状态', 'bigint(20)', 'Long', 'plantingCondition', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'plant_state', 9, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:28');
INSERT INTO `gen_table_column` VALUES (224, '26', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:28');
INSERT INTO `gen_table_column` VALUES (225, '27', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:50');
INSERT INTO `gen_table_column` VALUES (226, '27', 'base_id', '基地', 'bigint(20)', 'Long', 'baseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 2, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:50');
INSERT INTO `gen_table_column` VALUES (227, '27', 'responsible_person', '责任人', 'varchar(255)', 'String', 'responsiblePerson', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:50');
INSERT INTO `gen_table_column` VALUES (228, '27', 'tel_phone', '联系电话', 'varchar(255)', 'String', 'telPhone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:50');
INSERT INTO `gen_table_column` VALUES (229, '27', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:32:50');
INSERT INTO `gen_table_column` VALUES (230, '28', 'id', '主键', 'bigint(11)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:33:00');
INSERT INTO `gen_table_column` VALUES (231, '28', 'plan_id', '农事计划', 'bigint(11)', 'Long', 'planId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 2, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:33:00');
INSERT INTO `gen_table_column` VALUES (232, '28', 'picture', '图像', 'varchar(255)', 'String', 'picture', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:33:00');
INSERT INTO `gen_table_column` VALUES (233, '28', 'operating_time', '操作时间', 'datetime', 'Date', 'operatingTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 4, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:33:00');
INSERT INTO `gen_table_column` VALUES (234, '28', 'agricultural_stage_id', '农事阶段', 'bigint(11)', 'Long', 'agriculturalStageId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 5, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:33:00');
INSERT INTO `gen_table_column` VALUES (235, '28', 'remarks', '备注', 'varchar(255)', 'String', 'remarks', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-02-18 13:53:19', '', '2023-02-18 14:33:00');
INSERT INTO `gen_table_column` VALUES (236, '29', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, '1', NULL, 'EQ', 'input', '', 1, 'admin', '2023-03-02 15:55:01', '', '2023-03-02 16:31:06');
INSERT INTO `gen_table_column` VALUES (237, '29', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2023-03-02 15:55:01', '', '2023-03-02 16:31:06');
INSERT INTO `gen_table_column` VALUES (238, '29', 'product_id', '产品', 'bigint(20)', 'Long', 'productId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 3, 'admin', '2023-03-02 15:55:01', '', '2023-03-02 16:31:06');
INSERT INTO `gen_table_column` VALUES (239, '29', 'cmd_id', '指令', 'bigint(20)', 'Long', 'cmdId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 4, 'admin', '2023-03-02 15:55:01', '', '2023-03-02 16:31:06');
INSERT INTO `gen_table_column` VALUES (240, '29', 'fileds', '字段', 'varchar(255)', 'String', 'fileds', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'checkbox', '', 5, 'admin', '2023-03-02 15:55:01', '', '2023-03-02 16:31:06');
INSERT INTO `gen_table_column` VALUES (241, '29', 'enable', '启用', 'int(20)', 'Long', 'enable', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'status_enable', 6, 'admin', '2023-03-02 15:55:01', '', '2023-03-02 16:31:06');
INSERT INTO `gen_table_column` VALUES (242, '29', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 7, 'admin', '2023-03-02 15:55:01', '', '2023-03-02 16:31:06');
INSERT INTO `gen_table_column` VALUES (243, '29', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, '1', NULL, 'EQ', 'datetime', '', 8, 'admin', '2023-03-02 15:55:01', '', '2023-03-02 16:31:06');

-- ----------------------------
-- Table structure for iot_cmd
-- ----------------------------
DROP TABLE IF EXISTS `iot_cmd`;
CREATE TABLE `iot_cmd`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `product_id` bigint(20) NULL DEFAULT NULL COMMENT '产品',
  `protocol_id` bigint(20) NULL DEFAULT NULL COMMENT '协议id',
  `result_id` bigint(20) NULL DEFAULT NULL COMMENT '回复',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指令名称',
  `sponsor` int(255) NULL DEFAULT NULL COMMENT '发起方',
  `cmd_type` int(11) NULL DEFAULT NULL COMMENT '指令类型\r\n\r\n',
  `cmd_hex` varchar(24) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指令码',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `iot_cmd_iot_packet_id_fk`(`product_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 112 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '指令' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iot_cmd
-- ----------------------------
INSERT INTO `iot_cmd` VALUES (90, 10, 10, NULL, '上报环境数据', 1, 2, '0024A7', '上报环境数据', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (91, 10, 10, NULL, '空置', 1, 2, NULL, '备注', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (94, 13, NULL, 95, '读取空气环境数据', 2, 1, NULL, '读取空气环境数据，9个数据', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (95, 13, NULL, NULL, '回复空气环境数据', 1, 3, '9003', '回复空气环境数据，9个数据', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (96, 13, NULL, 97, '读取土壤环境数据', 2, 1, NULL, '读取土壤环境数据，7个', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (97, 13, NULL, -1, '回复土壤环境数据', 1, 3, '0103', '回复土壤环境数据，7个', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (98, 13, NULL, 99, '继电器控制', 2, 1, NULL, '继电器控开闭指令', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (99, 13, NULL, NULL, '回复继电器控制指令', 1, 3, '000F', '回复继电器控制指令', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (100, 13, NULL, 101, '查询DTU状态', 2, 1, NULL, '查询DTU状态', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (101, 13, NULL, NULL, '回复DTU状态信息', 1, 3, '0F03', '回复DTU状态信息', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (102, 13, NULL, 110, '第一路继电器闭合', 2, 1, NULL, '第一路继电器闭合', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (103, 13, NULL, 110, '第一路继电器断开', 2, 1, NULL, '第一路继电器断开', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (104, 13, NULL, 110, '第二路继电器闭合', 2, 1, NULL, '第二路继电器闭合', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (105, 13, NULL, 110, '第二路继电器断开', 2, 1, NULL, '第二路继电器断开', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (106, 13, NULL, 110, '第三路继电器闭合', 2, 1, NULL, '第三路继电器闭合', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (107, 13, NULL, 110, '第三路继电器断开', 2, 1, NULL, '第三路继电器断开', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (108, 13, NULL, 110, '第四路继电器闭合', 2, 1, NULL, '第四路继电器闭合', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (109, 13, NULL, 110, '第四路继电器断开', 2, 1, NULL, '第四路继电器断开', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (110, 13, NULL, NULL, '回复继电器状态值', 1, 3, '0F10', '回复继电器状态值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd` VALUES (111, 13, NULL, NULL, '语音播报', 2, 2, NULL, '语音播报', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for iot_cmd_decode
-- ----------------------------
DROP TABLE IF EXISTS `iot_cmd_decode`;
CREATE TABLE `iot_cmd_decode`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cmd_id` bigint(20) NULL DEFAULT NULL COMMENT '指令',
  `trim_type` int(255) NULL DEFAULT NULL COMMENT '截获类型',
  `trim_param` int(255) NULL DEFAULT NULL COMMENT '截获参数',
  `field_id` bigint(20) NULL DEFAULT NULL COMMENT '字段',
  `sequence` int(11) NULL DEFAULT NULL COMMENT '序列号',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `iot_cmd_decode_iot_cmd_id_fk`(`cmd_id`) USING BTREE,
  INDEX `iot_cmd_decode_iot_packet_field_id_fk`(`field_id`) USING BTREE,
  CONSTRAINT `iot_cmd_decode_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `iot_cmd_decode_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_product_field` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 341 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '指令解析' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iot_cmd_decode
-- ----------------------------
INSERT INTO `iot_cmd_decode` VALUES (246, 90, 1, 2, 152, 1, '风向', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (247, 90, 1, 2, 153, 2, '温度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (248, 90, 1, 1, 154, 3, '湿度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (249, 90, 1, 1, 155, 4, '风速', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (250, 90, 1, 1, 156, 5, '平均风速', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (251, 90, 1, 2, 157, 6, '累计雨量', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (252, 90, 1, 2, 158, 7, '紫外线辐射强度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (253, 90, 1, 3, 159, 8, '光照度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (254, 90, 1, 3, 160, 9, '气压值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (271, 97, 1, 1, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (272, 97, 1, 1, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (273, 97, 1, 1, 181, 3, ' 有效字节数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (274, 97, 1, 2, 170, 4, '土壤温度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (275, 97, 1, 2, 171, 5, '土壤湿度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (276, 97, 1, 2, 172, 6, '土壤电导率', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (277, 97, 1, 2, 173, 7, '土壤酸碱度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (278, 97, 1, 2, 174, 8, '土壤氮', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (279, 97, 1, 2, 175, 9, '土壤磷', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (280, 97, 1, 2, 176, 10, '土壤钾', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (281, 95, 1, 1, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (282, 95, 1, 1, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (283, 95, 1, 1, 181, 3, ' 有效字节数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (284, 95, 1, 2, 161, 4, '光照强度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (285, 95, 1, 2, 162, 5, '紫外线指数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (286, 95, 1, 2, 163, 6, '温度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (287, 95, 1, 2, 164, 7, '湿度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (288, 95, 1, 2, 165, 8, '风速', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (289, 95, 1, 2, 166, 9, '阵风风速', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (290, 95, 1, 2, 167, 10, '风向', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (291, 95, 1, 2, 168, 11, '降雨量', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (292, 95, 1, 2, 169, 12, '气压', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (293, 99, 1, 2, 184, 1, '继电器标识', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (294, 99, 1, 2, 185, 2, '继电器功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (295, 99, 1, 2, 186, 3, '继电器地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (296, 99, 1, 1, 187, 4, '继电器状态', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (316, 101, 1, 1, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (317, 101, 1, 1, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (318, 101, 1, 1, 181, 3, ' 有效字节数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (319, 101, 1, 4, 188, 4, '电流0号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (320, 101, 1, 4, 189, 5, '电流1号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (321, 101, 1, 4, 190, 6, '电流2号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (322, 101, 1, 4, 191, 7, '电流3号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (323, 101, 1, 4, 192, 8, '电流4号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (324, 101, 1, 4, 193, 9, '电流5号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (325, 101, 1, 4, 194, 10, '电流6号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (326, 101, 1, 4, 195, 11, '电流7号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (327, 101, 1, 2, 196, 12, '电压输入1', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (328, 101, 1, 2, 197, 13, '电压输入2', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (329, 101, 1, 2, 198, 14, '电压输出3', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (330, 101, 1, 2, 199, 15, '电压输出4', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (331, 101, 1, 2, 200, 16, '电压输出1', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (332, 101, 1, 2, 201, 17, '电压输出2', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (333, 101, 1, 2, 202, 18, '电压输出3', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (334, 101, 1, 2, 203, 19, '电压输出4', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (337, 110, 1, 1, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (338, 110, 1, 1, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (339, 110, 1, 2, 186, 3, '继电器地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_decode` VALUES (340, 110, 1, 2, 208, 4, '开关状态值', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for iot_cmd_encoder
-- ----------------------------
DROP TABLE IF EXISTS `iot_cmd_encoder`;
CREATE TABLE `iot_cmd_encoder`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cmd_id` bigint(20) NULL DEFAULT NULL COMMENT '指令',
  `field_id` bigint(20) NULL DEFAULT NULL COMMENT '字段',
  `sequence` int(11) NULL DEFAULT NULL COMMENT '序列号',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `creator` bigint(255) NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `iot_cmd_encoder_iot_cmd_id_fk`(`cmd_id`) USING BTREE,
  INDEX `iot_cmd_encoder_iot_packet_field_id_fk`(`field_id`) USING BTREE,
  CONSTRAINT `iot_cmd_encoder_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `iot_cmd_encoder_iot_packet_field_id_fk` FOREIGN KEY (`field_id`) REFERENCES `iot_product_field` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 98 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '指令编码' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iot_cmd_encoder
-- ----------------------------
INSERT INTO `iot_cmd_encoder` VALUES (9, 96, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (10, 96, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (11, 96, 179, 3, '寄存器起始地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (12, 96, 180, 4, '寄存器个数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (13, 94, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (14, 94, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (15, 94, 179, 3, '寄存器起始地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (16, 94, 180, 4, '寄存器个数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (17, 98, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (18, 98, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (19, 98, 179, 3, '寄存器起始地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (20, 98, 180, 4, '寄存器个数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (21, 98, 182, 5, '字节数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (22, 98, 183, 6, '一二位字节值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (23, 100, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (24, 100, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (25, 100, 179, 3, '寄存器起始地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (26, 100, 180, 4, '寄存器个数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (27, 102, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (28, 102, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (29, 102, 204, 3, '第一位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (30, 102, 205, 4, '第二位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (31, 102, 181, 5, ' 有效字节数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (32, 102, 206, 6, '第一位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (33, 102, 207, 7, '第二位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (34, 103, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (35, 103, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (36, 103, 204, 3, '第一位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (37, 103, 205, 4, '第二位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (38, 103, 181, 5, ' 有效字节数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (39, 103, 206, 6, '第一位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (40, 103, 207, 7, '第二位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (41, 104, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (42, 104, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (43, 104, 204, 3, '第一位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (44, 104, 205, 4, '第二位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (45, 104, 181, 5, ' 有效字节数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (46, 104, 206, 6, '第一位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (47, 104, 207, 7, '第二位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (62, 106, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (63, 106, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (64, 106, 204, 3, '第一位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (65, 106, 205, 4, '第二位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (66, 106, 181, 5, ' 有效字节数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (67, 106, 206, 6, '第一位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (68, 106, 207, 7, '第二位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (69, 105, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (70, 105, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (71, 105, 204, 3, '第一位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (72, 105, 205, 4, '第二位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (73, 105, 181, 5, ' 有效字节数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (74, 105, 206, 6, '第一位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (75, 105, 207, 7, '第二位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (76, 107, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (77, 107, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (78, 107, 204, 3, '第一位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (79, 107, 205, 4, '第二位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (80, 107, 181, 5, ' 有效字节数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (81, 107, 206, 6, '第一位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (82, 107, 207, 7, '第二位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (83, 108, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (84, 108, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (85, 108, 204, 3, '第一位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (86, 108, 205, 4, '第二位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (87, 108, 181, 5, ' 有效字节数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (88, 108, 206, 6, '第一位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (89, 108, 207, 7, '第二位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (90, 109, 177, 1, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (91, 109, 178, 2, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (92, 109, 204, 3, '第一位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (93, 109, 205, 4, '第二位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (94, 109, 181, 5, ' 有效字节数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (95, 109, 206, 6, '第一位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (96, 109, 207, 7, '第二位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_cmd_encoder` VALUES (97, 111, 209, 1, '语音输入', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for iot_cmd_explain
-- ----------------------------
DROP TABLE IF EXISTS `iot_cmd_explain`;
CREATE TABLE `iot_cmd_explain`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cmd_id` bigint(20) NULL DEFAULT NULL COMMENT '指令',
  `sponsor` bigint(20) NULL DEFAULT NULL COMMENT '发起方',
  `json` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '物模型',
  `frame` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '数据帧',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '说明',
  `creator` bigint(255) NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `iot_cmd_explain_iot_cmd_id_fk`(`cmd_id`) USING BTREE,
  CONSTRAINT `iot_cmd_explain_iot_cmd_id_fk` FOREIGN KEY (`cmd_id`) REFERENCES `iot_cmd` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 145 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '指令解释' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iot_cmd_explain
-- ----------------------------
INSERT INTO `iot_cmd_explain` VALUES (118, 90, 1, '{\"cmdId\":90,\"data\":{\"windDirection\":\"0\",\"temperature\":\"0\",\"humidity\":\"0\",\"windSpeed\":\"0\",\"gustSpeed\":\"0\",\"accumulated_rainfall\":\"0\",\"uv\":\"0\",\"light\":\"0\",\"pressure\":\"0\"},\"devId\":\"0000000001\"}', NULL, '环境检测仪 ==> 上报环境数据', 1, '2023-03-06 10:12:45');
INSERT INTO `iot_cmd_explain` VALUES (123, 96, 2, '{\"cmdId\":96,\"data\":{\"number\":\"7\",\"areaCode\":\"1\",\"startAdress\":\"512\",\"funCode\":\"3\"},\"devId\":\"BD000002BD\"}', NULL, '宽德综合环境检测站 ==&gt; 读取土壤环境数据，7个', 1, '2023-03-29 00:00:00');
INSERT INTO `iot_cmd_explain` VALUES (124, 97, 1, '{\"cmdId\":97,\"data\":{\"P\":\"0\",\"areaCode\":\"1\",\"len\":\"0\",\"temperature\":\"0\",\"PH\":\"0\",\"humidity\":\"0\",\"K\":\"0\",\"funCode\":\"3\",\"EC\":\"0\",\"N\":\"0\"},\"devId\":\"0000000001\"}', NULL, '宽德综合环境检测站 ==> 回复土壤环境数据，7个', 1, '2023-03-29 13:02:19');
INSERT INTO `iot_cmd_explain` VALUES (125, 94, 2, '{\"cmdId\":94,\"data\":{\"number\":\"9\",\"areaCode\":\"144\",\"startAdress\":\"357\",\"funCode\":\"3\"},\"devId\":\"BD000002BD\"}', NULL, '宽德综合环境检测站 ==&gt; 读取空气环境数据，9个数据', 1, '2023-03-29 00:00:00');
INSERT INTO `iot_cmd_explain` VALUES (126, 95, 1, '{\"cmdId\":95,\"data\":{\"rainFall\":\"0\",\"areaCode\":\"1\",\"len\":\"0\",\"light\":\"0\",\"temperature\":\"0\",\"uvi\":\"0\",\"humidity\":\"0\",\"windDirection\":\"0\",\"pressure\":\"0\",\"funCode\":\"3\",\"windSpeed\":\"0\",\"gustSpeed\":\"0\"},\"devId\":\"0000000001\"}', NULL, '宽德综合环境检测站 ==> 回复空气环境数据，9个数据', 1, '2023-03-29 13:05:40');
INSERT INTO `iot_cmd_explain` VALUES (127, 98, 2, '{\"cmdId\":98,\"data\":{\"number\":\"1\",\"areaCode\":\"15\",\"startAdress\":\"20\",\"byteLen\":\"2\",\"byteValue1\":\"1\",\"funCode\":\"16\"},\"devId\":\"000001\"}', NULL, '宽德综合环境检测站 ==&gt; 继电器控开闭指令', 1, '2023-04-03 00:00:00');
INSERT INTO `iot_cmd_explain` VALUES (128, 99, 1, '{\"cmdId\":99,\"data\":{\"relayState\":\"0\",\"relayAdress\":\"0\",\"relayMark\":\"0\",\"relayFuncode\":\"0\"},\"devId\":\"000001\"}', NULL, '宽德综合环境检测站 ==&gt; 回复继电器控制指令', 1, '2023-04-10 00:00:00');
INSERT INTO `iot_cmd_explain` VALUES (129, 100, 2, '{\"cmdId\":100,\"data\":{\"number\":\"24\",\"areaCode\":\"15\",\"startAdress\":\"0\",\"funCode\":\"3\"},\"devId\":\"BD000002BD\"}', NULL, '宽德综合环境检测站 ==&gt; 查询DTU状态', 1, '2023-04-27 00:00:00');
INSERT INTO `iot_cmd_explain` VALUES (131, 101, 1, '{\"cmdId\":101,\"data\":{\"electricity4Channel\":\"0\",\"voltageOupt4\":\"0\",\"voltageOupt3\":\"0\",\"voltageOupt2\":\"0\",\"voltageOupt1\":\"0\",\"voltageIupt1\":\"0\",\"areaCode\":\"1\",\"len\":\"0\",\"voltageIupt4\":\"0\",\"voltageIupt2\":\"0\",\"voltageIupt3\":\"0\",\"funCode\":\"3\",\"electricity6Channel\":\"0\",\"electricity5Channel\":\"0\",\"electricity7Channel\":\"0\",\"electricity0Channel\":\"0\",\"electricity1Channel\":\"0\",\"electricity2Channel\":\"0\",\"electricity3Channel\":\"0\"},\"devId\":\"0000000001\"}', NULL, '宽德综合环境检测站 ==> 回复DTU状态信息', 1, '2023-04-27 14:56:54');
INSERT INTO `iot_cmd_explain` VALUES (132, 102, 2, '{\"cmdId\":102,\"data\":{\"areaCode\":\"15\",\"twoRegisterValue\":\"1\",\"len\":\"2\",\"oneRegisterAdr\":\"20\",\"funCode\":\"16\",\"oneRegisterValue\":\"0\",\"twoRegisterAdr\":\"1\"},\"devId\":\"BD000002BD\"}', NULL, '宽德综合环境检测站 ==&gt; 第一路继电器闭合', 1, '2023-04-27 00:00:00');
INSERT INTO `iot_cmd_explain` VALUES (133, 103, 2, '{\"cmdId\":103,\"data\":{\"areaCode\":\"15\",\"twoRegisterValue\":\"0\",\"len\":\"2\",\"oneRegisterAdr\":\"20\",\"funCode\":\"16\",\"oneRegisterValue\":\"0\",\"twoRegisterAdr\":\"1\"},\"devId\":\"BD000002BD\"}', NULL, '宽德综合环境检测站 ==&gt; 第一路继电器断开', 1, '2023-04-27 00:00:00');
INSERT INTO `iot_cmd_explain` VALUES (136, 104, 2, '{\"cmdId\":104,\"data\":{\"areaCode\":\"15\",\"twoRegisterValue\":\"1\",\"len\":\"2\",\"oneRegisterAdr\":\"21\",\"funCode\":\"16\",\"oneRegisterValue\":\"0\",\"twoRegisterAdr\":\"1\"},\"devId\":\"BD000002BD\"}', NULL, '宽德综合环境检测站 ==&gt; 第二路继电器闭合', 1, '2023-04-27 00:00:00');
INSERT INTO `iot_cmd_explain` VALUES (137, 106, 2, '{\"cmdId\":106,\"data\":{\"areaCode\":\"15\",\"twoRegisterValue\":\"1\",\"len\":\"2\",\"oneRegisterAdr\":\"22\",\"funCode\":\"16\",\"oneRegisterValue\":\"0\",\"twoRegisterAdr\":\"1\"},\"devId\":\"BD000002BD\"}', NULL, '宽德综合环境检测站 ==&gt; 第三路继电器闭合', 1, '2023-04-27 00:00:00');
INSERT INTO `iot_cmd_explain` VALUES (138, 105, 2, '{\"cmdId\":105,\"data\":{\"areaCode\":\"15\",\"twoRegisterValue\":\"0\",\"len\":\"2\",\"oneRegisterAdr\":\"21\",\"funCode\":\"16\",\"oneRegisterValue\":\"0\",\"twoRegisterAdr\":\"1\"},\"devId\":\"BD000002BD\"}', NULL, '宽德综合环境检测站 ==&gt; 第二路继电器断开', 1, '2023-04-27 00:00:00');
INSERT INTO `iot_cmd_explain` VALUES (139, 107, 2, '{\"cmdId\":107,\"data\":{\"areaCode\":\"15\",\"twoRegisterValue\":\"0\",\"len\":\"2\",\"oneRegisterAdr\":\"22\",\"funCode\":\"16\",\"oneRegisterValue\":\"0\",\"twoRegisterAdr\":\"1\"},\"devId\":\"BD000002BD\"}', NULL, '宽德综合环境检测站 ==&gt; 第三路继电器断开', 1, '2023-04-27 00:00:00');
INSERT INTO `iot_cmd_explain` VALUES (140, 108, 2, '{\"cmdId\":108,\"data\":{\"areaCode\":\"15\",\"twoRegisterValue\":\"1\",\"len\":\"2\",\"oneRegisterAdr\":\"23\",\"funCode\":\"16\",\"oneRegisterValue\":\"0\",\"twoRegisterAdr\":\"1\"},\"devId\":\"BD000002BD\"}', NULL, '宽德综合环境检测站 ==&gt; 第四路继电器闭合', 1, '2023-04-27 00:00:00');
INSERT INTO `iot_cmd_explain` VALUES (141, 109, 2, '{\"cmdId\":109,\"data\":{\"areaCode\":\"15\",\"twoRegisterValue\":\"0\",\"len\":\"2\",\"oneRegisterAdr\":\"23\",\"funCode\":\"16\",\"oneRegisterValue\":\"0\",\"twoRegisterAdr\":\"1\"},\"devId\":\"BD000002BD\"}', NULL, '宽德综合环境检测站 ==&gt; 第四路继电器断开', 1, '2023-04-27 00:00:00');
INSERT INTO `iot_cmd_explain` VALUES (143, 110, 1, '{\"cmdId\":110,\"data\":{\"areaCode\":\"1\",\"relayAdress\":\"0\",\"openState\":\"0\",\"funCode\":\"3\"},\"devId\":\"0000000001\"}', NULL, '宽德综合环境检测站 ==> 回复继电器状态值', 1, '2023-04-28 13:41:32');
INSERT INTO `iot_cmd_explain` VALUES (144, 111, 2, '{\"cmdId\":111,\"data\":{\"voiceInput\":\"#\"},\"devId\":\"BD000002BD\"}', NULL, '宽德综合环境检测站 ==&gt; 语音播报', 1, '2023-04-28 00:00:00');

-- ----------------------------
-- Table structure for iot_device
-- ----------------------------
DROP TABLE IF EXISTS `iot_device`;
CREATE TABLE `iot_device`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dev_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `online` tinyint(1) NULL DEFAULT NULL COMMENT '在线',
  `is_fault` tinyint(1) UNSIGNED ZEROFILL NULL DEFAULT 1 COMMENT '故障',
  `project_id` bigint(20) NULL DEFAULT NULL COMMENT '项目',
  `product_id` bigint(20) NULL DEFAULT NULL COMMENT '产品',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `iot_device_iot_project_id_fk`(`project_id`) USING BTREE,
  INDEX `iot_device_iot_product_id_fk`(`product_id`) USING BTREE,
  CONSTRAINT `iot_device_iot_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `iot_project` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '设备' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iot_device
-- ----------------------------
INSERT INTO `iot_device` VALUES (8, 'BD000002BD', '空气环境检测设备', 1, 1, 10, 13, '空气环境检测设备', NULL, NULL, NULL, NULL);
INSERT INTO `iot_device` VALUES (9, '0331230001', '虫情测报灯', 1, 1, 11, 11, '虫情测报灯A', NULL, NULL, NULL, NULL);
INSERT INTO `iot_device` VALUES (10, '0403230001', '虫情测报灯2', 1, 1, 11, 11, '打开摄像头的虫情测报灯', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for iot_device_off_line
-- ----------------------------
DROP TABLE IF EXISTS `iot_device_off_line`;
CREATE TABLE `iot_device_off_line`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dev_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '设备',
  `project_id` bigint(20) NULL DEFAULT NULL COMMENT '项目',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `trigger_time` datetime(0) NULL DEFAULT NULL COMMENT '触发时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '设备离在线记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iot_device_off_line
-- ----------------------------

-- ----------------------------
-- Table structure for iot_frame
-- ----------------------------
DROP TABLE IF EXISTS `iot_frame`;
CREATE TABLE `iot_frame`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `type` int(255) NULL DEFAULT NULL COMMENT '帧类型',
  `pre_read_len` int(255) NULL DEFAULT NULL COMMENT '预读长度',
  `mark_flag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标志',
  `mark_index_list` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标志下标',
  `len` int(255) NULL DEFAULT NULL COMMENT '帧长度',
  `len_index_list` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '帧长度下标',
  `other_len` int(255) NULL DEFAULT NULL COMMENT '其他长度',
  `data_left_vector` int(255) NULL DEFAULT NULL COMMENT '左索引',
  `data_right_vector` int(255) NULL DEFAULT NULL COMMENT '右索引',
  `field_type` int(255) NULL DEFAULT NULL COMMENT '数据类型',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '协议帧类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iot_frame
-- ----------------------------
INSERT INTO `iot_frame` VALUES (4, 'tcp-modbus', 3, 4, '01', '0', -1, '2', 5, 0, 0, 2, 'modbus协议帧', NULL, NULL, NULL, NULL);
INSERT INTO `iot_frame` VALUES (5, 'kd-register', 1, 5, 'BD', '0', 5, '-1', -1, 0, 0, 8, '宽德智能-注册帧', NULL, NULL, NULL, NULL);
INSERT INTO `iot_frame` VALUES (6, 'msdz-frame', 3, 3, '0024A7', '0,1,2', 22, '', NULL, 1, 0, 2, '米速电子-数据帧', NULL, NULL, NULL, NULL);
INSERT INTO `iot_frame` VALUES (7, 'other-platform', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, '用于第三方的占位符', NULL, NULL, NULL, NULL);
INSERT INTO `iot_frame` VALUES (8, 'local-air-modbus', 3, 3, '9003', '0,1', -1, '2', 5, 0, 0, 2, '空气传感器的modbus协议', NULL, NULL, NULL, NULL);
INSERT INTO `iot_frame` VALUES (9, 'local-soil-modbus', 3, 3, '0103', '0,1', -1, '2', 5, 0, 0, 2, '土壤传感器', NULL, NULL, NULL, NULL);
INSERT INTO `iot_frame` VALUES (10, 'relay-modus', 3, 3, '000F', '0,1', 9, '-1', 0, 0, 0, 8, '继电器协议', NULL, NULL, NULL, NULL);
INSERT INTO `iot_frame` VALUES (11, '低功耗DTU电平协议', 5, 2, '0D0A', '0,1', NULL, '0D0A', NULL, 2, 2, 2, '低功耗DTU电平协议', NULL, NULL, NULL, NULL);
INSERT INTO `iot_frame` VALUES (12, '普通DTU电平协议', 3, 5, '0F03', '0,1', -1, '2', 5, 0, 0, 2, '普通DTU电平协议', NULL, NULL, NULL, NULL);
INSERT INTO `iot_frame` VALUES (13, '普通DTU电平控制协议', 3, 5, '0F10', '0,1', 8, '', 0, 0, 0, 2, '普通DTU电平控制协议', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for iot_platform
-- ----------------------------
DROP TABLE IF EXISTS `iot_platform`;
CREATE TABLE `iot_platform`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `company_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公司名称',
  `h5_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'h5',
  `web_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '后端',
  `user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '平台信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iot_platform
-- ----------------------------
INSERT INTO `iot_platform` VALUES (1, '宽德智能--本地测试平台', 'http://192.168.2.125/h5/login', 'http://192.168.2.125', 'admin', 'admin123', '靓仔的电脑', '2022-12-08 00:00:00');
INSERT INTO `iot_platform` VALUES (2, '宽德智能--展示平台', 'http://sf.zxqnywlw.cn/h5', 'http://sf.zxqnywlw.cn', 'admin', 'admin123', '测试', '2022-12-08 00:00:00');

-- ----------------------------
-- Table structure for iot_product
-- ----------------------------
DROP TABLE IF EXISTS `iot_product`;
CREATE TABLE `iot_product`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '名称',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资料',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `creator` bigint(255) NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `updater_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iot_product
-- ----------------------------
INSERT INTO `iot_product` VALUES (10, '环境检测仪', 'http://127.0.0.1:9300/statics/2023/03/28/637747504077147993397_20230328074518A002.jpg', NULL, '环境检测设备', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product` VALUES (11, '虫情测报灯', 'http://127.0.0.1:9300/statics/2023/03/28/cq_cbd_20230328074501A001.jpg', NULL, '虫情测报灯', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product` VALUES (13, '宽德综合环境检测站', 'http://127.0.0.1:9300/statics/2023/03/28/cq_cbd_20230328080341A004.jpg', NULL, '该传感器，更加小巧', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product` VALUES (14, '虫情报警灯', NULL, NULL, '虫情报警灯', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for iot_product_field
-- ----------------------------
DROP TABLE IF EXISTS `iot_product_field`;
CREATE TABLE `iot_product_field`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `product_id` bigint(20) NULL DEFAULT NULL COMMENT '数据包',
  `field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名',
  `def_val` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认值',
  `field_type` int(20) NULL DEFAULT NULL COMMENT '字段类型',
  `len` int(255) NULL DEFAULT NULL COMMENT '字段长度',
  `big_little_endian` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '大小端',
  `arithmetic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '算术',
  `unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `iot_packet_field_iot_packet_id_fk`(`product_id`) USING BTREE,
  CONSTRAINT `iot_packet_field_iot_packet_id_fk` FOREIGN KEY (`product_id`) REFERENCES `iot_product` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 210 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品-属性' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iot_product_field
-- ----------------------------
INSERT INTO `iot_product_field` VALUES (152, '风向', 10, 'windDirection', '0', 1, 2, NULL, NULL, '°', '风向数据', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (153, '温度', 10, 'temperature', '0', 1, 2, NULL, '(x - 400) * 0.1', '℃', '温度值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (154, '湿度', 10, 'humidity', '0', 1, 1, NULL, '', '%', '湿度数据', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (155, '风速', 10, 'windSpeed', '0', 1, 1, NULL, 'x/8*1.12', 'm/s', '风速值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (156, '平均风速', 10, 'gustSpeed', '0', 1, 1, NULL, 'x *1.12', 'm/s', '平均风速值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (157, '累计雨量', 10, 'accumulated_rainfall', '0', 1, 2, NULL, 'x*0.3', 'mm', '累计雨量数值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (158, '紫外线辐射强度', 10, 'uv', '0', 1, 2, NULL, NULL, 'uW/cm2', '紫外线辐射强度值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (159, '光照度', 10, 'light', '0', 1, 3, NULL, 'x * 0.1', ' Lux', '光照值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (160, '气压值', 10, 'pressure', '0', 1, 3, NULL, 'x * 0.01', 'hpa', '气压数据', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (161, '光照强度', 13, 'light', '0', 1, 2, NULL, 'x*10', 'Lux', '单位Lux', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (162, '紫外线指数', 13, 'uvi', '0', 1, 2, NULL, 'x * 0.1', 'uW/cm2', '紫外线', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (163, '温度', 13, 'temperature', '0', 1, 2, NULL, '(x - 400) * 0.1', '℃', '温度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (164, '湿度', 13, 'humidity', '0', 1, 2, NULL, NULL, '%', '湿度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (165, '风速', 13, 'windSpeed', '0', 1, 2, NULL, 'x * 0.1', 'm/s', '风速', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (166, '阵风风速', 13, 'gustSpeed', '0', 1, 2, NULL, 'x * 0.1', 'm/s', '阵风风速', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (167, '风向', 13, 'windDirection', '0', 1, 2, NULL, NULL, '°', '风向', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (168, '降雨量', 13, 'rainFall', '0', 1, 2, NULL, 'x * 0.1', 'mm', '降雨量', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (169, '气压', 13, 'pressure', '0', 1, 2, NULL, 'x * 0.1', 'hPa', '气压', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (170, '土壤温度', 13, 'temperature', '0', 1, 2, NULL, 'x*0.01', '℃', '土壤温度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (171, '土壤湿度', 13, 'humidity', '0', 1, 2, '', 'x*0.01', '%', '土壤湿度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (172, '土壤电导率', 13, 'EC', '0', 1, 2, NULL, 'x', 'us/cm', '土壤电导率', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (173, '土壤酸碱度', 13, 'PH', '0', 1, 2, NULL, 'x*0.1', NULL, '土壤酸碱度', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (174, '土壤氮', 13, 'N', '0', 1, 2, NULL, 'x', 'mg/L', '土壤氮', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (175, '土壤磷', 13, 'P', '0', 1, 2, NULL, 'x', 'mg/L', '土壤磷', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (176, '土壤钾', 13, 'K', '0', 1, 2, NULL, 'x', 'mg/L', '土壤钾', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (177, '地址码', 13, 'areaCode', '1', 1, 1, NULL, NULL, NULL, '地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (178, '功能码', 13, 'funCode', '3', 1, 1, NULL, NULL, NULL, '功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (179, '寄存器起始地址', 13, 'startAdress', '1', 1, 2, NULL, NULL, NULL, '起始地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (180, '寄存器个数', 13, 'number', '7', 1, 2, NULL, NULL, NULL, '寄存器个数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (181, ' 有效字节数', 13, 'len', '0', 1, 1, NULL, NULL, NULL, ' 有效字节数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (182, '字节数', 13, 'byteLen', '1', 1, 1, NULL, NULL, NULL, '功能码10，写入寄存器字节数', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (183, '一二位字节值', 13, 'byteValue1', '1', 1, 2, NULL, NULL, NULL, '功能码10，一二位字节值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (184, '继电器标识', 13, 'relayMark', '0', 1, 2, NULL, NULL, NULL, '继电器标识', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (185, '继电器功能码', 13, 'relayFuncode', '0', 4, 2, NULL, NULL, NULL, '继电器功能码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (186, '继电器地址码', 13, 'relayAdress', '0', 1, 2, NULL, NULL, NULL, '继电器地址码', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (187, '继电器状态', 13, 'relayState', '0', 1, 1, NULL, NULL, NULL, '继电器状态', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (188, '电流0号通道', 13, 'electricity0Channel', '0', 6, 4, '1234', NULL, NULL, '电流0号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (189, '电流1号通道', 13, 'electricity1Channel', '0', 6, 4, '1234', NULL, NULL, '电流1号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (190, '电流2号通道', 13, 'electricity2Channel', '0', 6, 4, '1234', NULL, NULL, '电流2号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (191, '电流3号通道', 13, 'electricity3Channel', '0', 6, 4, '1234', NULL, NULL, '电流3号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (192, '电流4号通道', 13, 'electricity4Channel', '0', 6, 4, '1234', NULL, NULL, '电流4号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (193, '电流5号通道', 13, 'electricity5Channel', '0', 6, 4, '1234', NULL, NULL, '电流5号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (194, '电流6号通道', 13, 'electricity6Channel', '0', 6, 4, '1234', NULL, NULL, '电流6号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (195, '电流7号通道', 13, 'electricity7Channel', '0', 6, 4, '1234', NULL, NULL, '电流7号通道', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (196, '电压输入1', 13, 'voltageIupt1', '0', 1, 2, NULL, NULL, NULL, '电压输入1', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (197, '电压输入2', 13, 'voltageIupt2', '0', 1, 2, NULL, NULL, NULL, '电压输入2', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (198, '电压输入3', 13, 'voltageIupt3', '0', 1, 2, NULL, NULL, NULL, '电压输入3', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (199, '电压输入4', 13, 'voltageIupt4', '0', 1, 2, NULL, NULL, NULL, '电压输入4', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (200, '电压输出1', 13, 'voltageOupt1', '0', 1, 2, NULL, NULL, NULL, '电压输出1', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (201, '电压输出2', 13, 'voltageOupt2', '0', 1, 2, NULL, NULL, NULL, '电压输出2', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (202, '电压输出3', 13, 'voltageOupt3', '0', 1, 2, NULL, NULL, NULL, '电压输出3', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (203, '电压输出4', 13, 'voltageOupt4', '0', 1, 2, NULL, NULL, NULL, '电压输出4', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (204, '第一位寄存器地址', 13, 'oneRegisterAdr', '0', 1, 2, NULL, NULL, NULL, '第一位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (205, '第二位寄存器地址', 13, 'twoRegisterAdr', '0', 1, 2, NULL, NULL, NULL, '第二位寄存器地址', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (206, '第一位寄存器地址值', 13, 'oneRegisterValue', '0', 1, 1, NULL, NULL, NULL, '第一位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (207, '第二位寄存器地址值', 13, 'twoRegisterValue', '0', 1, 1, NULL, NULL, NULL, '第二位寄存器地址值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (208, '开关状态值', 13, 'openState', '0', 1, 2, NULL, NULL, NULL, '开关状态值', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_field` VALUES (209, '语音输入', 13, 'voiceInput', '#', 11, -1, NULL, NULL, NULL, '语音输入', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for iot_product_filter
-- ----------------------------
DROP TABLE IF EXISTS `iot_product_filter`;
CREATE TABLE `iot_product_filter`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `product_id` bigint(20) NULL DEFAULT NULL COMMENT '产品',
  `cmd_id` bigint(20) NULL DEFAULT NULL COMMENT '指令',
  `fileds` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段',
  `enable` int(20) NULL DEFAULT NULL COMMENT '启用',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数据筛选' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iot_product_filter
-- ----------------------------
INSERT INTO `iot_product_filter` VALUES (1, '筛选水分温度值', 9, 89, '145,146,149,150,151', 1, '大鹏数据展示', '2023-03-03 14:14:53');
INSERT INTO `iot_product_filter` VALUES (5, '水分温度筛选', 9, 89, '150,151', 1, '水分温度筛选', '2023-03-03 16:56:21');
INSERT INTO `iot_product_filter` VALUES (6, '数据筛选', 10, 90, '152,153,154,155,156,157,158,159,160', 1, '筛选值', '2023-03-06 11:35:47');

-- ----------------------------
-- Table structure for iot_product_frame
-- ----------------------------
DROP TABLE IF EXISTS `iot_product_frame`;
CREATE TABLE `iot_product_frame`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `product_id` bigint(20) NULL DEFAULT NULL COMMENT '产品',
  `frame_id` bigint(20) NULL DEFAULT NULL COMMENT '协议',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品-数据帧' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iot_product_frame
-- ----------------------------
INSERT INTO `iot_product_frame` VALUES (16, 10, 5, '设备入网注册', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_frame` VALUES (17, 10, 6, '设备数据上报分析', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_frame` VALUES (20, 13, 5, '宽德注册帧', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_frame` VALUES (21, 13, 8, '空气传感器', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_frame` VALUES (22, 13, 9, '土壤传感器', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_frame` VALUES (23, 13, 10, '继电器协议', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_frame` VALUES (24, 14, 11, '电平协议', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_frame` VALUES (25, 14, 5, '虫情注册帧', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_frame` VALUES (26, 13, 12, '普通DTU电平协议', NULL, NULL, NULL, NULL);
INSERT INTO `iot_product_frame` VALUES (27, 13, 13, '继电器开关协议', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for iot_project
-- ----------------------------
DROP TABLE IF EXISTS `iot_project`;
CREATE TABLE `iot_project`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '地点',
  `longitude` double NULL DEFAULT NULL COMMENT '经度',
  `latitude` double NULL DEFAULT NULL COMMENT '纬度',
  `principal` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '负责人',
  `principal_tel` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '负责人电话',
  `startup_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `warranty_year` int(11) NULL DEFAULT NULL COMMENT '质保年份',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  `network_state` int(11) NULL DEFAULT 0 COMMENT '网络状态',
  `network_port` int(11) NULL DEFAULT NULL COMMENT '网络端口',
  `network_protocol` int(11) NULL DEFAULT NULL COMMENT '网络类型',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `crate_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '项目表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iot_project
-- ----------------------------
INSERT INTO `iot_project` VALUES (10, '南京现代服务大厦', '江苏省南京市秦淮区光华产业园', NULL, NULL, '赵晓强', '18010775624', '2023-03-07 00:00:00', 2, 1, 2, 10009, 1, '测试环境检测移', NULL, NULL, NULL, NULL);
INSERT INTO `iot_project` VALUES (11, '盱眙县林场淮河分场', '盱眙县林场淮河分场', NULL, NULL, '赵晓强', '18010775624', '2023-04-07 00:00:00', 2, 1, 0, 10005, 1, 'ecd23f31-c51e-4ff7-a403-7ae9546b4902', NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
