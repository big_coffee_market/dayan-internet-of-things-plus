/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50741
 Source Host           : localhost:3306
 Source Schema         : plantation_management

 Target Server Type    : MySQL
 Target Server Version : 50741
 File Encoding         : 65001

 Date: 18/02/2023 13:13:04
*/
DROP DATABASE IF EXISTS `plantation_management`;

CREATE DATABASE  `plantation_management` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

USE `plantation_management`;

-- ----------------------------
-- Table structure for agricultural resource
-- ----------------------------
DROP TABLE IF EXISTS `agricultural resource`;
CREATE TABLE `agricultural resource`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `classification_id` bigint(20) NULL DEFAULT NULL COMMENT '类别',
  `packing_specification` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '包装规格',
  `inventory` int(20) NULL DEFAULT NULL COMMENT '库存量',
  `manufacturer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生产厂家',
  `date_of_production` datetime(0) NULL DEFAULT NULL COMMENT '生产日期',
  `shelf_life` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '保质期',
  `approval_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '批准文号',
  `production_license` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生产许可证',
  `selling_firm` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '销售厂商',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '农资管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agricultural resource
-- ----------------------------

-- ----------------------------
-- Table structure for agricultural_crop
-- ----------------------------
DROP TABLE IF EXISTS `agricultural_crop`;
CREATE TABLE `agricultural_crop`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `resource_seed_id` bigint(20) NULL DEFAULT NULL COMMENT '类别',
  `alias` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '别名',
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图像',
  `stage` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所含阶段',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '农作物' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agricultural_crop
-- ----------------------------

-- ----------------------------
-- Table structure for agricultural_harvest
-- ----------------------------
DROP TABLE IF EXISTS `agricultural_harvest`;
CREATE TABLE `agricultural_harvest`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `plan_id` bigint(11) NULL DEFAULT NULL COMMENT '农事计划',
  `time` datetime(0) NULL DEFAULT NULL COMMENT '时间',
  `quantity` int(20) NULL DEFAULT NULL COMMENT '数量',
  `source_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '溯源码',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '采收管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agricultural_harvest
-- ----------------------------

-- ----------------------------
-- Table structure for agricultural_plan
-- ----------------------------
DROP TABLE IF EXISTS `agricultural_plan`;
CREATE TABLE `agricultural_plan`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `plot_id` bigint(20) NULL DEFAULT NULL COMMENT '地块',
  `area` decimal(20, 0) NULL DEFAULT NULL COMMENT '面积',
  `seed_id` bigint(20) NULL DEFAULT NULL COMMENT '种子',
  `dosage` int(11) NULL DEFAULT NULL COMMENT '用量',
  `blockchain_key` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区块链',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `planting_condition` bigint(20) NULL DEFAULT NULL COMMENT '种植状态',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '农事计划' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agricultural_plan
-- ----------------------------

-- ----------------------------
-- Table structure for agricultural_planting_plot
-- ----------------------------
DROP TABLE IF EXISTS `agricultural_planting_plot`;
CREATE TABLE `agricultural_planting_plot`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `base_id` bigint(20) NULL DEFAULT NULL COMMENT '基地',
  `responsible_person` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '责任人',
  `tel_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '种植地块' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agricultural_planting_plot
-- ----------------------------

-- ----------------------------
-- Table structure for agricultural_record
-- ----------------------------
DROP TABLE IF EXISTS `agricultural_record`;
CREATE TABLE `agricultural_record`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `plan_id` bigint(11) NULL DEFAULT NULL COMMENT '农事计划',
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图像',
  `operating_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `agricultural_stage_id` bigint(11) NULL DEFAULT NULL COMMENT '农事阶段',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '农事记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agricultural_record
-- ----------------------------

-- ----------------------------
-- Table structure for const_agricultural_stage
-- ----------------------------
DROP TABLE IF EXISTS `const_agricultural_stage`;
CREATE TABLE `const_agricultural_stage`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '农事阶段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of const_agricultural_stage
-- ----------------------------

-- ----------------------------
-- Table structure for const_certificate_stage
-- ----------------------------
DROP TABLE IF EXISTS `const_certificate_stage`;
CREATE TABLE `const_certificate_stage`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '证书类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of const_certificate_stage
-- ----------------------------

-- ----------------------------
-- Table structure for const_crop_stage
-- ----------------------------
DROP TABLE IF EXISTS `const_crop_stage`;
CREATE TABLE `const_crop_stage`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '生长阶段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of const_crop_stage
-- ----------------------------

-- ----------------------------
-- Table structure for const_processing_stage
-- ----------------------------
DROP TABLE IF EXISTS `const_processing_stage`;
CREATE TABLE `const_processing_stage`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '加工类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of const_processing_stage
-- ----------------------------

-- ----------------------------
-- Table structure for const_product_supplier
-- ----------------------------
DROP TABLE IF EXISTS `const_product_supplier`;
CREATE TABLE `const_product_supplier`  (
  `id` bigint(11) NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `phone_number` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `contact_person` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '供货商' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of const_product_supplier
-- ----------------------------

-- ----------------------------
-- Table structure for const_product_test_stage
-- ----------------------------
DROP TABLE IF EXISTS `const_product_test_stage`;
CREATE TABLE `const_product_test_stage`  (
  `id` int(11) NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '检测类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of const_product_test_stage
-- ----------------------------

-- ----------------------------
-- Table structure for const_resource_stage
-- ----------------------------
DROP TABLE IF EXISTS `const_resource_stage`;
CREATE TABLE `const_resource_stage`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '记录',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '农资类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of const_resource_stage
-- ----------------------------

-- ----------------------------
-- Table structure for product_certificate
-- ----------------------------
DROP TABLE IF EXISTS `product_certificate`;
CREATE TABLE `product_certificate`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `supplier_id` bigint(20) NULL DEFAULT NULL COMMENT '供应商',
  `certificate` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '证书',
  `type` bigint(255) NULL DEFAULT NULL COMMENT '类型',
  `issuing_authority` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '颁发机构',
  `issue_time` datetime(0) NULL DEFAULT NULL COMMENT '颁发时间',
  `certificate_uploading` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '证书上传',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品证书' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_certificate
-- ----------------------------

-- ----------------------------
-- Table structure for product_processing
-- ----------------------------
DROP TABLE IF EXISTS `product_processing`;
CREATE TABLE `product_processing`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `harvest_id` bigint(20) NULL DEFAULT NULL COMMENT '采收批次',
  `type` bigint(255) NULL DEFAULT NULL COMMENT '类型',
  `quantity` int(255) NULL DEFAULT NULL COMMENT '数量',
  `time` datetime(0) NULL DEFAULT NULL COMMENT '时间',
  `workshop` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '车间',
  `equipment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '设备',
  `supplier_id` bigint(20) NULL DEFAULT NULL COMMENT '供应商',
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品加工' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_processing
-- ----------------------------

-- ----------------------------
-- Table structure for product_testing
-- ----------------------------
DROP TABLE IF EXISTS `product_testing`;
CREATE TABLE `product_testing`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `harvest_id` bigint(20) NULL DEFAULT NULL COMMENT '采收批次',
  `type` bigint(255) NULL DEFAULT NULL COMMENT '类型',
  `institution` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '机构',
  `time` datetime(0) NULL DEFAULT NULL COMMENT '时间',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内容',
  `conclusion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '结论',
  `certificate_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '合格证名称',
  `certificate_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '证书编号',
  `certificate_picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '证书图片',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品检测' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_testing
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
