-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品-属性', '1061', '1', 'field', 'iot/field/index', 1, 0, 'C', '0', '0', 'iot:field:list', '#', 'admin', sysdate(), '', null, '产品-属性菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品-属性查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'iot:field:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品-属性新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'iot:field:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品-属性修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'iot:field:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品-属性删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'iot:field:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品-属性导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'iot:field:export',       '#', 'admin', sysdate(), '', null, '');