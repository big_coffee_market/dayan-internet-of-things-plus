-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('数据筛选', '1061', '1', 'product_filter', 'iot/product_filter/index', 1, 0, 'C', '0', '0', 'iot:product_filter:list', '#', 'admin', sysdate(), '', null, '数据筛选菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('数据筛选查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'iot:product_filter:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('数据筛选新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'iot:product_filter:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('数据筛选修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'iot:product_filter:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('数据筛选删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'iot:product_filter:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('数据筛选导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'iot:product_filter:export',       '#', 'admin', sysdate(), '', null, '');