-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品加工', '1191', '1', 'product_processing', 'plant/product_processing/index', 1, 0, 'C', '0', '0', 'plant:product_processing:list', '#', 'admin', sysdate(), '', null, '产品加工菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品加工查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_processing:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品加工新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_processing:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品加工修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_processing:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品加工删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_processing:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品加工导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_processing:export',       '#', 'admin', sysdate(), '', null, '');