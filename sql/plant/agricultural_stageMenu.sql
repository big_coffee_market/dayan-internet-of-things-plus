-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农事阶段', '1191', '1', 'agricultural_stage', 'plant/agricultural_stage/index', 1, 0, 'C', '0', '0', 'plant:agricultural_stage:list', '#', 'admin', sysdate(), '', null, '农事阶段菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农事阶段查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_stage:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农事阶段新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_stage:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农事阶段修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_stage:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农事阶段删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_stage:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农事阶段导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_stage:export',       '#', 'admin', sysdate(), '', null, '');