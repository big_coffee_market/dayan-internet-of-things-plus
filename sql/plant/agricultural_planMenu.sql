-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农事计划', '1191', '1', 'agricultural_plan', 'plant/agricultural_plan/index', 1, 0, 'C', '0', '0', 'plant:agricultural_plan:list', '#', 'admin', sysdate(), '', null, '农事计划菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农事计划查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_plan:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农事计划新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_plan:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农事计划修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_plan:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农事计划删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_plan:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农事计划导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_plan:export',       '#', 'admin', sysdate(), '', null, '');