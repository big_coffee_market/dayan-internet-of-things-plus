-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农资管理', '1191', '1', 'agricultural_resource', 'plant/agricultural_resource/index', 1, 0, 'C', '0', '0', 'plant:agricultural_resource:list', '#', 'admin', sysdate(), '', null, '农资管理菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农资管理查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_resource:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农资管理新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_resource:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农资管理修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_resource:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农资管理删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_resource:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农资管理导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_resource:export',       '#', 'admin', sysdate(), '', null, '');