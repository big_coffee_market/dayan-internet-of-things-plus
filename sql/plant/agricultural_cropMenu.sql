-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农作物', '1191', '1', 'agricultural_crop', 'plant/agricultural_crop/index', 1, 0, 'C', '0', '0', 'plant:agricultural_crop:list', '#', 'admin', sysdate(), '', null, '农作物菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农作物查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_crop:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农作物新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_crop:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农作物修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_crop:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农作物删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_crop:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('农作物导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_crop:export',       '#', 'admin', sysdate(), '', null, '');