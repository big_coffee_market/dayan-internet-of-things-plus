-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品证书', '1191', '1', 'product_certificate', 'plant/product_certificate/index', 1, 0, 'C', '0', '0', 'plant:product_certificate:list', '#', 'admin', sysdate(), '', null, '产品证书菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品证书查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_certificate:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品证书新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_certificate:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品证书修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_certificate:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品证书删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_certificate:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品证书导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_certificate:export',       '#', 'admin', sysdate(), '', null, '');