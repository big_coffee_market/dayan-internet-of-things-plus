-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品检测', '1191', '1', 'product_testing', 'plant/product_testing/index', 1, 0, 'C', '0', '0', 'plant:product_testing:list', '#', 'admin', sysdate(), '', null, '产品检测菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品检测查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_testing:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品检测新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_testing:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品检测修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_testing:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品检测删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_testing:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('产品检测导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'plant:product_testing:export',       '#', 'admin', sysdate(), '', null, '');