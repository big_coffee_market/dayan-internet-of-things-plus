-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('基地', '1191', '1', 'agricultural_base', 'plant/agricultural_base/index', 1, 0, 'C', '0', '0', 'plant:agricultural_base:list', '#', 'admin', sysdate(), '', null, '基地菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('基地查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_base:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('基地新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_base:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('基地修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_base:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('基地删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_base:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('基地导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_base:export',       '#', 'admin', sysdate(), '', null, '');