-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('加工类型', '1191', '1', 'processing_stage', 'plant/processing_stage/index', 1, 0, 'C', '0', '0', 'plant:processing_stage:list', '#', 'admin', sysdate(), '', null, '加工类型菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('加工类型查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'plant:processing_stage:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('加工类型新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'plant:processing_stage:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('加工类型修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'plant:processing_stage:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('加工类型删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'plant:processing_stage:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('加工类型导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'plant:processing_stage:export',       '#', 'admin', sysdate(), '', null, '');