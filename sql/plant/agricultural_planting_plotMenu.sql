-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('种植地块', '1191', '1', 'agricultural_planting_plot', 'plant/agricultural_planting_plot/index', 1, 0, 'C', '0', '0', 'plant:agricultural_planting_plot:list', '#', 'admin', sysdate(), '', null, '种植地块菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('种植地块查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_planting_plot:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('种植地块新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_planting_plot:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('种植地块修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_planting_plot:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('种植地块删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_planting_plot:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('种植地块导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'plant:agricultural_planting_plot:export',       '#', 'admin', sysdate(), '', null, '');