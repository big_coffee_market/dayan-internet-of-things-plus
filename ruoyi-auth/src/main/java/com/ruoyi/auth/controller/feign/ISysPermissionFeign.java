package com.ruoyi.auth.controller.feign;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.api.domain.SysUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Set;

@FeignClient(name = "ruoyi-system/permission/")
public interface ISysPermissionFeign {

    /**
     * 获取角色权限列表
     * @param user
     * @return
     */
    @PostMapping("sys/role")
    AjaxResult getRolePermission(@RequestBody SysUser user);

    /**
     * 获取菜单权限列表
     * @param user 用户
     * @return
     */
    @PostMapping("sys/menu")
    AjaxResult getMenuPermission(SysUser user);

}
