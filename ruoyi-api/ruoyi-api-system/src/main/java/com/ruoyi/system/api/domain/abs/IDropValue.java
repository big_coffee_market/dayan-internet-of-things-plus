package com.ruoyi.system.api.domain.abs;

public interface IDropValue {

    /**
     * 获取id
     * @return
     */
    Long getId();

    /**
     * 获取名称
     * @return
     */
    String getName();


}
