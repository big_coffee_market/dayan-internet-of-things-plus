package com.zxq.factory.decoder;

public interface IFieldDecoder<T> {

    T decoder(byte[] msg);

}
