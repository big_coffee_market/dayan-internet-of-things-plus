package com.zxq.factory.decoder.impl;

import com.zxq.factory.decoder.IFieldDecoder;

public class HexDecoder implements IFieldDecoder<String> {

    @Override
    public String decoder(byte[] msg) {
        String sum = "";
        for(byte b : msg) {
            String hexChar = String.format("%02x",b).toUpperCase();
            sum += hexChar;
        }
        return sum;
    }

}
