package com.zxq.factory.decoder.impl;

import com.zxq.factory.FastFrame;
import com.zxq.factory.decoder.IFieldDecoder;
import com.zxq.factory.model.CPTimeFrame;


public class CPTimeDecoder implements IFieldDecoder<String> {




    @Override
    public String decoder(byte[] msg) {
        byte[] dst = new byte[msg.length];
        for(int i = 0; i < dst.length;i++) {
            dst[i] = msg[msg.length - i - 1];
        }
        CPTimeFrame cpTimeFrame = new CPTimeFrame();
        FastFrame.decoder(cpTimeFrame,dst);
        String year = 20 + "" + cpTimeFrame.getYear();
        String month = cpTimeFrame.getMonth() + "";
        String day = cpTimeFrame.getDay() + "";
        String hour = cpTimeFrame.getHour() + "";
        String minute = cpTimeFrame.getMinute() + "";

        String second = ((cpTimeFrame.getSeconds() + 65536) % 65536) / 1000 + "";

        String timeStamp = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;

        return timeStamp;
    }

}
