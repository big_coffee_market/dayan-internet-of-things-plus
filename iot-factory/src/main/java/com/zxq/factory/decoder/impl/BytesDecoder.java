/**
 * FileName: BytesInterpreter
 * Author:   zhaoxiaoqiang
 * Date:     2019/8/19 16:06
 * Description: 二进制解析
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zxq.factory.decoder.impl;


import com.zxq.factory.decoder.IFieldDecoder;

/**
 * 〈二进制解析〉
 *
 * @author zhaoxiaoqiang
 * @date 2019/8/19
 * @since 1.0.0
 */

public class BytesDecoder implements IFieldDecoder<byte[]> {


    @Override
    public byte[] decoder(byte[] msg) {
        return msg;
    }
}
