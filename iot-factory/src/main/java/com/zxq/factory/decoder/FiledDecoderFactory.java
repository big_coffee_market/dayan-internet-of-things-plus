package com.zxq.factory.decoder;

import com.zxq.factory.decoder.impl.*;
import com.zxq.factory.enums.EField;

public class FiledDecoderFactory {

    static FiledDecoderFactory filedDecoderFactory = new FiledDecoderFactory();

    public  static FiledDecoderFactory instance() {
        return filedDecoderFactory;
    }

    StringDecoder stringDecoder = new StringDecoder();

    BoolDecoder boolDecoder = new BoolDecoder();

    BytesDecoder bytesDecoder = new BytesDecoder();

    FloatDecoder floatDecoder = new FloatDecoder();

    DoubleDecoder doubleDecoder = new DoubleDecoder();

    IntDecoder intDecoder = new IntDecoder();

    LongDecoder longDecoder = new LongDecoder();

    HexDecoder hexDecoder = new HexDecoder();

    CPTimeDecoder cpTimeDecoder = new CPTimeDecoder();

    public IFieldDecoder create(EField field) {
        IFieldDecoder iFieldDecoder = null;
        switch (field) {
            case String:
                iFieldDecoder = stringDecoder;
                break;
            case Boolean:
                iFieldDecoder = boolDecoder;
                break;
            case Bytes:
                iFieldDecoder = bytesDecoder;
                break;
            case Float:
                iFieldDecoder = floatDecoder;
                break;
            case Int:
                iFieldDecoder = intDecoder;
                break;
            case Long:
                iFieldDecoder = longDecoder;
                break;
            case Hex:
                iFieldDecoder = hexDecoder;
                break;
            case Double:
                iFieldDecoder = doubleDecoder;
                break;
            case CP56Time2a:
                iFieldDecoder = cpTimeDecoder;
                break;
        }
        return iFieldDecoder;
    }





}
