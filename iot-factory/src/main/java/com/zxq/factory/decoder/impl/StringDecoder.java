/**
 * FileName: AsciiInterpreter
 * Author:   Administrator
 * Date:     2019/6/9 13:44
 * Description: 解析成Ascii字符串类型
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zxq.factory.decoder.impl;



import com.zxq.factory.decoder.IFieldDecoder;

import java.io.UnsupportedEncodingException;

/**
 * 〈一句话功能简述〉<br> 
 * 〈解析成AscII字符串类型〉
 *
 * @author Administrator
 * @date 2019/6/9
 * @since 1.0.0
 */

public class StringDecoder implements IFieldDecoder<String> {


    @Override
    public String decoder(byte[] msg) {
        String ascStr = null;
        try {
            ascStr = new String(msg,"ISO8859-1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return ascStr;
    }
}
