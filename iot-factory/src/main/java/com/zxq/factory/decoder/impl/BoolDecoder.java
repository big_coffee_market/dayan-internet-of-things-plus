/**
 * FileName: BoolInterpreter
 * Author:   zhaoxiaoqiang
 * Date:     2019/8/20 14:43
 * Description: 波尔值解释器
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zxq.factory.decoder.impl;


import com.zxq.factory.decoder.IFieldDecoder;

/**
 * 〈波尔值解释器〉
 *
 * @author zhaoxiaoqiang
 * @date 2019/8/20
 * @since 1.0.0
 */

public class BoolDecoder implements IFieldDecoder<Boolean> {

    @Override
    public Boolean decoder(byte[] msg) {
        return msg[msg.length - 1] == 1;
    }

}
