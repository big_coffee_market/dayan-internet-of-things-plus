/**
 * FileName: LongInterpreter
 * Author:   赵晓强
 * Date:     2019/9/17 14:34
 * Description: 解析成Long类型
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zxq.factory.decoder.impl;
import com.zxq.factory.decoder.IFieldDecoder;


/**
 * 〈解析成Long类型〉
 *
 * @author zhaoxiaoqiang
 * @date 2019/9/17
 * @since 1.0.0
 */

public class LongDecoder implements IFieldDecoder<Long> {


    @Override
    public Long decoder(byte[] msg) {
        int len = msg.length;
        Long value = 0L;
        Long radix = 1L;
        Long sum = 0L;
        if(msg[0] >= 0) {
            //原码 - 真值
            for (int i = 0; i < len; i++) {
                Long temp = Long.valueOf((msg[len - i - 1] + 256) % 256);
                value = temp * radix;
                if (value < 0) {
                    value = value & 0xff;
                }
                sum += value;
                radix = radix * 256;
            }
        } else {
            //补码 - 反码
            // 补码运算
            int prefix = 0;
            int index = len - 1;
            do{
                if(msg[index] == (byte)0x00) {
                    //0xff进位的时候置为零，下一位加一
                    //  log.info("index: " + index + " num:" + buff[index]);
                    msg[index] = (byte) 0xff;
                    prefix = 1;
                    index = index - 1;
                } else {
                    //补码 + 1
                    msg[index] = (byte) (msg[index] + prefix);
                    msg[index] -= 1;
                    break;
                }
            }while (index > 0);
            //反码 - 原码
            for (int i = 0; i < len; i++) {
                msg[i] = (byte) ~msg[i];
            }

            //原码 - 真值
            for (int i = 0; i < len; i++) {
                byte temp = msg[len - i - 1];
                value = Long.valueOf(temp * radix);
                if (value < 0) {
                    value = value & 0xff;
                }
                sum += value;
                radix = radix * 256;
            }
            sum = -1 * sum;
        }
        return sum;
    }







}
