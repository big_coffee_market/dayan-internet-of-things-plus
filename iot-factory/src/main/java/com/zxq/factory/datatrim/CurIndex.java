/**
 * FileName: CurIndex
 * Author:   zhaoxiaoqiang
 * Date:     2019/7/23 17:43
 * Description: 包装类型
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zxq.factory.datatrim;

/**
 * 〈包装类型,表示数据位置的〉
 *
 * @author zhaoxiaoqiang
 * @date 2019/7/23
 * @since 1.0.0
 */
public class CurIndex {

    public  Integer pos = 0;

}
