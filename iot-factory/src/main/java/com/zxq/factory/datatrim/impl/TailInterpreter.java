/**
 * FileName: TailInterpreter
 * Author:   zhaoxiaoqiang
 * Date:     2019/7/23 11:11
 * Description: 尾长度解析
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zxq.factory.datatrim.impl;


import com.zxq.factory.datatrim.CurIndex;
import com.zxq.factory.datatrim.ITrimInterpreter;

/**
 * 〈尾长度解析〉
 *
 * @author zhaoxiaoqiang
 * @date 2019/7/23
 * @since 1.0.0
 */
public class TailInterpreter implements ITrimInterpreter {

    /**
     *
     * @param src 源数据
     * @param curIndex  当前的索引
     * @param param 倒数第几位的值
     * @return
     */

    @Override
    public byte[] interpreter(byte[] src, CurIndex curIndex, Integer param) {
         int endPos = src.length - param;
         int len = endPos - curIndex.pos;
         byte[] dst = new byte[len];
         System.arraycopy(src,curIndex.pos,dst,0,dst.length);
         curIndex.pos = endPos;

         return dst;
    }

}
