/**
 * FileName: DataTrimFactory
 * Author:   zhaoxiaoqiang
 * Date:     2019/8/19 16:11
 * Description: 数据截取工厂方法
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zxq.factory.datatrim;

import com.zxq.factory.datatrim.impl.LengthInterpreter;
import com.zxq.factory.datatrim.impl.TailInterpreter;
import com.zxq.factory.datatrim.impl.UntilsInterpreter;
import com.zxq.factory.enums.ETrimMethod;


/**
 * 〈数据截取工厂方法〉
 *
 * @author zhaoxiaoqiang
 * @date 2019/8/19
 * @since 1.0.0
 */

public class DataTrimFactory {

    static DataTrimFactory dataTrimFactory = new DataTrimFactory();
    public static DataTrimFactory instance() {
        return dataTrimFactory;
    }


    /**
     * 长度拦截
     */
    LengthInterpreter lengthInterpreter = new LengthInterpreter();

    /**
     * 倒数第几位拦截
     */
    TailInterpreter tailInterpreter = new TailInterpreter();

    /**
     * 直到拦截
     */
    UntilsInterpreter untilsInterpreter = new UntilsInterpreter();


    public ITrimInterpreter create(ETrimMethod trimMethod) {
        ITrimInterpreter trimInterpreter = null;
        switch (trimMethod) {
            case Tail:{
                trimInterpreter = tailInterpreter;
            }
            break;
            case Untils: {
                trimInterpreter = untilsInterpreter;
            }
            break;
            case Length: {
                trimInterpreter = lengthInterpreter;
            }
            break;
        }
       return trimInterpreter;
    }

}
