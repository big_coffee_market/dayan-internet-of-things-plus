/**
 * FileName: IInterpreterType
 * Author:   zhaoxiaoqiang
 * Date:     2019/7/23 10:50
 * Description: 解析具体的类型
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zxq.factory.datatrim;

/**
 * 〈解析具体的类型〉
 *
 * @author zhaoxiaoqiang
 * @create 2019/7/23
 * @since 1.0.0
 */

public interface ITrimInterpreter {

    /**
     * 根据长度来截取数据
     * @param src 源数据
     * @param curIndex  当前的索引
     * @param param 参数
     * @return 具体的数据值
     */
    byte[] interpreter(byte[] src, CurIndex curIndex, Integer param);

}
