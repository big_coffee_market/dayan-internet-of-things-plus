/**
 * FileName: LengthInterpreter
 * Author:   zhaoxiaoqiang
 * Date:     2019/7/23 11:06
 * Description: 长度解析
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zxq.factory.datatrim.impl;


import com.zxq.factory.datatrim.CurIndex;
import com.zxq.factory.datatrim.ITrimInterpreter;

/**
 * 〈长度解析〉
 *
 * @author zhaoxiaoqiang
 * @date 2019/7/23
 * @since 1.0.0
 */

public class LengthInterpreter implements ITrimInterpreter {

    /**
     *
     * @param src 源数据
     * @param curIndex  当前的索引
     * @param param 长度参数
     * @return
     */
    @Override
    public byte[] interpreter(byte[] src, CurIndex curIndex, Integer param) {
        byte[] dstBuff = new byte[param];
        System.arraycopy(src,curIndex.pos,dstBuff,0,dstBuff.length);

        curIndex.pos = curIndex.pos+dstBuff.length;

        return dstBuff;
    }


}
