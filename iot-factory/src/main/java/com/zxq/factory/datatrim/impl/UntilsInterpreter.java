/**
 * FileName: UntilsInterpreter
 * Author:   zhaoxiaoqiang
 * Date:     2019/7/23 11:14
 * Description: 直到的数据解析
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zxq.factory.datatrim.impl;


import com.zxq.factory.datatrim.CurIndex;
import com.zxq.factory.datatrim.ITrimInterpreter;

/**
 * 〈直到的数据解析〉
 *
 * @author zhaoxiaoqiang
 * @date 2019/7/23
 * @since 1.0.0
 */

public class UntilsInterpreter implements ITrimInterpreter {


    LengthInterpreter lengthInterpreter = new LengthInterpreter();
    /**
     *
     * @param src 源数据
     * @param curIndex  当前的索引 src解析的当前索引
     * @param param 参数 直到某个byte值
     * @return
     */
    @Override
    public byte[] interpreter(byte[] src, CurIndex curIndex, Integer param) {

        int endPos = -1;
        for(int i = curIndex.pos; i< src.length; i++) {
            if(src[i]==param.byteValue()) {
                endPos = i;
                break;
            }
        }

        if(endPos != -1) {
            Integer len = endPos - curIndex.pos;
            return lengthInterpreter.interpreter(src,curIndex,len);
        }
        return src;
    }


}
