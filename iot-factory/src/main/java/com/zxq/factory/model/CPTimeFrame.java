package com.zxq.factory.model;

import com.zxq.factory.annotate.DecoderField;
import com.zxq.factory.annotate.EncoderField;
import com.zxq.factory.annotate.Frame;
import lombok.Data;

@Frame
@Data
public class CPTimeFrame {

    /**
     * 98 B7 0E 11 10 03 14（开始时间：2020-03-16 17:14:47） 14 03 10 11 0E B7 98
     * 98B70E11100314（结束时间：2020-03-16 17:14:47）
     */
    @EncoderField(sequence = 0,len = 1)
    @DecoderField(sequence = 0,param = 1)
    Integer year;

    @EncoderField(sequence = 1,len = 1)
    @DecoderField(sequence = 1,param = 1)
    Integer month;

    @EncoderField(sequence = 2,len = 1)
    @DecoderField(sequence = 2,param = 1)
    Integer day;

    @EncoderField(sequence = 3,len = 1)
    @DecoderField(sequence = 3,param = 1)
    Integer hour;

    @EncoderField(sequence = 4,len = 1)
    @DecoderField(sequence = 4,param = 1)
    Integer minute;

    @EncoderField(sequence = 5)
    @DecoderField(sequence = 5)
    Integer seconds;



}
