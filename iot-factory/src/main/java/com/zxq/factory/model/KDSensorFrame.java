package com.zxq.factory.model;

import com.zxq.factory.annotate.DecoderField;
import com.zxq.factory.annotate.Frame;
import com.zxq.factory.enums.EField;
import lombok.Data;


@Frame
@Data
public class KDSensorFrame {



    /**
     * 24
     * 66
     * 65 E （E为最高位， >= 8 最高位 01 变换后为 0165， < 8 最高位 00 变换后为 0065）
     * 2 4D （2 补为为02 变换后为 02 4D）
     * 37
     * 0D
     * 03
     * 00 16
     * 00 00
     * 00 5F 42
     * 31
     * 4D
     * 01 8F 6A
     * FA
     *
     */

    @DecoderField(sequence = 1,field = EField.Int,param = 1)
    Integer tx;

    @DecoderField(sequence = 2,field = EField.Int,param = 1)
    Integer safeCode;

    @DecoderField(sequence = 3,field = EField.Int)
    Integer windDirection;

    @DecoderField(sequence = 4,field = EField.Int)
    Integer temperature;

    @DecoderField(sequence = 5,field = EField.Int,param = 1)
    Integer humidity;

    @DecoderField(sequence = 6,field = EField.Int,param = 1)
    Integer windSpeed;

    @DecoderField(sequence = 7,field = EField.Int,param = 1)
    Integer gustSpeed;

    @DecoderField(sequence = 8,field = EField.Int)
    Integer accumulated_rainfall;

    @DecoderField(sequence = 9,field = EField.Int)
    Integer uv;

    @DecoderField(sequence = 10,field = EField.Int)
    Integer light;

    @DecoderField(sequence = 11,field = EField.Int,param = 1)
    Integer crc;

    @DecoderField(sequence = 12,field = EField.Int,param = 1)
    Integer checksum;

    @DecoderField(sequence = 13,field = EField.Int,param = 3)
    Integer pressure;

    @DecoderField(sequence = 14,field = EField.Int,param = 1)
    Integer checksumValue;








}
