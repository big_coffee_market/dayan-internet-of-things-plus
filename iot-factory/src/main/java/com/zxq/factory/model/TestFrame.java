package com.zxq.factory.model;

import com.zxq.factory.FastFrame;
import com.zxq.factory.annotate.DecoderField;
import com.zxq.factory.annotate.Frame;
import com.zxq.factory.enums.EField;
import lombok.Data;
import org.apache.xmlbeans.impl.util.HexBin;

@Frame
@Data
public class TestFrame {

/**
 *  地址码   功能码  有效字节数   温度值    湿度值   光照值  校验码低位  校验码高位
 *   01      03       06      011B     0156    0072    25         6F
 */
    @DecoderField(sequence = 1,field = EField.Int,param = 1)
    Integer adr;
    @DecoderField(sequence = 2,field = EField.Int,param = 1)
    Integer funCode;
    @DecoderField(sequence = 3,field = EField.Int,param = 1)
    Integer len;
    @DecoderField(sequence = 4,field = EField.Int,param = 2)
    Integer temperature;
    @DecoderField(sequence = 5,field = EField.Int,param = 2)
    Integer humidity;
    @DecoderField(sequence = 6,field = EField.Int,param = 2)
    Integer illumination;
    @DecoderField(sequence = 7,field = EField.Int,param = 1)
    Integer crcLow;
    @DecoderField(sequence = 8,field = EField.Int,param = 1)
    Integer crcHigh;

    public static void main(String[] args) {
        byte[] msg =  HexBin.stringToBytes("010306011B01560072256F");
        TestFrame testFrame = new TestFrame();
        FastFrame.decoder(testFrame,msg);
        System.out.println(testFrame.toString());

    }




}

