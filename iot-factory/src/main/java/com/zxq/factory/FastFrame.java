package com.zxq.factory;


import lombok.SneakyThrows;


public class FastFrame {


    /**
     * 解析成对应的数据modeol
     * @param object 实体对象
     * @param msg 二进制数据
     * @return
     */
    @lombok.SneakyThrows
    public final static void decoder(Object object,byte[] msg) {
      DecoderFactory.decodeObj(object,msg);
    }


    @SneakyThrows
    public final static byte[] encoder(Object model) {
        byte[] buff =  EncoderFactory.encoder(model);
        return buff;
    }



}
