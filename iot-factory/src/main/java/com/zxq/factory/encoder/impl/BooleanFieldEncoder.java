package com.zxq.factory.encoder.impl;

import com.zxq.factory.encoder.IFieldEncoder;
import com.zxq.memory.ByteArrayPools;

public class BooleanFieldEncoder implements IFieldEncoder<Boolean> {

    /**
     * 编码格式
     * @param value 数据
     * @param len 长度
     * @return
     */
    @Override
    public byte[] encoder(Boolean value, int len) {

        byte[] buff = ByteArrayPools.getBytes(len + "");//new byte[len];

        if(value) {
            buff[len - 1] = 1;
        }else {
            buff[len - 1] = 0;
        }

        return buff;
    }

}
