/**
 * FileName: IFieldEncoder
 * Author:   zhaoxiaoqiang
 * Date:     2019/8/20 20:29
 * Description: 字段编码
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zxq.factory.encoder;

/**
 * 〈字段编码〉
 *
 * @author zhaoxiaoqiang
 * @create 2019/8/20
 * @since 1.0.0
 */
public interface IFieldEncoder<T> {

    byte[] encoder(T value,int len);

}
