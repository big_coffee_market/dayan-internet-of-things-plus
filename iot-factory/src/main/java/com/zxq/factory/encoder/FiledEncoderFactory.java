package com.zxq.factory.encoder;

import com.zxq.factory.encoder.impl.*;
import com.zxq.factory.enums.EField;


public class FiledEncoderFactory {

    static FiledEncoderFactory filedEncoderFactory = new FiledEncoderFactory();

    public  static FiledEncoderFactory instance() {
        return filedEncoderFactory;
    }


    /**
     * ascii编码
     */
    StringEncoder asciiEncoder = new StringEncoder();
    /**
     * boolean编码
     */
    BooleanFieldEncoder booleanFieldEncoder = new BooleanFieldEncoder();
    /**
     * int类型编码
     */
    IntFieldEncoder intEncoder = new IntFieldEncoder();
    /**
     * long类型编码
     */
    LongFieldEncoder longFieldEncoder = new LongFieldEncoder();

    /**
     * 二进制数组解析
     */
    BytesFieldEncoder bytesFieldEncoder = new BytesFieldEncoder();

    /**
     * hex二进制解析
     */
    HexFieldEncoder hexFieldEncoder = new HexFieldEncoder();

    /**
     * float编码类型
     */
    FloatFieldEncoder floatFieldEncoder = new FloatFieldEncoder();

    /**
     * 双精度解析过程
     */
    DoubleFieldEncoder doubleFieldEncoder = new DoubleFieldEncoder();

    /**
     * 时间编码
     */
    CPTimeFieldEncoder cpTimeFieldEncoder = new CPTimeFieldEncoder();

    /**
     * chinese code
     */
    GB2312Encoder gb2312Encoder = new GB2312Encoder();

    public IFieldEncoder create(EField field) {

        IFieldEncoder iFieldEncoder = null;
        switch (field) {
            case Bytes:
                iFieldEncoder = bytesFieldEncoder;
                break;
            case Int:
                iFieldEncoder = intEncoder;
                break;
            case Long:
                iFieldEncoder = longFieldEncoder;
                break;
            case String:
                iFieldEncoder = asciiEncoder;
                break;
            case Boolean:
                iFieldEncoder = booleanFieldEncoder;
                break;
            case Float:
                iFieldEncoder = floatFieldEncoder;
                break;
            case Hex:
                iFieldEncoder = hexFieldEncoder;
                break;
            case Double:
                iFieldEncoder = doubleFieldEncoder;
                break;
            case CP56Time2a:
                iFieldEncoder = cpTimeFieldEncoder;
                break;
            case GB2312:
                iFieldEncoder = gb2312Encoder;
                break;
        }

        return iFieldEncoder;
    }




}
