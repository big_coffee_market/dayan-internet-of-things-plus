package com.zxq.factory.encoder.impl;

import com.zxq.factory.encoder.IFieldEncoder;
import lombok.SneakyThrows;

public class GB2312Encoder implements IFieldEncoder<String> {


    @SneakyThrows
    @Override
    public byte[] encoder(String value, int len) {

        byte[] src = value.getBytes("gb2312");
        if(len == -1) {
            return src;
        }
        byte[] dst = new byte[len];
        if(src.length <= dst.length) {
            int startIndex = dst.length - src.length;
            System.arraycopy(src,0,dst,startIndex,src.length);
        }
        if(src.length > dst.length) {
            System.arraycopy(src,0,dst,0,dst.length);
        }

        return dst;
    }


}
