/**
 * FileName: AsciiInterpreter
 * Author:   Administrator
 * Date:     2019/6/9 13:44
 * Description: 解析成Ascii字符串类型
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zxq.factory.encoder.impl;

import com.zxq.factory.encoder.IFieldEncoder;


/**
 * 〈一句话功能简述〉<br>
 * 〈解析成AscII字符串类型〉
 *
 * @author Administrator
 * @date 2019/6/9
 * @since 1.0.0
 */
public class StringEncoder implements IFieldEncoder<String> {

    /**
     * 一个字符对应一个Byte
     * @param value 字符串
     * @param len 字符串长度
     * @return
     */
    @Override
    public byte[] encoder(String value, int len) {
        byte[] src = value.getBytes();
        if(len == -1) {
            return src;
        }
        byte[] dst = new byte[len];
        if(src.length <= dst.length) {
            int startIndex = dst.length - src.length;
            System.arraycopy(src,0,dst,startIndex,src.length);
        }
        if(src.length > dst.length) {
            System.arraycopy(src,0,dst,0,dst.length);
        }
        return dst;
    }






}
