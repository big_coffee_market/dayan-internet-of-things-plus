package com.zxq.factory.annotate;

import java.lang.annotation.*;

@Target({ElementType.TYPE,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Transcodings {

    Transcoding[] value();

}
