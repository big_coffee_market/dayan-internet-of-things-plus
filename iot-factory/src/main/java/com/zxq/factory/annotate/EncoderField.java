/**
 * FileName: EncoderField
 * Author:   zhaoxiaoqiang
 * Date:     2019/8/20 8:59
 * Description: 编码字段
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.zxq.factory.annotate;


import com.zxq.factory.enums.EField;

import java.lang.annotation.*;

/**
 * 〈编码字段〉
 *
 * @author zhaoxiaoqiang
 * @date 2019/8/20
 * @since 1.0.0
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface EncoderField {

    /**
     * 序列号
     * @return
     */
    int sequence() default 0;

    /**
     * 高位到低位
     * @return
     */
    EField field() default EField.Int;

    /**
     * 数据长度
     * @return
     */
    int len() default 2;
    
    /**
     * 这个字段很重要，添加了一下注释，默认空的
     * @return
     */
    String note() default "";


}
