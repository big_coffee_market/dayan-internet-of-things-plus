package com.zxq.factory.annotate;

import java.lang.annotation.*;

/**
 * 大小端
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface BigLittleEndianMark {

    /**
     * 这里是标明解析后的二进制数据，默认是1234，按顺序的
     * 如果他们非要二进制数组，4321，或者4123，在这里配置就可以了
     * @return
     */

    String serial() default "";

}
