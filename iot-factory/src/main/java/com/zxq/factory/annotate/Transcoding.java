package com.zxq.factory.annotate;

import java.lang.annotation.*;

@Repeatable(value = Transcodings.class)
@Target({ElementType.TYPE,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Transcoding {

    /**
     * 待转义的源码
     * @return
     */
    byte coding();


    /**
     * 转义后的数组
     * @return
     */
    byte[] transcoding();




}
