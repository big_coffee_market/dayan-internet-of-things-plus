package com.zxq.factory.annotate;

import java.lang.annotation.*;

/**
 * 说明这是一个可以用二进制编码或解码的帧,
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Frame {

    /**
     * id编号
     * @return
     */
    long id() default 0L;

    /**
     * 名称
     * @return
     */
    String remark() default "";


}
