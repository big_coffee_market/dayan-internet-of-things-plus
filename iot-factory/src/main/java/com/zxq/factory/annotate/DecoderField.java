package com.zxq.factory.annotate;

import com.zxq.factory.enums.EField;
import com.zxq.factory.enums.ETrimMethod;

import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface DecoderField {

    /**
     * 字段排序
     * @return
     */
    int sequence() default 0;

    /**
     * 字段类型
     * @return
     */
    EField field() default EField.Int;

    /**
     * 字段解析方法
     * @return
     */
    ETrimMethod method() default ETrimMethod.Length;

    /**
     * 字段解析的方法的参数 长度，某一个符号，倒数第几位
     * @return
     */
    int param() default 2;


    /**
     * 默认空的
     */
    String note() default "";

}
