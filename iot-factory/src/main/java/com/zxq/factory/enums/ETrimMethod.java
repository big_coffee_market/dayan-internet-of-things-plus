package com.zxq.factory.enums;

public enum ETrimMethod {

    /**
     * 未知
     */
    Unknow(-1),
    /**
     * 定长
     */
    Length(1),

    /**
     * 到倒数第几位
     */
    Tail(2),

    /**
     * 直到某一个固定的字节 分隔符 0x80
     */
    Untils(3);

    int value;
    ETrimMethod(int i) {
        this.value = i;
    }

    public static ETrimMethod getType(int value) {
        ETrimMethod eTrimMethod = ETrimMethod.Unknow;
        switch (value){
            case 1:
                eTrimMethod = ETrimMethod.Length;
                break;
            case 2:
                eTrimMethod = ETrimMethod.Tail;
                break;
            case 3:
                eTrimMethod = ETrimMethod.Untils;
                break;
        }
        return eTrimMethod;
    }



}
