package com.zxq.factory.enums;


public enum EField {

    /**
     * 未知
     */
    Unknow(-1),
    /**
     * 一个标注，类似于改字段是一个包裹的包
     */
    Frame(0),
    /**
     * INT类型.默认左边高位，右边低位
     */
    Int(1),
    /**
     * 原封不动，用于包装，类似于ip 到 tcp
     */
    Bytes(2),
    /**
     * long类型
     */
    Long(3),
    /**
     * String编码
     */
    String(4),
    /**
     * bool类型
     */
    Boolean(5),
    /**
     * float类型
     */
    Float(6),
    /**
     * 双精度数值
     */
    Double(7),
    /**
     * 一般用来设备编号
     */
    Hex(8),
    /**
     * time
     */
    CP56Time2a(9),
    /**
     * bianma
     */
    GB2312(10)
    ;
    int value;
    EField(int i) {
        this.value = i;
    }

    public int getValue() {
        return value;
    }

    /**
     * 获取类型
     * @param value
     * @return
     */
    public static EField getType(Integer value) {
        EField eField = EField.Unknow;
        switch (value) {
            case 0:
                eField = EField.Frame;
                break;
            case 1:
                eField = EField.Int;
                break;
            case 2:
                eField = EField.Bytes;
                break;
            case 3:
                eField = EField.Long;
                break;
            case 4:
                eField = EField.String;
                break;
            case 5:
                eField = EField.Boolean;
                break;
            case 6:
                eField = EField.Float;
                break;
            case 7:
                eField = EField.Double;
                break;
            case 8:
                eField = EField.Hex;
                break;
            case 10:
                eField = EField.CP56Time2a;
                break;
            case 11:
                eField = EField.GB2312;
                break;
        }
        return eField;
    }



    /**
     * 翻译数据，因为前端基本传来的统一做String处理
     * @param type 类型
     * @param value string值
     * @return
     */
    public static Object parseValue(EField type,String value) {

        Object objValue = null;
        switch (type) {
            case Int:
                objValue = Integer.parseInt(value);
                break;
            case Long:
                objValue = java.lang.Long.parseLong(value);
                break;
            case Boolean:
                objValue = value.equals("true");
                break;
            case Float:
                objValue = java.lang.Float.parseFloat(value);
                break;
            case Double:
                objValue = java.lang.Double.parseDouble(value);
                break;
            default:
                objValue = value;
                break;
        }
        return objValue;
    }



}
