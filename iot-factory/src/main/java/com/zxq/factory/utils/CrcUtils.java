package com.zxq.factory.utils;


public class CrcUtils {


    /**
     * 校验
     * @param
     * @return
     */
    public static byte[] makefcs(byte[] bytes) {
        int CRC = 0x0000ffff;
        int POLYNOMIAL = 0x0000a001;

        int i, j;
        for (i = 0; i < bytes.length; i++) {
            CRC ^= ((int) bytes[i] & 0x000000ff);
            for (j = 0; j < 8; j++) {
                if ((CRC & 0x00000001) != 0) {
                    CRC >>= 1;
                    CRC ^= POLYNOMIAL;
                } else {
                    CRC >>= 1;
                }
            }
        }
        return new byte[]{(byte) (CRC % 256), (byte) (CRC / 256)};
    }

    //00 03 00 00 00 08 45DD
//    public static void main(String[] args) {
//       byte[] hex = new byte[]{00, 03, 00, 00, (byte) 0x00, 8};
//       //01030000000045CA
//       byte[] msgBuff =  CrcUtils.makefcs(hex);
//       System.out.println("msgBuff:" + HexBin.encode(msgBuff));
//    }


//    public static void main(String[] args) {
//        //01030100000185F6
//        PlatformVO platformVO = new PlatformVO();
//        platformVO.setPduData("01030100000185F6");
//        platformVO.setProtocolId(1);
//        platformVO.setRegisterId("0102030405060708090A");
//        System.out.println("json:" + JSON.toJSONString(platformVO));
//        String msg =  HexBin.encode(makefcs(new byte[]{ 02,03,01,00,00,01}));
//        System.out.println("hexBuff:" + msg );
//    }



}
