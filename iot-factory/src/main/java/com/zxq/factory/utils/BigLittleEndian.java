package com.zxq.factory.utils;


import com.zxq.memory.ByteArrayPools;

public class BigLittleEndian {

    /**
     * 编码时候 数据排序
     * @param serialStr 大小端描述字符
     * @param msg 二进制数据
     * @return
     */
    public static byte[] encoder(String serialStr,byte[] msg) {

        if(msg.length != serialStr.length()) {
            System.out.println("SerialField: check your expression");
            return null;
        } else {
            byte[] tempBuff = ByteArrayPools.getBytes(msg.length + "");
            int i = 0;
            do{
                String strNum = serialStr.substring(i, i + 1);
                Integer index = Integer.parseInt(strNum) - 1;
                tempBuff[i] = msg[index];
                i++;
            }while (i != tempBuff.length);
            msg = tempBuff;
            return msg;
        }

    }

    /**
     * 解码时候 数据排序
     * @param serialStr 大小端描述字符
     * @param msg 二进制数据
     * @return
     */
    public static byte[] decoder(String serialStr,byte[] msg) {

        if(msg.length != serialStr.length()) {
            System.out.println("SerialField: check your expression");
            return null;
        } else {
            byte[] tempBuff = ByteArrayPools.getBytes(msg.length + "");
            int i = 0;
            do{
                String strNum = serialStr.substring(i,i + 1) + "";
                Integer index = Integer.parseInt(strNum) - 1;
                tempBuff[i] = msg[index];
                i++;
            } while (i != tempBuff.length);
            msg = tempBuff;
            return msg;
        }
    }










}
