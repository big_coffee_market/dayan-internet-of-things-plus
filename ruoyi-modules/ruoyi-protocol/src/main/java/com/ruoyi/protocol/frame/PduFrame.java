package com.ruoyi.protocol.frame;

import com.zxq.factory.annotate.DecoderField;
import com.zxq.factory.annotate.EncoderField;
import com.zxq.factory.annotate.Frame;
import com.zxq.factory.enums.EField;
import com.zxq.factory.enums.ETrimMethod;
import lombok.Data;

@Frame
@Data
public class PduFrame {

     /**
     * 地址
     */
    @EncoderField(sequence = 1,field = EField.Int,len = 1,note = "设备地址")
    @DecoderField(sequence = 1,field = EField.Int,param = 1,note = "设备地址")
    Integer address;

    /**
     * 对应iot_cmd 表的 cmdHex
     */
    @EncoderField(sequence = 2,field = EField.Int,len = 1,note = "功能码")
    @DecoderField(sequence = 2,field = EField.Int,param = 1,note = "功能码")
    Integer funCode;



    /**
     * 根据iot_cmd的 id，来配置解析的字段，对用户开放
     */
    @EncoderField(sequence = 3,field = EField.Bytes,note = "数据")
    @DecoderField(sequence = 3,field = EField.Bytes,method = ETrimMethod.Tail,note = "数据")
    byte[] data;

    /**
     * 校验位
     */
    @EncoderField(sequence = 4,field = EField.Int,note = "数据")
    @DecoderField(sequence = 4,field = EField.Int,note = "数据")
    Integer crc;

}
