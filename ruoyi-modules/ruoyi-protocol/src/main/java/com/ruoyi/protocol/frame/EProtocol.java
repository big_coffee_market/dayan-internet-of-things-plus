package com.ruoyi.protocol.frame;

public enum EProtocol {


    /**
     * 未知
     */
    unknow(-1),

    modbus(4);

    Integer value;
    EProtocol(Integer i) {
        this.value = i;
    }

    public Integer getValue() {
        return value;
    }

    public static EProtocol getType(Integer value){
        EProtocol eProtocol = EProtocol.unknow;
        switch (value) {
            case 4:
                eProtocol = EProtocol.modbus;
                break;
        }
        return eProtocol;
    }






}
