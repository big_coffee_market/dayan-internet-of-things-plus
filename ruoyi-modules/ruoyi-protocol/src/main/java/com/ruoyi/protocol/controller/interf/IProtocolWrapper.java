package com.ruoyi.protocol.controller.interf;

import com.ruoyi.protocol.domin.ProtocolDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface IProtocolWrapper {

    @PostMapping("/wrapper")
    ProtocolDTO wrapperProtocol(@RequestBody ProtocolDTO protocolDTO);

}
