package com.ruoyi.protocol.domin;

import lombok.Data;

@Data
public class ProtocolDTO {

    /**
     * 协议id
     */
    Integer protocolId;

    /**
     * 数据
     */
    String hexFrame;



}
