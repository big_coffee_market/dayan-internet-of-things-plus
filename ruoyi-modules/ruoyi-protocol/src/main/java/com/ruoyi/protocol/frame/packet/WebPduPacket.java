package com.ruoyi.protocol.frame.packet;

import com.zxq.factory.annotate.DecoderField;
import com.zxq.factory.annotate.EncoderField;
import com.zxq.factory.annotate.Frame;
import com.zxq.factory.enums.EField;
import com.zxq.factory.enums.ETrimMethod;
import lombok.Data;

@Frame
@Data
public class WebPduPacket {
    //03 05 08 09 04 02 03 06 03

    @EncoderField(sequence = 1,field = EField.Int,len = 1,note = "设备地址")
    @DecoderField(sequence = 1,field = EField.Int,param = 1,note = "设备地址")
    Integer address;

    /**
     * 对应iot_cmd 表的 cmdHex
     */
    @EncoderField(sequence = 2,field = EField.Int,len = 1,note = "功能码")
    @DecoderField(sequence = 2,field = EField.Int,param = 1,note = "功能码")
    Integer funCode;



    /**
     * 根据iot_cmd的 id，来配置解析的字段，对用户开放
     */
    @EncoderField(sequence = 3,field = EField.Bytes,note = "数据")
    @DecoderField(sequence = 3,param = 0,field = EField.Bytes,method = ETrimMethod.Tail,note = "数据")
    byte[] data;


}
