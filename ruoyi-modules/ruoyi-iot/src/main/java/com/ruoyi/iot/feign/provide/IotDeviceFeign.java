package com.ruoyi.iot.feign.provide;

import com.ruoyi.iot.domain.IotDevice;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


public interface IotDeviceFeign {


    /**
     * 根据设备码获取设备信息
     * @param devCode
     * @return
     */
    @RequestMapping(path = "/query/{devCode}",method = RequestMethod.GET)
    IotDevice selectIotDeviceByDevCode(@PathVariable("devCode") String devCode);

    /**
     * 设备在线通知
     * @param devCode
     */
    @RequestMapping(path = "/online/{devCode}",method = RequestMethod.GET)
    void reportIotDeviceOnline(@PathVariable("devCode") String devCode);

    /**
     * 设备离线通知
     * @param devCode
     */
    @RequestMapping(path = "/offline/{devCode}",method = RequestMethod.GET)
    void reportIotDeviceOffline(@PathVariable("devCode") String devCode);


}
