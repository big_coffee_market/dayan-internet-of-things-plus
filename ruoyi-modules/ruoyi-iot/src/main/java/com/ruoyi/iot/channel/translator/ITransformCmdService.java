package com.ruoyi.iot.channel.translator;


import com.alibaba.fastjson.JSONObject;

public interface ITransformCmdService {



    /**
     * 平台json对象转换成hex码
     * @param cmdId 指令id
     * @param json 根据iot_cmd_encoder生成的json数据
     * @return
     */
    byte[] platformTransform(Long cmdId, JSONObject json);


    /**
     * hex码转换为平台json
     * @param cmdId 指令id
     * @param frame 帧数据
     * @return
     */
    JSONObject deviceTransform(Long cmdId,byte[] frame);

}
