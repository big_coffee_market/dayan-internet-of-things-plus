package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.IotProductField;

/**
 * 产品-属性Service接口
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
public interface IIotProductFieldService 
{
    /**
     * 查询产品-属性
     * 
     * @param id 产品-属性主键
     * @return 产品-属性
     */
    public IotProductField selectIotProductFieldById(Long id);

    /**
     * 查询产品-属性列表
     * 
     * @param iotProductField 产品-属性
     * @return 产品-属性集合
     */
    public List<IotProductField> selectIotProductFieldList(IotProductField iotProductField);

    /**
     * 新增产品-属性
     * 
     * @param iotProductField 产品-属性
     * @return 结果
     */
    public int insertIotProductField(IotProductField iotProductField);

    /**
     * 修改产品-属性
     * 
     * @param iotProductField 产品-属性
     * @return 结果
     */
    public int updateIotProductField(IotProductField iotProductField);

    /**
     * 批量删除产品-属性
     * 
     * @param ids 需要删除的产品-属性主键集合
     * @return 结果
     */
    public int deleteIotProductFieldByIds(Long[] ids);

    /**
     * 删除产品-属性信息
     * 
     * @param id 产品-属性主键
     * @return 结果
     */
    public int deleteIotProductFieldById(Long id);
}
