package com.ruoyi.iot.exchange.network.tcp.handler.dispatch;


import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

public interface ITcpDispatchHandler {

    /**
     * 解析当前帧
     * @return
     */
    void decodeFrame();

    /**
     * 是这个Frame
     * @return
     */
    boolean isThisFrame();

    /**
     * 注入通道信息
     * @param channel 通道
     * @param in 数据信息
     */
    void injectByteBuf(ChannelHandlerContext channel, ByteBuf in);
}
