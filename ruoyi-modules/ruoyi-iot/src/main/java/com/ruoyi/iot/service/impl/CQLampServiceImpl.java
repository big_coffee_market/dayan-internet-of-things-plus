package com.ruoyi.iot.service.impl;

import com.ruoyi.iot.domain.IotDevice;
import com.ruoyi.iot.domain.IotProject;
import com.ruoyi.iot.domain.vo.TreeVO;
import com.ruoyi.iot.service.ICQLampService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CQLampServiceImpl implements ICQLampService {

    Long productId = 11L;

    /**
     * 设备服务
     */
    @Autowired
    IotDeviceServiceImpl iotDeviceService;

    /**
     * 产品服务协议
     */
    @Autowired
    IotProductServiceImpl productService;

    /**
     * 项目服务
     */
    @Autowired
    IotProjectServiceImpl iotProjectService;

    @Override
    public TreeVO queryDevTree() {
        List<IotProject> projects = iotProjectService.selectIotProjectList(new IotProject());
        TreeVO treeVO = new TreeVO();
        List<TreeVO.Node> childs = new ArrayList<>();
        treeVO.setData(childs);
        IotDevice query  = new IotDevice();
        query.setProductId(productId);
        for(IotProject item: projects) {
            TreeVO.Node child = new TreeVO.Node();
            childs.add(child);
            Long projectId = item.getId();
            child.setLabel(item.getName());
            child.setRemark(item.getRemark());
            child.setId(projectId + "");
            child.setLevel(1);
            query.setProjectId(projectId);

            List<TreeVO.Node> grandsons = new ArrayList<>();
            child.setChildren(grandsons);
            List<IotDevice> devices = iotDeviceService.selectIotDeviceList(query);
            if(devices.size() == 0) {
                childs.remove(child);
                continue;
            }
            for(IotDevice device: devices) {
                TreeVO.Node grandson = new TreeVO.Node();
                grandson.setLabel(device.getName());
                grandson.setParentId(projectId + "");
                grandson.setLabel(device.getName());
                grandson.setRemark(item.getRemark());
                grandson.setLevel(2);
                grandson.setId(device.getDevId() + "");
                grandsons.add(grandson);
            }

        }

        return treeVO;
    }

    @Override
    public String queryPreImage() {
        String preImage =  productService.selectIotProductById(productId).getImage();
        return preImage;
    }


}
