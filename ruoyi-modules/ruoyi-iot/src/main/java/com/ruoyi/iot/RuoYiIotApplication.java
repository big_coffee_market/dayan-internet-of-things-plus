package com.ruoyi.iot;
import com.ruoyi.common.security.annotation.EnableCustomConfig;
import com.ruoyi.common.security.annotation.EnableRyFeignClients;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@EnableCustomConfig
@EnableRyFeignClients
@SpringBootApplication
@Slf4j
public class RuoYiIotApplication {


    public static void main(String[] args) {
        SpringApplication.run(RuoYiIotApplication.class, args);
    }


}
