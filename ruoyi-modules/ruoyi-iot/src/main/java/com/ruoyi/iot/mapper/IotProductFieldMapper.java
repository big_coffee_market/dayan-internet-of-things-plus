package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.IotProductField;

/**
 * 产品-属性Mapper接口
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
public interface IotProductFieldMapper 
{
    /**
     * 查询产品-属性
     * 
     * @param id 产品-属性主键
     * @return 产品-属性
     */
    public IotProductField selectIotProductFieldById(Long id);

    /**
     * 查询产品-属性列表
     * 
     * @param iotProductField 产品-属性
     * @return 产品-属性集合
     */
    public List<IotProductField> selectIotProductFieldList(IotProductField iotProductField);

    /**
     * 新增产品-属性
     * 
     * @param iotProductField 产品-属性
     * @return 结果
     */
    public int insertIotProductField(IotProductField iotProductField);

    /**
     * 修改产品-属性
     * 
     * @param iotProductField 产品-属性
     * @return 结果
     */
    public int updateIotProductField(IotProductField iotProductField);

    /**
     * 删除产品-属性
     * 
     * @param id 产品-属性主键
     * @return 结果
     */
    public int deleteIotProductFieldById(Long id);

    /**
     * 批量删除产品-属性
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteIotProductFieldByIds(Long[] ids);
}
