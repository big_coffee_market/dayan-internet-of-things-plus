package com.ruoyi.iot.feign;


import com.ruoyi.iot.domain.dto.ProtocolDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "ruoyi-protocol")
public interface IProtocolRemoteFeign {

    @PostMapping("/wrapper")
    ProtocolDTO wrapperProtocol(@RequestBody ProtocolDTO protocolDTO);

}
