package com.ruoyi.iot.redis;

import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.iot.redis.action.IRedisAction;
import com.ruoyi.iot.redis.enity.DataMO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




@Service
public class DataRedis implements IRedisAction<DataMO> {

    /**
     * redis服务
     */
    @Autowired
    RedisService redisService;

    /**
     * 上报数据接口
     */
    final String DATA_KEY = "project:%s:dev:%s";

    /**
     * 数据上报通知
     */
    final String NOTIFY_DATA_KEY = "notify:data:project:%s";


    @Override
    public void publish(DataMO obj) {
        String channel = String.format(NOTIFY_DATA_KEY,obj.getProjectId());

        redisService.redisTemplate.convertAndSend(channel, obj);
    }


    @Override
    public void push(DataMO obj) {
        String redisKey = String.format(DATA_KEY,obj.getProjectId(),obj.getDevId());

        redisService.redisTemplate.boundListOps(redisKey).rightPush(obj);
    }


}
