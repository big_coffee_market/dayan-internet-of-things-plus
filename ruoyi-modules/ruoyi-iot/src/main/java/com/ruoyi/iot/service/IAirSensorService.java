package com.ruoyi.iot.service;


import com.ruoyi.iot.domain.mongo.DeviceSensorMO;
import com.ruoyi.iot.domain.vo.AirSensorVO;
import com.ruoyi.iot.domain.vo.TreeVO;

import java.util.List;

public interface IAirSensorService {

    /**
     * 查询项目下的，设备列表
     * @return
     */
    TreeVO queryDevTree();


    /**
     * 获取空气传感器的VO数据
     * @return
     */
    AirSensorVO queryAirSensorVO();


    /**
     * 查询实时数据
     * @param devId 查询实时的传感器数据
     * @return
     */
    DeviceSensorMO queryActualSensorVO(String devId);


    /**
     * 查询历史数据
     * @param devId 设备id
     * @return
     */
    List<DeviceSensorMO> queryHistorySensorVO(String devId,Long page, Long pageNum);

    /**
     * 查询历史传感器数量
     * @return
     */
    Long queryHistorySensorNum(String devId);


    /**
     * 距离最新的数据信息
     * @param devId 设备id
     * @param interval 间隔多久，默认是小时
     * @param numbs 取多少条数据
     * @return
     */
    List<DeviceSensorMO> queryCurrentSensorVO(String devId, Integer interval, Integer numbs);


}
