package com.ruoyi.iot.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.iot.domain.IotFrame;
import com.ruoyi.iot.service.IIotFrameService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 厂家协议Controller
 * 
 * @author ruoyi
 * @date 2022-12-02
 */
@RestController
@RequestMapping("/frame")
public class IotFrameController extends BaseController
{
    @Autowired
    private IIotFrameService iotFrameService;

    /**
     * 查询厂家协议列表
     */
    @RequiresPermissions("iot:frame:list")
    @GetMapping("/list")
    public TableDataInfo list(IotFrame iotFrame)
    {
        startPage();
        List<IotFrame> list = iotFrameService.selectIotFrameList(iotFrame);
        return getDataTable(list);
    }

    /**
     * 导出厂家协议列表
     */
    @RequiresPermissions("iot:frame:export")
    @Log(title = "厂家协议", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, IotFrame iotFrame)
    {
        List<IotFrame> list = iotFrameService.selectIotFrameList(iotFrame);
        ExcelUtil<IotFrame> util = new ExcelUtil<IotFrame>(IotFrame.class);
        util.exportExcel(response, list, "厂家协议数据");
    }

    /**
     * 获取厂家协议详细信息
     */
    @RequiresPermissions("iot:frame:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(iotFrameService.selectIotFrameById(id));
    }

    /**
     * 新增厂家协议
     */
    @RequiresPermissions("iot:frame:add")
    @Log(title = "厂家协议", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody IotFrame iotFrame)
    {
        return toAjax(iotFrameService.insertIotFrame(iotFrame));
    }

    /**
     * 修改厂家协议
     */
    @RequiresPermissions("iot:frame:edit")
    @Log(title = "厂家协议", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody IotFrame iotFrame)
    {
        return toAjax(iotFrameService.updateIotFrame(iotFrame));
    }

    /**
     * 删除厂家协议
     */
    @RequiresPermissions("iot:frame:remove")
    @Log(title = "厂家协议", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(iotFrameService.deleteIotFrameByIds(ids));
    }
}
