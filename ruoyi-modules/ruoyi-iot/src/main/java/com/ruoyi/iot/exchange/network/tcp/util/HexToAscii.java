package com.ruoyi.iot.exchange.network.tcp.util;

import java.util.HashMap;

public class HexToAscii {
    /**
     * hash对照表
     */
    static HashMap<String,String> hexLink = new HashMap<>();
    static HashMap<String,String> asciiLink = new HashMap<>();
    static {
        hexLink.put("0","30");
        hexLink.put("1","31");
        hexLink.put("2","32");
        hexLink.put("3","33");
        hexLink.put("4","34");
        hexLink.put("5","35");
        hexLink.put("6","36");
        hexLink.put("7","37");
        hexLink.put("8","38");
        hexLink.put("9","39");
        hexLink.put("A","41");
        hexLink.put("B","42");
        hexLink.put("C","43");
        hexLink.put("D","44");
        hexLink.put("E","45");
        hexLink.put("F","46");
        //38 36 39 35 31 36 30 35 38 39 34 32 33 36 33
        //08 06  09 05  01 06  03 05 08 09 04 02 03 06 03
        asciiLink.put("30","00");
        asciiLink.put("31","01");
        asciiLink.put("32","02");
        asciiLink.put("33","03");
        asciiLink.put("34","04");
        asciiLink.put("35","05");
        asciiLink.put("36","06");
        asciiLink.put("37","07");
        asciiLink.put("38","08");
        asciiLink.put("39","09");
        asciiLink.put("41","0A");
        asciiLink.put("42","0B");
        asciiLink.put("43","0C");
        asciiLink.put("44","0D");
        asciiLink.put("45","0E");
        asciiLink.put("46","0F");

    }

    private static String _1asciiToHex(String ascii) {

        return asciiLink.get(ascii);
    }

    private static String _1hexToAscii(String hex) {

        return hexLink.get(hex);
    }



    public static String asciiToHex(String ascii) {
        StringBuilder asciiText = new StringBuilder();

        for(int i = 0; i < ascii.length()/ 2; i++) {
            String ascIICode =  ascii.substring(2* i,2* i + 2);
            String hexCode = HexToAscii._1asciiToHex(ascIICode);
            asciiText.append(hexCode);
        }
        String hexFrameText  = asciiText.toString();

        return hexFrameText;
    }

    public static String hexToAscii(String hex) {
        StringBuilder ascIIText = new StringBuilder();
        for(int i = 0; i < hex.length() ; i++) {
            String hexCode =  hex.substring( i, i + 1);
            String ascIICode = HexToAscii._1hexToAscii(hexCode);
            ascIIText.append(ascIICode);
        }
        String hexFrameText  = ascIIText.toString();
        return hexFrameText;
    }










}
