package com.ruoyi.iot.service;


import com.alibaba.fastjson.JSONObject;
import com.ruoyi.iot.domain.mongo.DeviceSensorMO;

import java.util.List;

public interface IMongoService {

    /**
     * 插入mongo数据
     * @return
     */
    Boolean insertMongo(Long filterId, String devId, JSONObject environment_data);


    /**
     * 查询实时数据
     * @return
     */
    DeviceSensorMO queryActualInfo(String devId,Long filterId);

    /**
     * 查询历史数据
     * @param devId
     * @return
     */
    List<DeviceSensorMO> queryHistoryInfo(String devId,Long filterId, Long page, Long pageNum);


    /**
     * 查询历史记录数据总条数
     * @return
     */
    Long queryHistoryTotal(String devId,Long filterId);

    /**
     * 距离最新的数据信息
     * @param devId 设备id
     * @param interval 间隔多久，默认是小时
     * @param numbs 取多少条数据
     * @return
     */
    List<DeviceSensorMO> queryCurrentSensorVO(String devId,Long filterId, Integer interval, Integer numbs);
}
