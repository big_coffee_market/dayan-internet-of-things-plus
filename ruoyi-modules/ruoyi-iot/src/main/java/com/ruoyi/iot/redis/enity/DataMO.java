package com.ruoyi.iot.redis.enity;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;

@Data
public class DataMO {

    /**
     * 设备id
     */
    String devId;

    /**
     * 项目id
     */
    Long projectId;

    /**
     * 指令id
     */
    Long cmdId;

    /**
     * 指令对应的jsonId
     */
    JSONObject data;

    /**
     * 触发时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    Date time;


}
