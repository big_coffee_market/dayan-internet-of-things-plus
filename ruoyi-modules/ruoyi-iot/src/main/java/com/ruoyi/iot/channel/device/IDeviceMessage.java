package com.ruoyi.iot.channel.device;


import com.ruoyi.iot.domain.dto.ExchangeDTO;

public interface IDeviceMessage {



    /**
     * 设备 -> 大衍物联网 -> 平台
     * 上报指令
     * @param exchangeDTO 设备上传的信息
     */
    void deviceReport(ExchangeDTO exchangeDTO);


    /**
     * 回复指令
     * @param exchangeDTO 设备上传的信息
     */
    void deviceResult(ExchangeDTO exchangeDTO);





}
