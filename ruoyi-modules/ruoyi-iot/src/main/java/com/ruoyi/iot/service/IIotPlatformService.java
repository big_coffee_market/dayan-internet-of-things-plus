package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.IotPlatform;

/**
 * 平台信息Service接口
 * 
 * @author ruoyi
 * @date 2022-12-08
 */
public interface IIotPlatformService 
{
    /**
     * 查询平台信息
     * 
     * @param id 平台信息主键
     * @return 平台信息
     */
    public IotPlatform selectIotPlatformById(Long id);

    /**
     * 查询平台信息列表
     * 
     * @param iotPlatform 平台信息
     * @return 平台信息集合
     */
    public List<IotPlatform> selectIotPlatformList(IotPlatform iotPlatform);

    /**
     * 新增平台信息
     * 
     * @param iotPlatform 平台信息
     * @return 结果
     */
    public int insertIotPlatform(IotPlatform iotPlatform);

    /**
     * 修改平台信息
     * 
     * @param iotPlatform 平台信息
     * @return 结果
     */
    public int updateIotPlatform(IotPlatform iotPlatform);

    /**
     * 批量删除平台信息
     * 
     * @param ids 需要删除的平台信息主键集合
     * @return 结果
     */
    public int deleteIotPlatformByIds(Long[] ids);

    /**
     * 删除平台信息信息
     * 
     * @param id 平台信息主键
     * @return 结果
     */
    public int deleteIotPlatformById(Long id);
}
