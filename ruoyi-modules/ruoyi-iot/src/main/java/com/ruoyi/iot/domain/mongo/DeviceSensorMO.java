package com.ruoyi.iot.domain.mongo;


import com.alibaba.fastjson.JSONObject;
import lombok.Data;


/**
 * 设备传感器的模型数据
 */
@Data
public class DeviceSensorMO {


    /**
     * 数据
     */
    JSONObject data;

    /**
     * 时间
     */

    Long time;


}
