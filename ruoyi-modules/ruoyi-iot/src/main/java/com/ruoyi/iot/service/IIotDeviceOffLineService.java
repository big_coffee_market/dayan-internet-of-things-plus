package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.IotDeviceOffLine;

/**
 * 设备离在线记录Service接口
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
public interface IIotDeviceOffLineService 
{
    /**
     * 查询设备离在线记录
     * 
     * @param id 设备离在线记录主键
     * @return 设备离在线记录
     */
    public IotDeviceOffLine selectIotDeviceOffLineById(Long id);

    /**
     * 查询设备离在线记录列表
     * 
     * @param iotDeviceOffLine 设备离在线记录
     * @return 设备离在线记录集合
     */
    public List<IotDeviceOffLine> selectIotDeviceOffLineList(IotDeviceOffLine iotDeviceOffLine);

    /**
     * 新增设备离在线记录
     * 
     * @param iotDeviceOffLine 设备离在线记录
     * @return 结果
     */
    public int insertIotDeviceOffLine(IotDeviceOffLine iotDeviceOffLine);

    /**
     * 修改设备离在线记录
     * 
     * @param iotDeviceOffLine 设备离在线记录
     * @return 结果
     */
    public int updateIotDeviceOffLine(IotDeviceOffLine iotDeviceOffLine);

    /**
     * 批量删除设备离在线记录
     * 
     * @param ids 需要删除的设备离在线记录主键集合
     * @return 结果
     */
    public int deleteIotDeviceOffLineByIds(Long[] ids);

    /**
     * 删除设备离在线记录信息
     * 
     * @param id 设备离在线记录主键
     * @return 结果
     */
    public int deleteIotDeviceOffLineById(Long id);
}
