package com.ruoyi.iot.domain.vo.enums;

public enum  EPlatformCmdType {
    UNKNOW(-1),
    CMD(1),
    NOTICE(2),
    RESULT(3),
    NOTICE_OR_RESULT(4);

    Integer value;
    EPlatformCmdType(int i) {
        this.value = i;
    }

    public Integer getValue() {
        return value;
    }

    public static  EPlatformCmdType getType(Integer v){
        EPlatformCmdType platformCmdType = EPlatformCmdType.UNKNOW;
        switch (v) {
            case 1:{
                platformCmdType = CMD;
            }
            break;
            case 2: {
                platformCmdType = NOTICE;
            }
            break;
            case 3: {
                platformCmdType = RESULT;
            }
            break;
            case 4: {
                platformCmdType = NOTICE_OR_RESULT;
            }
            break;
        }
        return platformCmdType;
    }



}
