package com.ruoyi.iot.feign;
import com.ruoyi.iot.domain.vo.ProjectVO;
import com.ruoyi.iot.domain.vo.exchange.ProjectFrameDTO;
import org.springframework.web.bind.annotation.*;

/**
 * 需要启动大衍物联网服务
 */

public interface IExchangeService {


    /**
     * 查询设备编码是否存在
     * @param iotWrapperDTO 设备编码
     */
    Boolean devOnline(@RequestBody String devNum);

    /**
     * 启动产品协议服务
     * @param projectFrameDTO 产品协议服务
     */
    void startProjectServer(@RequestBody ProjectFrameDTO projectFrameDTO);


    /**
     *
     * @param projectVO
     */
    void stopProjectServer(@RequestBody ProjectVO projectVO);


}
