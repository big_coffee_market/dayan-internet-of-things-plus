package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.IotFrameMapper;
import com.ruoyi.iot.domain.IotFrame;
import com.ruoyi.iot.service.IIotFrameService;

/**
 * 厂家协议Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-12-02
 */
@Service
public class IotFrameServiceImpl implements IIotFrameService 
{
    @Autowired
    private IotFrameMapper iotFrameMapper;

    /**
     * 查询厂家协议
     * 
     * @param id 厂家协议主键
     * @return 厂家协议
     */
    @Override
    public IotFrame selectIotFrameById(Long id)
    {
        return iotFrameMapper.selectIotFrameById(id);
    }

    /**
     * 查询厂家协议列表
     * 
     * @param iotFrame 厂家协议
     * @return 厂家协议
     */
    @Override
    public List<IotFrame> selectIotFrameList(IotFrame iotFrame)
    {
        return iotFrameMapper.selectIotFrameList(iotFrame);
    }

    /**
     * 新增厂家协议
     * 
     * @param iotFrame 厂家协议
     * @return 结果
     */
    @Override
    public int insertIotFrame(IotFrame iotFrame)
    {
        return iotFrameMapper.insertIotFrame(iotFrame);
    }

    /**
     * 修改厂家协议
     * 
     * @param iotFrame 厂家协议
     * @return 结果
     */
    @Override
    public int updateIotFrame(IotFrame iotFrame)
    {
        return iotFrameMapper.updateIotFrame(iotFrame);
    }

    /**
     * 批量删除厂家协议
     * 
     * @param ids 需要删除的厂家协议主键
     * @return 结果
     */
    @Override
    public int deleteIotFrameByIds(Long[] ids)
    {
        return iotFrameMapper.deleteIotFrameByIds(ids);
    }

    /**
     * 删除厂家协议信息
     * 
     * @param id 厂家协议主键
     * @return 结果
     */
    @Override
    public int deleteIotFrameById(Long id)
    {
        return iotFrameMapper.deleteIotFrameById(id);
    }
}
