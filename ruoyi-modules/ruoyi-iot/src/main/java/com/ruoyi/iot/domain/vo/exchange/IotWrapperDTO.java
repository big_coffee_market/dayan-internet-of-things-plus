package com.ruoyi.iot.domain.vo.exchange;

import lombok.Data;

import java.io.Serializable;

@Data
public class IotWrapperDTO<T> implements Serializable {

    T data;

}
