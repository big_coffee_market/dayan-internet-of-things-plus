package com.ruoyi.iot.domain.vo.drop;

import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.iot.domain.IotProduct;
import com.ruoyi.iot.domain.IotProject;
import lombok.Data;
import java.util.List;

@Data
public class ProductProjectVoInfo extends TableDataInfo {

    List<IotProject> projectList;

    List<IotProduct> productList;

}
