package com.ruoyi.iot.domain.vo.weixin;

import com.ruoyi.iot.domain.vo.weixin.element.WxCmdFieldDetail;
import lombok.Data;

import java.util.List;

/**
 * 也用分段锁
 */
@Data
public class WxCmdVO {



    /**
     * cmd 字段 信息
     */
    List<WxCmdFieldDetail> wxCmdFieldDetails;

}
