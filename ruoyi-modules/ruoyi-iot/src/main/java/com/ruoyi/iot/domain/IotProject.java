package com.ruoyi.iot.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 项目对象 iot_project
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
public class IotProject extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @Excel(name = "主键")
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 地点 */
    @Excel(name = "地点")
    private String place;

    /** 经度 */
    @Excel(name = "经度")
    private Double longitude;

    /** 纬度 */
    @Excel(name = "纬度")
    private Double latitude;

    /** 负责人 */
    @Excel(name = "负责人")
    private String principal;

    /** 负责人电话 */
    @Excel(name = "负责人电话")
    private String principalTel;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startupTime;

    /** 质保年份 */
    @Excel(name = "质保年份")
    private Long warrantyYear;

    /** 状态（立项，施工，竣工，验收，结束） */
    @Excel(name = "状态", readConverterExp = "立项，施工，竣工，验收，结束")
    private Long state;

    /** 网络状态（0 就绪，1启动，2停止） */
    @Excel(name = "网络状态", readConverterExp = "0=,就=绪，1启动，2停止")
    private Long networkState;

    /** 网络端口 */
    @Excel(name = "网络端口")
    private Long networkPort;

    /** 网络类型（1 tcp, 2 udp) */
    @Excel(name = "网络类型", readConverterExp = "网络类型（1 tcp, 2 udp)")
    private Long networkProtocol;

    /** 创建人 */
    @Excel(name = "创建人")
    private Long creator;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date crateDate;

    /** 更新者 */
    @Excel(name = "更新者")
    private Long updater;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updateDate;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPlace(String place) 
    {
        this.place = place;
    }

    public String getPlace() 
    {
        return place;
    }
    public void setLongitude(Double longitude) 
    {
        this.longitude = longitude;
    }

    public Double getLongitude() 
    {
        return longitude;
    }
    public void setLatitude(Double latitude) 
    {
        this.latitude = latitude;
    }

    public Double getLatitude() 
    {
        return latitude;
    }
    public void setPrincipal(String principal) 
    {
        this.principal = principal;
    }

    public String getPrincipal() 
    {
        return principal;
    }
    public void setPrincipalTel(String principalTel) 
    {
        this.principalTel = principalTel;
    }

    public String getPrincipalTel() 
    {
        return principalTel;
    }
    public void setStartupTime(Date startupTime) 
    {
        this.startupTime = startupTime;
    }

    public Date getStartupTime() 
    {
        return startupTime;
    }
    public void setWarrantyYear(Long warrantyYear) 
    {
        this.warrantyYear = warrantyYear;
    }

    public Long getWarrantyYear() 
    {
        return warrantyYear;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }
    public void setNetworkState(Long networkState) 
    {
        this.networkState = networkState;
    }

    public Long getNetworkState() 
    {
        return networkState;
    }
    public void setNetworkPort(Long networkPort) 
    {
        this.networkPort = networkPort;
    }

    public Long getNetworkPort() 
    {
        return networkPort;
    }
    public void setNetworkProtocol(Long networkProtocol) 
    {
        this.networkProtocol = networkProtocol;
    }

    public Long getNetworkProtocol() 
    {
        return networkProtocol;
    }
    public void setCreator(Long creator) 
    {
        this.creator = creator;
    }

    public Long getCreator() 
    {
        return creator;
    }
    public void setCrateDate(Date crateDate) 
    {
        this.crateDate = crateDate;
    }

    public Date getCrateDate() 
    {
        return crateDate;
    }
    public void setUpdater(Long updater) 
    {
        this.updater = updater;
    }

    public Long getUpdater() 
    {
        return updater;
    }
    public void setUpdateDate(Date updateDate) 
    {
        this.updateDate = updateDate;
    }

    public Date getUpdateDate() 
    {
        return updateDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("place", getPlace())
            .append("longitude", getLongitude())
            .append("latitude", getLatitude())
            .append("principal", getPrincipal())
            .append("principalTel", getPrincipalTel())
            .append("startupTime", getStartupTime())
            .append("warrantyYear", getWarrantyYear())
            .append("state", getState())
            .append("networkState", getNetworkState())
            .append("networkPort", getNetworkPort())
            .append("networkProtocol", getNetworkProtocol())
            .append("remark", getRemark())
            .append("creator", getCreator())
            .append("crateDate", getCrateDate())
            .append("updater", getUpdater())
            .append("updateDate", getUpdateDate())
            .toString();
    }
}
