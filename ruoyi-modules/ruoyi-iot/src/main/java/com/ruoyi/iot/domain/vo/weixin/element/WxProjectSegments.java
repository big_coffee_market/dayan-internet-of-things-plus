package com.ruoyi.iot.domain.vo.weixin.element;
import lombok.Data;


@Data
public class WxProjectSegments {

    /**
     * 状态
     */
    Integer state;

    /**
     * 名称
     */
    String name;

}
