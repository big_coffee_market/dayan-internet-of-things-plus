package com.ruoyi.iot.service;

import com.ruoyi.iot.domain.vo.TreeVO;

public interface ICQLampService {


    /**
     * 获取设备的列表
     * @return
     */
    TreeVO queryDevTree();

    /**
     * 获取预览图片
     * @return
     */
    String queryPreImage();

}
