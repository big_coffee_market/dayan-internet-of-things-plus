package com.ruoyi.iot.domain.vo.enums;

public enum ESponsor {
    unknow(-1L),
    device(1L),
    platform(2L);

    Long value;
    ESponsor(Long i) {
        this.value = i;
    }

    /**
     * 获取到对应的发起方值
     * @param value 值
     * @return
     */
    public static ESponsor getType(int value) {
        ESponsor eSponsor = ESponsor.unknow;
        switch (value) {
            case 1:
                eSponsor = ESponsor.device;
                break;
            case 2:
                eSponsor = ESponsor.platform;
                break;
        }
        return eSponsor;
    }

    public Long getValue() {
        return value;
    }
}
