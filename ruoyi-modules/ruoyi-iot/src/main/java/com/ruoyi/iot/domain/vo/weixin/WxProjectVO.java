package com.ruoyi.iot.domain.vo.weixin;

import com.ruoyi.iot.domain.vo.weixin.element.WxProjectDetail;
import com.ruoyi.iot.domain.vo.weixin.element.WxProjectSegments;
import lombok.Data;

import java.util.List;

@Data
public class WxProjectVO {

    List<WxProjectSegments> segments;

    List<WxProjectDetail> data;
}
