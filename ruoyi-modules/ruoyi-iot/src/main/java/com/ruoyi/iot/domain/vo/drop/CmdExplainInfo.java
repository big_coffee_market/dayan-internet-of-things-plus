package com.ruoyi.iot.domain.vo.drop;

import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.iot.domain.IotCmd;
import lombok.Data;
import java.util.List;

@Data
public class CmdExplainInfo extends TableDataInfo {

    /**
     * 指令列表
     */
    List<IotCmd> cmdList;


}
