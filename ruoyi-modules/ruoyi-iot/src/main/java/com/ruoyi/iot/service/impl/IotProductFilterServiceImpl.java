package com.ruoyi.iot.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.IotProductFilterMapper;
import com.ruoyi.iot.domain.IotProductFilter;
import com.ruoyi.iot.service.IIotProductFilterService;

/**
 * 数据筛选Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-02
 */
@Service
public class IotProductFilterServiceImpl implements IIotProductFilterService 
{
    @Autowired
    private IotProductFilterMapper iotProductFilterMapper;

    /**
     * 查询数据筛选
     * 
     * @param id 数据筛选主键
     * @return 数据筛选
     */
    @Override
    public IotProductFilter selectIotProductFilterById(Long id)
    {
        return iotProductFilterMapper.selectIotProductFilterById(id);
    }

    /**
     * 查询数据筛选列表
     * 
     * @param iotProductFilter 数据筛选
     * @return 数据筛选
     */
    @Override
    public List<IotProductFilter> selectIotProductFilterList(IotProductFilter iotProductFilter)
    {
        return iotProductFilterMapper.selectIotProductFilterList(iotProductFilter);
    }

    /**
     * 新增数据筛选
     * 
     * @param iotProductFilter 数据筛选
     * @return 结果
     */
    @Override
    public int insertIotProductFilter(IotProductFilter iotProductFilter)
    {
        iotProductFilter.setCreateTime(DateUtils.getNowDate());
        return iotProductFilterMapper.insertIotProductFilter(iotProductFilter);
    }

    /**
     * 修改数据筛选
     * 
     * @param iotProductFilter 数据筛选
     * @return 结果
     */
    @Override
    public int updateIotProductFilter(IotProductFilter iotProductFilter)
    {
        return iotProductFilterMapper.updateIotProductFilter(iotProductFilter);
    }

    /**
     * 批量删除数据筛选
     * 
     * @param ids 需要删除的数据筛选主键
     * @return 结果
     */
    @Override
    public int deleteIotProductFilterByIds(Long[] ids)
    {
        return iotProductFilterMapper.deleteIotProductFilterByIds(ids);
    }

    /**
     * 删除数据筛选信息
     * 
     * @param id 数据筛选主键
     * @return 结果
     */
    @Override
    public int deleteIotProductFilterById(Long id)
    {
        return iotProductFilterMapper.deleteIotProductFilterById(id);
    }
}
