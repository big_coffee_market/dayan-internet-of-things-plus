package com.ruoyi.iot.service;


import com.ruoyi.iot.domain.IotCmdEncoder;

import java.util.List;

/**
 * 指令编码Service接口
 * 
 * @author ruoyi
 * @date 2022-09-14
 */
public interface IIotCmdEncoderService 
{
    /**
     * 查询指令编码
     * 
     * @param id 指令编码主键
     * @return 指令编码
     */
    public IotCmdEncoder selectIotCmdEncoderById(Long id);

    /**
     * 查询指令编码列表
     * 
     * @param iotCmdEncoder 指令编码
     * @return 指令编码集合
     */
    public List<IotCmdEncoder> selectIotCmdEncoderList(IotCmdEncoder iotCmdEncoder);

    /**
     * 新增指令编码
     * 
     * @param iotCmdEncoder 指令编码
     * @return 结果
     */
    public int insertIotCmdEncoder(IotCmdEncoder iotCmdEncoder);

    /**
     * 修改指令编码
     * 
     * @param iotCmdEncoder 指令编码
     * @return 结果
     */
    public int updateIotCmdEncoder(IotCmdEncoder iotCmdEncoder);

    /**
     * 批量删除指令编码
     * 
     * @param ids 需要删除的指令编码主键集合
     * @return 结果
     */
    public int deleteIotCmdEncoderByIds(Long[] ids);

    /**
     * 删除指令编码信息
     * 
     * @param id 指令编码主键
     * @return 结果
     */
    public int deleteIotCmdEncoderById(Long id);
}
