package com.ruoyi.iot.domain.vo.drop;

import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.iot.domain.IotProduct;
import com.ruoyi.iot.domain.vo.CmdProductFieldVO;
import com.ruoyi.iot.domain.vo.IotProtocolVO;
import lombok.Data;

import java.util.List;

@Data
public class CmdVoInfo extends TableDataInfo {

   //报文字段
   private List<IotProduct> productList;

   //报文map
   private List<CmdProductFieldVO> productFields;


   private List<IotProtocolVO> protocolVOList;


}
