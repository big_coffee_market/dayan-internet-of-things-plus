package com.ruoyi.iot.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.github.pagehelper.PageInfo;
import com.ruoyi.iot.domain.IotCmd;
import com.ruoyi.iot.domain.IotProductField;
import com.ruoyi.iot.domain.vo.drop.CmdEncoderVoInfo;
import com.ruoyi.iot.domain.vo.exchange.IotWrapperDTO;
import com.ruoyi.iot.domain.IotCmdEncoder;
import com.ruoyi.iot.domain.vo.plus.IotCmdEncoderPlus;
import com.ruoyi.iot.service.IIotCmdEncoderService;
import com.ruoyi.iot.service.impl.IotCmdServiceImpl;
import com.ruoyi.iot.service.impl.IotProductFieldServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 指令编码Controller
 *
 * @author ruoyi
 * @date 2022-09-14
 */
@RestController
@RequestMapping("/cmdEncoder")
public class IotCmdEncoderController extends BaseController
{
    @Autowired
    private IIotCmdEncoderService iotCmdEncoderService;

    @Autowired
    private IotCmdServiceImpl iotCmdServiceImpl;

    @Autowired
    private IotProductFieldServiceImpl productFieldServiceImpl;


    /**
     * 查询指令编码列表
     */
    @RequiresPermissions("iot:cmdEncoder:list")
    @GetMapping("/list")
    public CmdEncoderVoInfo list(IotCmdEncoder iotCmdEncoder)
    {
        startPage();

        CmdEncoderVoInfo cmdEncoderVoInfo = new CmdEncoderVoInfo();
        List<IotCmdEncoder> list = iotCmdEncoderService.selectIotCmdEncoderList(iotCmdEncoder);
        List<IotCmdEncoderPlus> plusList = new ArrayList<>();
        for(IotCmdEncoder item : list) {
            IotCmdEncoderPlus iotCmdEncoderPlus = new IotCmdEncoderPlus();
            BeanUtils.copyProperties(item,iotCmdEncoderPlus);
            iotCmdEncoderPlus.setCmdName(iotCmdServiceImpl.selectIotCmdById(item.getCmdId()).getName());
            iotCmdEncoderPlus.setFieldName(productFieldServiceImpl.selectIotProductFieldById(item.getFieldId()).getName());
            plusList.add(iotCmdEncoderPlus);
        }

        cmdEncoderVoInfo.setCode(200);
        cmdEncoderVoInfo.setCmdList(iotCmdServiceImpl.selectIotCmdList(new IotCmd()));
        cmdEncoderVoInfo.setFieldList(productFieldServiceImpl.selectIotProductFieldList(new IotProductField()));

        cmdEncoderVoInfo.setMsg("查询成功");
        cmdEncoderVoInfo.setRows(plusList);
        cmdEncoderVoInfo.setTotal((new PageInfo(list)).getTotal());

        return cmdEncoderVoInfo;
    }

    @PostMapping("/query/list")
    public IotWrapperDTO<List<IotCmdEncoder>> queryList(IotCmdEncoder iotCmdEncoder)
    {
        IotWrapperDTO iotListDTO = new IotWrapperDTO();
        List<IotCmdEncoder> list = iotCmdEncoderService.selectIotCmdEncoderList(iotCmdEncoder);
        iotListDTO.setData(list);
        return iotListDTO;
    }

    /**
     * 导出指令编码列表
     */
    @RequiresPermissions("iot:cmdEncoder:export")
    @Log(title = "指令编码", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, IotCmdEncoder iotCmdEncoder)
    {
        List<IotCmdEncoder> list = iotCmdEncoderService.selectIotCmdEncoderList(iotCmdEncoder);
        ExcelUtil<IotCmdEncoder> util = new ExcelUtil<IotCmdEncoder>(IotCmdEncoder.class);
        util.exportExcel(response, list, "指令编码数据");
    }

    /**
     * 获取指令编码详细信息
     */
    @RequiresPermissions("iot:cmdEncoder:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(iotCmdEncoderService.selectIotCmdEncoderById(id));
    }

    /**
     * 新增指令编码
     */
    @RequiresPermissions("iot:cmdEncoder:add")
    @Log(title = "指令编码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody IotCmdEncoder iotCmdEncoder)
    {
        return toAjax(iotCmdEncoderService.insertIotCmdEncoder(iotCmdEncoder));
    }

    /**
     * 修改指令编码
     */
    @RequiresPermissions("iot:cmdEncoder:edit")
    @Log(title = "指令编码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody IotCmdEncoder iotCmdEncoder)
    {
        return toAjax(iotCmdEncoderService.updateIotCmdEncoder(iotCmdEncoder));
    }

    /**
     * 删除指令编码
     */
    @RequiresPermissions("iot:cmdEncoder:remove")
    @Log(title = "指令编码", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(iotCmdEncoderService.deleteIotCmdEncoderByIds(ids));
    }
}
