package com.ruoyi.iot.feign.provide;
import com.ruoyi.common.core.web.domain.AjaxResult;
import org.springframework.web.bind.annotation.GetMapping;

public interface IWxProjectFeign {

    /**
     * 获取微信项目数据
     * @return
     */
    @GetMapping("/takeWxProject")
    AjaxResult takeWxProject();

}
