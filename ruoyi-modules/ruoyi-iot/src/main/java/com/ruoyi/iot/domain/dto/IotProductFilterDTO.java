package com.ruoyi.iot.domain.dto;
import com.ruoyi.iot.domain.vo.TreeVO;
import lombok.Data;

import java.util.List;

@Data
public class IotProductFilterDTO {

    /** 主键 */
    private Long id;

    /** 名称 */
    private String name;

    /** 选中节点 */
    private List<TreeVO.Node> checkNodes;

    /** 启用 */
    private Long enable;

    /** 备注 **/
    private String remark;

}
