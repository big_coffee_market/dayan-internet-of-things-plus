package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.IotProductFilter;

/**
 * 数据筛选Service接口
 * 
 * @author ruoyi
 * @date 2023-03-02
 */
public interface IIotProductFilterService 
{
    /**
     * 查询数据筛选
     * 
     * @param id 数据筛选主键
     * @return 数据筛选
     */
    public IotProductFilter selectIotProductFilterById(Long id);

    /**
     * 查询数据筛选列表
     * 
     * @param iotProductFilter 数据筛选
     * @return 数据筛选集合
     */
    public List<IotProductFilter> selectIotProductFilterList(IotProductFilter iotProductFilter);

    /**
     * 新增数据筛选
     * 
     * @param iotProductFilter 数据筛选
     * @return 结果
     */
    public int insertIotProductFilter(IotProductFilter iotProductFilter);

    /**
     * 修改数据筛选
     * 
     * @param iotProductFilter 数据筛选
     * @return 结果
     */
    public int updateIotProductFilter(IotProductFilter iotProductFilter);

    /**
     * 批量删除数据筛选
     * 
     * @param ids 需要删除的数据筛选主键集合
     * @return 结果
     */
    public int deleteIotProductFilterByIds(Long[] ids);

    /**
     * 删除数据筛选信息
     * 
     * @param id 数据筛选主键
     * @return 结果
     */
    public int deleteIotProductFilterById(Long id);
}
