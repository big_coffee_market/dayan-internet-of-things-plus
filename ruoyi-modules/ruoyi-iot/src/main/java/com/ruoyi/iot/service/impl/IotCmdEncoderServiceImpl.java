package com.ruoyi.iot.service.impl;

import java.util.List;

import com.ruoyi.iot.domain.IotCmdEncoder;
import com.ruoyi.iot.mapper.IotCmdEncoderMapper;
import com.ruoyi.iot.service.IIotCmdEncoderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;


/**
 * 指令编码Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-09-14
 */
@Service
@Primary
public class IotCmdEncoderServiceImpl implements IIotCmdEncoderService
{
    @Autowired
    private IotCmdEncoderMapper iotCmdEncoderMapper;

    /**
     * 查询指令编码
     * 
     * @param id 指令编码主键
     * @return 指令编码
     */
    @Override
    public IotCmdEncoder selectIotCmdEncoderById(Long id)
    {
        return iotCmdEncoderMapper.selectIotCmdEncoderById(id);
    }

    /**
     * 查询指令编码列表
     * 
     * @param iotCmdEncoder 指令编码
     * @return 指令编码
     */
    @Override
    public List<IotCmdEncoder> selectIotCmdEncoderList(IotCmdEncoder iotCmdEncoder)
    {
        return iotCmdEncoderMapper.selectIotCmdEncoderList(iotCmdEncoder);
    }

    /**
     * 新增指令编码
     * 
     * @param iotCmdEncoder 指令编码
     * @return 结果
     */
    @Override
    public int insertIotCmdEncoder(IotCmdEncoder iotCmdEncoder)
    {
        return iotCmdEncoderMapper.insertIotCmdEncoder(iotCmdEncoder);
    }

    /**
     * 修改指令编码
     * 
     * @param iotCmdEncoder 指令编码
     * @return 结果
     */
    @Override
    public int updateIotCmdEncoder(IotCmdEncoder iotCmdEncoder)
    {
        return iotCmdEncoderMapper.updateIotCmdEncoder(iotCmdEncoder);
    }

    /**
     * 批量删除指令编码
     * 
     * @param ids 需要删除的指令编码主键
     * @return 结果
     */
    @Override
    public int deleteIotCmdEncoderByIds(Long[] ids)
    {
        return iotCmdEncoderMapper.deleteIotCmdEncoderByIds(ids);
    }

    /**
     * 删除指令编码信息
     * 
     * @param id 指令编码主键
     * @return 结果
     */
    @Override
    public int deleteIotCmdEncoderById(Long id)
    {
        return iotCmdEncoderMapper.deleteIotCmdEncoderById(id);
    }
}
