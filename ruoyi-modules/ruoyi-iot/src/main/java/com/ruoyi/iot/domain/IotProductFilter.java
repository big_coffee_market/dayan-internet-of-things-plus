package com.ruoyi.iot.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 数据筛选对象 iot_product_filter
 * 
 * @author ruoyi
 * @date 2023-03-02
 */
public class IotProductFilter extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @Excel(name = "主键")
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 产品 */
    @Excel(name = "产品")
    private Long productId;

    /** 指令 */
    @Excel(name = "指令")
    private Long cmdId;

    /** 字段 */
    @Excel(name = "字段")
    private String fileds;

    /** 启用 */
    @Excel(name = "启用")
    private Long enable;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setCmdId(Long cmdId) 
    {
        this.cmdId = cmdId;
    }

    public Long getCmdId() 
    {
        return cmdId;
    }
    public void setFileds(String fileds) 
    {
        this.fileds = fileds;
    }

    public String getFileds() 
    {
        return fileds;
    }
    public void setEnable(Long enable) 
    {
        this.enable = enable;
    }

    public Long getEnable() 
    {
        return enable;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("productId", getProductId())
            .append("cmdId", getCmdId())
            .append("fileds", getFileds())
            .append("enable", getEnable())
            .append("remark", getRemark())
            .append("createTime", getCreateTime())
            .toString();
    }
}
