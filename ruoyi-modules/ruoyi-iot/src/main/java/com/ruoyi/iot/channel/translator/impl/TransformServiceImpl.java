package com.ruoyi.iot.channel.translator.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.iot.channel.translator.ITransformService;
import com.ruoyi.iot.domain.dto.ExchangeDTO;
import com.ruoyi.iot.domain.vo.DevCmdVo;
import com.ruoyi.iot.domain.IotCmd;
import com.ruoyi.iot.domain.IotDevice;
import com.ruoyi.iot.service.impl.IotCmdServiceImpl;
import com.ruoyi.iot.service.impl.IotDeviceServiceImpl;
import com.ruoyi.iot.service.impl.IotProductServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
@Slf4j
public class TransformServiceImpl implements ITransformService {



    /**
     * json 转 byte[]
     */
    @Autowired
    TransformCmdServiceImpl transformCmdService;

    /**
     * cmd服务
     */
    @Autowired
    IotCmdServiceImpl iotCmdService;

    /**
     * 设备服务
     */
    @Autowired
    IotDeviceServiceImpl iotDeviceService;

    /**
     * 产品服务
     */
    @Autowired
    IotProductServiceImpl iotProductService;


    @Override
    public ExchangeDTO platTransform(DevCmdVo devCmdVo) {

        IotDevice iotDevice = iotDeviceService.selectIotDeviceByDevNum(devCmdVo.getDevId());
        if(iotDevice == null) {
            log.info(String.format("dev:%s exist error!",devCmdVo.getDevId()));
        }
        Long productId = iotDevice.getProductId();
        IotCmd iotCmd = iotCmdService.selectIotCmdById(devCmdVo.getCmdId());
        String cmdHex =   iotCmd.getCmdHex();
        Long protocolId = iotCmd.getProtocolId();

        if(devCmdVo.getData() == null) {
            log.info(String.format("dev:%s not exist data",devCmdVo.getDevId() ));
            return null;
        }
        byte[] total = transformCmdService.platformTransform(devCmdVo.getCmdId(),devCmdVo.getData());

        ExchangeDTO exchangeDTO = new ExchangeDTO();
        exchangeDTO.setData(total);
        exchangeDTO.setCmdHex(cmdHex);
        exchangeDTO.setProductId(productId);
        exchangeDTO.setProtocolId(protocolId);
        exchangeDTO.setDevId(devCmdVo.getDevId());

        return exchangeDTO;
    }




    @Override
    public DevCmdVo devTransform(ExchangeDTO exchangeDTO) {
        String devId = exchangeDTO.getDevId();

        IotDevice iotDeviceCondition = new IotDevice();
        iotDeviceCondition.setDevId(devId);
        List<IotDevice> iotDeviceDos = iotDeviceService.selectIotDeviceList(iotDeviceCondition);
        if(iotDeviceDos.size() == .0) {
            log.info("not find device:" + exchangeDTO.getDevId());
            return null;
        }
        IotDevice iotDeviceDo = iotDeviceDos.get(0);
        Long productId =  iotDeviceDo.getProductId();

        String cmdHex =  exchangeDTO.getCmdHex();
        IotCmd iotCmdCondition = new IotCmd();
        iotCmdCondition.setProductId(productId);
        iotCmdCondition.setCmdHex(cmdHex);

        List<IotCmd> iotCmdDos = iotCmdService.selectIotCmdList(iotCmdCondition);
        if(iotCmdDos.size() == 0) {
            log.info(String.format("device:%s not find decode cmdHex:%s",devId,cmdHex));
            return null;
        }
        IotCmd iotCmd = iotCmdDos.get(0);
        Long cmd_id =  iotCmd.getId();
      //  log.info(String.format("decode device:%s cmdId:%s",devId,cmd_id + ""));
        JSONObject json = transformCmdService.deviceTransform(cmd_id,exchangeDTO.getData());

        DevCmdVo devCmdVo = new DevCmdVo();
        devCmdVo.setCmdId(iotCmd.getId());
        devCmdVo.setDevId(devId);
        devCmdVo.setData(json);

        //log.info("devCmdVo:" + JSON.toJSONString(devCmdVo));

        return devCmdVo;
    }




}
