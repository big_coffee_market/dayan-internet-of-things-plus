package com.ruoyi.iot.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSON;
import com.github.pagehelper.PageInfo;
import com.ruoyi.iot.domain.IotProduct;
import com.ruoyi.iot.domain.vo.drop.ProductFieldVoInfo;
import com.ruoyi.iot.domain.vo.plus.IotProductFieldPlus;
import com.ruoyi.iot.service.impl.IotProductServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.iot.domain.IotProductField;
import com.ruoyi.iot.service.IIotProductFieldService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

/**
 * 产品-属性Controller
 *
 * @author ruoyi
 * @date 2022-12-27
 */
@RestController
@RequestMapping("/field")
@Slf4j
public class IotProductFieldController extends BaseController
{
    @Autowired
    private IIotProductFieldService iotProductFieldService;

    @Autowired
    private IotProductServiceImpl iotProductService;

    /**
     * 查询产品-属性列表
     */
    @RequiresPermissions("iot:field:list")
    @GetMapping("/list")
    public ProductFieldVoInfo list(IotProductField iotProductField)
    {

        startPage();
        ProductFieldVoInfo rspData = new ProductFieldVoInfo();

        List<IotProductField> list = iotProductFieldService.selectIotProductFieldList(iotProductField);
        log.info("field size:" + list.size());

        List<IotProductFieldPlus> plusList = new ArrayList<>();
        for(IotProductField item: list) {
            IotProductFieldPlus iotProductFieldPlus = new IotProductFieldPlus();
            BeanUtils.copyProperties(item,iotProductFieldPlus);
            if(item.getProductId() != null) {
                String productName = iotProductService.selectIotProductById(item.getProductId()).getName();
                iotProductFieldPlus.setProductName(productName);
            }
            plusList.add(iotProductFieldPlus);
        }
        log.info("plusList size:" + plusList.size());
        rspData.setCode(200);
        rspData.setMsg("查询成功");
        rspData.setRows(plusList);
        rspData.setTotal(new PageInfo(list).getTotal());

        List<IotProduct> productList = iotProductService.selectIotProductList(new IotProduct());
        rspData.setProductList(productList);

        return rspData;
    }

    /**
     * 导出产品-属性列表
     */
    @RequiresPermissions("iot:field:export")
    @Log(title = "产品-属性", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, IotProductField iotProductField)
    {
        List<IotProductField> list = iotProductFieldService.selectIotProductFieldList(iotProductField);
        ExcelUtil<IotProductField> util = new ExcelUtil<IotProductField>(IotProductField.class);
        util.exportExcel(response, list, "产品-属性数据");
    }

    /**
     * 获取产品-属性详细信息
     */
    @RequiresPermissions("iot:field:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(iotProductFieldService.selectIotProductFieldById(id));
    }

    /**
     * 新增产品-属性
     */
    @RequiresPermissions("iot:field:add")
    @Log(title = "产品-属性", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody IotProductField iotProductField)
    {
        return toAjax(iotProductFieldService.insertIotProductField(iotProductField));
    }

    /**
     * 修改产品-属性
     */
    @RequiresPermissions("iot:field:edit")
    @Log(title = "产品-属性", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody IotProductField iotProductField)
    {
        return toAjax(iotProductFieldService.updateIotProductField(iotProductField));
    }

    /**
     * 删除产品-属性
     */
    @RequiresPermissions("iot:field:remove")
    @Log(title = "产品-属性", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(iotProductFieldService.deleteIotProductFieldByIds(ids));
    }
}
