package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.IotPlatformMapper;
import com.ruoyi.iot.domain.IotPlatform;
import com.ruoyi.iot.service.IIotPlatformService;

/**
 * 平台信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-12-08
 */
@Service
public class IotPlatformServiceImpl implements IIotPlatformService 
{
    @Autowired
    private IotPlatformMapper iotPlatformMapper;

    /**
     * 查询平台信息
     * 
     * @param id 平台信息主键
     * @return 平台信息
     */
    @Override
    public IotPlatform selectIotPlatformById(Long id)
    {
        return iotPlatformMapper.selectIotPlatformById(id);
    }

    /**
     * 查询平台信息列表
     * 
     * @param iotPlatform 平台信息
     * @return 平台信息
     */
    @Override
    public List<IotPlatform> selectIotPlatformList(IotPlatform iotPlatform)
    {
        return iotPlatformMapper.selectIotPlatformList(iotPlatform);
    }

    /**
     * 新增平台信息
     * 
     * @param iotPlatform 平台信息
     * @return 结果
     */
    @Override
    public int insertIotPlatform(IotPlatform iotPlatform)
    {
        return iotPlatformMapper.insertIotPlatform(iotPlatform);
    }

    /**
     * 修改平台信息
     * 
     * @param iotPlatform 平台信息
     * @return 结果
     */
    @Override
    public int updateIotPlatform(IotPlatform iotPlatform)
    {
        return iotPlatformMapper.updateIotPlatform(iotPlatform);
    }

    /**
     * 批量删除平台信息
     * 
     * @param ids 需要删除的平台信息主键
     * @return 结果
     */
    @Override
    public int deleteIotPlatformByIds(Long[] ids)
    {
        return iotPlatformMapper.deleteIotPlatformByIds(ids);
    }

    /**
     * 删除平台信息信息
     * 
     * @param id 平台信息主键
     * @return 结果
     */
    @Override
    public int deleteIotPlatformById(Long id)
    {
        return iotPlatformMapper.deleteIotPlatformById(id);
    }
}
