package com.ruoyi.iot.domain.dto.interf;

public interface IRabbitPublishKey {


    /**
     * 网站将数据发布到微服务
     * @return
     */
    String getWebToMqRouteKey();


    /**
     * Micro Service
     * 设备将数据发布到微服务
     * @return
     */
    String getDeviceToMqRouteKey();


}
