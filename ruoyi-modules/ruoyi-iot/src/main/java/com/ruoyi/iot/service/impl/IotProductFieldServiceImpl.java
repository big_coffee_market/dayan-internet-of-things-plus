package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.IotProductFieldMapper;
import com.ruoyi.iot.domain.IotProductField;
import com.ruoyi.iot.service.IIotProductFieldService;

/**
 * 产品-属性Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
@Service
@Primary
public class IotProductFieldServiceImpl implements IIotProductFieldService 
{
    @Autowired
    private IotProductFieldMapper iotProductFieldMapper;

    /**
     * 查询产品-属性
     * 
     * @param id 产品-属性主键
     * @return 产品-属性
     */
    @Override
    public IotProductField selectIotProductFieldById(Long id)
    {
        return iotProductFieldMapper.selectIotProductFieldById(id);
    }

    /**
     * 查询产品-属性列表
     * 
     * @param iotProductField 产品-属性
     * @return 产品-属性
     */
    @Override
    public List<IotProductField> selectIotProductFieldList(IotProductField iotProductField)
    {
        return iotProductFieldMapper.selectIotProductFieldList(iotProductField);
    }

    /**
     * 新增产品-属性
     * 
     * @param iotProductField 产品-属性
     * @return 结果
     */
    @Override
    public int insertIotProductField(IotProductField iotProductField)
    {
        return iotProductFieldMapper.insertIotProductField(iotProductField);
    }

    /**
     * 修改产品-属性
     * 
     * @param iotProductField 产品-属性
     * @return 结果
     */
    @Override
    public int updateIotProductField(IotProductField iotProductField)
    {
        return iotProductFieldMapper.updateIotProductField(iotProductField);
    }

    /**
     * 批量删除产品-属性
     * 
     * @param ids 需要删除的产品-属性主键
     * @return 结果
     */
    @Override
    public int deleteIotProductFieldByIds(Long[] ids)
    {
        return iotProductFieldMapper.deleteIotProductFieldByIds(ids);
    }

    /**
     * 删除产品-属性信息
     * 
     * @param id 产品-属性主键
     * @return 结果
     */
    @Override
    public int deleteIotProductFieldById(Long id)
    {
        return iotProductFieldMapper.deleteIotProductFieldById(id);
    }
}
