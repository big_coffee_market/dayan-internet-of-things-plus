package com.ruoyi.iot.domain.vo;

import lombok.Data;

@Data
public class IotProductFieldVo {

    /** id */
    private Long id;

    /** 产品id */
    private Long productId;

    /** 名称 */
    private String name;

    /**
     * 数据字段长度
     */
    private Long len;


}
