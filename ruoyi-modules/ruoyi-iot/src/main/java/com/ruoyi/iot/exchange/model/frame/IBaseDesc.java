package com.ruoyi.iot.exchange.model.frame;


public interface IBaseDesc {


    /**
     * 获取帧长度
     * @return
     */
    Long getLen();


    /**
     * 预读长度
     */
    Long getPreReadLen();

    /**
     * 标志
     */
    String getMarkFlag();

    /**
     * 标志的索引值
     */
    String getMarkIndexList();

    Long getType();
    /**
     * 表示数据长度的，下标索引，默认左高右底
     */
    String getLenIndexList();

    /**
     * 获取数据左边的索引，启始位置
     * @return
     */
    Long getDataLeftVector();

    /**
     * 获取数据右边的索引，结束位置
     * @return
     */
    Long getDataRightVector();

    /**
     * 获取数据类型
     * @return
     */
    Long getFieldType();


    /**
     * 其他长度
     */
    Long getOtherLen();
}
