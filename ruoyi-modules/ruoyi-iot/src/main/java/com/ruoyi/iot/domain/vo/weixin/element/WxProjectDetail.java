package com.ruoyi.iot.domain.vo.weixin.element;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ruoyi.iot.domain.IotProject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;

@Data
@Slf4j
public class WxProjectDetail {

    /**
     * 项目id
     */
    Long id;

    /**
     * 项目名臣
     */
    String name;

    /**
     * 状态值
     */
    Integer state;
    /**
     * 项目地点
     */
    String place;

    /**
     * 精度
     */
    Double longitude;

    /**
     * 维度
     */
    Double latitude;

    /**
     * 负责人
     */
    String principal;

    /**
     * 负责电话
     */
    String principalTel;

    /**
     * 启动时间
     */
    String startupTime;

    /**
     * 结束时间
     */
    String endTime;

    /**
     * 质保年份
     */
    String warrantyYear;

    /**
     * 备注
     */
    String remark;

    /**
     * 时间转化器
     */
    @JsonIgnore
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    public void parse(IotProject iotProject) {

        this.id = iotProject.getId();

        this.name = iotProject.getName();

        this.state = iotProject.getState().intValue();

        this.place = iotProject.getPlace();

        this.longitude = iotProject.getLongitude();

        this.latitude = iotProject.getLatitude();

        this.principal = iotProject.getPrincipal();

        this.principalTel = iotProject.getPrincipalTel();

        if(iotProject.getStartupTime() != null) {
            this.startupTime = df.format(iotProject.getStartupTime());
        }

        this.warrantyYear = iotProject.getWarrantyYear() + "";

        this.remark = iotProject.getRemark();
    }


}
