package com.ruoyi.iot.domain.vo;


import com.alibaba.fastjson.JSONObject;
import lombok.Data;


@Data
public class DevCmdVo {

    /**
     * 设备id
     */
    String devId;

    /**
     * 指令id
     */
    Long cmdId;

    /**
     * 指令对应的jsonId
     */
    JSONObject data;


}
