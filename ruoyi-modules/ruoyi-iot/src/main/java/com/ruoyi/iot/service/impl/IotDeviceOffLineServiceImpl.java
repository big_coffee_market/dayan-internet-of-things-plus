package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.IotDeviceOffLineMapper;
import com.ruoyi.iot.domain.IotDeviceOffLine;
import com.ruoyi.iot.service.IIotDeviceOffLineService;

/**
 * 设备离在线记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
@Service
@Primary
public class IotDeviceOffLineServiceImpl implements IIotDeviceOffLineService 
{
    @Autowired
    private IotDeviceOffLineMapper iotDeviceOffLineMapper;

    /**
     * 查询设备离在线记录
     * 
     * @param id 设备离在线记录主键
     * @return 设备离在线记录
     */
    @Override
    public IotDeviceOffLine selectIotDeviceOffLineById(Long id)
    {
        return iotDeviceOffLineMapper.selectIotDeviceOffLineById(id);
    }

    /**
     * 查询设备离在线记录列表
     * 
     * @param iotDeviceOffLine 设备离在线记录
     * @return 设备离在线记录
     */
    @Override
    public List<IotDeviceOffLine> selectIotDeviceOffLineList(IotDeviceOffLine iotDeviceOffLine)
    {
        return iotDeviceOffLineMapper.selectIotDeviceOffLineList(iotDeviceOffLine);
    }

    /**
     * 新增设备离在线记录
     * 
     * @param iotDeviceOffLine 设备离在线记录
     * @return 结果
     */
    @Override
    public int insertIotDeviceOffLine(IotDeviceOffLine iotDeviceOffLine)
    {
        return iotDeviceOffLineMapper.insertIotDeviceOffLine(iotDeviceOffLine);
    }

    /**
     * 修改设备离在线记录
     * 
     * @param iotDeviceOffLine 设备离在线记录
     * @return 结果
     */
    @Override
    public int updateIotDeviceOffLine(IotDeviceOffLine iotDeviceOffLine)
    {
        return iotDeviceOffLineMapper.updateIotDeviceOffLine(iotDeviceOffLine);
    }

    /**
     * 批量删除设备离在线记录
     * 
     * @param ids 需要删除的设备离在线记录主键
     * @return 结果
     */
    @Override
    public int deleteIotDeviceOffLineByIds(Long[] ids)
    {
        return iotDeviceOffLineMapper.deleteIotDeviceOffLineByIds(ids);
    }

    /**
     * 删除设备离在线记录信息
     * 
     * @param id 设备离在线记录主键
     * @return 结果
     */
    @Override
    public int deleteIotDeviceOffLineById(Long id)
    {
        return iotDeviceOffLineMapper.deleteIotDeviceOffLineById(id);
    }
}
