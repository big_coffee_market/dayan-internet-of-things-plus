package com.ruoyi.iot.exchange.network.tcp;




import com.ruoyi.iot.exchange.network.tcp.handler.TCPDispatchHandlerGatherProxy;
import com.ruoyi.iot.exchange.network.tcp.handler.TCPSplitFrameDecoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * 客户端发送请求
 * @author zhb
 *
 */
@Slf4j
public class TcpNetty implements Runnable {


    // 服务器的端口
    private int port;
    // tcp代理
    private TCPDispatchHandlerGatherProxy tcpDispatchHandlerGatherProxy;


    public TcpNetty(Integer port, TCPDispatchHandlerGatherProxy tcpDispatchHandlerGatherProxy){
        this.port = port;
        this.tcpDispatchHandlerGatherProxy = tcpDispatchHandlerGatherProxy;
    }

    EventLoopGroup bossEventLoopGroup;
    EventLoopGroup workEventLoopGroup;


    @Override
    public void run() {

        bossEventLoopGroup = new NioEventLoopGroup();
        workEventLoopGroup = new NioEventLoopGroup();

        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(bossEventLoopGroup,workEventLoopGroup)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG,128)
                //.option(ChannelOption.SO_RCVBUF,5)
                .option(ChannelOption.TCP_NODELAY,true)
                .handler(new LoggingHandler(LogLevel.INFO))
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        socketChannel.pipeline().addLast(new TCPSplitFrameDecoder(tcpDispatchHandlerGatherProxy));
                    }
                });
        try {
            log.debug("will start server");
            ChannelFuture closeFuture =  bootstrap.bind(port).sync();
            log.debug("server is closing");
            closeFuture.channel().closeFuture().sync();
            bossEventLoopGroup.shutdownGracefully();
            workEventLoopGroup.shutdownGracefully();
            log.debug("server is closed");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            bossEventLoopGroup.shutdownGracefully();
            workEventLoopGroup.shutdownGracefully();
            log.debug("release event loop group");
        }

    }

    public void cancel() {

        if(bossEventLoopGroup != null) {
            bossEventLoopGroup.shutdownGracefully();
            bossEventLoopGroup = null;
        }

        if(workEventLoopGroup != null) {
            workEventLoopGroup.shutdownGracefully();
            workEventLoopGroup = null;
        }

    }
}
