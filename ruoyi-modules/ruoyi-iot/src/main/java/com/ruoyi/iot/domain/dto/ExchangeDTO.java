package com.ruoyi.iot.domain.dto;


import com.ruoyi.iot.domain.dto.interf.IRabbitPublishKey;
import lombok.Data;
import org.apache.xmlbeans.impl.util.HexBin;

import java.io.Serializable;


@Data
public class ExchangeDTO extends ProductDTOTransData implements IRabbitPublishKey, Serializable {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 101111;
    /**
     * 指令id，会和iot_cmd表内的cmd_hex字段对应起来，完成解析，很重要
     */
    String cmdHex;

    /**
     *  protocolId
     */
    Long protocolId;


    @Override
    public String getWebToMqRouteKey() {
        return  "http-" + productId;
    }

    @Override
    public String getDeviceToMqRouteKey() {
        return "tcp-" + productId;
    }

    @Override
    public String toString() {

        return String.format("devId:%s cmdHex:%s frame:%s",devId, cmdHex, HexBin.bytesToString(data));
    }
}
