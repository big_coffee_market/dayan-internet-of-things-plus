package com.ruoyi.iot.domain.vo.field;

import lombok.Data;

@Data
public class EncodeObj {


    Integer index;

    Long fieldId;

    String fieldName;

}
