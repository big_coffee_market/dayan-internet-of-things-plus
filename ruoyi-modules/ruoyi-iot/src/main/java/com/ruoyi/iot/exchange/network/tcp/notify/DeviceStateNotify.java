package com.ruoyi.iot.exchange.network.tcp.notify;

import com.ruoyi.iot.exchange.facade.ServerFacade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Slf4j
public class DeviceStateNotify {

    static DeviceStateNotify deviceStateNotify = null;

    public static DeviceStateNotify getInstance(){

        return deviceStateNotify;
    }


    /**
     * 初始操作
     */
    @PostConstruct
    public void  initialOperation() {
        deviceStateNotify = this;
    }




    /**
     * 上线
     * @param devId 设备id
     */
    public void online(String devId){
        log.info(String.format("devId:%s online",devId));
        ServerFacade.getInstance().reportIotDeviceOnline(devId);

    }

    /**
     * 下线
     * @param devId 设备id
     */
    public void offline(String devId){
        log.info(String.format("devId:%s offline",devId));
        ServerFacade.getInstance().reportIotDeviceOffline(devId);

    }

}
