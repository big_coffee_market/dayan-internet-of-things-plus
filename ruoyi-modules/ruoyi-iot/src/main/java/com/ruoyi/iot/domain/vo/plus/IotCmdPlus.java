package com.ruoyi.iot.domain.vo.plus;

import com.ruoyi.iot.domain.IotCmd;
import lombok.Data;

@Data
public class IotCmdPlus extends IotCmd {


    String productName;


    String protocolName;


}
