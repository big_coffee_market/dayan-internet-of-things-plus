package com.ruoyi.iot.redis.action;

public interface IRedisAction<T> {

    /**
     * 推送消息
     * @param obj 数据对象
     */
    void publish(T obj);

    /**
     * 存储消息
     * @param obj 数据对象
     */
    void push(T obj);




}
