package com.ruoyi.iot.domain.vo.field;

import lombok.Data;

@Data
public class DecodeObj {

    Integer index;

    Integer trimType;

    Integer trimParam;

    Long fieldId;

    String fieldName;

}
