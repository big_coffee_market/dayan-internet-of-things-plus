package com.ruoyi.iot.domain.dto;

import lombok.Data;

@Data
public class ExplainTranslateDTO {

    /**
     * 指令
     */
    Long cmdId;

    /**
     * 发起方
     */
    Long sponsor;

    /**
     * 数据
     */
    String data;

}
