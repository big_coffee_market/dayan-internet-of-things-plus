package com.ruoyi.iot.domain.vo.drop;

import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.iot.domain.IotProduct;
import lombok.Data;

import java.util.List;


@Data
public class ProductFieldVoInfo extends TableDataInfo {

    List<IotProduct> productList;

}
