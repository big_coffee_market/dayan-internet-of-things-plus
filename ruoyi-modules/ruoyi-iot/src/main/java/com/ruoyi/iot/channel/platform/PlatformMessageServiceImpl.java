package com.ruoyi.iot.channel.platform;


import com.alibaba.fastjson.JSONObject;
import com.ruoyi.iot.channel.SynchroniseMessage;
import com.ruoyi.iot.channel.translator.impl.TransformServiceImpl;
import com.ruoyi.iot.domain.IotProject;
import com.ruoyi.iot.domain.dto.EFrame;
import com.ruoyi.iot.domain.dto.ExchangeDTO;
import com.ruoyi.iot.domain.vo.enums.EProtocol;
import com.ruoyi.iot.domain.vo.DevCmdVo;
import com.ruoyi.iot.domain.IotDevice;
import com.ruoyi.iot.domain.IotProduct;
import com.ruoyi.iot.rabbitmq.ExchangeRabbitHelper;
import com.ruoyi.iot.service.impl.IotCmdServiceImpl;
import com.ruoyi.iot.service.impl.IotDeviceServiceImpl;
import com.ruoyi.iot.service.impl.IotProductServiceImpl;
import com.ruoyi.iot.service.impl.IotProjectServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PlatformMessageServiceImpl implements IPlatformMessage {



    /**
     * 同步消息中心
     */
    @Autowired
    SynchroniseMessage synchroniseMessage;

    /**
     * 传输服务
     */
    @Autowired
    TransformServiceImpl iTransformService;

    /**
     * 产品服务调用
     */
    @Autowired
    IotProductServiceImpl iotProductService;

    /**
     * 设备服务调用
     */
    @Autowired
    IotDeviceServiceImpl iotDeviceService;

    /**
     * 项目服务
     */
    @Autowired
    IotProjectServiceImpl iotProjectService;

    /**
     * 指令服务调用
     */
    @Autowired
    IotCmdServiceImpl iotCmdService;

    /**
     * rabbit消息处理模板
     */
    @Autowired
    ExchangeRabbitHelper exchangeRabbitHelper;

    @Override
    public DevCmdVo platformCmd(DevCmdVo devCmdVo) {
        DevCmdVo result = new DevCmdVo();
        result.setDevId(devCmdVo.getDevId());
        JSONObject json = new JSONObject();
        IotDevice iotDevice = iotDeviceService.selectIotDeviceByDevNum(devCmdVo.getDevId());
        Long productId = iotDevice.getProductId();
        IotProduct iotProduct = iotProductService.selectIotProductById(productId);
        String devId = devCmdVo.getDevId();
        String reasonStr = "";
        if(iotDevice == null) {
            reasonStr = String.format("dev:%s exist error!",devCmdVo.getDevId());
            json.put("tip",reasonStr);
            result.setData(json);
            return result;
        }
        if(iotProduct == null) {
            reasonStr =  String.format("dev:%s product error!",devCmdVo.getDevId());
            json.put("tip",reasonStr);
            result.setData(json);
            return result;
        }

        Long projectId = iotDevice.getProjectId();
        IotProject iotProject = iotProjectService.selectIotProjectById(projectId);

        EProtocol eProtocol = EProtocol.getType(iotProject.getNetworkProtocol().intValue());
        if(eProtocol == EProtocol.UNKNOW) {
            reasonStr = String.format("devId:%s not find protocol",devId);
            json.put("tip",reasonStr);
            result.setData(json);
            return result;
        }

        ExchangeDTO exchangeDTO = iTransformService.platTransform(devCmdVo);
        if(exchangeDTO == null) {
            reasonStr = String.format("devId:%s not translate cmd",devId);
            json.put("tip",reasonStr);
            result.setData(json);
            return result;
        }
        switch (eProtocol) {
            case TCP:{
                Long resultId = iotCmdService.selectIotCmdById(devCmdVo.getCmdId()).getResultId();
                exchangeRabbitHelper.webPublishToMQ(exchangeDTO);
                DevCmdVo responseCmd =  synchroniseMessage.waitCmdMessage(devCmdVo.getDevId(),resultId);
                if(responseCmd == null) {
                    reasonStr = String.format("devId:%s not response",devId);
                    json.put("info",reasonStr);
                    result.setData(json);
                    return result;
                }
                result = responseCmd;
            }
            break;
        }

        return result;
    }


    @Override
    public void platformNotify(DevCmdVo devCmdVo) {

        IotDevice iotDevice = iotDeviceService.selectIotDeviceByDevNum(devCmdVo.getDevId());
        Long productId = iotDevice.getProductId();
        IotProduct iotProduct = iotProductService.selectIotProductById(productId);
        String devNum = devCmdVo.getDevId();
        if(iotDevice == null) {
            log.info(String.format("dev:{0} exist error!",devCmdVo.getDevId()));
            return ;
        }
        if(iotProduct == null) {
            log.info(String.format("dev:{0} product error!",devCmdVo.getDevId()));
            return ;
        }

        Long projectId = iotDevice.getProjectId();
        IotProject iotProject = iotProjectService.selectIotProjectById(projectId);

        EProtocol eProtocol = EProtocol.getType(iotProject.getNetworkProtocol().intValue());
        if(eProtocol == EProtocol.UNKNOW) {
            log.info(String.format("devId:{0} not find protocol",devNum));
            return ;
        }

        ExchangeDTO exchangeDTO = iTransformService.platTransform(devCmdVo);
        if(exchangeDTO == null) {
            log.info(String.format("devId:{0} not translate cmd",devNum));
            return ;
        }
        switch (eProtocol) {
            case TCP:{
                exchangeRabbitHelper.webPublishToMQ(exchangeDTO);
            }
            break;
        }

    }




}
