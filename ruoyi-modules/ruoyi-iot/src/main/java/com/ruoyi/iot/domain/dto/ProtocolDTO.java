package com.ruoyi.iot.domain.dto;

import lombok.Data;

@Data
public class ProtocolDTO {

    /**
     * 协议id
     */
    Integer protocolId;

    /**
     * 数据
     */
    String hexFrame;



}
