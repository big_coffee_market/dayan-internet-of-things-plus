package com.ruoyi.iot.domain.vo.plus;

import com.ruoyi.iot.domain.IotProductField;
import lombok.Data;

@Data
public class IotProductFieldPlus extends IotProductField {

    String productName;

}
