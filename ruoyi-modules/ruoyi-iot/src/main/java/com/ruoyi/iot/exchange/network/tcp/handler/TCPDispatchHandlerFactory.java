package com.ruoyi.iot.exchange.network.tcp.handler;


import com.alibaba.fastjson.JSON;
import com.ruoyi.iot.domain.IotFrame;
import com.ruoyi.iot.exchange.model.enums.EFrameTypes;
import com.ruoyi.iot.exchange.network.tcp.handler.dispatch.TCPDispatchHandler;
import com.ruoyi.iot.exchange.network.tcp.handler.impl.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TCPDispatchHandlerFactory {


    /**
     * 根据tcp类型，生成对应的handler
     * @param iotFrame 数据帧类型
     * @return 分发抽象类
     */
    public static TCPDispatchHandler create(IotFrame iotFrame) {

        TCPDispatchHandler dispatchHandler = null;
        log.info("frame data:" + JSON.toJSONString(iotFrame));
        switch (EFrameTypes.getType(iotFrame.getType().intValue())) {
            case register:{
                 dispatchHandler = new HexRegistrationDispatchHandler(iotFrame);
            }
            break;
            case heartSkip:{
                 dispatchHandler = new HexHeartSkipDispatchHandler(iotFrame);
            }
            break;
            case hexData: {
                 dispatchHandler = new HexDataDispatchHandler(iotFrame);
            }
            break;
            case asciiData:{
                 dispatchHandler = new AsciiDataDispatchHandler(iotFrame);
            }
            break;
            case headTailData:{
                dispatchHandler = new HexHeadTailDispatchHandler(iotFrame);
            }
            break;
        }

        return dispatchHandler;
    }


}
