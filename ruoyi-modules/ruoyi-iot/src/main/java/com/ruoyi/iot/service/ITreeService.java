package com.ruoyi.iot.service;

import com.ruoyi.iot.domain.vo.TreeVO;

public interface ITreeService {

    /**
     * 字段筛选得VO
     * @return
     */
    TreeVO filedFilterTreeVO();

}
