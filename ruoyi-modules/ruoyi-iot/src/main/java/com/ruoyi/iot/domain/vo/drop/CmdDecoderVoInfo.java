package com.ruoyi.iot.domain.vo.drop;

import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.iot.domain.IotCmd;
import com.ruoyi.iot.domain.IotProductField;
import lombok.Data;

import java.util.List;

@Data
public class CmdDecoderVoInfo extends TableDataInfo {

    /**
     * 指令列表
     */
    List<IotCmd>  cmdList;

    /**
     * 字段列表
     */
    List<IotProductField> fieldList;


}
