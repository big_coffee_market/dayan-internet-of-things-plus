package com.ruoyi.iot.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 指令解释对象 iot_cmd_explain
 * 
 * @author zxq
 * @date 2022-09-26
 */
public class IotCmdExplain extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @Excel(name = "主键")
    private Long id;

    /** 指令id */
    @Excel(name = "指令id")
    private Long cmdId;

    /** 发起方 */
    @Excel(name = "发起方")
    private Long sponsor;

    /** 报文 */
    @Excel(name = "物模型")
    private String json;

    /** 数据帧 */
    @Excel(name = "数据帧")
    private String frame;

    /** 创建者 */
    @Excel(name = "创建者")
    private Long creator;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCmdId(Long cmdId) 
    {
        this.cmdId = cmdId;
    }

    public Long getCmdId() 
    {
        return cmdId;
    }
    public void setSponsor(Long sponsor) 
    {
        this.sponsor = sponsor;
    }

    public Long getSponsor() 
    {
        return sponsor;
    }
    public void setJson(String json) 
    {
        this.json = json;
    }

    public String getJson() 
    {
        return json;
    }

    public String getFrame() {
        return frame;
    }

    public void setFrame(String frame) {
        this.frame = frame;
    }

    public void setCreator(Long creator)
    {
        this.creator = creator;
    }

    public Long getCreator() 
    {
        return creator;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }




    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("cmdId", getCmdId())
            .append("sponsor", getSponsor())
            .append("json", getJson())
            .append("frame", getFrame())
            .append("remark", getRemark())
            .append("creator", getCreator())
            .append("createDate", getCreateDate())
            .toString();
    }
}
