package com.ruoyi.iot.domain.vo.weixin.element;

import com.ruoyi.iot.domain.vo.weixin.field.WxCmdFieldInnerDetail;
import lombok.Data;
import java.util.List;

@Data
public class WxCmdFieldDetail {



    /**
     * 指令id
     */
    Long id;

    /**
     * 指令名称
     */
    String name;

    /**
     * 指令具体字段
     */
    List<WxCmdFieldInnerDetail> wxCmdFieldInnerDetails;


}
