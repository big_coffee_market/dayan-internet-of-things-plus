package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.IotDevice;

/**
 * 设备Service接口
 *
 * @author ruoyi
 * @date 2022-12-27
 */
public interface IIotDeviceService
{
    /**
     * 查询设备
     *
     * @param id 设备主键
     * @return 设备
     */
    public IotDevice selectIotDeviceById(Long id);

    /**
     * 查询设备
     * @param devNum 设备编号
     * @return
     */
    public IotDevice selectIotDeviceByDevNum(String devNum);

    /**
     * 查询设备列表
     *
     * @param iotDevice 设备
     * @return 设备集合
     */
    public List<IotDevice> selectIotDeviceList(IotDevice iotDevice);


    /**
     * 新增设备
     *
     * @param iotDevice 设备
     * @return 结果
     */
    public int insertIotDevice(IotDevice iotDevice);

    /**
     * 修改设备
     *
     * @param iotDevice 设备
     * @return 结果
     */
    public int updateIotDevice(IotDevice iotDevice);

    /**
     * 批量删除设备
     *
     * @param ids 需要删除的设备主键集合
     * @return 结果
     */
    public int deleteIotDeviceByIds(Long[] ids);

    /**
     * 删除设备信息
     *
     * @param id 设备主键
     * @return 结果
     */
    public int deleteIotDeviceById(Long id);
}
