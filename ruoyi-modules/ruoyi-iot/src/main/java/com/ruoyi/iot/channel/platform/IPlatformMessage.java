package com.ruoyi.iot.channel.platform;

import com.ruoyi.iot.domain.vo.DevCmdVo;

public interface IPlatformMessage {


    /**
     * 平台 -> 大衍物联网 -> 设备
     * 设备 -> 大衍物联网 -> 平台  平台指令  阻塞操作
     * 平台发起的指令，设备回复指令,一般命令指令走这里
     * @param devCmdVo 平台发起有回复的指令
     * @return
     */
    DevCmdVo platformCmd(DevCmdVo devCmdVo);


    /**
     * 平台 -> 大衍物联网 -> 设备
     * 下发指令，不需要回复
     * @param devCmdVo 平台指令
     */
    void platformNotify(DevCmdVo devCmdVo);


}
