package com.ruoyi.iot.domain.dto;

public enum EFrame {


    /**
     * 未知
     */
    unknow(-1),
    /**
     * 离线
     */
    offline(0),

    /**
     * 三丰注册包
     */
    sf_register(1),


    /**
     * com.zxq.frame.modbus
     */
    modbus(2),
    /**
     * 电表
     */
    dlt645(3),
    /**
     * com.zxq.frame.jt808
     */
    jt808(4),

    /**
     * 电力规约
     */
    iec_104(5),

    /**
     * UART是通用异步收发传输器
     */
    uart(6),
    ;

    Integer value;
    EFrame(Integer i) {
        this.value = i;
    }

    public Integer getValue() {
        return value;
    }

    public static EFrame getType(Integer value){
        EFrame eFrame = EFrame.unknow;
        switch (value) {
            case 0:
                eFrame = EFrame.offline;
                break;
            case 1:
                eFrame = EFrame.sf_register;
                break;
            case 2:
                eFrame = EFrame.modbus;
                break;
            case 3:
                eFrame = EFrame.dlt645;
                break;
            case 4:
                eFrame = EFrame.jt808;
                break;
            case 5:
                eFrame = EFrame.iec_104;
                break;
        }
        return eFrame;
    }






}
