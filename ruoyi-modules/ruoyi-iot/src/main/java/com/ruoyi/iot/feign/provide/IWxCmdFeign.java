package com.ruoyi.iot.feign.provide;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.iot.domain.vo.DevCmdVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * cmd 获取产品对应的指令信息
 */
public interface IWxCmdFeign {

    /**
     * 根据产品id 获取对应得指令信息
     * @param productId 产品id
     * @return
     */
    @GetMapping("/takeWxCmdVO/{productId}")
    AjaxResult takeWxCmdVO(@PathVariable("productId") Long productId);

    /**
     * 取得微信指令结果
     * @param devCmdVo 交互指令
     * @return
     */
    @PostMapping("/takeWxCmdResult")
    AjaxResult takeWxCmdResult(@RequestBody DevCmdVo devCmdVo);

}
