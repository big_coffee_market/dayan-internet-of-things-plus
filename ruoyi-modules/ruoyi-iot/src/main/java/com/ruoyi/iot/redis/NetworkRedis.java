package com.ruoyi.iot.redis;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.iot.redis.action.IRedisAction;
import com.ruoyi.iot.redis.enity.NetworkMO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class NetworkRedis implements IRedisAction<NetworkMO> {


    /**
     * redis服务
     */
    @Autowired
    RedisService redisService;


    /**
     * 网络Key
     */
    final String NETWORK_KEY = "network:project:%s:dev:%s";

    /**
     * 通知网络Key
     */
    final String NOTIFY_NETWORK_kEY = "notify:network:project:%s";


    @Override
    public void publish(NetworkMO obj) {
        String channel = String.format(NOTIFY_NETWORK_kEY,obj.getProjectId());
        String json = JSON.toJSONString(obj);
        redisService.redisTemplate.convertAndSend(channel, json);
    }


    @Override
    public void push(NetworkMO obj) {

        String redisKey = String.format(NETWORK_KEY,obj.getProjectId(),obj.getDevId());
        String json = JSON.toJSONString(obj);
        redisService.redisTemplate.boundListOps(redisKey).rightPush(json);

    }


}
