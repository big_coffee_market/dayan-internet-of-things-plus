package com.ruoyi.iot.controller;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.iot.service.impl.AirSensorServiceImpl;
import com.ruoyi.iot.service.impl.CQLampServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Slf4j
@RequestMapping("/cq_sensor")
public class SensorCQController extends BaseController {


    /**
     * /iot/cq_sensor/query_tree
     * /iot/cq_sensor/query_image
     * 空气传感器服务
     */
    @Autowired
    CQLampServiceImpl cqLampService;


    @GetMapping("/query_tree")
    public AjaxResult queryDevTree() {

        return AjaxResult.success(cqLampService.queryDevTree());
    }

    @GetMapping("/query_image")
    public AjaxResult queryPreImage() {

        return AjaxResult.success(cqLampService.queryPreImage());
    }



}
