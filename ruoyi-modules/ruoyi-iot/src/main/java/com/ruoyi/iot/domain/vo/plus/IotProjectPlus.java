package com.ruoyi.iot.domain.vo.plus;


import com.ruoyi.iot.domain.IotProject;
import lombok.Data;

@Data
public class IotProjectPlus extends IotProject {

   /**
    * 下一个动作名称
   * */
   String nextAction;

}
