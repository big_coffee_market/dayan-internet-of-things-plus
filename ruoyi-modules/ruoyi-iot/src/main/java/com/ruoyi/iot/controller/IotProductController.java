package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSON;
import com.github.pagehelper.PageInfo;
import com.ruoyi.iot.domain.IotProduct;
import com.ruoyi.iot.domain.IotProductField;
import com.ruoyi.iot.domain.IotProductFrame;
import com.ruoyi.iot.domain.vo.ProjectVO;
import com.ruoyi.iot.domain.vo.drop.ProductVoInfo;
import com.ruoyi.iot.domain.vo.exchange.IotWrapperDTO;
import com.ruoyi.iot.exchange.IotExchangeServiceImpl;
import com.ruoyi.iot.service.impl.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

/**
 * 产品Controller
 *
 * @author ruoyi
 * @date 2022-12-27
 */
@Slf4j
@RestController
@RequestMapping("/product")
public class IotProductController extends BaseController
{
    /**
     * 产品服务
     */
    @Autowired
    private IotProductServiceImpl iotProductService;

    /**
     * 数据包服务
     */
    @Autowired
    private IotProductFieldServiceImpl iotProductFieldService;

    /**
     * 异步产品服务
     */
    @Autowired
    private IotExchangeServiceImpl iExchangeController;

    /**
     * iot和产品协议的关系
     */
    @Autowired
    private IotProductFrameServiceImpl iotProductFrameService;




    /**
     * 查询产品列表
     */
    @RequiresPermissions("iot:product:list")
    @GetMapping("/list")
    public ProductVoInfo list(IotProduct iotProduct) {
        startPage();
        List<IotProduct> list = iotProductService.selectIotProductList(iotProduct);

        ProductVoInfo rspData = new ProductVoInfo();
        rspData.setCode(200);
        rspData.setRows(list);
        rspData.setMsg("查询成功");
        rspData.setTotal((new PageInfo(list)).getTotal());
        List<IotProductField> products = iotProductFieldService.selectIotProductFieldList(new IotProductField());
        rspData.setProductFieldList(products);

        return rspData;
    }

    /**
     * 获取设备列表
     * @param iotProduct 设备产品列表
     * @return
     */
    @PostMapping(value = "/iot/query/list")
    public IotWrapperDTO<List<IotProduct>> iotQueryList(@RequestBody IotProduct iotProduct) {
        IotWrapperDTO iotListDTO = new IotWrapperDTO();
        log.info("iot_product:" + JSON.toJSONString(iotProduct));
        List<IotProduct> list = iotProductService.selectIotProductList(iotProduct);
        iotListDTO.setData(list);
        return iotListDTO;
    }

    /**
     * 查询得到的列表数据
     * @param iotProductFrame
     * @return
     */
    @PostMapping("/iot/query/frame/list")
    public IotWrapperDTO<List<IotProductFrame>> queryList(@RequestBody IotProductFrame iotProductFrame)
    {
        IotWrapperDTO iotListDTO = new IotWrapperDTO();
        List<IotProductFrame> list = iotProductFrameService.selectIotProductFrameList(iotProductFrame);
        iotListDTO.setData(list);
        return iotListDTO;
    }

    /**
     * 导出产品列表
     */
    @RequiresPermissions("iot:product:export")
    @Log(title = "产品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, IotProduct iotProduct)
    {
        List<IotProduct> list = iotProductService.selectIotProductList(iotProduct);
        ExcelUtil<IotProduct> util = new ExcelUtil<IotProduct>(IotProduct.class);
        util.exportExcel(response, list, "产品数据");
    }

    /**
     * 获取产品详细信息
     */
    @RequiresPermissions("iot:product:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(iotProductService.selectIotProductById(id));
    }

    /**
     * 获取产品详细信息,提供给iot服务使用
     */
    @GetMapping(value = "/iot/{id}")
    public IotProduct getIotInfo(@PathVariable("id") Long id)
    {
        return iotProductService.selectIotProductById(id);
    }

    /**
     * 新增产品
     */
    @RequiresPermissions("iot:product:add")
    @Log(title = "产品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody IotProduct iotProduct)
    {
        return toAjax(iotProductService.insertIotProduct(iotProduct));
    }

    /**
     * 修改产品
     */
    @RequiresPermissions("iot:product:edit")
    @Log(title = "产品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody IotProduct iotProduct)
    {
        return toAjax(iotProductService.updateIotProduct(iotProduct));
    }


    @PostMapping(path = "/iot/update")
    public IotWrapperDTO<Boolean> iotUpdate(@RequestBody IotProduct iotProduct)
    {
        IotWrapperDTO iotListDTO = new IotWrapperDTO();
        Integer rst = iotProductService.updateIotProduct(iotProduct);
        iotListDTO.setData(rst > 0);
        return iotListDTO;
    }

    /**
     * 删除产品
     */
    @RequiresPermissions("iot:product:remove")
    @Log(title = "产品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        for(Long id : ids ) {
            IotProduct iotProduct = iotProductService.selectIotProductById(id);
            ProjectVO projectVO = new ProjectVO();
            projectVO.setProjectId(id);
            projectVO.setProjectName(iotProduct.getName());
            iExchangeController.stopProjectServer(projectVO);
        }

        return toAjax(iotProductService.deleteIotProductByIds(ids));
    }






}
