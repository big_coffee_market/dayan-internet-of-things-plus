package com.ruoyi.iot.domain.vo.drop;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.iot.domain.IotFrame;
import com.ruoyi.iot.domain.IotProduct;
import lombok.Data;
import java.util.List;

@Data
public class ProductFrameVoInfo extends TableDataInfo {

    /**
     * 产品列表
     */
    private List<IotProduct> productList;

    /**
     * 协议帧id
     */
    private List<IotFrame> frameList;

}
