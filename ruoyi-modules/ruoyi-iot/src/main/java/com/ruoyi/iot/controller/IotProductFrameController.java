package com.ruoyi.iot.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.github.pagehelper.PageInfo;
import com.ruoyi.iot.domain.IotFrame;
import com.ruoyi.iot.domain.IotProduct;
import com.ruoyi.iot.domain.vo.drop.ProductFrameVoInfo;
import com.ruoyi.iot.domain.vo.plus.IotProductFramePlus;
import com.ruoyi.iot.service.impl.IotFrameServiceImpl;
import com.ruoyi.iot.service.impl.IotProductServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.iot.domain.IotProductFrame;
import com.ruoyi.iot.service.IIotProductFrameService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

/**
 * 产品-数据帧Controller
 *
 * @author ruoyi
 * @date 2022-12-27
 */
@RestController
@RequestMapping("/product_frame")
public class IotProductFrameController extends BaseController
{
    @Autowired
    private IIotProductFrameService iotProductFrameService;

    @Autowired
    private IotProductServiceImpl productService;

    @Autowired
    private IotFrameServiceImpl frameService;

    /**
     * 查询产品-数据帧列表
     */
    @RequiresPermissions("iot:product_frame:list")
    @GetMapping("/list")
    public ProductFrameVoInfo list(IotProductFrame iotProductFrame)
    {
        startPage();
        List<IotProductFrame> list = iotProductFrameService.selectIotProductFrameList(iotProductFrame);
        ProductFrameVoInfo rspData = new ProductFrameVoInfo();
        rspData.setCode(200);
        rspData.setMsg("查询成功");
        rspData.setFrameList(frameService.selectIotFrameList(new IotFrame()));
        rspData.setProductList(productService.selectIotProductList(new IotProduct()));

        List<IotProductFramePlus> plusList = new ArrayList<>();
        for(IotProductFrame item: list) {

            IotProductFramePlus  iotProductFramePlus = new IotProductFramePlus();
            BeanUtils.copyProperties(item,iotProductFramePlus);

            iotProductFramePlus.setProductName(productService.selectIotProductById(item.getProductId()).getName());
            iotProductFramePlus.setFrameName(frameService.selectIotFrameById(item.getFrameId()).getName());
            plusList.add(iotProductFramePlus);
        }

        rspData.setRows(plusList);
        rspData.setTotal((new PageInfo(list)).getTotal());

        return rspData;
    }

    /**
     * 导出产品-数据帧列表
     */
    @RequiresPermissions("iot:product_frame:export")
    @Log(title = "产品-数据帧", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, IotProductFrame iotProductFrame)
    {
        List<IotProductFrame> list = iotProductFrameService.selectIotProductFrameList(iotProductFrame);
        ExcelUtil<IotProductFrame> util = new ExcelUtil<IotProductFrame>(IotProductFrame.class);
        util.exportExcel(response, list, "产品-数据帧数据");
    }

    /**
     * 获取产品-数据帧详细信息
     */
    @RequiresPermissions("iot:product_frame:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(iotProductFrameService.selectIotProductFrameById(id));
    }

    /**
     * 新增产品-数据帧
     */
    @RequiresPermissions("iot:product_frame:add")
    @Log(title = "产品-数据帧", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody IotProductFrame iotProductFrame)
    {
        return toAjax(iotProductFrameService.insertIotProductFrame(iotProductFrame));
    }

    /**
     * 修改产品-数据帧
     */
    @RequiresPermissions("iot:product_frame:edit")
    @Log(title = "产品-数据帧", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody IotProductFrame iotProductFrame)
    {
        return toAjax(iotProductFrameService.updateIotProductFrame(iotProductFrame));
    }

    /**
     * 删除产品-数据帧
     */
    @RequiresPermissions("iot:product_frame:remove")
    @Log(title = "产品-数据帧", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(iotProductFrameService.deleteIotProductFrameByIds(ids));
    }
}
