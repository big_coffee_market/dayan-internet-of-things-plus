package com.ruoyi.iot.exchange.network.tcp.handler.impl;


import com.ruoyi.iot.domain.IotFrame;
import com.ruoyi.iot.exchange.network.tcp.handler.dispatch.TCPDispatchHandler;
import com.zxq.factory.decoder.FiledDecoderFactory;
import com.zxq.factory.enums.EField;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HexDataDispatchHandler extends TCPDispatchHandler {


    public HexDataDispatchHandler(IotFrame iotFrame) {
        super(iotFrame);
    }



    @Override
    protected byte[] patternHex(byte[] frame) {

        return frame;
    }


    @Override
    protected String takeDevId(byte[] frame) {

        return null;
    }





    @Override
    protected Integer getDataLen(byte[] dataLenBuff) {
        return  (Integer) FiledDecoderFactory.instance().create(EField.Int).decoder(dataLenBuff);
    }





}
