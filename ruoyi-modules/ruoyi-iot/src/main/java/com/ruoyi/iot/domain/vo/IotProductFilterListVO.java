package com.ruoyi.iot.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class IotProductFilterListVO {

    /** 主键 */
    private Long id;

    /** 名称 */
    private String name;

    /** 产品 */
    private String productName;

    /** 指令 */
    private String cmdName;

    /** 字段 */
    private String filedNames;

    /** 启用 */
    private String enable;

    /** 备注 **/
    private String remark;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
