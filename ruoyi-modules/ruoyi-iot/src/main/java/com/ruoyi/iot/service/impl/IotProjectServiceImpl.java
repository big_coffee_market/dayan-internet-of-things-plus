package com.ruoyi.iot.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.iot.domain.*;
import com.ruoyi.iot.domain.vo.enums.ENetworkState;
import com.ruoyi.iot.domain.vo.exchange.ProjectFrameDTO;
import com.ruoyi.iot.exchange.IotExchangeServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.IotProjectMapper;
import com.ruoyi.iot.service.IIotProjectService;

/**
 * 项目Service业务层处理
 *
 * @author ruoyi
 * @date 2022-12-27
 */
@Service
@Primary
@Slf4j
public class IotProjectServiceImpl implements IIotProjectService
{

    @Autowired
    private IotProjectMapper iotProjectMapper;

    /**
     * 设备服务
     */
    @Autowired
    private IotDeviceServiceImpl deviceServiceImpl;

    /**
     * 产品服务
     */
    @Autowired
    private IotProductServiceImpl productServiceImpl;

    /**
     * 产品协议服务
     */
    @Autowired
    private IotProductFrameServiceImpl iotProductFrameService;

    /**
     * 协议帧服务
     */
    @Autowired
    private IotFrameServiceImpl iotFrameService;

    /**
     * 交换机服务
     */
    @Autowired
    private IotExchangeServiceImpl iotExchangeServiceImpl;

    /**
     * 查询项目
     *
     * @param id 项目主键
     * @return 项目
     */
    @Override
    public IotProject selectIotProjectById(Long id)
    {
        return iotProjectMapper.selectIotProjectById(id);
    }



    /**
     * 启动项目服务
     * @param iotProject 启动项目服务
     */
    @Override
    public void startUpProjectServer(IotProject iotProject) {
        //需要添加分发逻辑

        List<IotProduct> products = queryProducts(iotProject);
        ProjectFrameDTO projectFrameDTO = new ProjectFrameDTO();
        projectFrameDTO.setProjectId(iotProject.getId());
        projectFrameDTO.setProjectName(iotProject.getName());
        projectFrameDTO.setTransport(iotProject.getNetworkPort());
        List<IotFrame> frames = new ArrayList<>();
        for(IotProduct item: products) {
            IotProductFrame iotProductFrame = new IotProductFrame();
            iotProductFrame.setProductId(item.getId());
            List<IotProductFrame> productFrames = iotProductFrameService
                    .selectIotProductFrameList(iotProductFrame);
            for(IotProductFrame productFrameItem: productFrames) {
                Long frameId =  productFrameItem.getFrameId();
                IotFrame iotFrame = iotFrameService.selectIotFrameById(frameId);
                frames.add(iotFrame);
            }
        }

        projectFrameDTO.setFrames(frames);
        log.info("json:" + JSON.toJSONString(projectFrameDTO));
        iotExchangeServiceImpl.startProjectServer(projectFrameDTO);
        log.info(String.format("run project %s  port:%s",iotProject.getName(),iotProject.getNetworkPort()));
        iotProject.setNetworkState(ENetworkState.run.getValue().longValue());
        updateIotProject(iotProject);
    }


    /**
     * 查询产品们
     * @param iotProject 项目列表
     * @return
     */
    private List<IotProduct> queryProducts(IotProject iotProject) {
        IotDevice iotDevice = new IotDevice();
        iotDevice.setProjectId(iotProject.getId());
        List<IotDevice> iotDevices = deviceServiceImpl.selectIotDeviceList(iotDevice);
        List<IotProduct> products = new ArrayList<>();
        for(IotDevice item: iotDevices) {
            Long productId = item.getProductId();
            IotProduct iotProduct = productServiceImpl.selectIotProductById(productId);
            products.add(iotProduct);
        }
        return products;
    }


    /**
     * 查询项目列表
     *
     * @param iotProject 项目
     * @return 项目
     */
    @Override
    public List<IotProject> selectIotProjectList(IotProject iotProject)
    {
        return iotProjectMapper.selectIotProjectList(iotProject);
    }

    /**
     * 新增项目
     *
     * @param iotProject 项目
     * @return 结果
     */
    @Override
    public int insertIotProject(IotProject iotProject)
    {
        return iotProjectMapper.insertIotProject(iotProject);
    }

    /**
     * 修改项目
     *
     * @param iotProject 项目
     * @return 结果
     */
    @Override
    public int updateIotProject(IotProject iotProject)
    {
        return iotProjectMapper.updateIotProject(iotProject);
    }

    /**
     * 批量删除项目
     *
     * @param ids 需要删除的项目主键
     * @return 结果
     */
    @Override
    public int deleteIotProjectByIds(Long[] ids)
    {
        return iotProjectMapper.deleteIotProjectByIds(ids);
    }

    /**
     * 删除项目信息
     *
     * @param id 项目主键
     * @return 结果
     */
    @Override
    public int deleteIotProjectById(Long id)
    {
        return iotProjectMapper.deleteIotProjectById(id);
    }
}
