package com.ruoyi.iot.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
@RefreshScope
public class LockConfig {

    @Value("${lock.timeout}")
    Integer timeOut;

    public Integer getTimeOut() {
        return timeOut;
    }
}
