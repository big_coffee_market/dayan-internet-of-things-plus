package com.ruoyi.iot.domain.vo.enums;

public enum  EProtocol {

    TCP(1),
    UDP(2),
    MQTT(3),
    UNKNOW(-1)
    ;

    int value;
    EProtocol(int i) {
        this.value = i;
    }

    public int getValue() {
        return value;
    }

    public static EProtocol getType(int value) {
        EProtocol eProtocol = EProtocol.UNKNOW;
        switch (value) {
            case 1:
                eProtocol = EProtocol.TCP;
                break;
            case 2:
                eProtocol = EProtocol.UDP;
                break;
            case 3:
                eProtocol = EProtocol.MQTT;
                break;
        }
       return eProtocol;
    }




}
