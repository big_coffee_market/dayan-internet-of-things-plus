package com.ruoyi.iot.domain.vo.drop;

import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.iot.domain.IotProductField;
import lombok.Data;

import java.util.List;


@Data
public class ProductVoInfo extends TableDataInfo {

    /**
     * 包名
     */
    private List<IotProductField> productFieldList;


}
