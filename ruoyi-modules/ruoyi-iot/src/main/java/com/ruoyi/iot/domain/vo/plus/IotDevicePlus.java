package com.ruoyi.iot.domain.vo.plus;

import com.ruoyi.iot.domain.IotDevice;
import lombok.Data;

@Data
public class IotDevicePlus extends IotDevice {

    private String projectName;

    private String productName;



}
