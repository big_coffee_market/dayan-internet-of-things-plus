package com.ruoyi.iot.aop;

import java.lang.annotation.*;

@Retention(value = RetentionPolicy.SOURCE)
@Target(ElementType.METHOD)
@Inherited
public @interface IotFilter {



}
