package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.IotCmdMapper;
import com.ruoyi.iot.domain.IotCmd;
import com.ruoyi.iot.service.IIotCmdService;

/**
 * 指令Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
@Service
@Primary
public class IotCmdServiceImpl implements IIotCmdService 
{
    @Autowired
    private IotCmdMapper iotCmdMapper;

    /**
     * 查询指令
     * 
     * @param id 指令主键
     * @return 指令
     */
    @Override
    public IotCmd selectIotCmdById(Long id)
    {
        return iotCmdMapper.selectIotCmdById(id);
    }

    /**
     * 查询指令列表
     * 
     * @param iotCmd 指令
     * @return 指令
     */
    @Override
    public List<IotCmd> selectIotCmdList(IotCmd iotCmd)
    {
        return iotCmdMapper.selectIotCmdList(iotCmd);
    }

    /**
     * 新增指令
     * 
     * @param iotCmd 指令
     * @return 结果
     */
    @Override
    public int insertIotCmd(IotCmd iotCmd)
    {
        return iotCmdMapper.insertIotCmd(iotCmd);
    }

    /**
     * 修改指令
     * 
     * @param iotCmd 指令
     * @return 结果
     */
    @Override
    public int updateIotCmd(IotCmd iotCmd)
    {
        return iotCmdMapper.updateIotCmd(iotCmd);
    }

    /**
     * 批量删除指令
     * 
     * @param ids 需要删除的指令主键
     * @return 结果
     */
    @Override
    public int deleteIotCmdByIds(Long[] ids)
    {
        return iotCmdMapper.deleteIotCmdByIds(ids);
    }

    /**
     * 删除指令信息
     * 
     * @param id 指令主键
     * @return 结果
     */
    @Override
    public int deleteIotCmdById(Long id)
    {
        return iotCmdMapper.deleteIotCmdById(id);
    }
}
