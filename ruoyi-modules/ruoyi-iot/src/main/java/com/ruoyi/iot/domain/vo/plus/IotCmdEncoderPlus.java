package com.ruoyi.iot.domain.vo.plus;

import com.ruoyi.iot.domain.IotCmdEncoder;
import lombok.Data;

@Data
public class IotCmdEncoderPlus extends IotCmdEncoder {

    /**
     * 指令名称
     */
    String cmdName;

    /**
     * 字段名称
     */
    String fieldName;


}
