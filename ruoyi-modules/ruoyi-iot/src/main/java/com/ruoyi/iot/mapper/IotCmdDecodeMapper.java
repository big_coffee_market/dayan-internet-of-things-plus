package com.ruoyi.iot.mapper;


import com.ruoyi.iot.domain.IotCmdDecode;

import java.util.List;

/**
 * 指令解析Mapper接口
 * 
 * @author ruoyi
 * @date 2022-09-14
 */
public interface IotCmdDecodeMapper 
{
    /**
     * 查询指令解析
     * 
     * @param id 指令解析主键
     * @return 指令解析
     */
    public IotCmdDecode selectIotCmdDecodeById(Long id);

    /**
     * 查询指令解析列表
     * 
     * @param iotCmdDecode 指令解析
     * @return 指令解析集合
     */
    public List<IotCmdDecode> selectIotCmdDecodeList(IotCmdDecode iotCmdDecode);

    /**
     * 新增指令解析
     * 
     * @param iotCmdDecode 指令解析
     * @return 结果
     */
    public int insertIotCmdDecode(IotCmdDecode iotCmdDecode);

    /**
     * 修改指令解析
     * 
     * @param iotCmdDecode 指令解析
     * @return 结果
     */
    public int updateIotCmdDecode(IotCmdDecode iotCmdDecode);

    /**
     * 删除指令解析
     * 
     * @param id 指令解析主键
     * @return 结果
     */
    public int deleteIotCmdDecodeById(Long id);

    /**
     * 批量删除指令解析
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteIotCmdDecodeByIds(Long[] ids);
}
