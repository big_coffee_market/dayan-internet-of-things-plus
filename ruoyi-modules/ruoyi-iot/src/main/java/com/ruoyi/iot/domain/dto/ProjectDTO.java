package com.ruoyi.iot.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ProjectDTO {

    /** 名称 */
    private String name;

    /** 地点 */
    private String place;

    /** 经度 */
    private Double longitude;

    /** 纬度 */
    private Double latitude;

    /** 负责人 */
    private String principal;

    /** 负责人电话 */
    private String principalTel;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startupTime;

    /** 状态（立项，施工，竣工，验收，结束） */
    private Long state;

    /** 质保年份 */
    private Long warrantyYear;

    /**
     * 备注
     */
    private String remark;

}
