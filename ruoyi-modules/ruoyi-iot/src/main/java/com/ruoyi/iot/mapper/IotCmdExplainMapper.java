package com.ruoyi.iot.mapper;


import com.ruoyi.iot.domain.IotCmdExplain;

import java.util.List;

/**
 * 指令解释Mapper接口
 * 
 * @author zxq
 * @date 2022-09-26
 */
public interface IotCmdExplainMapper 
{
    /**
     * 查询指令解释
     * 
     * @param id 指令解释主键
     * @return 指令解释
     */
    public IotCmdExplain selectIotCmdExplainById(Long id);

    /**
     * 查询指令解释列表
     * 
     * @param iotCmdExplain 指令解释
     * @return 指令解释集合
     */
    public List<IotCmdExplain> selectIotCmdExplainList(IotCmdExplain iotCmdExplain);

    /**
     * 新增指令解释
     * 
     * @param iotCmdExplain 指令解释
     * @return 结果
     */
    public int insertIotCmdExplain(IotCmdExplain iotCmdExplain);

    /**
     * 修改指令解释
     * 
     * @param iotCmdExplain 指令解释
     * @return 结果
     */
    public int updateIotCmdExplain(IotCmdExplain iotCmdExplain);

    /**
     * 删除指令解释
     * 
     * @param id 指令解释主键
     * @return 结果
     */
    public int deleteIotCmdExplainById(Long id);

    /**
     * 批量删除指令解释
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteIotCmdExplainByIds(Long[] ids);
}
