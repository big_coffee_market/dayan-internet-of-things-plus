package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.IotProductFilter;

/**
 * 数据筛选Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-02
 */
public interface IotProductFilterMapper 
{
    /**
     * 查询数据筛选
     * 
     * @param id 数据筛选主键
     * @return 数据筛选
     */
    public IotProductFilter selectIotProductFilterById(Long id);

    /**
     * 查询数据筛选列表
     * 
     * @param iotProductFilter 数据筛选
     * @return 数据筛选集合
     */
    public List<IotProductFilter> selectIotProductFilterList(IotProductFilter iotProductFilter);

    /**
     * 新增数据筛选
     * 
     * @param iotProductFilter 数据筛选
     * @return 结果
     */
    public int insertIotProductFilter(IotProductFilter iotProductFilter);

    /**
     * 修改数据筛选
     * 
     * @param iotProductFilter 数据筛选
     * @return 结果
     */
    public int updateIotProductFilter(IotProductFilter iotProductFilter);

    /**
     * 删除数据筛选
     * 
     * @param id 数据筛选主键
     * @return 结果
     */
    public int deleteIotProductFilterById(Long id);

    /**
     * 批量删除数据筛选
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteIotProductFilterByIds(Long[] ids);
}
