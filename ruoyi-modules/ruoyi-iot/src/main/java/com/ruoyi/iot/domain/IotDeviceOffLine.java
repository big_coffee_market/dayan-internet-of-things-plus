package com.ruoyi.iot.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 设备离在线记录对象 iot_device_off_line
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
public class IotDeviceOffLine extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @Excel(name = "主键")
    private Long id;

    /** 设备Id */
    @Excel(name = "设备Id")
    private String devId;

    /** 项目id */
    @Excel(name = "项目id")
    private Long projectId;

    /** 设备状态 0离线 1在线 */
    @Excel(name = "设备状态 0离线 1在线")
    private Long status;

    /** 触发时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "触发时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date triggerTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDevId(String devId) 
    {
        this.devId = devId;
    }

    public String getDevId() 
    {
        return devId;
    }
    public void setProjectId(Long projectId) 
    {
        this.projectId = projectId;
    }

    public Long getProjectId() 
    {
        return projectId;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setTriggerTime(Date triggerTime) 
    {
        this.triggerTime = triggerTime;
    }

    public Date getTriggerTime() 
    {
        return triggerTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("devId", getDevId())
            .append("projectId", getProjectId())
            .append("status", getStatus())
            .append("triggerTime", getTriggerTime())
            .toString();
    }
}
