package com.ruoyi.iot.channel.translator;

import com.ruoyi.iot.domain.dto.ExchangeDTO;
import com.ruoyi.iot.domain.vo.DevCmdVo;



public interface ITransformService {
    /**
     * 平台信息，转变为设备信息
     * @param devCmdVo 平台信息
     * @return
     */
    ExchangeDTO platTransform(DevCmdVo devCmdVo);

    /**
     * 设备信息，转变为平台信息
     * @param exchangeDTO 设备信息
     * @return
     */
    DevCmdVo devTransform(ExchangeDTO exchangeDTO);
}
