package com.ruoyi.iot.service;


import com.ruoyi.iot.domain.IotCmdExplain;

import java.util.List;

/**
 * 指令解释Service接口
 * 
 * @author zxq
 * @date 2022-09-26
 */
public interface IIotCmdExplainService 
{
    /**
     * 查询指令解释
     * 
     * @param id 指令解释主键
     * @return 指令解释
     */
    public IotCmdExplain selectIotCmdExplainById(Long id);

    /**
     * 查询指令解释列表
     * 
     * @param iotCmdExplain 指令解释
     * @return 指令解释集合
     */
    public List<IotCmdExplain> selectIotCmdExplainList(IotCmdExplain iotCmdExplain);

    /**
     * 新增指令解释
     * 
     * @param iotCmdExplain 指令解释
     * @return 结果
     */
    public int insertIotCmdExplain(IotCmdExplain iotCmdExplain);

    /**
     * 修改指令解释
     * 
     * @param iotCmdExplain 指令解释
     * @return 结果
     */
    public int updateIotCmdExplain(IotCmdExplain iotCmdExplain);

    /**
     * 批量删除指令解释
     * 
     * @param ids 需要删除的指令解释主键集合
     * @return 结果
     */
    public int deleteIotCmdExplainByIds(Long[] ids);

    /**
     * 删除指令解释信息
     * 
     * @param id 指令解释主键
     * @return 结果
     */
    public int deleteIotCmdExplainById(Long id);
}
