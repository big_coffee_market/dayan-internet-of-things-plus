package com.ruoyi.iot.domain.vo;

import lombok.Data;

@Data
public class UserOperateVO<T> {

    T data;
    private Boolean success;
    private Integer code;
    private String message;


}
