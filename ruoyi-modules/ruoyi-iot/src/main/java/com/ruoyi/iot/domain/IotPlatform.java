package com.ruoyi.iot.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 平台信息对象 iot_platform
 * 
 * @author ruoyi
 * @date 2022-12-08
 */
public class IotPlatform extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @Excel(name = "主键")
    private Long id;

    /** 公司名称 */
    @Excel(name = "公司名称")
    private String companyName;

    /** h5地址 */
    @Excel(name = "h5地址")
    private String h5Url;

    /** 路径地址 */
    @Excel(name = "路径地址")
    private String webUrl;

    /** 用户名 */
    @Excel(name = "用户名")
    private String user;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCompanyName(String companyName) 
    {
        this.companyName = companyName;
    }

    public String getCompanyName() 
    {
        return companyName;
    }
    public void setH5Url(String h5Url) 
    {
        this.h5Url = h5Url;
    }

    public String getH5Url() 
    {
        return h5Url;
    }
    public void setWebUrl(String webUrl) 
    {
        this.webUrl = webUrl;
    }

    public String getWebUrl() 
    {
        return webUrl;
    }
    public void setUser(String user) 
    {
        this.user = user;
    }

    public String getUser() 
    {
        return user;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("companyName", getCompanyName())
            .append("h5Url", getH5Url())
            .append("webUrl", getWebUrl())
            .append("user", getUser())
            .append("password", getPassword())
            .append("remark", getRemark())
            .append("createDate", getCreateDate())
            .toString();
    }
}
