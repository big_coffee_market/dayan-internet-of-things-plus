package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.IotProductMapper;
import com.ruoyi.iot.domain.IotProduct;
import com.ruoyi.iot.service.IIotProductService;

/**
 * 产品Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
@Service
@Primary
public class IotProductServiceImpl implements IIotProductService 
{
    @Autowired
    private IotProductMapper iotProductMapper;

    /**
     * 查询产品
     * 
     * @param id 产品主键
     * @return 产品
     */
    @Override
    public IotProduct selectIotProductById(Long id)
    {
        return iotProductMapper.selectIotProductById(id);
    }

    /**
     * 查询产品列表
     * 
     * @param iotProduct 产品
     * @return 产品
     */
    @Override
    public List<IotProduct> selectIotProductList(IotProduct iotProduct)
    {
        return iotProductMapper.selectIotProductList(iotProduct);
    }

    /**
     * 新增产品
     * 
     * @param iotProduct 产品
     * @return 结果
     */
    @Override
    public int insertIotProduct(IotProduct iotProduct)
    {
        return iotProductMapper.insertIotProduct(iotProduct);
    }

    /**
     * 修改产品
     * 
     * @param iotProduct 产品
     * @return 结果
     */
    @Override
    public int updateIotProduct(IotProduct iotProduct)
    {
        return iotProductMapper.updateIotProduct(iotProduct);
    }

    /**
     * 批量删除产品
     * 
     * @param ids 需要删除的产品主键
     * @return 结果
     */
    @Override
    public int deleteIotProductByIds(Long[] ids)
    {
        return iotProductMapper.deleteIotProductByIds(ids);
    }

    /**
     * 删除产品信息
     * 
     * @param id 产品主键
     * @return 结果
     */
    @Override
    public int deleteIotProductById(Long id)
    {
        return iotProductMapper.deleteIotProductById(id);
    }
}
