package com.ruoyi.iot.exchange.model.enums;


public enum EFrameTypes {

    /**
     * 注册帧
     */
    register(1),
    /**
     * 心跳帧
     */
    heartSkip(2),
    /**
     * 数据帧
     */
    hexData(3),
    /**
     * ascii码
     */
    asciiData(4),
    /**
     * 头尾截获得数据帧
     */
    headTailData(5),
    /**
     * 未知
     */
    unKnow(-1);

    Integer value;
    EFrameTypes(int i) {
     this.value = i;
    }

    public Integer getValue() {
        return value;
    }

    /**
     * 获取帧类型
     * @param index 下标值
     * @return
     */
    public static EFrameTypes getType(Integer index) {
        EFrameTypes eFrameTypes = EFrameTypes.unKnow;
        switch (index){
            case 1:
                eFrameTypes = EFrameTypes.register;
                break;
            case 2:
                eFrameTypes = EFrameTypes.heartSkip;
                break;
            case 3:
                eFrameTypes = EFrameTypes.hexData;
                break;
            case 4:
                eFrameTypes = EFrameTypes.asciiData;
                break;
            case 5:
                eFrameTypes = EFrameTypes.headTailData;
                break;
        }
        return eFrameTypes;
    }

}
