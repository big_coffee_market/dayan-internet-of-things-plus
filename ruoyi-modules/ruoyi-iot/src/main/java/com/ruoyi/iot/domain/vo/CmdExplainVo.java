package com.ruoyi.iot.domain.vo;


import com.alibaba.fastjson.JSONObject;
import lombok.Data;

@Data
public class CmdExplainVo {

    Long cmdId;

    String cmdName;

    JSONObject cmdJson;

}
