package com.ruoyi.iot.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class CmdProductFieldVO {

    String productId;

    List<IotProductFieldVo> productFieldVoList;

}
