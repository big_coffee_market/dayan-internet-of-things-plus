package com.ruoyi.iot.exchange.redis;

import com.ruoyi.iot.domain.vo.exchange.ProjectFrameDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

@Deprecated
@Service
public class RedisProductCache {

    static final String PROJECT_KEY = "iot-project";


    @Autowired
    RedisTemplate redisTemplate;

    @Deprecated
    public void saveProjectServer(ProjectFrameDTO projectFrameDTO) {
        Long projectId = projectFrameDTO.getProjectId();
        redisTemplate.boundHashOps(PROJECT_KEY).put(projectId + "", projectFrameDTO);

    }

    @Deprecated
    public void removeProjectServer(Long projectId) {
        redisTemplate.boundHashOps(PROJECT_KEY).delete(projectId + "");
    }

    @Deprecated
    public Collection<ProjectFrameDTO> getProjectServer() {
        List list = new ArrayList<ProjectFrameDTO>();
        Set<Map.Entry<Long, ProjectFrameDTO>> entrySet = redisTemplate.boundHashOps(PROJECT_KEY).entries().entrySet();
        entrySet.forEach(item -> {
           list.add(item.getValue());
        });
        return list;
    }






}
