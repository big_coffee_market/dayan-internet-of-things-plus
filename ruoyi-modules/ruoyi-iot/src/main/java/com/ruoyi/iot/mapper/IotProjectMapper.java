package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.IotProject;

/**
 * 项目Mapper接口
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
public interface IotProjectMapper 
{
    /**
     * 查询项目
     * 
     * @param id 项目主键
     * @return 项目
     */
    public IotProject selectIotProjectById(Long id);

    /**
     * 查询项目列表
     * 
     * @param iotProject 项目
     * @return 项目集合
     */
    public List<IotProject> selectIotProjectList(IotProject iotProject);

    /**
     * 新增项目
     * 
     * @param iotProject 项目
     * @return 结果
     */
    public int insertIotProject(IotProject iotProject);

    /**
     * 修改项目
     * 
     * @param iotProject 项目
     * @return 结果
     */
    public int updateIotProject(IotProject iotProject);

    /**
     * 删除项目
     * 
     * @param id 项目主键
     * @return 结果
     */
    public int deleteIotProjectById(Long id);

    /**
     * 批量删除项目
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteIotProjectByIds(Long[] ids);
}
