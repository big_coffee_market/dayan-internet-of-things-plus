package com.ruoyi.iot.service.impl;

import com.ruoyi.iot.domain.IotCmd;
import com.ruoyi.iot.domain.IotCmdDecode;
import com.ruoyi.iot.domain.IotProduct;
import com.ruoyi.iot.domain.IotProductField;
import com.ruoyi.iot.domain.vo.TreeVO;
import com.ruoyi.iot.service.ITreeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class TreeVoServiceImpl implements ITreeService {

    /**
     * 产品服务
     */
    @Autowired
    IotProductServiceImpl productService;

    /**
     * 指令服务
     */
    @Autowired
    IotCmdServiceImpl cmdService;

    /**
     * 字段服务
     */
    @Autowired
    IotProductFieldServiceImpl fieldService;

    /**
     * 解析服务
     */
    @Autowired
    IotCmdDecodeServiceImpl decodeService;



    @Override
    public TreeVO filedFilterTreeVO() {
        TreeVO treeVO = new TreeVO();
        List<TreeVO.Node> productNodes = queryProductNodes();
        treeVO.setData(productNodes);
        for(TreeVO.Node productNode: productNodes) {
           Long productId =  Long.parseLong(productNode.getId());
           List<TreeVO.Node> cmdNodes = queryCmdNodes(productId);
           for(TreeVO.Node cmdNode:cmdNodes) {
               Long cmdId = Long.parseLong(cmdNode.getId());
               List<TreeVO.Node> filedNodes = queryFiledNodes(cmdId);
               cmdNode.setChildren(filedNodes);
           }
           productNode.setChildren(cmdNodes);
        }

        return treeVO;
    }



    /**
     * 查询产品对象
     * @return
     */
    private List<TreeVO.Node> queryProductNodes() {
        List<TreeVO.Node> nodes = new ArrayList<>();
        List<IotProduct> products = productService.selectIotProductList(new IotProduct());
        for(IotProduct product: products) {
            TreeVO.Node node = new TreeVO.Node();
            node.setId(product.getId() + "");
            node.setLabel(product.getName());
            nodes.add(node);
            node.setLevel(1);
        }

        return nodes;
    }

    /**
     * 查询指令对象
     * @return
     */
    private List<TreeVO.Node> queryCmdNodes(Long productId) {

        IotCmd iotCmd = new IotCmd();
        iotCmd.setProductId(productId);

        List<TreeVO.Node> nodes = new ArrayList<>();
        List<IotCmd> cmdList = cmdService.selectIotCmdList(iotCmd);
        for(IotCmd cmd : cmdList) {
            Long cmdId = cmd.getId();
            String name = cmd.getName();
            TreeVO.Node node = new TreeVO.Node();
            node.setId(cmdId + "");
            node.setLabel(name);
            node.setParentId(productId + "");
            nodes.add(node);
            node.setLevel(2);
        }

        return nodes;
    }

    /**
     * 查询字段
     * @param cmdId
     * @return
     */
    private List<TreeVO.Node> queryFiledNodes(Long cmdId) {
        IotCmdDecode query = new IotCmdDecode();
        query.setCmdId(cmdId);
        List<TreeVO.Node> nodes = new ArrayList<>();
        List<IotCmdDecode> decodeList =  decodeService.selectIotCmdDecodeList(query);
        for(IotCmdDecode decode: decodeList) {
            Long fieldID = decode.getFieldId();
            IotProductField productField = fieldService.selectIotProductFieldById(fieldID);
            String name = productField.getName();
            TreeVO.Node node = new TreeVO.Node();
            node.setId(fieldID + "");
            node.setLabel(name);
            node.setParentId(cmdId + "");
            nodes.add(node);
            node.setLevel(3);
        }

        return nodes;
    }



}
