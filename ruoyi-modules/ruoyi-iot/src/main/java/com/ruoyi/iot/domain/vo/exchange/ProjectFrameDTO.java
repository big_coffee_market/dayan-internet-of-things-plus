package com.ruoyi.iot.domain.vo.exchange;


import com.ruoyi.iot.domain.IotFrame;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProjectFrameDTO implements Serializable {

    /**
     * 产品名称
     */
    String projectName;

    /**
     * 产品id
     */
    Long projectId;

    /**
     * 协议ids
     */
    List<IotFrame> frames;

    /**
     * 协议传输端口
     */
    Long transport;



}
