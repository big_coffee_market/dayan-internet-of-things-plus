package com.ruoyi.iot.domain.vo;

import lombok.Data;

@Data
public class ProjectVO {

    /**
     * 产品id
     */
    Long projectId;

    /**
     * 产品名称
     */
    String projectName;



}
