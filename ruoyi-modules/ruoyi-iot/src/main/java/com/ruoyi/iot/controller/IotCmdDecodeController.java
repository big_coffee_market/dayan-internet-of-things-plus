package com.ruoyi.iot.controller;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.nacos.shaded.org.checkerframework.checker.units.qual.C;
import com.github.pagehelper.PageInfo;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.iot.domain.IotCmd;
import com.ruoyi.iot.domain.IotProductField;
import com.ruoyi.iot.domain.vo.drop.CmdDecoderVoInfo;
import com.ruoyi.iot.domain.vo.drop.CmdEncoderVoInfo;
import com.ruoyi.iot.domain.vo.exchange.IotWrapperDTO;
import com.ruoyi.iot.domain.IotCmdDecode;
import com.ruoyi.iot.domain.vo.plus.IotCmdDecodePlus;
import com.ruoyi.iot.domain.vo.plus.IotCmdEncoderPlus;
import com.ruoyi.iot.service.IIotCmdDecodeService;
import com.ruoyi.iot.service.impl.IotCmdDecodeServiceImpl;
import com.ruoyi.iot.service.impl.IotCmdEncoderServiceImpl;
import com.ruoyi.iot.service.impl.IotCmdServiceImpl;
import com.ruoyi.iot.service.impl.IotProductFieldServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 指令解析Controller
 *
 * @author ruoyi
 * @date 2022-09-14
 */
@RestController
@RequestMapping("/cmdDecode")
public class IotCmdDecodeController extends BaseController
{
    @Autowired
    private IIotCmdDecodeService iotCmdDecodeService;

    @Autowired
    private IotCmdServiceImpl iotCmdService;

    @Autowired
    private IotProductFieldServiceImpl iotProductFieldService;



    /**
     * 查询指令解析列表
     */
    @RequiresPermissions("iot:cmdDecode:list")
    @GetMapping("/list")
    public CmdDecoderVoInfo  list(IotCmdDecode iotCmdDecode)
    {
        CmdDecoderVoInfo cmdDecoderVoInfo = new CmdDecoderVoInfo();
        startPage();
        List<IotCmdDecode> list = iotCmdDecodeService.selectIotCmdDecodeList(iotCmdDecode);

        List<IotCmdDecodePlus> plusList = new ArrayList<>();

        for(IotCmdDecode item : list) {
            IotCmdDecodePlus iotCmdDecoderPlus = new IotCmdDecodePlus();
            BeanUtils.copyProperties(item, iotCmdDecoderPlus);
            iotCmdDecoderPlus.setCmdName(iotCmdService.selectIotCmdById(item.getCmdId()).getName());
            iotCmdDecoderPlus.setFieldName(iotProductFieldService.selectIotProductFieldById(item.getFieldId()).getName());
            plusList.add(iotCmdDecoderPlus);
        }

        cmdDecoderVoInfo.setCode(200);

        cmdDecoderVoInfo.setCmdList(iotCmdService.selectIotCmdList(new IotCmd()));
        cmdDecoderVoInfo.setFieldList(iotProductFieldService.selectIotProductFieldList(new IotProductField()));

        cmdDecoderVoInfo.setMsg("查询成功");
        cmdDecoderVoInfo.setRows(plusList);
        cmdDecoderVoInfo.setTotal((new PageInfo(list)).getTotal());


        return cmdDecoderVoInfo;
    }

    /**
     * 查询指令解析列表
     */
    @PostMapping("/query/list")
    public IotWrapperDTO<List<IotCmdDecode>> queryList(IotCmdDecode iotCmdDecode)
    {
        IotWrapperDTO iotListDTO = new IotWrapperDTO();
        List<IotCmdDecode> list = iotCmdDecodeService.selectIotCmdDecodeList(iotCmdDecode);
        iotListDTO.setData(list);
        return iotListDTO;
    }



    /**
     * 导出指令解析列表
     */
    @RequiresPermissions("iot:cmdDecode:export")
    @Log(title = "指令解析", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, IotCmdDecode iotCmdDecode)
    {
        List<IotCmdDecode> list = iotCmdDecodeService.selectIotCmdDecodeList(iotCmdDecode);
        ExcelUtil<IotCmdDecode> util = new ExcelUtil<IotCmdDecode>(IotCmdDecode.class);
        util.exportExcel(response, list, "指令解析数据");
    }

    /**
     * 获取指令解析详细信息
     */
    @RequiresPermissions("iot:cmdDecode:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(iotCmdDecodeService.selectIotCmdDecodeById(id));
    }

    /**
     * 新增指令解析
     */
    @RequiresPermissions("iot:cmdDecode:add")
    @Log(title = "指令解析", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody IotCmdDecode iotCmdDecode)
    {
        return toAjax(iotCmdDecodeService.insertIotCmdDecode(iotCmdDecode));
    }

    /**
     * 修改指令解析
     */
    @RequiresPermissions("iot:cmdDecode:edit")
    @Log(title = "指令解析", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody IotCmdDecode iotCmdDecode)
    {
        return toAjax(iotCmdDecodeService.updateIotCmdDecode(iotCmdDecode));
    }

    /**
     * 删除指令解析
     */
    @RequiresPermissions("iot:cmdDecode:remove")
    @Log(title = "指令解析", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(iotCmdDecodeService.deleteIotCmdDecodeByIds(ids));
    }
}
