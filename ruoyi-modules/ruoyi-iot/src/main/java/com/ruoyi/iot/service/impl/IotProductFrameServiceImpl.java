package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.IotProductFrameMapper;
import com.ruoyi.iot.domain.IotProductFrame;
import com.ruoyi.iot.service.IIotProductFrameService;

/**
 * 产品-数据帧Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
@Service
@Primary
public class IotProductFrameServiceImpl implements IIotProductFrameService 
{
    @Autowired
    private IotProductFrameMapper iotProductFrameMapper;

    /**
     * 查询产品-数据帧
     * 
     * @param id 产品-数据帧主键
     * @return 产品-数据帧
     */
    @Override
    public IotProductFrame selectIotProductFrameById(Long id)
    {
        return iotProductFrameMapper.selectIotProductFrameById(id);
    }

    /**
     * 查询产品-数据帧列表
     * 
     * @param iotProductFrame 产品-数据帧
     * @return 产品-数据帧
     */
    @Override
    public List<IotProductFrame> selectIotProductFrameList(IotProductFrame iotProductFrame)
    {
        return iotProductFrameMapper.selectIotProductFrameList(iotProductFrame);
    }

    /**
     * 新增产品-数据帧
     * 
     * @param iotProductFrame 产品-数据帧
     * @return 结果
     */
    @Override
    public int insertIotProductFrame(IotProductFrame iotProductFrame)
    {
        return iotProductFrameMapper.insertIotProductFrame(iotProductFrame);
    }

    /**
     * 修改产品-数据帧
     * 
     * @param iotProductFrame 产品-数据帧
     * @return 结果
     */
    @Override
    public int updateIotProductFrame(IotProductFrame iotProductFrame)
    {
        return iotProductFrameMapper.updateIotProductFrame(iotProductFrame);
    }

    /**
     * 批量删除产品-数据帧
     * 
     * @param ids 需要删除的产品-数据帧主键
     * @return 结果
     */
    @Override
    public int deleteIotProductFrameByIds(Long[] ids)
    {
        return iotProductFrameMapper.deleteIotProductFrameByIds(ids);
    }

    /**
     * 删除产品-数据帧信息
     * 
     * @param id 产品-数据帧主键
     * @return 结果
     */
    @Override
    public int deleteIotProductFrameById(Long id)
    {
        return iotProductFrameMapper.deleteIotProductFrameById(id);
    }
}
