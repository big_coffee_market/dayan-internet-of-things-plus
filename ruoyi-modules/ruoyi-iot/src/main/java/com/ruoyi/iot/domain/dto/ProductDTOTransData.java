package com.ruoyi.iot.domain.dto;

import lombok.Data;

@Data
public class ProductDTOTransData extends TransFrameDataDTO {

    /**
     * 帧id
     */
    Long productId;


}
