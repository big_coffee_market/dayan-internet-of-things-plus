package com.ruoyi.iot.exchange.network.tcp.handler.impl;

import com.ruoyi.iot.domain.IotFrame;
import com.ruoyi.iot.exchange.network.tcp.handler.dispatch.TCPDispatchHandler;
import com.ruoyi.iot.exchange.util.HexToAsciiUtils;
import com.zxq.factory.decoder.impl.IntDecoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.xmlbeans.impl.util.HexBin;

@Slf4j
public class AsciiDataDispatchHandler extends TCPDispatchHandler {


    public AsciiDataDispatchHandler(IotFrame iotFrame) {
        super(iotFrame);
    }



    @Override
    protected byte[] patternHex(byte[] frame) {
        String asciiText = HexBin.bytesToString(frame);
        log.info("asciiText:" + asciiText);
        String hexText = HexToAsciiUtils.asciiToHex(asciiText);
        log.info("hexText:" + hexText);
        byte[] hexFrame =  HexBin.stringToBytes(hexText);

        return hexFrame;
    }


    @Override
    protected String takeDevId(byte[] frame) {
        return null;
    }



   IntDecoder intDecoder = new IntDecoder();

    @Override
    protected Integer getDataLen(byte[] dataLenBuff) {
        String asciiText = HexBin.bytesToString(dataLenBuff);
        log.info("len asciiText:" + asciiText);
        String hexData = HexToAsciiUtils.asciiToHex(asciiText);
        log.info("len hexText:" + hexData);
        byte[] hexBuff = HexBin.stringToBytes(hexData);
        Integer dataLen =  intDecoder.decoder(hexBuff);
        log.info("len:" + dataLen);
        return dataLen;
    }


}
