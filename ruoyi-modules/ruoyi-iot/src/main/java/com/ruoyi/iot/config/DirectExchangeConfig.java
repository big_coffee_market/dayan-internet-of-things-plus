package com.ruoyi.iot.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RefreshScope
public class DirectExchangeConfig {


	@Value("${spring.rabbitmq.template.exchange}")
	String exchange;

	@Value("${spring.rabbitmq.template.tcp-queue}")
	String network_queue;
	@Value("${spring.rabbitmq.template.tcp-route-key}")
	String network_route_key;

	@Value("${spring.rabbitmq.template.http-queue}")
	String web_queue;
	@Value("${spring.rabbitmq.template.http-route-key}")
	String web_route_key;



	@Bean
 	public DirectExchange directExchange(){
		DirectExchange directExchange = new DirectExchange(exchange);
 		return directExchange;
 	}
	
	@Bean
    public Queue directWebQueue() {
       Queue queue = new Queue(web_queue);
       return queue;
    }

 	
 	@Bean
    public Queue directNetworkQueue() {
       Queue queue = new Queue(network_queue);
       return queue;
    }



 	//3个binding将交换机和相应队列连起来
 	@Bean
 	public Binding bindingWeb(){
 		Binding binding = BindingBuilder.bind(directWebQueue()).to(directExchange()).with( web_route_key);
 		return binding;
 	}

 	
 	@Bean
 	public Binding bindingNetwork(){
 		Binding binding = BindingBuilder.bind(directNetworkQueue()).to(directExchange()).with(network_route_key);
 		return binding;
 	}



 	
 	
 	
}
