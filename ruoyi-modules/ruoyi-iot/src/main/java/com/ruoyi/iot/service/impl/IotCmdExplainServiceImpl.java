package com.ruoyi.iot.service.impl;


import com.ruoyi.iot.domain.IotCmdExplain;
import com.ruoyi.iot.mapper.IotCmdExplainMapper;
import com.ruoyi.iot.service.IIotCmdExplainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 指令解释Service业务层处理
 * 
 * @author zxq
 * @date 2022-09-26
 */
@Service
@Primary
public class IotCmdExplainServiceImpl implements IIotCmdExplainService
{
    @Autowired
    private IotCmdExplainMapper iotCmdExplainMapper;

    /**
     * 查询指令解释
     * 
     * @param id 指令解释主键
     * @return 指令解释
     */
    @Override
    public IotCmdExplain selectIotCmdExplainById(Long id)
    {
        return iotCmdExplainMapper.selectIotCmdExplainById(id);
    }

    /**
     * 查询指令解释列表
     * 
     * @param iotCmdExplain 指令解释
     * @return 指令解释
     */
    @Override
    public List<IotCmdExplain> selectIotCmdExplainList(IotCmdExplain iotCmdExplain)
    {
        return iotCmdExplainMapper.selectIotCmdExplainList(iotCmdExplain);
    }

    /**
     * 新增指令解释
     * 
     * @param iotCmdExplain 指令解释
     * @return 结果
     */
    @Override
    public int insertIotCmdExplain(IotCmdExplain iotCmdExplain)
    {
        return iotCmdExplainMapper.insertIotCmdExplain(iotCmdExplain);
    }

    /**
     * 修改指令解释
     * 
     * @param iotCmdExplain 指令解释
     * @return 结果
     */
    @Override
    public int updateIotCmdExplain(IotCmdExplain iotCmdExplain)
    {
        return iotCmdExplainMapper.updateIotCmdExplain(iotCmdExplain);
    }

    /**
     * 批量删除指令解释
     * 
     * @param ids 需要删除的指令解释主键
     * @return 结果
     */
    @Override
    public int deleteIotCmdExplainByIds(Long[] ids)
    {
        return iotCmdExplainMapper.deleteIotCmdExplainByIds(ids);
    }

    /**
     * 删除指令解释信息
     * 
     * @param id 指令解释主键
     * @return 结果
     */
    @Override
    public int deleteIotCmdExplainById(Long id)
    {
        return iotCmdExplainMapper.deleteIotCmdExplainById(id);
    }
}
