package com.ruoyi.iot.domain.vo.weixin.field;

import lombok.Data;

@Data
public class WxCmdFieldInnerDetail {

    /**
     * 字段名
     */
    String fieldName;

    /**
     * 字段Key
     */
    String fieldKey;

    /**
     * 字段提示
     */
    String remark;

    /**
     * 默认值，请求数据返回时需要
     */
    Object value;


}


