package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.IotCmd;

/**
 * 指令Mapper接口
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
public interface IotCmdMapper 
{
    /**
     * 查询指令
     * 
     * @param id 指令主键
     * @return 指令
     */
    public IotCmd selectIotCmdById(Long id);

    /**
     * 查询指令列表
     * 
     * @param iotCmd 指令
     * @return 指令集合
     */
    public List<IotCmd> selectIotCmdList(IotCmd iotCmd);

    /**
     * 新增指令
     * 
     * @param iotCmd 指令
     * @return 结果
     */
    public int insertIotCmd(IotCmd iotCmd);

    /**
     * 修改指令
     * 
     * @param iotCmd 指令
     * @return 结果
     */
    public int updateIotCmd(IotCmd iotCmd);

    /**
     * 删除指令
     * 
     * @param id 指令主键
     * @return 结果
     */
    public int deleteIotCmdById(Long id);

    /**
     * 批量删除指令
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteIotCmdByIds(Long[] ids);
}
