package com.ruoyi.iot.domain.vo.weixin.element;

import lombok.Data;

import java.util.Objects;

public class WxProductSegments {

    /**
     * 产品id
     */
    Long productId;

    /**
     * 名称
     */
    String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WxProductSegments)) return false;
        WxProductSegments that = (WxProductSegments) o;
        return Objects.equals(productId, that.productId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId);
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
