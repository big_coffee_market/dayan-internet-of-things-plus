package com.ruoyi.iot.rabbitmq;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import com.ruoyi.iot.channel.SynchroniseMessage;
import com.ruoyi.iot.channel.device.DeviceMessageServiceImpl;
import com.ruoyi.iot.channel.translator.impl.TransformServiceImpl;
import com.ruoyi.iot.domain.dto.ExchangeDTO;
import com.ruoyi.iot.domain.vo.enums.EPlatformCmdType;
import com.ruoyi.iot.domain.vo.enums.ESponsor;
import com.ruoyi.iot.domain.vo.DevCmdVo;
import com.ruoyi.iot.domain.*;
import com.ruoyi.iot.exchange.network.tcp.cache.TcpChannelCache;
import com.ruoyi.iot.service.impl.*;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.xmlbeans.impl.util.HexBin;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.ByteBuffer;
import java.util.List;

/**
 * 服务调用的总入口
 */
@Component
@Slf4j
public class ExchangeRabbitHelper  {

    /**
     * 设备信息处理器
     */
    @Autowired
    DeviceMessageServiceImpl deviceMessageService;

    /**
     * 设备信息服务
     */
    @Autowired
    IotDeviceServiceImpl iotDeviceService;

    /**
     * 产品信息服务
     */
    @Autowired
    IotProductServiceImpl iotProductService;

    /**
     * 指令服务
     */
    @Autowired
    IotCmdServiceImpl iotCmdService;


    /**
     * 产品筛选服务
     */
    @Autowired
    IotProductFilterServiceImpl productFilterService;


    /**
     * 产品字段服务
     */
    @Autowired
    IotProductFieldServiceImpl productFieldService;

    /**
     * 智能设备写入mongo服务
     */
    @Autowired
    IotFilterMongoServiceImpl iotFilterMongoService;

    /**
     * 同步消息服务
     */
    @Autowired
    SynchroniseMessage synchroniseMessage;

    /**
     * 消息转换服务
     */
    @Autowired
    TransformServiceImpl transformService;

    /**
     * rabbit发送消息
     */
    @Autowired
    RabbitTemplate rabbitTemplate;


    static ExchangeRabbitHelper exchangeRabbitHelper;

    public static ExchangeRabbitHelper getInstance(){
        return exchangeRabbitHelper;
    }

    @PostConstruct
    public void initFactory(){
        exchangeRabbitHelper = this;
    }


    @RabbitListener(queues = "${spring.rabbitmq.template.http-queue}")
    public void receiveWebChannel(Message message, Channel channel) throws Exception {
        String exchangeJson =new String(message.getBody());
        log.info("http-message:" + exchangeJson);
        ExchangeDTO exchangeDTO = JSON.parseObject(exchangeJson, ExchangeDTO.class);
        log.info("prepare notify web => " + exchangeDTO.toString());
        IotCmd iotCmd = queryDevice(exchangeDTO);
        String devNum = exchangeDTO.getDevId();
        if(iotCmd != null) {
            EPlatformCmdType ePlatformCmdType = EPlatformCmdType.getType(iotCmd.getCmdType().intValue());
            log.info("platform type => " + ePlatformCmdType.toString());
            switch (ePlatformCmdType){
                case CMD:{
                    deviceMessageService.deviceReport(exchangeDTO);
                    DevCmdVo devCmdVo = synchroniseMessage.waitCmdMessage(devNum,iotCmd.getResultId());
                    ExchangeDTO exchangeDTOResult = transformService.platTransform(devCmdVo);
                    //设备上报请求返回结果，取到数据后，走mq通道，返回到设备
                    webPublishToMQ(exchangeDTOResult);
                }
                break;
                case NOTICE: {
                    deviceMessageService.deviceReport(exchangeDTO);
                }
                    break;
                case RESULT:{
                    deviceMessageService.deviceResult(exchangeDTO);
                }
                    break;
                case NOTICE_OR_RESULT:{
                    Boolean isExist = synchroniseMessage.isExistBlockKey(devNum,iotCmd.getId());
                    if(isExist) {
                        deviceMessageService.deviceResult(exchangeDTO);
                    } else {
                        deviceMessageService.deviceReport(exchangeDTO);
                    }
                }
                    break;
            }
        }
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
    }


    @RabbitListener(queues = "${spring.rabbitmq.template.tcp-queue}")
    public void receiveTcpMessage(Message message, Channel channel) throws Exception {
        String exchangeJson =new String(message.getBody());


        ExchangeDTO exchangeDTO = JSON.parseObject(exchangeJson, ExchangeDTO.class);
       // log.info("tcp-message:" + exchangeJson);
        log.info("publish-dev: " + HexBin.bytesToString(exchangeDTO.getData()));
        //#900301650008496E
        ChannelHandlerContext nettyChannel = TcpChannelCache.getChannel(exchangeDTO.getDevId());
        if( nettyChannel != null) {

            ByteBuf byteBuf = Unpooled.copiedBuffer(exchangeDTO.getData());
            nettyChannel.pipeline().writeAndFlush(byteBuf);
            log.info("publish-dev: end!");
        } else {
            log.info("dev:%s offline",exchangeDTO.getDevId());
        }

        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
    }

    /**
     * 处理通知类型得tcp数据
     * @param exchangeDTO
     */
    private void tcpHandlerNotice(ExchangeDTO exchangeDTO) {
        DevCmdVo devCmdVo = transformService.devTransform(exchangeDTO);
        Long cmdId = devCmdVo.getCmdId();
        IotProductFilter queryFilter = new IotProductFilter();
        queryFilter.setCmdId(cmdId);
        queryFilter.setEnable(1L);
        List<IotProductFilter> productFilters = productFilterService.selectIotProductFilterList(queryFilter);
        log.info("product filter:" + JSON.toJSONString(productFilters));
        if(productFilters.size() != 0) {
            for(IotProductFilter item: productFilters) {
                 writeMongoMessage(item,devCmdVo);
            }
        }
    }

    /**
     * 写入指令数据
     * @param item 数据
     * @param devCmdVo 交换数据
     */
    private void writeMongoMessage(IotProductFilter item, DevCmdVo devCmdVo) {
          String[] filedIds =   item.getFileds().split(",");
          JSONObject env_json = new JSONObject();
          JSONObject src_json = devCmdVo.getData();
          for(String fieldId : filedIds) {
             String filedKey =  productFieldService.selectIotProductFieldById(Long.parseLong(fieldId)).getField();
             env_json.put(filedKey,src_json.get(filedKey));
          }
          iotFilterMongoService.insertMongo(item.getId(),devCmdVo.getDevId(),env_json);
    }

    /**
     * 处理result得指令
     * @param exchangeDTO
     * @throws Exception
     */
    private void tcpHandlerResult(ExchangeDTO exchangeDTO) throws Exception {
        String devNum = exchangeDTO.getDevId();
        String recFormat = "devId:%s frame:%s";
        byte[] data = exchangeDTO.getData();
        String publishMsg = String.format(recFormat,devNum, HexBin.bytesToString(data));
        log.info("publish to device:" + publishMsg);
        ByteBuf buffer = Unpooled.copiedBuffer(data);

        ChannelHandlerContext nettyChannel = TcpChannelCache.getChannel(exchangeDTO.getDevId());
        if( nettyChannel != null) {
            nettyChannel.pipeline().writeAndFlush(buffer);
        } else {
            log.info("dev:%s offline",exchangeDTO.getDevId());
        }
    }



    /**
     * 发送信息到rabbitmq中处理
     * @param exchangeDTO
     */
    public void webPublishToMQ(ExchangeDTO exchangeDTO) {
         String webMQRoutKey =  exchangeDTO.getWebToMqRouteKey();
         String jsonStr = JSON.toJSONString(exchangeDTO);
         log.info(String.format("rabbit: RouteKey:%s  Message:%s",webMQRoutKey,jsonStr));

         rabbitTemplate.convertAndSend(webMQRoutKey,jsonStr.getBytes());
    }

    /**
     * 设备发送信息到MQ中，tcp通道信息调用
     * @param exchangeDTO tcp通道信息调用
     */
    public void devPublishToMQ(ExchangeDTO exchangeDTO) {
        String devMQRouteKey = exchangeDTO.getDeviceToMqRouteKey();
        String jsonStr = JSON.toJSONString(exchangeDTO);
        log.info(String.format("devPublishMQ: RouteKey:%s  Message:%s",devMQRouteKey,jsonStr));
        rabbitTemplate.convertAndSend(devMQRouteKey,jsonStr.getBytes());
    }


    /**
     * 根据交换得数据模型得到对应得指令数据
     * @param exchangeDTO 交换机dto
     * @return
     */
    private IotCmd queryDevice(ExchangeDTO exchangeDTO) {

        String devNum = exchangeDTO.getDevId();
        String cmdHex = exchangeDTO.getCmdHex();
        IotDevice iotDevice = iotDeviceService.selectIotDeviceByDevNum(devNum);
        Long productId = iotDevice.getProductId();

        IotCmd iotCmdCondition = new IotCmd();
        iotCmdCondition.setProductId(productId);
        iotCmdCondition.setSponsor(ESponsor.device.getValue());
        iotCmdCondition.setCmdHex(cmdHex);
        List<IotCmd> iotCmds = iotCmdService.selectIotCmdList(iotCmdCondition);
        //log.info(String.format("find dev cmd size:%s!",iotCmds.size()));
        if(iotCmds.size() != 1) {
           // log.info("repeat:" + JSON.toJSONString(iotCmds));
            log.info(String.format("dev:%s cmd config empty!",devNum));
            return null;
        } else {
            IotCmd iotCmd =  iotCmds.get(0);
            //log.info("query cmd:" + JSON.toJSONString(iotCmd));
            return iotCmd;
        }

    }



}
