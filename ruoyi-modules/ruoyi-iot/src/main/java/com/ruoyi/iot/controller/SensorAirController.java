package com.ruoyi.iot.controller;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.iot.domain.mongo.DeviceSensorMO;

import com.ruoyi.iot.service.impl.AirSensorServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@Slf4j
@RequestMapping("/air_sensor")
public class SensorAirController extends BaseController {

    /**
     * 空气传感器服务
     */
    @Autowired
    AirSensorServiceImpl airSensorService;


    @GetMapping("/query_tree")
    public AjaxResult queryDevTree() {

        return AjaxResult.success(airSensorService.queryDevTree());
    }

    @GetMapping("/air_sensor")
    public AjaxResult queryAirSensorVO() {

        return AjaxResult.success(airSensorService.queryAirSensorVO());
    }

    @GetMapping("/actual_sensor")
    public AjaxResult queryActualSensorVO(@RequestParam("devId") String devId) {

        return AjaxResult.success(airSensorService.queryActualSensorVO(devId));
    }

    @GetMapping("/history_sensor")
    public TableDataInfo queryHistorySensorVO(@RequestParam("devId") String devId,
                                              @RequestParam("page") Long page,
                                              @RequestParam("pageNum") Long pageNum) {

        TableDataInfo tableDataInfo  = new TableDataInfo();
        List<DeviceSensorMO> dataNum = airSensorService.queryHistorySensorVO(devId,page,pageNum);
        tableDataInfo.setCode(200);
        tableDataInfo.setMsg("success");
        tableDataInfo.setRows(dataNum);
        Long total = airSensorService.queryHistorySensorNum(devId);
        tableDataInfo.setTotal(total);

        return tableDataInfo;
    }


    @GetMapping("/current_sensor")
    public AjaxResult queryCurrentSensorVO(@RequestParam("devId")String devId,
                                           @RequestParam("interval")Integer interval,
                                           @RequestParam("numbs")Integer numbs) {
        log.info("devId:" + devId + " interval:" + interval + " numbs:" + numbs);
        List<DeviceSensorMO> list = airSensorService.queryCurrentSensorVO(devId,interval,numbs);

        log.info("current_sensor:" + JSON.toJSONString(list));

        return AjaxResult.success(list);
    }


}
