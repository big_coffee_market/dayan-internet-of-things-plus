package com.ruoyi.iot.domain.vo;

import com.ruoyi.common.core.web.page.TableDataInfo;

import com.ruoyi.iot.domain.IotFrame;
import com.ruoyi.iot.domain.IotProduct;
import com.ruoyi.iot.domain.IotProject;
import lombok.Data;

import java.util.List;

@Data
public class DeviceVoInfo extends TableDataInfo {

    private List<IotProduct> productList;

    private List<IotProject> projectList;

    private List<IotFrame> frameList;


}
