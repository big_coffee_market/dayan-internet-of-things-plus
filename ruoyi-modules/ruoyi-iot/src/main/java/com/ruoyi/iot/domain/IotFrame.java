package com.ruoyi.iot.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.iot.exchange.model.frame.IBaseDesc;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 厂家协议对象 iot_frame
 * 
 * @author ruoyi
 * @date 2022-12-02
 */
public class IotFrame extends BaseEntity implements IBaseDesc
{
    private static final long serialVersionUID = 1L;

    /** 协议帧id */
    @Excel(name = "协议帧id")
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 帧类型 */
    @Excel(name = "帧类型")
    private Long type;

    /** 预读长度 */
    @Excel(name = "预读长度")
    private Long preReadLen;

    /** 协议标志 */
    @Excel(name = "协议标志")
    private String markFlag;

    /** 协议标志下标 */
    @Excel(name = "协议标志下标")
    private String markIndexList;

    /** 帧长度 */
    @Excel(name = "帧长度")
    private Long len;

    /** 帧长度下标 */
    @Excel(name = "帧长度下标")
    private String lenIndexList;

    /** 其他长度 */
    @Excel(name = "其他长度")
    private Long otherLen;

    /** 数据左边的索引 */
    @Excel(name = "数据左边的索引")
    private Long dataLeftVector;

    /** 数据右边的索引 */
    @Excel(name = "数据右边的索引")
    private Long dataRightVector;

    /** 数据类型 */
    @Excel(name = "数据类型")
    private Long fieldType;

    /** 创建者 */
    @Excel(name = "创建者")
    private Long creator;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 更新者 */
    @Excel(name = "更新者")
    private Long updater;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updateDate;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setPreReadLen(Long preReadLen) 
    {
        this.preReadLen = preReadLen;
    }

    public Long getPreReadLen() 
    {
        return preReadLen;
    }
    public void setMarkFlag(String markFlag) 
    {
        this.markFlag = markFlag;
    }

    public String getMarkFlag() 
    {
        return markFlag;
    }
    public void setMarkIndexList(String markIndexList) 
    {
        this.markIndexList = markIndexList;
    }

    public String getMarkIndexList() 
    {
        return markIndexList;
    }
    public void setLen(Long len) 
    {
        this.len = len;
    }

    public Long getLen() 
    {
        return len;
    }
    public void setLenIndexList(String lenIndexList) 
    {
        this.lenIndexList = lenIndexList;
    }

    public String getLenIndexList() 
    {
        return lenIndexList;
    }
    public void setOtherLen(Long otherLen) 
    {
        this.otherLen = otherLen;
    }

    public Long getOtherLen() 
    {
        return otherLen;
    }
    public void setDataLeftVector(Long dataLeftVector) 
    {
        this.dataLeftVector = dataLeftVector;
    }

    public Long getDataLeftVector() 
    {
        return dataLeftVector;
    }
    public void setDataRightVector(Long dataRightVector) 
    {
        this.dataRightVector = dataRightVector;
    }

    public Long getDataRightVector() 
    {
        return dataRightVector;
    }
    public void setFieldType(Long fieldType) 
    {
        this.fieldType = fieldType;
    }

    public Long getFieldType() 
    {
        return fieldType;
    }
    public void setCreator(Long creator) 
    {
        this.creator = creator;
    }

    public Long getCreator() 
    {
        return creator;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setUpdater(Long updater) 
    {
        this.updater = updater;
    }

    public Long getUpdater() 
    {
        return updater;
    }
    public void setUpdateDate(Date updateDate) 
    {
        this.updateDate = updateDate;
    }

    public Date getUpdateDate() 
    {
        return updateDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("type", getType())
            .append("preReadLen", getPreReadLen())
            .append("markFlag", getMarkFlag())
            .append("markIndexList", getMarkIndexList())
            .append("len", getLen())
            .append("lenIndexList", getLenIndexList())
            .append("otherLen", getOtherLen())
            .append("dataLeftVector", getDataLeftVector())
            .append("dataRightVector", getDataRightVector())
            .append("fieldType", getFieldType())
            .append("remark", getRemark())
            .append("creator", getCreator())
            .append("createDate", getCreateDate())
            .append("updater", getUpdater())
            .append("updateDate", getUpdateDate())
            .toString();
    }
}
