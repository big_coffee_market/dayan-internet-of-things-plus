package com.ruoyi.iot.domain.vo.weixin;

import com.ruoyi.iot.domain.vo.weixin.element.WxDeviceDetail;
import com.ruoyi.iot.domain.vo.weixin.element.WxProductSegments;
import lombok.Data;

import java.util.List;

@Data
public class WxDeviceVO {

    List<WxProductSegments> wxProductSegments;

    List<WxDeviceDetail> wxDeviceDetails;

}
