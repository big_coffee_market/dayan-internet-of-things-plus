package com.ruoyi.iot.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 指令对象 iot_cmd
 *
 * @author ruoyi
 * @date 2022-12-27
 */
public class IotCmd extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    @Excel(name = "id")
    private Long id;

    /** 产品id */
    @Excel(name = "产品id")
    private Long productId;

    @Excel(name = "协议id")
    private Long protocolId;

    /** 回复的cmdid，用来配对消息收发 */
    @Excel(name = "回复的cmdid")
    private Long resultId;

    /** 指令名称 */
    @Excel(name = "指令名称")
    private String name;

    /** 发起方 */
    @Excel(name = "发起方")
    private Long sponsor;

    /** 指令类型*/
    @Excel(name = "指令类型")
    private Long cmdType;

    /** 指令码标识 */
    @Excel(name = "指令码标识")
    private String cmdHex;

    /** 创建者 */
    @Excel(name = "创建者")
    private Long creator;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 更新者 */
    @Excel(name = "更新者")
    private Long updater;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updateDate;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setProductId(Long productId)
    {
        this.productId = productId;
    }

    public Long getProductId()
    {
        return productId;
    }
    public void setResultId(Long resultId)
    {
        this.resultId = resultId;
    }

    public Long getResultId()
    {
        return resultId;
    }

    public Long getProtocolId() {
        return protocolId;
    }

    public void setProtocolId(Long protocolId) {
        this.protocolId = protocolId;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setSponsor(Long sponsor)
    {
        this.sponsor = sponsor;
    }

    public Long getSponsor()
    {
        return sponsor;
    }
    public void setCmdType(Long cmdType)
    {
        this.cmdType = cmdType;
    }

    public Long getCmdType()
    {
        return cmdType;
    }
    public void setCmdHex(String cmdHex)
    {
        this.cmdHex = cmdHex;
    }

    public String getCmdHex()
    {
        return cmdHex;
    }
    public void setCreator(Long creator)
    {
        this.creator = creator;
    }

    public Long getCreator()
    {
        return creator;
    }
    public void setCreateDate(Date createDate)
    {
        this.createDate = createDate;
    }

    public Date getCreateDate()
    {
        return createDate;
    }
    public void setUpdater(Long updater)
    {
        this.updater = updater;
    }

    public Long getUpdater()
    {
        return updater;
    }
    public void setUpdateDate(Date updateDate)
    {
        this.updateDate = updateDate;
    }

    public Date getUpdateDate()
    {
        return updateDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productId", getProductId())
            .append("protocolId",getProtocolId())
            .append("resultId", getResultId())
            .append("name", getName())
            .append("sponsor", getSponsor())
            .append("cmdType", getCmdType())
            .append("cmdHex", getCmdHex())
            .append("remark", getRemark())
            .append("creator", getCreator())
            .append("createDate", getCreateDate())
            .append("updater", getUpdater())
            .append("updateDate", getUpdateDate())
            .toString();
    }
}
