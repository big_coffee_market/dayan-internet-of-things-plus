package com.ruoyi.iot.service.impl;


import com.ruoyi.iot.domain.IotDevice;
import com.ruoyi.iot.domain.IotProject;
import com.ruoyi.iot.domain.mongo.DeviceSensorMO;
import com.ruoyi.iot.domain.vo.AirSensorVO;
import com.ruoyi.iot.domain.vo.TreeVO;
import com.ruoyi.iot.service.IAirSensorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AirSensorServiceImpl implements IAirSensorService {

    /**
     * mongo展示数据
     */
    @Autowired
    IotFilterMongoServiceImpl iotFilterMongoService;

    /**
     * 项目服务
     */
    @Autowired
    IotProjectServiceImpl iotProjectService;

    /**
     * 设备服务
     */
    @Autowired
    IotDeviceServiceImpl iotDeviceService;

    /**
     * 产品服务协议
     */
    @Autowired
    IotProductServiceImpl productService;


    Long productId = 10L;

    @Override
    public TreeVO queryDevTree() {

        List<IotProject> projects = iotProjectService.selectIotProjectList(new IotProject());
        TreeVO treeVO = new TreeVO();
        List<TreeVO.Node> childs = new ArrayList<>();
        treeVO.setData(childs);
        IotDevice  query  = new IotDevice();
        query.setProductId(productId);
        for(IotProject item: projects) {
            TreeVO.Node child = new TreeVO.Node();
            childs.add(child);
            Long projectId = item.getId();
            child.setLabel(item.getName());
            child.setId(projectId + "");
            child.setLevel(1);
            query.setProjectId(projectId);

            List<TreeVO.Node> grandsons = new ArrayList<>();
            child.setChildren(grandsons);
            List<IotDevice> devices = iotDeviceService.selectIotDeviceList(query);
            if(devices.size() == 0) {
                childs.remove(child);
                continue;
            }
            for(IotDevice device: devices) {
              TreeVO.Node grandson = new TreeVO.Node();
              grandson.setLabel(device.getName());
              grandson.setParentId(projectId + "");
              grandson.setLabel(device.getName());
              grandson.setLevel(2);
              grandson.setId(device.getId() + "");
              grandsons.add(grandson);
            }

        }

        return treeVO;
    }

    @Override
    public AirSensorVO queryAirSensorVO() {

         String preImage =  productService.selectIotProductById(productId).getImage();

         AirSensorVO airSensorVO = new AirSensorVO();
         airSensorVO.setPreImage(preImage);

        return airSensorVO;
    }

    @Override
    public DeviceSensorMO queryActualSensorVO(String devId) {
        IotDevice iotDevice = iotDeviceService.selectIotDeviceById(Long.parseLong(devId));
        if(iotDevice == null)
            return null;

        return iotFilterMongoService.queryActualInfo(iotDevice.getDevId(),6L);
    }


    @Override
    public List<DeviceSensorMO> queryHistorySensorVO(String devId,Long page, Long pageNum) {

        IotDevice iotDevice = iotDeviceService.selectIotDeviceById(Long.parseLong(devId));
        if(iotDevice == null)
            return null;

        return iotFilterMongoService.queryHistoryInfo(iotDevice.getDevId(),6L,page,pageNum);
    }

    @Override
    public Long queryHistorySensorNum(String devId) {
        IotDevice iotDevice = iotDeviceService.selectIotDeviceById(Long.parseLong(devId));
        if(iotDevice == null)
            return null;
        Long total = iotFilterMongoService.queryHistoryTotal(iotDevice.getDevId(),6L);
        return total;
    }

    @Override
    public List<DeviceSensorMO> queryCurrentSensorVO(String devId, Integer interval, Integer numbs) {
        IotDevice iotDevice = iotDeviceService.selectIotDeviceById(Long.parseLong(devId));
        if(iotDevice == null)
            return null;
        return iotFilterMongoService.queryCurrentSensorVO(iotDevice.getDevId(),6L,interval,numbs);
    }


}
