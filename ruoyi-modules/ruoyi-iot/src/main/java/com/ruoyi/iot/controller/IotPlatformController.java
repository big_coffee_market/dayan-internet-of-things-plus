package com.ruoyi.iot.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.iot.domain.IotPlatform;
import com.ruoyi.iot.service.IIotPlatformService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 平台信息Controller
 * 
 * @author ruoyi
 * @date 2022-12-08
 */
@RestController
@RequestMapping("/platform")
public class IotPlatformController extends BaseController
{
    @Autowired
    private IIotPlatformService iotPlatformService;

    /**
     * 查询平台信息列表
     */
    @RequiresPermissions("iot:platform:list")
    @GetMapping("/list")
    public TableDataInfo list(IotPlatform iotPlatform)
    {
        startPage();
        List<IotPlatform> list = iotPlatformService.selectIotPlatformList(iotPlatform);
        return getDataTable(list);
    }


    /**
     * 获取全部的平台元素
     * @return
     */
    @GetMapping("/total")
    public List<IotPlatform> getTotalPages() {
        List<IotPlatform> list = iotPlatformService.selectIotPlatformList(new IotPlatform());
        return list;
    }



    /**
     * 导出平台信息列表
     */
    @RequiresPermissions("iot:platform:export")
    @Log(title = "平台信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, IotPlatform iotPlatform)
    {
        List<IotPlatform> list = iotPlatformService.selectIotPlatformList(iotPlatform);
        ExcelUtil<IotPlatform> util = new ExcelUtil<IotPlatform>(IotPlatform.class);
        util.exportExcel(response, list, "平台信息数据");
    }

    /**
     * 获取平台信息详细信息
     */
    @RequiresPermissions("iot:platform:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(iotPlatformService.selectIotPlatformById(id));
    }

    /**
     * 新增平台信息
     */
    @RequiresPermissions("iot:platform:add")
    @Log(title = "平台信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody IotPlatform iotPlatform)
    {
        return toAjax(iotPlatformService.insertIotPlatform(iotPlatform));
    }

    /**
     * 修改平台信息
     */
    @RequiresPermissions("iot:platform:edit")
    @Log(title = "平台信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody IotPlatform iotPlatform)
    {
        return toAjax(iotPlatformService.updateIotPlatform(iotPlatform));
    }

    /**
     * 删除平台信息
     */
    @RequiresPermissions("iot:platform:remove")
    @Log(title = "平台信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(iotPlatformService.deleteIotPlatformByIds(ids));
    }
}
