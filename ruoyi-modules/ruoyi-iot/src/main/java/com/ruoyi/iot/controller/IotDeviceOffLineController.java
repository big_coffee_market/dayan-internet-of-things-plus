package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.iot.domain.IotDeviceOffLine;
import com.ruoyi.iot.service.IIotDeviceOffLineService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 设备离在线记录Controller
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
@RestController
@RequestMapping("/line")
public class IotDeviceOffLineController extends BaseController
{
    @Autowired
    private IIotDeviceOffLineService iotDeviceOffLineService;

    /**
     * 查询设备离在线记录列表
     */
    @RequiresPermissions("iot:line:list")
    @GetMapping("/list")
    public TableDataInfo list(IotDeviceOffLine iotDeviceOffLine)
    {
        startPage();
        List<IotDeviceOffLine> list = iotDeviceOffLineService.selectIotDeviceOffLineList(iotDeviceOffLine);
        return getDataTable(list);
    }

    /**
     * 导出设备离在线记录列表
     */
    @RequiresPermissions("iot:line:export")
    @Log(title = "设备离在线记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, IotDeviceOffLine iotDeviceOffLine)
    {
        List<IotDeviceOffLine> list = iotDeviceOffLineService.selectIotDeviceOffLineList(iotDeviceOffLine);
        ExcelUtil<IotDeviceOffLine> util = new ExcelUtil<IotDeviceOffLine>(IotDeviceOffLine.class);
        util.exportExcel(response, list, "设备离在线记录数据");
    }

    /**
     * 获取设备离在线记录详细信息
     */
    @RequiresPermissions("iot:line:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(iotDeviceOffLineService.selectIotDeviceOffLineById(id));
    }

    /**
     * 新增设备离在线记录
     */
    @RequiresPermissions("iot:line:add")
    @Log(title = "设备离在线记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody IotDeviceOffLine iotDeviceOffLine)
    {
        return toAjax(iotDeviceOffLineService.insertIotDeviceOffLine(iotDeviceOffLine));
    }

    /**
     * 修改设备离在线记录
     */
    @RequiresPermissions("iot:line:edit")
    @Log(title = "设备离在线记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody IotDeviceOffLine iotDeviceOffLine)
    {
        return toAjax(iotDeviceOffLineService.updateIotDeviceOffLine(iotDeviceOffLine));
    }

    /**
     * 删除设备离在线记录
     */
    @RequiresPermissions("iot:line:remove")
    @Log(title = "设备离在线记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(iotDeviceOffLineService.deleteIotDeviceOffLineByIds(ids));
    }
}
