package com.ruoyi.iot.domain.vo.weixin.element;

import com.ruoyi.iot.domain.IotDevice;
import lombok.Data;

@Data
public class WxDeviceDetail {

    /**
     * id
     */
    Long id;


    /**
     * 设备编号
     */
    String devId;


    /**
     * 产品id
     */
    Long productId;

    /**
     * 设备名称
     */
    String name;


    /**
     * 是否在线
     */
    Boolean online;


    /**
     * 是否故障
     */
    Boolean isFault;


    public void parse(IotDevice iotDevice) {

        this.id = iotDevice.getId();

        this.devId = iotDevice.getDevId();

        this.productId = iotDevice.getProductId();

        this.name = iotDevice.getName();

        this.online = iotDevice.getOnline() == 1;

        this.isFault = iotDevice.getIsFault() == 0;
    }


}
