package com.ruoyi.iot.domain.vo.enums;

public enum ENetworkState {


    prepare(1),
    run(2),
    stop(3),
    unknow(-1)
    ;


    Integer value;
    ENetworkState(int i) {
        this.value = i;
    }

    public Integer getValue() {
        return value;
    }

    public static ENetworkState getType(Integer index) {
        ENetworkState state = ENetworkState.unknow;
        switch (index) {
            case 1:
                state = prepare;
                break;
            case 2:
                state = run;
                break;
            case 3:
                state = stop;
                break;
        }

        return state;
    }





}
