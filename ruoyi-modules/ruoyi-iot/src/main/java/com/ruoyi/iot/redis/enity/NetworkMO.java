package com.ruoyi.iot.redis.enity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;

@Data
public class NetworkMO {


    /**
     * 设备id
     */
    String devId;

    /**
     * 项目id
     */
    Long projectId;

    /**
     * 备注
     */
    String remark;

    /**
     * 项目名称
     */
    String projectName;

    /**
     * 在线状态
     */
    Boolean online;

    /**
     * 触发时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    Date time;
}
