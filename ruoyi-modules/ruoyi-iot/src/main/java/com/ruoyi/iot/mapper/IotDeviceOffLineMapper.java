package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.IotDeviceOffLine;

/**
 * 设备离在线记录Mapper接口
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
public interface IotDeviceOffLineMapper 
{
    /**
     * 查询设备离在线记录
     * 
     * @param id 设备离在线记录主键
     * @return 设备离在线记录
     */
    public IotDeviceOffLine selectIotDeviceOffLineById(Long id);

    /**
     * 查询设备离在线记录列表
     * 
     * @param iotDeviceOffLine 设备离在线记录
     * @return 设备离在线记录集合
     */
    public List<IotDeviceOffLine> selectIotDeviceOffLineList(IotDeviceOffLine iotDeviceOffLine);

    /**
     * 新增设备离在线记录
     * 
     * @param iotDeviceOffLine 设备离在线记录
     * @return 结果
     */
    public int insertIotDeviceOffLine(IotDeviceOffLine iotDeviceOffLine);

    /**
     * 修改设备离在线记录
     * 
     * @param iotDeviceOffLine 设备离在线记录
     * @return 结果
     */
    public int updateIotDeviceOffLine(IotDeviceOffLine iotDeviceOffLine);

    /**
     * 删除设备离在线记录
     * 
     * @param id 设备离在线记录主键
     * @return 结果
     */
    public int deleteIotDeviceOffLineById(Long id);

    /**
     * 批量删除设备离在线记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteIotDeviceOffLineByIds(Long[] ids);
}
