package com.ruoyi.iot.domain.vo.plus;

import com.ruoyi.iot.domain.IotProductFrame;
import lombok.Data;

@Data
public class IotProductFramePlus extends IotProductFrame {

    String productName;

    String frameName;

}
