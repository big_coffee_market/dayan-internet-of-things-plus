package com.ruoyi.iot.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class TreeVO {



    /**
     * 数组
     */
    List<Node> data;




    @Data
    public static class Node {

        /**
         * 身份标识
         */
        String id;

        /**
         * 父节点id
         */
        String parentId;

        /**
         * 等级 字段 3， 指令 2 ， 产品 1
         */
        Integer level;

        /**
         * 标签
         */
        String label;

        /**
         * 备注
         */
        String remark;

        /**
         * 数据
         */
        List<Node> children;

    }




}
