package com.ruoyi.iot.service.impl;

import java.util.List;

import com.ruoyi.iot.feign.provide.IotDeviceFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.IotDeviceMapper;
import com.ruoyi.iot.domain.IotDevice;
import com.ruoyi.iot.service.IIotDeviceService;

/**
 * 设备Service业务层处理
 *
 * @author ruoyi
 * @date 2022-12-27
 */
@Service
@Primary
@Slf4j
public class IotDeviceServiceImpl implements IIotDeviceService , IotDeviceFeign
{
    @Autowired
    private IotDeviceMapper iotDeviceMapper;

    /**
     * 查询设备
     *
     * @param id 设备主键
     * @return 设备
     */
    @Override
    public IotDevice selectIotDeviceById(Long id)
    {
        return iotDeviceMapper.selectIotDeviceById(id);
    }

    /**
     * 通过设备编号，查找设备
     * @param devNum 设备编号
     * @return
     */
    @Override
    public IotDevice selectIotDeviceByDevNum(String devNum) {
        IotDevice iotDevice = new IotDevice();
        iotDevice.setDevId(devNum);
        List<IotDevice> iotDevices = selectIotDeviceList(iotDevice);
        if(iotDevices.size() == 0) {
            log.info(String.format("devId:%s not register platform",devNum));
            return null;
        }
        if(iotDevices.size() > 1) {
            log.info(String.format("devId:%s repeat register",devNum));
            return null;
        }
        return iotDevices.get(0);
    }




    /**
     * 查询设备列表
     *
     * @param iotDevice 设备
     * @return 设备
     */
    @Override
    public List<IotDevice> selectIotDeviceList(IotDevice iotDevice)
    {
        return iotDeviceMapper.selectIotDeviceList(iotDevice);
    }



    /**
     * 新增设备
     *
     * @param iotDevice 设备
     * @return 结果
     */
    @Override
    public int insertIotDevice(IotDevice iotDevice)
    {
        return iotDeviceMapper.insertIotDevice(iotDevice);
    }

    /**
     * 修改设备
     *
     * @param iotDevice 设备
     * @return 结果
     */
    @Override
    public int updateIotDevice(IotDevice iotDevice)
    {
        return iotDeviceMapper.updateIotDevice(iotDevice);
    }

    /**
     * 批量删除设备
     *
     * @param ids 需要删除的设备主键
     * @return 结果
     */
    @Override
    public int deleteIotDeviceByIds(Long[] ids)
    {
        return iotDeviceMapper.deleteIotDeviceByIds(ids);
    }

    /**
     * 删除设备信息
     *
     * @param id 设备主键
     * @return 结果
     */
    @Override
    public int deleteIotDeviceById(Long id)
    {
        return iotDeviceMapper.deleteIotDeviceById(id);
    }

    @Override
    public IotDevice selectIotDeviceByDevCode(String devCode) {
        IotDevice iotDevice = selectIotDeviceByDevNum(devCode);

        return iotDevice;
    }

    @Override
    public void reportIotDeviceOnline(String devCode) {

        IotDevice iotDevice = selectIotDeviceByDevNum(devCode);
        iotDevice.setOnline(1);
        updateIotDevice(iotDevice);
        //log.info(String.format("devId:%s online",devCode));
    }

    @Override
    public void reportIotDeviceOffline(String devCode) {
        IotDevice iotDevice = selectIotDeviceByDevNum(devCode);
        iotDevice.setOnline(2);
        updateIotDevice(iotDevice);
        //log.info(String.format("devId:%s offline",devCode));
    }
}
