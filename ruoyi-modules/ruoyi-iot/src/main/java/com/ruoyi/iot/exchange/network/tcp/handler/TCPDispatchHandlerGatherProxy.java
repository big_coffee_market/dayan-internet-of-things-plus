package com.ruoyi.iot.exchange.network.tcp.handler;

import com.ruoyi.iot.exchange.network.tcp.handler.dispatch.ITcpDispatchHandler;
import com.ruoyi.iot.exchange.network.tcp.handler.dispatch.TCPDispatchHandler;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TCPDispatchHandlerGatherProxy implements ITcpDispatchHandler {

    /**
     * 标志头
     */
    private TCPDispatchHandler head = null;

    /**
     * 标志尾
     */
    private TCPDispatchHandler tail = null;


    /**
     * 添加handler
     * @param item
     */
    public void addHandler(TCPDispatchHandler item) {

        if(head == null) {
            head = item;
            tail = item;
        } else {
            tail.next = item;
            tail = item;
            item.head = head;
        }

    }


    /**
     * 解析帧数据
     */
    @Override
    public void decodeFrame() {
        //log.info("decodeFrameHandler:" + head.toString());
        head.handlerFrame();
    }

    /**
     * 因为是代理，不调用这个，不处理
     * @return
     */
    @Override
    public boolean isThisFrame() {

        return false;
    }

    /**
     *
     * @param channel 通道
     * @param in 数据信息
     */
    @Override
    public void injectByteBuf(ChannelHandlerContext channel, ByteBuf in) {
        TCPDispatchHandler node = head;
        do{
            if(node != null) {
                node.injectByteBuf(channel,in);
            }
            node = node.next;
        } while (node != null);
    }


}
