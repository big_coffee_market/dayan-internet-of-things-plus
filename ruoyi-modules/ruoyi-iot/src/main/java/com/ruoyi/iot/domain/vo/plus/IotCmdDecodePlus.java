package com.ruoyi.iot.domain.vo.plus;

import com.ruoyi.iot.domain.IotCmdDecode;
import lombok.Data;

@Data
public class IotCmdDecodePlus extends IotCmdDecode {

    /**
     * 指令名称
     */
    String cmdName;

    /**
     * 字段名称
     */
    String fieldName;


}
