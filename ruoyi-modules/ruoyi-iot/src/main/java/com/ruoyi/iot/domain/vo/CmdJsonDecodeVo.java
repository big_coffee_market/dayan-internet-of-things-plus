package com.ruoyi.iot.domain.vo;


import com.ruoyi.iot.domain.vo.field.DecodeObj;
import lombok.Data;

import java.util.List;


@Data
public class CmdJsonDecodeVo {

    private Long cmdId;

    private List<DecodeObj> decodeObjs;

}
