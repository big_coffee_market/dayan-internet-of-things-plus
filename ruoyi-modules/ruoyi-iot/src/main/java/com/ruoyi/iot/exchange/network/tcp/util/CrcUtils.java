package com.ruoyi.iot.exchange.network.tcp.util;

public class CrcUtils {



    /**
     * crc 16 校验
     * @param bytes 数组
     * @return
     */
    public static byte[] crc16(byte[] bytes) {
        int CRC = 0x0000ffff;
        int POLYNOMIAL = 0x0000a001;

        int i, j;
        for (i = 0; i < bytes.length; i++) {
            CRC ^= ((int) bytes[i] & 0x000000ff);
            for (j = 0; j < 8; j++) {
                if ((CRC & 0x00000001) != 0) {
                    CRC >>= 1;
                    CRC ^= POLYNOMIAL;
                } else {
                    CRC >>= 1;
                }
            }
        }
        return new byte[]{(byte) (CRC % 256), (byte) (CRC / 256)};
    }





}
