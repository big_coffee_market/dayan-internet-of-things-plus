package com.ruoyi.iot.channel.device;


import com.ruoyi.iot.channel.SynchroniseMessage;
import com.ruoyi.iot.channel.translator.impl.TransformServiceImpl;
import com.ruoyi.iot.domain.dto.ExchangeDTO;
import com.ruoyi.iot.domain.vo.DevCmdVo;
import com.ruoyi.iot.redis.DataRedis;
import com.ruoyi.iot.redis.enity.DataMO;
import com.ruoyi.iot.service.impl.IotDeviceServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
@Slf4j
public class DeviceMessageServiceImpl implements IDeviceMessage {

    /**
     * http客户端操作，请求
     */
    @Autowired
    DataRedis dataRedis;

    /**
     * 翻译的操作
     */
    @Autowired
    TransformServiceImpl transformService;

    /**
     * 同步消息对象
     */
    @Autowired
    SynchroniseMessage synchroniseMessage;

    /**
     * 设备服务
     */
    @Autowired
    IotDeviceServiceImpl deviceService;


    @Override
    public void deviceReport(ExchangeDTO exchangeDTO) {

        DevCmdVo devCmdVo = transformService.devTransform(exchangeDTO);
        DataMO dataMO = new DataMO();
        BeanUtils.copyProperties(devCmdVo,dataMO);

        Long projectId = deviceService.selectIotDeviceByDevCode(devCmdVo.getDevId()).getProjectId();
        dataMO.setTime(new Date());
        dataMO.setProjectId(projectId);

        dataRedis.push(dataMO);
        dataRedis.publish(dataMO);
    }



    @Override
    public void deviceResult(ExchangeDTO exchangeDTO) {
        DevCmdVo devCmdVo = transformService.devTransform(exchangeDTO);
        Long resultId = devCmdVo.getCmdId();
        synchroniseMessage.notifyPlatformMessage(devCmdVo.getDevId(),resultId,devCmdVo);
    }


}
