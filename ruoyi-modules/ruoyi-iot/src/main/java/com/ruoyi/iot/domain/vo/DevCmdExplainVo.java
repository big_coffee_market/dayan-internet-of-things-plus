package com.ruoyi.iot.domain.vo;

import lombok.Data;

import java.util.List;


@Data
public class DevCmdExplainVo {

    /**
     * 设备id
     */
    String devId;

    /**
     * cmd解释文本
     */
    List<CmdExplainVo> cmdExplainVos;

}
