package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.IotProductFrame;

/**
 * 产品-数据帧Mapper接口
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
public interface IotProductFrameMapper 
{
    /**
     * 查询产品-数据帧
     * 
     * @param id 产品-数据帧主键
     * @return 产品-数据帧
     */
    public IotProductFrame selectIotProductFrameById(Long id);

    /**
     * 查询产品-数据帧列表
     * 
     * @param iotProductFrame 产品-数据帧
     * @return 产品-数据帧集合
     */
    public List<IotProductFrame> selectIotProductFrameList(IotProductFrame iotProductFrame);

    /**
     * 新增产品-数据帧
     * 
     * @param iotProductFrame 产品-数据帧
     * @return 结果
     */
    public int insertIotProductFrame(IotProductFrame iotProductFrame);

    /**
     * 修改产品-数据帧
     * 
     * @param iotProductFrame 产品-数据帧
     * @return 结果
     */
    public int updateIotProductFrame(IotProductFrame iotProductFrame);

    /**
     * 删除产品-数据帧
     * 
     * @param id 产品-数据帧主键
     * @return 结果
     */
    public int deleteIotProductFrameById(Long id);

    /**
     * 批量删除产品-数据帧
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteIotProductFrameByIds(Long[] ids);
}
