package com.ruoyi.iot.service.impl;

import com.ruoyi.iot.domain.IotCmdDecode;
import com.ruoyi.iot.mapper.IotCmdDecodeMapper;
import com.ruoyi.iot.service.IIotCmdDecodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 指令解析Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-09-14
 */
@Service
@Primary
public class IotCmdDecodeServiceImpl implements IIotCmdDecodeService
{
    @Autowired
    private IotCmdDecodeMapper iotCmdDecodeMapper;

    /**
     * 查询指令解析
     * 
     * @param id 指令解析主键
     * @return 指令解析
     */
    @Override
    public IotCmdDecode selectIotCmdDecodeById(Long id)
    {
        return iotCmdDecodeMapper.selectIotCmdDecodeById(id);
    }

    /**
     * 查询指令解析列表
     * 
     * @param iotCmdDecode 指令解析
     * @return 指令解析
     */
    @Override
    public List<IotCmdDecode> selectIotCmdDecodeList(IotCmdDecode iotCmdDecode)
    {
        return iotCmdDecodeMapper.selectIotCmdDecodeList(iotCmdDecode);
    }

    /**
     * 新增指令解析
     * 
     * @param iotCmdDecode 指令解析
     * @return 结果
     */
    @Override
    public int insertIotCmdDecode(IotCmdDecode iotCmdDecode)
    {
        return iotCmdDecodeMapper.insertIotCmdDecode(iotCmdDecode);
    }

    /**
     * 修改指令解析
     * 
     * @param iotCmdDecode 指令解析
     * @return 结果
     */
    @Override
    public int updateIotCmdDecode(IotCmdDecode iotCmdDecode)
    {
        return iotCmdDecodeMapper.updateIotCmdDecode(iotCmdDecode);
    }

    /**
     * 批量删除指令解析
     * 
     * @param ids 需要删除的指令解析主键
     * @return 结果
     */
    @Override
    public int deleteIotCmdDecodeByIds(Long[] ids)
    {
        return iotCmdDecodeMapper.deleteIotCmdDecodeByIds(ids);
    }

    /**
     * 删除指令解析信息
     * 
     * @param id 指令解析主键
     * @return 结果
     */
    @Override
    public int deleteIotCmdDecodeById(Long id)
    {
        return iotCmdDecodeMapper.deleteIotCmdDecodeById(id);
    }
}
