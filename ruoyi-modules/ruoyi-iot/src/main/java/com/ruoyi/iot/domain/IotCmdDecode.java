package com.ruoyi.iot.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 指令解析对象 iot_cmd_decode
 * 
 * @author ruoyi
 * @date 2022-09-14
 */
public class IotCmdDecode extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 指令id */
    @Excel(name = "指令id")
    private Long cmdId;

    /** 截获类型 */
    @Excel(name = "截获类型")
    private Long trimType;

    /** 截获参数 */
    @Excel(name = "截获参数")
    private Long trimParam;

    /** 字段id */
    @Excel(name = "字段id")
    private Long fieldId;

    /** 序列号 */
    @Excel(name = "序列号")
    private Long sequence;

    /** 创建者 */
    private Long creator;

    /** 创建时间 */
    private Date createDate;

    /** 更新者 */
    private Long updater;

    /** 更新时间 */
    private Date updateDate;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCmdId(Long cmdId) 
    {
        this.cmdId = cmdId;
    }

    public Long getCmdId() 
    {
        return cmdId;
    }
    public void setTrimType(Long trimType) 
    {
        this.trimType = trimType;
    }

    public Long getTrimType() 
    {
        return trimType;
    }
    public void setTrimParam(Long trimParam) 
    {
        this.trimParam = trimParam;
    }

    public Long getTrimParam() 
    {
        return trimParam;
    }
    public void setFieldId(Long fieldId) 
    {
        this.fieldId = fieldId;
    }

    public Long getFieldId() 
    {
        return fieldId;
    }
    public void setSequence(Long sequence) 
    {
        this.sequence = sequence;
    }

    public Long getSequence() 
    {
        return sequence;
    }
    public void setCreator(Long creator) 
    {
        this.creator = creator;
    }

    public Long getCreator() 
    {
        return creator;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setUpdater(Long updater) 
    {
        this.updater = updater;
    }

    public Long getUpdater() 
    {
        return updater;
    }
    public void setUpdateDate(Date updateDate) 
    {
        this.updateDate = updateDate;
    }

    public Date getUpdateDate() 
    {
        return updateDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("cmdId", getCmdId())
            .append("trimType", getTrimType())
            .append("trimParam", getTrimParam())
            .append("fieldId", getFieldId())
            .append("sequence", getSequence())
            .append("remark", getRemark())
            .append("creator", getCreator())
            .append("createDate", getCreateDate())
            .append("updater", getUpdater())
            .append("updateDate", getUpdateDate())
            .toString();
    }
}
