package com.ruoyi.iot.domain.vo;


import com.ruoyi.iot.domain.vo.field.EncodeObj;
import lombok.Data;
import java.util.List;

@Data
public class CmdJsonEncodeVo {


   private Long cmdId;

   private List<EncodeObj> encodeObjs;

}
