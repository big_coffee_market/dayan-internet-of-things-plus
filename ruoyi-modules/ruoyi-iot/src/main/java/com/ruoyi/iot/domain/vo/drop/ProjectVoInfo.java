package com.ruoyi.iot.domain.vo.drop;

import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.api.domain.SysUser;
import lombok.Data;
import java.util.List;


@Data
public class ProjectVoInfo extends TableDataInfo {

    private List<SysUser> sysUsers;

}
