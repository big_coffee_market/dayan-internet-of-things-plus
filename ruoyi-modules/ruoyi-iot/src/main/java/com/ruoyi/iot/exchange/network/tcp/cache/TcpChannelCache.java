package com.ruoyi.iot.exchange.network.tcp.cache;

import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 设备id和通道绑定，都走这个方法
 */
@Slf4j
public class TcpChannelCache {

    /**
     * 根据注册码，需找通道
     */
    static ConcurrentHashMap<String,ChannelHandlerContext> findChannelCache = new ConcurrentHashMap<>();

    /**
     * 根据通道，需找注册码
     */
    static ConcurrentHashMap<ChannelHandlerContext,String> findIdCache = new ConcurrentHashMap<>();

    /**
     * 放netty注册码，通道接口
     * @param devId 注册码
     * @param context 上下文
     */
    public static void putNettyChannel(String devId, ChannelHandlerContext context) {
        findChannelCache.put(devId,context);
        findIdCache.put(context,devId);
    }

    /**
     * 根据注册码，获取通道
     * @param devId 设备注册码
     * @return
     */
    public static ChannelHandlerContext getChannel(String devId) {
        ChannelHandlerContext context = findChannelCache.get(devId);
        return context;
    }

    /**
     * 根据通道，获取上下文
     * @param context 上下文
     * @return
     */
    public static String getDevId(ChannelHandlerContext context) {
         String registerId = findIdCache.get(context);
         return registerId;
    }


    /**
     * 移除某一个通道,通道异常的时候调用
     * @param context 通道上下文
     */
    public static void removeChannel(ChannelHandlerContext context) {
         String devId =  findIdCache.get(context);
         if(StringUtils.isNotEmpty(devId)) {
             log.error(String.format("dev:%s offline", devId));
             findChannelCache.remove(devId);
             findIdCache.remove(context);
         }
    }






}
