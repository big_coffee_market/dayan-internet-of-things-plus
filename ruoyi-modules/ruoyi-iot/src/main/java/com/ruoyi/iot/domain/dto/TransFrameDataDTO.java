package com.ruoyi.iot.domain.dto;

import lombok.Data;

@Data
public class TransFrameDataDTO {

    /**
     * 设备id
     */
    String devId;

    /**
     * 数据
     */
    byte[] data;

}
