package com.ruoyi.iot.feign.provide;

import com.ruoyi.common.core.web.domain.AjaxResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

public interface IWxDeviceFeign {

    /**
     * 获取微信信息
     * @return
     */
    @GetMapping("/takeWxDeviceVO/{projectId}")
    AjaxResult takeWxDeviceVO(@PathVariable("projectId") String projectId);



}
