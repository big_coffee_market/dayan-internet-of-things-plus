package com.ruoyi.iot.channel.translator.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.iot.channel.translator.ITransformCmdService;
import com.ruoyi.iot.domain.IotCmdDecode;
import com.ruoyi.iot.domain.IotCmdEncoder;
import com.ruoyi.iot.domain.IotProductField;
import com.ruoyi.iot.service.impl.IotCmdDecodeServiceImpl;
import com.ruoyi.iot.service.impl.IotCmdEncoderServiceImpl;
import com.ruoyi.iot.service.impl.IotProductFieldServiceImpl;
import com.zxq.factory.datatrim.CurIndex;
import com.zxq.factory.datatrim.DataTrimFactory;
import com.zxq.factory.datatrim.ITrimInterpreter;
import com.zxq.factory.decoder.FiledDecoderFactory;
import com.zxq.factory.decoder.IFieldDecoder;
import com.zxq.factory.encoder.FiledEncoderFactory;
import com.zxq.factory.encoder.IFieldEncoder;
import com.zxq.factory.enums.EField;
import com.zxq.factory.enums.ETrimMethod;
import com.zxq.factory.utils.BigLittleEndian;
import com.zxq.memory.ByteCombination;
import lombok.extern.slf4j.Slf4j;
import org.apache.xmlbeans.impl.util.HexBin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import toolgood.algorithm.AlgorithmEngine;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class TransformCmdServiceImpl implements ITransformCmdService {

    /**
     * 设备指令解码
     */
    @Autowired
    IotCmdDecodeServiceImpl iotCmdDecodeService;

    /**
     * 平台指令编码
     */
    @Autowired
    IotCmdEncoderServiceImpl iotCmdEncodedService;



    /**
     * 寻找包内所有字段
     */
    @Autowired
    IotProductFieldServiceImpl iotProductFieldService;


    @Override
    public byte[] platformTransform(Long cmdId, JSONObject json) {
        byte[] total = null;
        IotCmdEncoder iotCmdEncoderCondition = new IotCmdEncoder();
        iotCmdEncoderCondition.setCmdId(cmdId);
        List<IotCmdEncoder> iotCmdEncoderDos = iotCmdEncodedService.selectIotCmdEncoderList(iotCmdEncoderCondition);
        iotCmdEncoderDos.sort((o1,o2) ->{
            return o1.getSequence().intValue() - o2.getSequence().intValue();
        });
        List<byte[]> cmdTotal = new ArrayList<>();
        iotCmdEncoderDos.forEach(item -> {
            //log.info("encode item:" + JSON.toJSONString(item));
            byte[] msg = fieldEncoder(item,json);
            //log.info("msg:" + HexBin.bytesToString(msg));
            cmdTotal.add(msg);
        });
        try {
            total =   ByteCombination.combinationContent(cmdTotal);
        } catch (Exception e){
            log.info("transform error:" + e.toString());
        }

        return total;
    }


    @Override
    public JSONObject deviceTransform(Long cmdId, byte[] frame) {
        AlgorithmEngine algorithmEngine = new AlgorithmEngine();

        IotCmdDecode iotCmdDecodeDo = new IotCmdDecode();
        iotCmdDecodeDo.setCmdId(cmdId);
        List<IotCmdDecode> iotCmdDecodeDos = iotCmdDecodeService.selectIotCmdDecodeList(iotCmdDecodeDo);
        iotCmdDecodeDos.sort(((o1, o2) -> {
            return o1.getSequence().intValue() - o2.getSequence().intValue();
        }));
       // String message = String.format("cmdId:%s container filed number:%s", cmdId + "",iotCmdDecodeDos.size() + "");
      //  log.info(message);
        JSONObject json = new JSONObject();
        CurIndex curIndex = new CurIndex();
        iotCmdDecodeDos.forEach(item ->{
            Object value = fieldDecoder(item,frame,curIndex);
            Long fieldId = item.getFieldId();
            IotProductField iotProductFieldDo = iotProductFieldService.selectIotProductFieldById(fieldId);
            String field = iotProductFieldDo.getField();
            json.put(field,value);

            String arithmetic = iotProductFieldDo.getArithmetic();
            if(!StringUtils.isEmpty(arithmetic)) {
              String mathExpression =  arithmetic.replace("x",value + "");
              Double mathValue = algorithmEngine.TryEvaluate(mathExpression,0.0);
              json.put(field,mathValue);
            }

            String unit = iotProductFieldDo.getUnit();
            if(!StringUtils.isEmpty(unit)) {
               Object takeValue = json.get(field);
               json.put("_" + field,takeValue + " " + unit);
            }
        });

        return json;
    }

    /**
     * 一个标注，类似于改字段是一个包裹的包
     *     Frame(0),
     *     Int(1),
     *     Bytes(2),
     *     Long(3),
     *     String(4),
     *     Boolean(5),
     *     Float(6),
     *     Double(7),
     *     Hex(8),
     *     Byte(9),
     * frame不支持，因为是帧类型，不太好处理关系
     */
    private byte[] fieldEncoder(IotCmdEncoder item, JSONObject jsonObject) {
        Long fieldId = item.getFieldId();
        IotProductField iotProductFieldDo = iotProductFieldService.selectIotProductFieldById(fieldId);

        log.info("product field:" + JSON.toJSONString(iotProductFieldDo));

        if(iotProductFieldDo == null) {
            log.info("not find field");
        }
        String filed = iotProductFieldDo.getField();
        Integer fieldType = iotProductFieldDo.getFieldType().intValue();
        EField eField = EField.getType(fieldType.intValue());
        log.info("product field:" + eField.toString());
        IFieldEncoder iFieldEncoder = FiledEncoderFactory.instance().create(eField);
        byte[] msg = null;
     //   log.info("json:" + jsonObject.toString());
        String val = jsonObject.getString(filed);
     //   log.info("field:" + filed + " val:" + val);
        try {
        Object wrapVal =  EField.parseValue(eField,val);
       // log.info("wrapVal:" + wrapVal);
        if(iFieldEncoder != null && wrapVal != null ) {
            msg = iFieldEncoder.encoder(wrapVal,iotProductFieldDo.getLen().intValue());
            if(msg == null){
                log.info("encoder result is null");
                return null;
            }
          //  log.info("filed:" + filed + " hex:" + HexBin.bytesToString(msg));
            if(iotProductFieldDo.getBigLittleEndian() != null && iotProductFieldDo.getBigLittleEndian().isEmpty()) {
            //    log.info("big little start:" + iotProductFieldDo.getBigLittleEndian());
                msg = BigLittleEndian.encoder(iotProductFieldDo.getBigLittleEndian(), msg);
            }
        } }catch (Exception e){
         // log.info("transform error:" + e.toString());
        }
        return msg;
    }


    /**
     * 字段解码
     * @param item cmd内的一个记录，记录cmd内字段描述
     * @param msg byte数据
     * @param curIndex 索引记录器
     * @return
     */
    private Object fieldDecoder(IotCmdDecode item, byte[] msg, CurIndex curIndex) {
        ETrimMethod eTrimMethod = ETrimMethod.getType(item.getTrimType().intValue());
        ITrimInterpreter iTrimInterpreter = DataTrimFactory.instance().create(eTrimMethod);
        Object value = new Object();
        try {
            byte[] data = iTrimInterpreter.interpreter(msg,curIndex,item.getTrimParam().intValue());
            Long fieldId =  item.getFieldId();
            IotProductField iotProductFieldDo = iotProductFieldService.selectIotProductFieldById(fieldId);
            EField eField = EField.getType(iotProductFieldDo.getFieldType().intValue());
            IFieldDecoder iFieldDecoder = FiledDecoderFactory.instance().create(eField);
            value = iFieldDecoder.decoder(data);
        }catch (Exception e){
            log.info("e:" + e.toString());
        }

        return value;
    }



}
