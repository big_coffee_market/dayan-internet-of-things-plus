package com.ruoyi.iot.domain.vo.enums;

public enum ECodeEnum {
    Success(200,"成功"),
    ProductNoPay(201,"产品未付款"),
    ProductExpire(202,"产品已过期"),
    ProductAlreadyRun(203,"产品已运行"),
    ProductPortAlreadyUse(204,"产品端口已被占用"),
    ProductNoRunTask(205,"产品无任务"),
    ProductRunStop(207,"产品已停止"),
    UserNoPermission(208,"用户无权限"),
    IllegalDevice(209,"非法设备"),
    NotFindProduct(210,"未找到产品"),
    NotFindCmd(211,"未找到指令"),
    DeviceOffline(212,"设备未连接到平台"),
    SponsorError(213,"发送方不是来自平台"),
    RstCmdError(214,"返回的指令ID未在平台配置")
    ;

    Integer code;
    String msg;
    ECodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
