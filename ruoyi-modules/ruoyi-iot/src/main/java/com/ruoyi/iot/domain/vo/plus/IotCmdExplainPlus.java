package com.ruoyi.iot.domain.vo.plus;

import com.ruoyi.iot.domain.IotCmdExplain;
import lombok.Data;

@Data
public class IotCmdExplainPlus  extends IotCmdExplain {

    /**
     * 指令名称
     */
    String cmdName;

    /**
     * 指令数据
     */
    String cmdJson;


}
