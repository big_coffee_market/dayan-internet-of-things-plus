package com.ruoyi.iot.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 产品-属性对象 iot_product_field
 *
 * @author ruoyi
 * @date 2022-12-27
 */
public class IotProductField extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    @Excel(name = "id")
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 产品id */
    @Excel(name = "产品id")
    private Long productId;

    /** 字段名 */
    @Excel(name = "字段名")
    private String field;

    /** 默认值 */
    @Excel(name = "默认值")
    private String defVal;

    /** 字段类型 */
    @Excel(name = "字段类型")
    private Long fieldType;

    /** 字段长度 */
    @Excel(name = "字段长度")
    private Long len;

    /** 大小端 */
    @Excel(name = "大小端")
    private String bigLittleEndian;

    /** 表达式 */
    @Excel(name = "表达式")
    private String arithmetic;

    /** 单位 */
    @Excel(name = "单位")
    private String unit;

    /** 创建者 */
    @Excel(name = "创建者")
    private Long creator;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 更新者 */
    @Excel(name = "更新者")
    private Long updater;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updateDate;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setProductId(Long productId)
    {
        this.productId = productId;
    }

    public Long getProductId()
    {
        return productId;
    }
    public void setField(String field)
    {
        this.field = field;
    }

    public String getField()
    {
        return field;
    }
    public void setDefVal(String defVal)
    {
        this.defVal = defVal;
    }

    public String getDefVal()
    {
        return defVal;
    }
    public void setFieldType(Long fieldType)
    {
        this.fieldType = fieldType;
    }

    public Long getFieldType()
    {
        return fieldType;
    }
    public void setLen(Long len)
    {
        this.len = len;
    }

    public Long getLen()
    {
        return len;
    }
    public void setBigLittleEndian(String bigLittleEndian)
    {
        this.bigLittleEndian = bigLittleEndian;
    }

    public String getBigLittleEndian()
    {
        return bigLittleEndian;
    }
    public void setCreator(Long creator)
    {
        this.creator = creator;
    }

    public Long getCreator()
    {
        return creator;
    }
    public void setCreateDate(Date createDate)
    {
        this.createDate = createDate;
    }

    public Date getCreateDate()
    {
        return createDate;
    }
    public void setUpdater(Long updater)
    {
        this.updater = updater;
    }

    public Long getUpdater()
    {
        return updater;
    }
    public void setUpdateDate(Date updateDate)
    {
        this.updateDate = updateDate;
    }

    public Date getUpdateDate()
    {
        return updateDate;
    }

    public String getArithmetic() {
        return arithmetic;
    }

    public void setArithmetic(String arithmetic) {
        this.arithmetic = arithmetic;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("productId", getProductId())
            .append("field", getField())
            .append("defVal", getDefVal())
            .append("fieldType", getFieldType())
            .append("len", getLen())
            .append("bigLittleEndian", getBigLittleEndian())
                .append("arithmetic", getArithmetic())
                .append("unit", getUnit())
            .append("remark", getRemark())
            .append("creator", getCreator())
            .append("createDate", getCreateDate())
            .append("updater", getUpdater())
            .append("updateDate", getUpdateDate())
            .toString();
    }
}
