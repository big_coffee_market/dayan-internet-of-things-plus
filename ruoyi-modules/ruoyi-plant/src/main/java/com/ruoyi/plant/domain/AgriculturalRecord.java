package com.ruoyi.plant.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 农事记录对象 agricultural_record
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public class AgriculturalRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 农事计划 */
    @Excel(name = "农事计划")
    private Long planId;

    /** 图像 */
    @Excel(name = "图像")
    private String picture;

    /** 操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date operatingTime;

    /** 农事阶段 */
    @Excel(name = "农事阶段")
    private Long agriculturalStageId;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPlanId(Long planId) 
    {
        this.planId = planId;
    }

    public Long getPlanId() 
    {
        return planId;
    }
    public void setPicture(String picture) 
    {
        this.picture = picture;
    }

    public String getPicture() 
    {
        return picture;
    }
    public void setOperatingTime(Date operatingTime) 
    {
        this.operatingTime = operatingTime;
    }

    public Date getOperatingTime() 
    {
        return operatingTime;
    }
    public void setAgriculturalStageId(Long agriculturalStageId) 
    {
        this.agriculturalStageId = agriculturalStageId;
    }

    public Long getAgriculturalStageId() 
    {
        return agriculturalStageId;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("planId", getPlanId())
            .append("picture", getPicture())
            .append("operatingTime", getOperatingTime())
            .append("agriculturalStageId", getAgriculturalStageId())
            .append("remarks", getRemarks())
            .toString();
    }
}
