package com.ruoyi.plant.mapper;

import java.util.List;
import com.ruoyi.plant.domain.ConstResourceStage;

/**
 * 农资类型Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-22
 */
public interface ConstResourceStageMapper 
{
    /**
     * 查询农资类型
     * 
     * @param id 农资类型主键
     * @return 农资类型
     */
    public ConstResourceStage selectConstResourceStageById(Long id);

    /**
     * 查询农资类型列表
     * 
     * @param constResourceStage 农资类型
     * @return 农资类型集合
     */
    public List<ConstResourceStage> selectConstResourceStageList(ConstResourceStage constResourceStage);

    /**
     * 新增农资类型
     * 
     * @param constResourceStage 农资类型
     * @return 结果
     */
    public int insertConstResourceStage(ConstResourceStage constResourceStage);

    /**
     * 修改农资类型
     * 
     * @param constResourceStage 农资类型
     * @return 结果
     */
    public int updateConstResourceStage(ConstResourceStage constResourceStage);

    /**
     * 删除农资类型
     * 
     * @param id 农资类型主键
     * @return 结果
     */
    public int deleteConstResourceStageById(Long id);

    /**
     * 批量删除农资类型
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteConstResourceStageByIds(Long[] ids);
}
