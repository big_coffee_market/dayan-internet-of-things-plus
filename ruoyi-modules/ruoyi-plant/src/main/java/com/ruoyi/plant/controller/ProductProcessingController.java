package com.ruoyi.plant.controller;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.plant.domain.AgriculturalHarvest;
import com.ruoyi.plant.domain.ConstProcessingStage;
import com.ruoyi.plant.domain.ConstProductSupplier;
import com.ruoyi.plant.service.impl.AgriculturalHarvestServiceImpl;
import com.ruoyi.plant.service.impl.ConstProcessingStageServiceImpl;
import com.ruoyi.plant.service.impl.ConstProductSupplierServiceImpl;
import com.ruoyi.plant.vo.ProductProcessingVO;
import com.ruoyi.plant.vo.tablist.ProductProcessingTableDataInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.ProductProcessing;
import com.ruoyi.plant.service.IProductProcessingService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 产品加工Controller
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/product_processing")
public class ProductProcessingController extends BaseController
{
    @Autowired
    private IProductProcessingService productProcessingService;

    /**
     * 采收
     */
    @Autowired
    private AgriculturalHarvestServiceImpl harvestService;

    /**
     * 加工
     */
    @Autowired
    private ConstProcessingStageServiceImpl processingStageService;


    /**
     * 产品供货商
     */
    @Autowired
    private ConstProductSupplierServiceImpl constProductSupplierService;


    /**
     * 查询产品加工列表
     */
    @RequiresPermissions("plant:product_processing:list")
    @GetMapping("/list")
    public TableDataInfo list(ProductProcessing productProcessing)
    {
        startPage();
        List<ProductProcessing> list = productProcessingService.selectProductProcessingList(productProcessing);
        ProductProcessingTableDataInfo processingTableDataInfo = new ProductProcessingTableDataInfo();
        List<ProductProcessingVO> voList = new ArrayList<>();

        for(ProductProcessing item: list) {
            ProductProcessingVO vo = new ProductProcessingVO();
            BeanUtils.copyProperties(item,vo);
            AgriculturalHarvest agriculturalHarvest = harvestService.selectAgriculturalHarvestById(item.getHarvestId());
            if(agriculturalHarvest != null) {
                vo.setHarvestName(agriculturalHarvest.getName());
            }

            ConstProcessingStage constProcessingStage = processingStageService.selectConstProcessingStageById(item.getType());
            if(constProcessingStage != null) {
                vo.setType(constProcessingStage.getName());
            }

            ConstProductSupplier constProductSupplier = constProductSupplierService.selectConstProductSupplierById(item.getSupplierId());
            if(constProductSupplier != null) {
                vo.setSupplierName(constProductSupplier.getName());
            }

            voList.add(vo);
        }

        AgriculturalHarvest queryHarvest = new AgriculturalHarvest();
        List<AgriculturalHarvest> agriculturalHarvests = harvestService.selectAgriculturalHarvestList(queryHarvest);
        processingTableDataInfo.setHarvestDropList(agriculturalHarvests);

        ConstProductSupplier querySupplier = new ConstProductSupplier();
        List<ConstProductSupplier> constProductSuppliers = constProductSupplierService.selectConstProductSupplierList(querySupplier);
        processingTableDataInfo.setSupplierDropList(constProductSuppliers);

        ConstProcessingStage queryProcessing = new ConstProcessingStage();
        List<ConstProcessingStage> constProcessingStages = processingStageService.selectConstProcessingStageList(queryProcessing);
        processingTableDataInfo.setProcessingDropList(constProcessingStages);


        return getDataTable(list);
    }

    /**
     * 导出产品加工列表
     */
    @RequiresPermissions("plant:product_processing:export")
    @Log(title = "产品加工", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProductProcessing productProcessing)
    {
        List<ProductProcessing> list = productProcessingService.selectProductProcessingList(productProcessing);
        ExcelUtil<ProductProcessing> util = new ExcelUtil<ProductProcessing>(ProductProcessing.class);
        util.exportExcel(response, list, "产品加工数据");
    }

    /**
     * 获取产品加工详细信息
     */
    @RequiresPermissions("plant:product_processing:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(productProcessingService.selectProductProcessingById(id));
    }

    /**
     * 新增产品加工
     */
    @RequiresPermissions("plant:product_processing:add")
    @Log(title = "产品加工", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductProcessing productProcessing)
    {
        return toAjax(productProcessingService.insertProductProcessing(productProcessing));
    }

    /**
     * 修改产品加工
     */
    @RequiresPermissions("plant:product_processing:edit")
    @Log(title = "产品加工", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductProcessing productProcessing)
    {
        return toAjax(productProcessingService.updateProductProcessing(productProcessing));
    }

    /**
     * 删除产品加工
     */
    @RequiresPermissions("plant:product_processing:remove")
    @Log(title = "产品加工", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(productProcessingService.deleteProductProcessingByIds(ids));
    }
}
