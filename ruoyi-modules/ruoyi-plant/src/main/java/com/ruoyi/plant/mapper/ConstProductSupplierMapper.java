package com.ruoyi.plant.mapper;

import java.util.List;
import com.ruoyi.plant.domain.ConstProductSupplier;

/**
 * 供货商Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface ConstProductSupplierMapper 
{
    /**
     * 查询供货商
     * 
     * @param id 供货商主键
     * @return 供货商
     */
    public ConstProductSupplier selectConstProductSupplierById(Long id);

    /**
     * 查询供货商列表
     * 
     * @param constProductSupplier 供货商
     * @return 供货商集合
     */
    public List<ConstProductSupplier> selectConstProductSupplierList(ConstProductSupplier constProductSupplier);

    /**
     * 新增供货商
     * 
     * @param constProductSupplier 供货商
     * @return 结果
     */
    public int insertConstProductSupplier(ConstProductSupplier constProductSupplier);

    /**
     * 修改供货商
     * 
     * @param constProductSupplier 供货商
     * @return 结果
     */
    public int updateConstProductSupplier(ConstProductSupplier constProductSupplier);

    /**
     * 删除供货商
     * 
     * @param id 供货商主键
     * @return 结果
     */
    public int deleteConstProductSupplierById(Long id);

    /**
     * 批量删除供货商
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteConstProductSupplierByIds(Long[] ids);
}
