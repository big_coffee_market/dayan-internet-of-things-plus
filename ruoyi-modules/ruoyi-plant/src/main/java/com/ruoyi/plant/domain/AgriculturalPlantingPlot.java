package com.ruoyi.plant.domain;

import com.ruoyi.system.api.domain.abs.IDropValue;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 种植地块对象 agricultural_planting_plot
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class AgriculturalPlantingPlot extends BaseEntity implements IDropValue
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /**
     * 名称
     */
    private String name;

    /** 基地 */
    @Excel(name = "基地")
    private Long baseId;

    /** 责任人 */
    @Excel(name = "责任人")
    private Long usrId;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;



}
