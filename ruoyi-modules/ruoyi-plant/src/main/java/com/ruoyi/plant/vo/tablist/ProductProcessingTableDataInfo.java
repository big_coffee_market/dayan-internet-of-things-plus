package com.ruoyi.plant.vo.tablist;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.api.domain.abs.IDropValue;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 产品加工对象 product_processing
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class ProductProcessingTableDataInfo extends TableDataInfo
{

    /** 采收批次 */
    private List<? extends IDropValue> harvestDropList;

    /** 类型 */
    private List<? extends IDropValue>  processingDropList;


    /** 供应商 */
    private List<? extends IDropValue>  supplierDropList;



}
