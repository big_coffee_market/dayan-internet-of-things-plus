package com.ruoyi.plant.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 产品证书对象 product_certificate
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class ProductCertificateVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;

    /** 证书 */
    @Excel(name = "证书")
    private String certificate;

    /** 类型 */
    @Excel(name = "类型")
    private Long type;

    /** 颁发机构 */
    @Excel(name = "颁发机构")
    private String issuingAuthority;

    /** 颁发时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "颁发时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date issueTime;

    /** 证书上传 */
    @Excel(name = "证书上传")
    private String certificateUploading;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;


}
