package com.ruoyi.plant.service;

import java.util.List;
import com.ruoyi.plant.domain.ConstCertificateStage;

/**
 * 证书类型Service接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface IConstCertificateStageService 
{
    /**
     * 查询证书类型
     * 
     * @param id 证书类型主键
     * @return 证书类型
     */
    public ConstCertificateStage selectConstCertificateStageById(Long id);

    /**
     * 查询证书类型列表
     * 
     * @param constCertificateStage 证书类型
     * @return 证书类型集合
     */
    public List<ConstCertificateStage> selectConstCertificateStageList(ConstCertificateStage constCertificateStage);

    /**
     * 新增证书类型
     * 
     * @param constCertificateStage 证书类型
     * @return 结果
     */
    public int insertConstCertificateStage(ConstCertificateStage constCertificateStage);

    /**
     * 修改证书类型
     * 
     * @param constCertificateStage 证书类型
     * @return 结果
     */
    public int updateConstCertificateStage(ConstCertificateStage constCertificateStage);

    /**
     * 批量删除证书类型
     * 
     * @param ids 需要删除的证书类型主键集合
     * @return 结果
     */
    public int deleteConstCertificateStageByIds(Long[] ids);

    /**
     * 删除证书类型信息
     * 
     * @param id 证书类型主键
     * @return 结果
     */
    public int deleteConstCertificateStageById(Long id);
}
