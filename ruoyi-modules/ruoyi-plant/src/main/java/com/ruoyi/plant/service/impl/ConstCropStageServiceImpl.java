package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.ConstCropStageMapper;
import com.ruoyi.plant.domain.ConstCropStage;
import com.ruoyi.plant.service.IConstCropStageService;

/**
 * 生长阶段Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class ConstCropStageServiceImpl implements IConstCropStageService
{
    @Autowired
    private ConstCropStageMapper constCropStageMapper;

    /**
     * 查询生长阶段
     *
     * @param id 生长阶段主键
     * @return 生长阶段
     */
    @Override
    public ConstCropStage selectConstCropStageById(Long id)
    {
        return constCropStageMapper.selectConstCropStageById(id);
    }

    /**
     * 查询生长阶段列表
     *
     * @param constCropStage 生长阶段
     * @return 生长阶段
     */
    @Override
    public List<ConstCropStage> selectConstCropStageList(ConstCropStage constCropStage)
    {
        return constCropStageMapper.selectConstCropStageList(constCropStage);
    }

    /**
     * 新增生长阶段
     *
     * @param constCropStage 生长阶段
     * @return 结果
     */
    @Override
    public int insertConstCropStage(ConstCropStage constCropStage)
    {
        return constCropStageMapper.insertConstCropStage(constCropStage);
    }

    /**
     * 修改生长阶段
     *
     * @param constCropStage 生长阶段
     * @return 结果
     */
    @Override
    public int updateConstCropStage(ConstCropStage constCropStage)
    {
        return constCropStageMapper.updateConstCropStage(constCropStage);
    }

    /**
     * 批量删除生长阶段
     *
     * @param ids 需要删除的生长阶段主键
     * @return 结果
     */
    @Override
    public int deleteConstCropStageByIds(Long[] ids)
    {
        return constCropStageMapper.deleteConstCropStageByIds(ids);
    }

    /**
     * 删除生长阶段信息
     *
     * @param id 生长阶段主键
     * @return 结果
     */
    @Override
    public int deleteConstCropStageById(Long id)
    {
        return constCropStageMapper.deleteConstCropStageById(id);
    }
}
