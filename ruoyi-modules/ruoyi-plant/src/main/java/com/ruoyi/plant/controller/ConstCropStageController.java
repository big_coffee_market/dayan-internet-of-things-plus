package com.ruoyi.plant.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.ConstCropStage;
import com.ruoyi.plant.service.IConstCropStageService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 生长阶段Controller
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/crop_stage")
public class ConstCropStageController extends BaseController
{
    @Autowired
    private IConstCropStageService constCropStageService;

    /**
     * 查询生长阶段列表
     */
    @RequiresPermissions("plant:crop_stage:list")
    @GetMapping("/list")
    public TableDataInfo list(ConstCropStage constCropStage)
    {
        startPage();
        List<ConstCropStage> list = constCropStageService.selectConstCropStageList(constCropStage);
        return getDataTable(list);
    }

    /**
     * 导出生长阶段列表
     */
    @RequiresPermissions("plant:crop_stage:export")
    @Log(title = "生长阶段", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ConstCropStage constCropStage)
    {
        List<ConstCropStage> list = constCropStageService.selectConstCropStageList(constCropStage);
        ExcelUtil<ConstCropStage> util = new ExcelUtil<ConstCropStage>(ConstCropStage.class);
        util.exportExcel(response, list, "生长阶段数据");
    }

    /**
     * 获取生长阶段详细信息
     */
    @RequiresPermissions("plant:crop_stage:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(constCropStageService.selectConstCropStageById(id));
    }

    /**
     * 新增生长阶段
     */
    @RequiresPermissions("plant:crop_stage:add")
    @Log(title = "生长阶段", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ConstCropStage constCropStage)
    {
        return toAjax(constCropStageService.insertConstCropStage(constCropStage));
    }

    /**
     * 修改生长阶段
     */
    @RequiresPermissions("plant:crop_stage:edit")
    @Log(title = "生长阶段", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ConstCropStage constCropStage)
    {
        return toAjax(constCropStageService.updateConstCropStage(constCropStage));
    }

    /**
     * 删除生长阶段
     */
    @RequiresPermissions("plant:crop_stage:remove")
    @Log(title = "生长阶段", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(constCropStageService.deleteConstCropStageByIds(ids));
    }
}
