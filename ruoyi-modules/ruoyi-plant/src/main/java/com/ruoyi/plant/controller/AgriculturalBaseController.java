package com.ruoyi.plant.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.plant.remote.ISysUserRemote;
import com.ruoyi.plant.vo.AgriculturalBaseVO;
import com.ruoyi.plant.vo.tablist.AgriculturalBaseTableDataInfo;
import com.ruoyi.system.api.domain.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.AgriculturalBase;
import com.ruoyi.plant.service.IAgriculturalBaseService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 基地Controller
 *
 * @author ruoyi
 * @date 2023-02-21
 */
@RestController
@RequestMapping("/agricultural_base")
@Slf4j
public class AgriculturalBaseController extends BaseController
{
    @Autowired
    private IAgriculturalBaseService agriculturalBaseService;

    @Autowired
    ISysUserRemote userRemote;

    /**
     * 查询基地列表
     */
    @RequiresPermissions("plant:agricultural_base:list")
    @GetMapping("/list")
    public TableDataInfo list(AgriculturalBase agriculturalBase)
    {
        startPage();
        List<AgriculturalBase> list = agriculturalBaseService.selectAgriculturalBaseList(agriculturalBase);
        log.info("excute method 1");
        List<AgriculturalBaseVO> voList = new ArrayList<>();
        for(AgriculturalBase item: list) {
            AgriculturalBaseVO vo = new AgriculturalBaseVO();
            BeanUtils.copyProperties(item,vo);

            SysUser sysUser = (SysUser) userRemote.getInfo(item.getUsrId()).get(AjaxResult.DATA_TAG);
            if(sysUser != null) {
                vo.setUsrName(sysUser.getName());
            }

            voList.add(vo);
        }

        AgriculturalBaseTableDataInfo agriculturalBaseTableDataInfo = new AgriculturalBaseTableDataInfo();

        SysUser queryUser = new SysUser();
        List<SysUser> sysUserList = (List<SysUser>) userRemote.queryList(queryUser).get(AjaxResult.DATA_TAG);

        if(sysUserList != null) {
            agriculturalBaseTableDataInfo.setUsrDropList(sysUserList);
        }
        log.info("excute method 2");
        return packVOList(agriculturalBaseTableDataInfo,voList);
    }

    /**
     * 导出基地列表
     */
    @RequiresPermissions("plant:agricultural_base:export")
    @Log(title = "基地", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AgriculturalBase agriculturalBase)
    {
        List<AgriculturalBase> list = agriculturalBaseService.selectAgriculturalBaseList(agriculturalBase);
        ExcelUtil<AgriculturalBase> util = new ExcelUtil<AgriculturalBase>(AgriculturalBase.class);
        util.exportExcel(response, list, "基地数据");
    }

    /**
     * 获取基地详细信息
     */
    @RequiresPermissions("plant:agricultural_base:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(agriculturalBaseService.selectAgriculturalBaseById(id));
    }

    /**
     * 新增基地
     */
    @RequiresPermissions("plant:agricultural_base:add")
    @Log(title = "基地", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AgriculturalBase agriculturalBase)
    {
        return toAjax(agriculturalBaseService.insertAgriculturalBase(agriculturalBase));
    }

    /**
     * 修改基地
     */
    @RequiresPermissions("plant:agricultural_base:edit")
    @Log(title = "基地", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AgriculturalBase agriculturalBase)
    {
        return toAjax(agriculturalBaseService.updateAgriculturalBase(agriculturalBase));
    }

    /**
     * 删除基地
     */
    @RequiresPermissions("plant:agricultural_base:remove")
    @Log(title = "基地", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(agriculturalBaseService.deleteAgriculturalBaseByIds(ids));
    }
}
