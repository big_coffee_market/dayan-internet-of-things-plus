package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.ProductTestingMapper;
import com.ruoyi.plant.domain.ProductTesting;
import com.ruoyi.plant.service.IProductTestingService;

/**
 * 产品检测Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class ProductTestingServiceImpl implements IProductTestingService
{
    @Autowired
    private ProductTestingMapper productTestingMapper;

    /**
     * 查询产品检测
     *
     * @param id 产品检测主键
     * @return 产品检测
     */
    @Override
    public ProductTesting selectProductTestingById(Long id)
    {
        return productTestingMapper.selectProductTestingById(id);
    }

    /**
     * 查询产品检测列表
     *
     * @param productTesting 产品检测
     * @return 产品检测
     */
    @Override
    public List<ProductTesting> selectProductTestingList(ProductTesting productTesting)
    {
        return productTestingMapper.selectProductTestingList(productTesting);
    }

    /**
     * 新增产品检测
     *
     * @param productTesting 产品检测
     * @return 结果
     */
    @Override
    public int insertProductTesting(ProductTesting productTesting)
    {
        return productTestingMapper.insertProductTesting(productTesting);
    }

    /**
     * 修改产品检测
     *
     * @param productTesting 产品检测
     * @return 结果
     */
    @Override
    public int updateProductTesting(ProductTesting productTesting)
    {
        return productTestingMapper.updateProductTesting(productTesting);
    }

    /**
     * 批量删除产品检测
     *
     * @param ids 需要删除的产品检测主键
     * @return 结果
     */
    @Override
    public int deleteProductTestingByIds(Long[] ids)
    {
        return productTestingMapper.deleteProductTestingByIds(ids);
    }

    /**
     * 删除产品检测信息
     *
     * @param id 产品检测主键
     * @return 结果
     */
    @Override
    public int deleteProductTestingById(Long id)
    {
        return productTestingMapper.deleteProductTestingById(id);
    }
}
