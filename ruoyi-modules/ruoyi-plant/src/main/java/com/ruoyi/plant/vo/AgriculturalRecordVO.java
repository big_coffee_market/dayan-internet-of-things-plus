package com.ruoyi.plant.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 农事记录对象 agricultural_record
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class AgriculturalRecordVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 农事计划 */
    @Excel(name = "农事计划")
    private String planName;

    /** 图像 */
    @Excel(name = "图像")
    private String picture;

    /** 操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date operatingTime;

    /** 农事阶段 */
    @Excel(name = "农事阶段")
    private String agriculturalStageName;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;


}
