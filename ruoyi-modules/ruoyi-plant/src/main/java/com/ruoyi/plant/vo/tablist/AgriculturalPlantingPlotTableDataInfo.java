package com.ruoyi.plant.vo.tablist;


import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.api.domain.abs.IDropValue;
import lombok.Data;

import java.util.List;

/**
 * 种植地块对象 agricultural_planting_plot
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class AgriculturalPlantingPlotTableDataInfo extends TableDataInfo
{

    /**
     * 用户列表
     */
    List<? extends IDropValue> usrDropList;

    /**
     * 基地列表
     */
    List<? extends IDropValue> baseDropList;


}
