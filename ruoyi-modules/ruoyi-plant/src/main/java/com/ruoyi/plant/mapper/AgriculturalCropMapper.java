package com.ruoyi.plant.mapper;

import java.util.List;
import com.ruoyi.plant.domain.AgriculturalCrop;

/**
 * 农作物Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface AgriculturalCropMapper 
{
    /**
     * 查询农作物
     * 
     * @param id 农作物主键
     * @return 农作物
     */
    public AgriculturalCrop selectAgriculturalCropById(Long id);

    /**
     * 查询农作物列表
     * 
     * @param agriculturalCrop 农作物
     * @return 农作物集合
     */
    public List<AgriculturalCrop> selectAgriculturalCropList(AgriculturalCrop agriculturalCrop);

    /**
     * 新增农作物
     * 
     * @param agriculturalCrop 农作物
     * @return 结果
     */
    public int insertAgriculturalCrop(AgriculturalCrop agriculturalCrop);

    /**
     * 修改农作物
     * 
     * @param agriculturalCrop 农作物
     * @return 结果
     */
    public int updateAgriculturalCrop(AgriculturalCrop agriculturalCrop);

    /**
     * 删除农作物
     * 
     * @param id 农作物主键
     * @return 结果
     */
    public int deleteAgriculturalCropById(Long id);

    /**
     * 批量删除农作物
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAgriculturalCropByIds(Long[] ids);
}
