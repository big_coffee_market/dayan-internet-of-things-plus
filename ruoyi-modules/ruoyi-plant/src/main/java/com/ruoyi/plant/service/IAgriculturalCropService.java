package com.ruoyi.plant.service;

import java.util.List;
import com.ruoyi.plant.domain.AgriculturalCrop;

/**
 * 农作物Service接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface IAgriculturalCropService 
{
    /**
     * 查询农作物
     * 
     * @param id 农作物主键
     * @return 农作物
     */
    public AgriculturalCrop selectAgriculturalCropById(Long id);

    /**
     * 查询农作物列表
     * 
     * @param agriculturalCrop 农作物
     * @return 农作物集合
     */
    public List<AgriculturalCrop> selectAgriculturalCropList(AgriculturalCrop agriculturalCrop);

    /**
     * 新增农作物
     * 
     * @param agriculturalCrop 农作物
     * @return 结果
     */
    public int insertAgriculturalCrop(AgriculturalCrop agriculturalCrop);

    /**
     * 修改农作物
     * 
     * @param agriculturalCrop 农作物
     * @return 结果
     */
    public int updateAgriculturalCrop(AgriculturalCrop agriculturalCrop);

    /**
     * 批量删除农作物
     * 
     * @param ids 需要删除的农作物主键集合
     * @return 结果
     */
    public int deleteAgriculturalCropByIds(Long[] ids);

    /**
     * 删除农作物信息
     * 
     * @param id 农作物主键
     * @return 结果
     */
    public int deleteAgriculturalCropById(Long id);
}
