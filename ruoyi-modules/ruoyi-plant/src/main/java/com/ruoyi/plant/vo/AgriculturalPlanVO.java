package com.ruoyi.plant.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 农事计划对象 agricultural_plan
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class AgriculturalPlanVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 地块 */
    @Excel(name = "地块")
    private String plotName;

    /** 面积 */
    @Excel(name = "面积")
    private Long area;

    /** 种子 */
    @Excel(name = "种子")
    private String seedName;

    /** 用量 */
    @Excel(name = "用量")
    private Long dosage;

    /** 区块链 */
    @Excel(name = "区块链")
    private String blockchainKey;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 种植状态 */
    @Excel(name = "种植状态")
    private Long plantingCondition;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

}
