package com.ruoyi.plant.controller;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.plant.domain.ConstResourceStage;
import com.ruoyi.plant.service.impl.ConstResourceStageServiceImpl;
import com.ruoyi.plant.vo.AgriculturalResourceVO;
import com.ruoyi.plant.vo.tablist.AgriculturalResourceTableDataInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.AgriculturalResource;
import com.ruoyi.plant.service.IAgriculturalResourceService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 农资管理Controller
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/agricultural_resource")
public class AgriculturalResourceController extends BaseController
{
    @Autowired
    private IAgriculturalResourceService agriculturalResourceService;

    /**
     * 农资类别
     */
    @Autowired
    private ConstResourceStageServiceImpl constResourceStageService;

    /**
     * 查询农资管理列表
     */
    @RequiresPermissions("plant:agricultural_resource:list")
    @GetMapping("/list")
    public TableDataInfo list(AgriculturalResource agriculturalResource)
    {
        startPage();
        List<AgriculturalResource> list = agriculturalResourceService.selectAgriculturalResourceList(agriculturalResource);
        List<AgriculturalResourceVO> voList = new ArrayList<>();
        for(AgriculturalResource item: list) {
             AgriculturalResourceVO vo = new AgriculturalResourceVO();
             BeanUtils.copyProperties(item,vo);
             AgriculturalResource resultItem  = agriculturalResourceService.selectAgriculturalResourceById(item.getClassificationId());
             if(resultItem != null) {
                 vo.setClassificationName(resultItem.getName());
             }
             voList.add(vo);
        }
        ConstResourceStage constResourceStage = new ConstResourceStage();
        List<ConstResourceStage> constResourceStages = constResourceStageService.selectConstResourceStageList(constResourceStage);

        AgriculturalResourceTableDataInfo agriculturalResourceTableDataInfo = new AgriculturalResourceTableDataInfo();
        agriculturalResourceTableDataInfo.setClassificationDropList(constResourceStages);




        return packVOList(agriculturalResourceTableDataInfo,voList);
    }

    /**
     * 导出农资管理列表
     */
    @RequiresPermissions("plant:agricultural_resource:export")
    @Log(title = "农资管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AgriculturalResource agriculturalResource)
    {
        List<AgriculturalResource> list = agriculturalResourceService.selectAgriculturalResourceList(agriculturalResource);
        ExcelUtil<AgriculturalResource> util = new ExcelUtil<AgriculturalResource>(AgriculturalResource.class);
        util.exportExcel(response, list, "农资管理数据");
    }

    /**
     * 获取农资管理详细信息
     */
    @RequiresPermissions("plant:agricultural_resource:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(agriculturalResourceService.selectAgriculturalResourceById(id));
    }

    /**
     * 新增农资管理
     */
    @RequiresPermissions("plant:agricultural_resource:add")
    @Log(title = "农资管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AgriculturalResource agriculturalResource)
    {
        return toAjax(agriculturalResourceService.insertAgriculturalResource(agriculturalResource));
    }

    /**
     * 修改农资管理
     */
    @RequiresPermissions("plant:agricultural_resource:edit")
    @Log(title = "农资管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AgriculturalResource agriculturalResource)
    {
        return toAjax(agriculturalResourceService.updateAgriculturalResource(agriculturalResource));
    }

    /**
     * 删除农资管理
     */
    @RequiresPermissions("plant:agricultural_resource:remove")
    @Log(title = "农资管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(agriculturalResourceService.deleteAgriculturalResourceByIds(ids));
    }
}
