package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.ConstProductSupplierMapper;
import com.ruoyi.plant.domain.ConstProductSupplier;
import com.ruoyi.plant.service.IConstProductSupplierService;

/**
 * 供货商Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class ConstProductSupplierServiceImpl implements IConstProductSupplierService
{
    @Autowired
    private ConstProductSupplierMapper constProductSupplierMapper;

    /**
     * 查询供货商
     *
     * @param id 供货商主键
     * @return 供货商
     */
    @Override
    public ConstProductSupplier selectConstProductSupplierById(Long id)
    {
        return constProductSupplierMapper.selectConstProductSupplierById(id);
    }

    /**
     * 查询供货商列表
     *
     * @param constProductSupplier 供货商
     * @return 供货商
     */
    @Override
    public List<ConstProductSupplier> selectConstProductSupplierList(ConstProductSupplier constProductSupplier)
    {
        return constProductSupplierMapper.selectConstProductSupplierList(constProductSupplier);
    }

    /**
     * 新增供货商
     *
     * @param constProductSupplier 供货商
     * @return 结果
     */
    @Override
    public int insertConstProductSupplier(ConstProductSupplier constProductSupplier)
    {
        return constProductSupplierMapper.insertConstProductSupplier(constProductSupplier);
    }

    /**
     * 修改供货商
     *
     * @param constProductSupplier 供货商
     * @return 结果
     */
    @Override
    public int updateConstProductSupplier(ConstProductSupplier constProductSupplier)
    {
        return constProductSupplierMapper.updateConstProductSupplier(constProductSupplier);
    }

    /**
     * 批量删除供货商
     *
     * @param ids 需要删除的供货商主键
     * @return 结果
     */
    @Override
    public int deleteConstProductSupplierByIds(Long[] ids)
    {
        return constProductSupplierMapper.deleteConstProductSupplierByIds(ids);
    }

    /**
     * 删除供货商信息
     *
     * @param id 供货商主键
     * @return 结果
     */
    @Override
    public int deleteConstProductSupplierById(Long id)
    {
        return constProductSupplierMapper.deleteConstProductSupplierById(id);
    }
}
