package com.ruoyi.plant.service;

import java.util.List;
import com.ruoyi.plant.domain.ConstProcessingStage;

/**
 * 加工类型Service接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface IConstProcessingStageService 
{
    /**
     * 查询加工类型
     * 
     * @param id 加工类型主键
     * @return 加工类型
     */
    public ConstProcessingStage selectConstProcessingStageById(Long id);

    /**
     * 查询加工类型列表
     * 
     * @param constProcessingStage 加工类型
     * @return 加工类型集合
     */
    public List<ConstProcessingStage> selectConstProcessingStageList(ConstProcessingStage constProcessingStage);

    /**
     * 新增加工类型
     * 
     * @param constProcessingStage 加工类型
     * @return 结果
     */
    public int insertConstProcessingStage(ConstProcessingStage constProcessingStage);

    /**
     * 修改加工类型
     * 
     * @param constProcessingStage 加工类型
     * @return 结果
     */
    public int updateConstProcessingStage(ConstProcessingStage constProcessingStage);

    /**
     * 批量删除加工类型
     * 
     * @param ids 需要删除的加工类型主键集合
     * @return 结果
     */
    public int deleteConstProcessingStageByIds(Long[] ids);

    /**
     * 删除加工类型信息
     * 
     * @param id 加工类型主键
     * @return 结果
     */
    public int deleteConstProcessingStageById(Long id);
}
