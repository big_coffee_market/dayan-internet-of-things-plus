package com.ruoyi.plant.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 产品检测对象 product_testing
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class ProductTestingVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 采收批次 */
    @Excel(name = "采收批次")
    private String harvestName;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** 机构 */
    @Excel(name = "机构")
    private String institution;

    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date time;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 结论 */
    @Excel(name = "结论")
    private String conclusion;

    /** 合格证名称 */
    @Excel(name = "合格证名称")
    private String certificateName;

    /** 证书编号 */
    @Excel(name = "证书编号")
    private String certificateNumber;

    /** 证书图片 */
    @Excel(name = "证书图片")
    private String certificatePicture;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;


}
