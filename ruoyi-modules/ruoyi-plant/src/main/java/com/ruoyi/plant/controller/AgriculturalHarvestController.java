package com.ruoyi.plant.controller;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.plant.domain.AgriculturalResource;
import com.ruoyi.plant.service.impl.AgriculturalResourceServiceImpl;
import com.ruoyi.plant.vo.AgriculturalHarvestVO;
import com.ruoyi.plant.vo.AgriculturalResourceVO;
import com.ruoyi.plant.vo.tablist.AgriculturalResourceTableDataInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.AgriculturalHarvest;
import com.ruoyi.plant.service.IAgriculturalHarvestService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 采收管理Controller
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/agricultural_harvest")
public class AgriculturalHarvestController extends BaseController
{

    /**
     * 采收管理
     */
    @Autowired
    private IAgriculturalHarvestService agriculturalHarvestService;

    /**
     * 农资类别
     */
    @Autowired
    private AgriculturalResourceServiceImpl agriculturalResourceService;

    /**
     * 查询采收管理列表
     */
    @RequiresPermissions("plant:agricultural_harvest:list")
    @GetMapping("/list")
    public TableDataInfo list(AgriculturalHarvest agriculturalHarvest)
    {
        startPage();
        List<AgriculturalHarvest> list = agriculturalHarvestService.selectAgriculturalHarvestList(agriculturalHarvest);
        List<AgriculturalHarvestVO> voList = new ArrayList<>();

        for(AgriculturalHarvest item: list) {
            AgriculturalHarvestVO agriculturalHarvestVO = new AgriculturalHarvestVO();
            BeanUtils.copyProperties(item,agriculturalHarvestVO);

            AgriculturalResource agriculturalResource = agriculturalResourceService.selectAgriculturalResourceById(item.getPlanId());
            if(agriculturalResource != null) {
                agriculturalHarvestVO.setPlanName(agriculturalResource.getName());
            }

            voList.add(agriculturalHarvestVO);
        }


        AgriculturalResourceTableDataInfo agriculturalResourceTableDataInfo = new AgriculturalResourceTableDataInfo();

        AgriculturalResource agriculturalResource = new AgriculturalResource();

        List<AgriculturalResource> agriculturalResourceList =  agriculturalResourceService.selectAgriculturalResourceList(agriculturalResource);
        agriculturalResourceTableDataInfo.setClassificationDropList(agriculturalResourceList);


        return packVOList(agriculturalResourceTableDataInfo,list);
    }

    /**
     * 导出采收管理列表
     */
    @RequiresPermissions("plant:agricultural_harvest:export")
    @Log(title = "采收管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AgriculturalHarvest agriculturalHarvest)
    {
        List<AgriculturalHarvest> list = agriculturalHarvestService.selectAgriculturalHarvestList(agriculturalHarvest);

        ExcelUtil<AgriculturalHarvest> util = new ExcelUtil<AgriculturalHarvest>(AgriculturalHarvest.class);
        util.exportExcel(response, list, "采收管理数据");
    }

    /**
     * 获取采收管理详细信息
     */
    @RequiresPermissions("plant:agricultural_harvest:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(agriculturalHarvestService.selectAgriculturalHarvestById(id));
    }

    /**
     * 新增采收管理
     */
    @RequiresPermissions("plant:agricultural_harvest:add")
    @Log(title = "采收管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AgriculturalHarvest agriculturalHarvest)
    {
        return toAjax(agriculturalHarvestService.insertAgriculturalHarvest(agriculturalHarvest));
    }

    /**
     * 修改采收管理
     */
    @RequiresPermissions("plant:agricultural_harvest:edit")
    @Log(title = "采收管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AgriculturalHarvest agriculturalHarvest)
    {
        return toAjax(agriculturalHarvestService.updateAgriculturalHarvest(agriculturalHarvest));
    }

    /**
     * 删除采收管理
     */
    @RequiresPermissions("plant:agricultural_harvest:remove")
    @Log(title = "采收管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(agriculturalHarvestService.deleteAgriculturalHarvestByIds(ids));
    }
}
