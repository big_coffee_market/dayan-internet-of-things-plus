package com.ruoyi.plant.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 产品加工对象 product_processing
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public class ProductProcessing extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 采收批次 */
    @Excel(name = "采收批次")
    private Long harvestId;

    /** 类型 */
    @Excel(name = "类型")
    private Long type;

    /** 数量 */
    @Excel(name = "数量")
    private Long quantity;

    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date time;

    /** 车间 */
    @Excel(name = "车间")
    private String workshop;

    /** 设备 */
    @Excel(name = "设备")
    private String equipment;

    /** 供应商 */
    @Excel(name = "供应商")
    private Long supplierId;

    /** 图片 */
    @Excel(name = "图片")
    private String picture;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setHarvestId(Long harvestId) 
    {
        this.harvestId = harvestId;
    }

    public Long getHarvestId() 
    {
        return harvestId;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setQuantity(Long quantity) 
    {
        this.quantity = quantity;
    }

    public Long getQuantity() 
    {
        return quantity;
    }
    public void setTime(Date time) 
    {
        this.time = time;
    }

    public Date getTime() 
    {
        return time;
    }
    public void setWorkshop(String workshop) 
    {
        this.workshop = workshop;
    }

    public String getWorkshop() 
    {
        return workshop;
    }
    public void setEquipment(String equipment) 
    {
        this.equipment = equipment;
    }

    public String getEquipment() 
    {
        return equipment;
    }
    public void setSupplierId(Long supplierId) 
    {
        this.supplierId = supplierId;
    }

    public Long getSupplierId() 
    {
        return supplierId;
    }
    public void setPicture(String picture) 
    {
        this.picture = picture;
    }

    public String getPicture() 
    {
        return picture;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("harvestId", getHarvestId())
            .append("type", getType())
            .append("quantity", getQuantity())
            .append("time", getTime())
            .append("workshop", getWorkshop())
            .append("equipment", getEquipment())
            .append("supplierId", getSupplierId())
            .append("picture", getPicture())
            .append("remarks", getRemarks())
            .toString();
    }
}
