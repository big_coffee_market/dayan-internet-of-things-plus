package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.ProductProcessingMapper;
import com.ruoyi.plant.domain.ProductProcessing;
import com.ruoyi.plant.service.IProductProcessingService;

/**
 * 产品加工Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class ProductProcessingServiceImpl implements IProductProcessingService
{
    @Autowired
    private ProductProcessingMapper productProcessingMapper;

    /**
     * 查询产品加工
     *
     * @param id 产品加工主键
     * @return 产品加工
     */
    @Override
    public ProductProcessing selectProductProcessingById(Long id)
    {
        return productProcessingMapper.selectProductProcessingById(id);
    }

    /**
     * 查询产品加工列表
     *
     * @param productProcessing 产品加工
     * @return 产品加工
     */
    @Override
    public List<ProductProcessing> selectProductProcessingList(ProductProcessing productProcessing)
    {
        return productProcessingMapper.selectProductProcessingList(productProcessing);
    }

    /**
     * 新增产品加工
     *
     * @param productProcessing 产品加工
     * @return 结果
     */
    @Override
    public int insertProductProcessing(ProductProcessing productProcessing)
    {
        return productProcessingMapper.insertProductProcessing(productProcessing);
    }

    /**
     * 修改产品加工
     *
     * @param productProcessing 产品加工
     * @return 结果
     */
    @Override
    public int updateProductProcessing(ProductProcessing productProcessing)
    {
        return productProcessingMapper.updateProductProcessing(productProcessing);
    }

    /**
     * 批量删除产品加工
     *
     * @param ids 需要删除的产品加工主键
     * @return 结果
     */
    @Override
    public int deleteProductProcessingByIds(Long[] ids)
    {
        return productProcessingMapper.deleteProductProcessingByIds(ids);
    }

    /**
     * 删除产品加工信息
     *
     * @param id 产品加工主键
     * @return 结果
     */
    @Override
    public int deleteProductProcessingById(Long id)
    {
        return productProcessingMapper.deleteProductProcessingById(id);
    }
}
