package com.ruoyi.plant.vo.tablist;

import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.api.domain.abs.IDropValue;
import lombok.Data;

import java.util.List;

/**
 * 农作物对象 agricultural_crop
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class AgriculturalCropTableDataInfo extends TableDataInfo
{

    /**
     * 种子选择列表
     */
    List<? extends IDropValue> seedDropList;


}
