package com.ruoyi.plant.vo.tablist;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.api.domain.abs.IDropValue;
import lombok.Data;

import java.util.List;


/**
 * 农事计划对象 agricultural_plan
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class AgriculturalPlanTableDataInfo extends TableDataInfo
{

    /** 地块 */
    private List<? extends IDropValue> plotDropList;


    /** 种子下拉列表 */
    private List<? extends IDropValue> seedDropList;

}
