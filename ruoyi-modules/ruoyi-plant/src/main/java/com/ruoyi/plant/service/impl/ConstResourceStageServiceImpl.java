package com.ruoyi.plant.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.plant.service.ITreeService;
import com.ruoyi.plant.vo.TreeVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.ConstResourceStageMapper;
import com.ruoyi.plant.domain.ConstResourceStage;
import com.ruoyi.plant.service.IConstResourceStageService;

import javax.annotation.PostConstruct;

/**
 * 农资类型Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-22
 */
@Service
@Slf4j
public class ConstResourceStageServiceImpl implements IConstResourceStageService , ITreeService<ConstResourceStage>
{
    @Autowired
    private ConstResourceStageMapper constResourceStageMapper;

    /**
     * 查询农资类型
     *
     * @param id 农资类型主键
     * @return 农资类型
     */
    @Override
    public ConstResourceStage selectConstResourceStageById(Long id)
    {
        return constResourceStageMapper.selectConstResourceStageById(id);
    }

    /**
     * 查询农资类型列表
     *
     * @param constResourceStage 农资类型
     * @return 农资类型
     */
    @Override
    public List<ConstResourceStage> selectConstResourceStageList(ConstResourceStage constResourceStage)
    {
        return constResourceStageMapper.selectConstResourceStageList(constResourceStage);
    }

    /**
     * 新增农资类型
     *
     * @param constResourceStage 农资类型
     * @return 结果
     */
    @Override
    public int insertConstResourceStage(ConstResourceStage constResourceStage)
    {
        return constResourceStageMapper.insertConstResourceStage(constResourceStage);
    }

    /**
     * 修改农资类型
     *
     * @param constResourceStage 农资类型
     * @return 结果
     */
    @Override
    public int updateConstResourceStage(ConstResourceStage constResourceStage)
    {
        return constResourceStageMapper.updateConstResourceStage(constResourceStage);
    }

    /**
     * 批量删除农资类型
     *
     * @param ids 需要删除的农资类型主键
     * @return 结果
     */
    @Override
    public int deleteConstResourceStageByIds(Long[] ids)
    {
        return constResourceStageMapper.deleteConstResourceStageByIds(ids);
    }

    /**
     * 删除农资类型信息
     *
     * @param id 农资类型主键
     * @return 结果
     */
    @Override
    public int deleteConstResourceStageById(Long id)
    {
        return constResourceStageMapper.deleteConstResourceStageById(id);
    }


    @Override
    public TreeVO takeTree() {
        TreeVO treeVO = new TreeVO();
        List<TreeVO.Node> nodeData = takeNode(-1L);
        treeVO.setData(nodeData);
        return treeVO;
    }

    /**
     * 获取该节点得数据
     */
    @Override
    public List<TreeVO.Node> takeNode(Long nodeId) {

        ConstResourceStage query = new ConstResourceStage();
        query.setParentId(nodeId);
        List<TreeVO.Node> node = new ArrayList<TreeVO.Node>();

        List<ConstResourceStage> result = selectConstResourceStageList(query);
        if(result.size() == 0) {
           return null;
        } else {
            for(ConstResourceStage item : result) {
                TreeVO.Node child = new TreeVO.Node();
                StringBuilder labelBuilder = new StringBuilder();
                labelBuilder.append(item.getName());

                if(!StringUtils.isEmpty(item.getRemarks())) {
                    labelBuilder.append("(").append(item.getRemarks()).append(")");
                }

                child.setLabel(labelBuilder.toString());

                child.setId(item.getId());
                List<TreeVO.Node> childNode = takeNode(item.getId());
                child.setChildren(childNode);
                node.add(child);
            }
        }

        return node;
    }

    /**
     * 增加节点
     */
    @Override
    public Boolean addNode(Long nodeId, ConstResourceStage model) {
        model.setParentId(nodeId);
        Boolean rst = insertConstResourceStage(model) >= 1;

        return rst;
    }

    /**
     * 移除节点
     */
    @Override
    public Boolean remove(Long nodeId) {

        List<TreeVO.Node> nodeData = takeNode(nodeId);
        for(TreeVO.Node item : nodeData) {
            Long childNodeId = item.getId();
            int rows =  deleteConstResourceStageById(childNodeId);
            if(rows == 0) {
                return false;
            }
        }

        return true;
    }









}
