package com.ruoyi.plant.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.ConstProductSupplier;
import com.ruoyi.plant.service.IConstProductSupplierService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 供货商Controller
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/product_supplier")
public class ConstProductSupplierController extends BaseController
{
    @Autowired
    private IConstProductSupplierService constProductSupplierService;

    /**
     * 查询供货商列表
     */
    @RequiresPermissions("plant:product_supplier:list")
    @GetMapping("/list")
    public TableDataInfo list(ConstProductSupplier constProductSupplier)
    {
        startPage();
        List<ConstProductSupplier> list = constProductSupplierService.selectConstProductSupplierList(constProductSupplier);
        return getDataTable(list);
    }


    /**
     * 导出供货商列表
     */
    @RequiresPermissions("plant:product_supplier:export")
    @Log(title = "供货商", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ConstProductSupplier constProductSupplier)
    {
        List<ConstProductSupplier> list = constProductSupplierService.selectConstProductSupplierList(constProductSupplier);
        ExcelUtil<ConstProductSupplier> util = new ExcelUtil<ConstProductSupplier>(ConstProductSupplier.class);
        util.exportExcel(response, list, "供货商数据");
    }

    /**
     * 获取供货商详细信息
     */
    @RequiresPermissions("plant:product_supplier:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(constProductSupplierService.selectConstProductSupplierById(id));
    }

    /**
     * 新增供货商
     */
    @RequiresPermissions("plant:product_supplier:add")
    @Log(title = "供货商", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ConstProductSupplier constProductSupplier)
    {
        return toAjax(constProductSupplierService.insertConstProductSupplier(constProductSupplier));
    }

    /**
     * 修改供货商
     */
    @RequiresPermissions("plant:product_supplier:edit")
    @Log(title = "供货商", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ConstProductSupplier constProductSupplier)
    {
        return toAjax(constProductSupplierService.updateConstProductSupplier(constProductSupplier));
    }

    /**
     * 删除供货商
     */
    @RequiresPermissions("plant:product_supplier:remove")
    @Log(title = "供货商", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(constProductSupplierService.deleteConstProductSupplierByIds(ids));
    }
}
