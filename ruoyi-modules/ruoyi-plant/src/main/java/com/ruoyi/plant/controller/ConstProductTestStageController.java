package com.ruoyi.plant.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.ConstProductTestStage;
import com.ruoyi.plant.service.IConstProductTestStageService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 检测类型Controller
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/product_test_stage")
public class ConstProductTestStageController extends BaseController
{
    @Autowired
    private IConstProductTestStageService constProductTestStageService;

    /**
     * 查询检测类型列表
     */
    @RequiresPermissions("plant:product_test_stage:list")
    @GetMapping("/list")
    public TableDataInfo list(ConstProductTestStage constProductTestStage)
    {
        startPage();
        List<ConstProductTestStage> list = constProductTestStageService.selectConstProductTestStageList(constProductTestStage);
        return getDataTable(list);
    }

    /**
     * 导出检测类型列表
     */
    @RequiresPermissions("plant:product_test_stage:export")
    @Log(title = "检测类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ConstProductTestStage constProductTestStage)
    {
        List<ConstProductTestStage> list = constProductTestStageService.selectConstProductTestStageList(constProductTestStage);
        ExcelUtil<ConstProductTestStage> util = new ExcelUtil<ConstProductTestStage>(ConstProductTestStage.class);
        util.exportExcel(response, list, "检测类型数据");
    }

    /**
     * 获取检测类型详细信息
     */
    @RequiresPermissions("plant:product_test_stage:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(constProductTestStageService.selectConstProductTestStageById(id));
    }

    /**
     * 新增检测类型
     */
    @RequiresPermissions("plant:product_test_stage:add")
    @Log(title = "检测类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ConstProductTestStage constProductTestStage)
    {
        return toAjax(constProductTestStageService.insertConstProductTestStage(constProductTestStage));
    }

    /**
     * 修改检测类型
     */
    @RequiresPermissions("plant:product_test_stage:edit")
    @Log(title = "检测类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ConstProductTestStage constProductTestStage)
    {
        return toAjax(constProductTestStageService.updateConstProductTestStage(constProductTestStage));
    }

    /**
     * 删除检测类型
     */
    @RequiresPermissions("plant:product_test_stage:remove")
    @Log(title = "检测类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(constProductTestStageService.deleteConstProductTestStageByIds(ids));
    }
}
