package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.ConstProcessingStageMapper;
import com.ruoyi.plant.domain.ConstProcessingStage;
import com.ruoyi.plant.service.IConstProcessingStageService;

/**
 * 加工类型Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class ConstProcessingStageServiceImpl implements IConstProcessingStageService
{
    @Autowired
    private ConstProcessingStageMapper constProcessingStageMapper;

    /**
     * 查询加工类型
     *
     * @param id 加工类型主键
     * @return 加工类型
     */
    @Override
    public ConstProcessingStage selectConstProcessingStageById(Long id)
    {
        return constProcessingStageMapper.selectConstProcessingStageById(id);
    }

    /**
     * 查询加工类型列表
     *
     * @param constProcessingStage 加工类型
     * @return 加工类型
     */
    @Override
    public List<ConstProcessingStage> selectConstProcessingStageList(ConstProcessingStage constProcessingStage)
    {
        return constProcessingStageMapper.selectConstProcessingStageList(constProcessingStage);
    }

    /**
     * 新增加工类型
     *
     * @param constProcessingStage 加工类型
     * @return 结果
     */
    @Override
    public int insertConstProcessingStage(ConstProcessingStage constProcessingStage)
    {
        return constProcessingStageMapper.insertConstProcessingStage(constProcessingStage);
    }

    /**
     * 修改加工类型
     *
     * @param constProcessingStage 加工类型
     * @return 结果
     */
    @Override
    public int updateConstProcessingStage(ConstProcessingStage constProcessingStage)
    {
        return constProcessingStageMapper.updateConstProcessingStage(constProcessingStage);
    }

    /**
     * 批量删除加工类型
     *
     * @param ids 需要删除的加工类型主键
     * @return 结果
     */
    @Override
    public int deleteConstProcessingStageByIds(Long[] ids)
    {
        return constProcessingStageMapper.deleteConstProcessingStageByIds(ids);
    }

    /**
     * 删除加工类型信息
     *
     * @param id 加工类型主键
     * @return 结果
     */
    @Override
    public int deleteConstProcessingStageById(Long id)
    {
        return constProcessingStageMapper.deleteConstProcessingStageById(id);
    }
}
