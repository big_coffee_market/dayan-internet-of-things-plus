package com.ruoyi.plant.mapper;

import java.util.List;
import com.ruoyi.plant.domain.ConstCropStage;

/**
 * 生长阶段Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface ConstCropStageMapper 
{
    /**
     * 查询生长阶段
     * 
     * @param id 生长阶段主键
     * @return 生长阶段
     */
    public ConstCropStage selectConstCropStageById(Long id);

    /**
     * 查询生长阶段列表
     * 
     * @param constCropStage 生长阶段
     * @return 生长阶段集合
     */
    public List<ConstCropStage> selectConstCropStageList(ConstCropStage constCropStage);

    /**
     * 新增生长阶段
     * 
     * @param constCropStage 生长阶段
     * @return 结果
     */
    public int insertConstCropStage(ConstCropStage constCropStage);

    /**
     * 修改生长阶段
     * 
     * @param constCropStage 生长阶段
     * @return 结果
     */
    public int updateConstCropStage(ConstCropStage constCropStage);

    /**
     * 删除生长阶段
     * 
     * @param id 生长阶段主键
     * @return 结果
     */
    public int deleteConstCropStageById(Long id);

    /**
     * 批量删除生长阶段
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteConstCropStageByIds(Long[] ids);
}
