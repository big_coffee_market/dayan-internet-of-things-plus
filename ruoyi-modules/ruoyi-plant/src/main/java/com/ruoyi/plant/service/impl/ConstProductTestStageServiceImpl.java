package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.ConstProductTestStageMapper;
import com.ruoyi.plant.domain.ConstProductTestStage;
import com.ruoyi.plant.service.IConstProductTestStageService;

/**
 * 检测类型Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class ConstProductTestStageServiceImpl implements IConstProductTestStageService
{
    @Autowired
    private ConstProductTestStageMapper constProductTestStageMapper;

    /**
     * 查询检测类型
     *
     * @param id 检测类型主键
     * @return 检测类型
     */
    @Override
    public ConstProductTestStage selectConstProductTestStageById(Long id)
    {
        return constProductTestStageMapper.selectConstProductTestStageById(id);
    }

    /**
     * 查询检测类型列表
     *
     * @param constProductTestStage 检测类型
     * @return 检测类型
     */
    @Override
    public List<ConstProductTestStage> selectConstProductTestStageList(ConstProductTestStage constProductTestStage)
    {
        return constProductTestStageMapper.selectConstProductTestStageList(constProductTestStage);
    }

    /**
     * 新增检测类型
     *
     * @param constProductTestStage 检测类型
     * @return 结果
     */
    @Override
    public int insertConstProductTestStage(ConstProductTestStage constProductTestStage)
    {
        return constProductTestStageMapper.insertConstProductTestStage(constProductTestStage);
    }

    /**
     * 修改检测类型
     *
     * @param constProductTestStage 检测类型
     * @return 结果
     */
    @Override
    public int updateConstProductTestStage(ConstProductTestStage constProductTestStage)
    {
        return constProductTestStageMapper.updateConstProductTestStage(constProductTestStage);
    }

    /**
     * 批量删除检测类型
     *
     * @param ids 需要删除的检测类型主键
     * @return 结果
     */
    @Override
    public int deleteConstProductTestStageByIds(Long[] ids)
    {
        return constProductTestStageMapper.deleteConstProductTestStageByIds(ids);
    }

    /**
     * 删除检测类型信息
     *
     * @param id 检测类型主键
     * @return 结果
     */
    @Override
    public int deleteConstProductTestStageById(Long id)
    {
        return constProductTestStageMapper.deleteConstProductTestStageById(id);
    }
}
