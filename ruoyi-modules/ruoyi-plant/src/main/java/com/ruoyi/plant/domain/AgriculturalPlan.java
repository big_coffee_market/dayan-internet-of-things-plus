package com.ruoyi.plant.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 农事计划对象 agricultural_plan
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public class AgriculturalPlan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 地块 */
    @Excel(name = "地块")
    private Long plotId;

    /** 面积 */
    @Excel(name = "面积")
    private Long area;

    /** 种子 */
    @Excel(name = "种子")
    private Long seedId;

    /** 用量 */
    @Excel(name = "用量")
    private Long dosage;

    /** 区块链 */
    @Excel(name = "区块链")
    private String blockchainKey;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 种植状态 */
    @Excel(name = "种植状态")
    private Long plantingCondition;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPlotId(Long plotId) 
    {
        this.plotId = plotId;
    }

    public Long getPlotId() 
    {
        return plotId;
    }
    public void setArea(Long area) 
    {
        this.area = area;
    }

    public Long getArea() 
    {
        return area;
    }
    public void setSeedId(Long seedId) 
    {
        this.seedId = seedId;
    }

    public Long getSeedId() 
    {
        return seedId;
    }
    public void setDosage(Long dosage) 
    {
        this.dosage = dosage;
    }

    public Long getDosage() 
    {
        return dosage;
    }
    public void setBlockchainKey(String blockchainKey) 
    {
        this.blockchainKey = blockchainKey;
    }

    public String getBlockchainKey() 
    {
        return blockchainKey;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setPlantingCondition(Long plantingCondition) 
    {
        this.plantingCondition = plantingCondition;
    }

    public Long getPlantingCondition() 
    {
        return plantingCondition;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("plotId", getPlotId())
            .append("area", getArea())
            .append("seedId", getSeedId())
            .append("dosage", getDosage())
            .append("blockchainKey", getBlockchainKey())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("plantingCondition", getPlantingCondition())
            .append("remarks", getRemarks())
            .toString();
    }
}
