package com.ruoyi.plant.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 产品证书对象 product_certificate
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public class ProductCertificate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 供应商 */
    @Excel(name = "供应商")
    private Long supplierId;

    /** 证书 */
    @Excel(name = "证书")
    private String certificate;

    /** 类型 */
    @Excel(name = "类型")
    private Long type;

    /** 颁发机构 */
    @Excel(name = "颁发机构")
    private String issuingAuthority;

    /** 颁发时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "颁发时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date issueTime;

    /** 证书上传 */
    @Excel(name = "证书上传")
    private String certificateUploading;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setSupplierId(Long supplierId) 
    {
        this.supplierId = supplierId;
    }

    public Long getSupplierId() 
    {
        return supplierId;
    }
    public void setCertificate(String certificate) 
    {
        this.certificate = certificate;
    }

    public String getCertificate() 
    {
        return certificate;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setIssuingAuthority(String issuingAuthority) 
    {
        this.issuingAuthority = issuingAuthority;
    }

    public String getIssuingAuthority() 
    {
        return issuingAuthority;
    }
    public void setIssueTime(Date issueTime) 
    {
        this.issueTime = issueTime;
    }

    public Date getIssueTime() 
    {
        return issueTime;
    }
    public void setCertificateUploading(String certificateUploading) 
    {
        this.certificateUploading = certificateUploading;
    }

    public String getCertificateUploading() 
    {
        return certificateUploading;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("supplierId", getSupplierId())
            .append("certificate", getCertificate())
            .append("type", getType())
            .append("issuingAuthority", getIssuingAuthority())
            .append("issueTime", getIssueTime())
            .append("certificateUploading", getCertificateUploading())
            .append("remarks", getRemarks())
            .toString();
    }
}
