package com.ruoyi.plant.mapper;

import java.util.List;
import com.ruoyi.plant.domain.AgriculturalRecord;

/**
 * 农事记录Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface AgriculturalRecordMapper 
{
    /**
     * 查询农事记录
     * 
     * @param id 农事记录主键
     * @return 农事记录
     */
    public AgriculturalRecord selectAgriculturalRecordById(Long id);

    /**
     * 查询农事记录列表
     * 
     * @param agriculturalRecord 农事记录
     * @return 农事记录集合
     */
    public List<AgriculturalRecord> selectAgriculturalRecordList(AgriculturalRecord agriculturalRecord);

    /**
     * 新增农事记录
     * 
     * @param agriculturalRecord 农事记录
     * @return 结果
     */
    public int insertAgriculturalRecord(AgriculturalRecord agriculturalRecord);

    /**
     * 修改农事记录
     * 
     * @param agriculturalRecord 农事记录
     * @return 结果
     */
    public int updateAgriculturalRecord(AgriculturalRecord agriculturalRecord);

    /**
     * 删除农事记录
     * 
     * @param id 农事记录主键
     * @return 结果
     */
    public int deleteAgriculturalRecordById(Long id);

    /**
     * 批量删除农事记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAgriculturalRecordByIds(Long[] ids);
}
