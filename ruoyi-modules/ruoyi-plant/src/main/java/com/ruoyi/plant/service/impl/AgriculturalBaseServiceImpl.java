package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.AgriculturalBaseMapper;
import com.ruoyi.plant.domain.AgriculturalBase;
import com.ruoyi.plant.service.IAgriculturalBaseService;

/**
 * 基地Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-21
 */
@Service
public class AgriculturalBaseServiceImpl implements IAgriculturalBaseService 
{
    @Autowired
    private AgriculturalBaseMapper agriculturalBaseMapper;

    /**
     * 查询基地
     * 
     * @param id 基地主键
     * @return 基地
     */
    @Override
    public AgriculturalBase selectAgriculturalBaseById(Long id)
    {
        return agriculturalBaseMapper.selectAgriculturalBaseById(id);
    }

    /**
     * 查询基地列表
     * 
     * @param agriculturalBase 基地
     * @return 基地
     */
    @Override
    public List<AgriculturalBase> selectAgriculturalBaseList(AgriculturalBase agriculturalBase)
    {
        return agriculturalBaseMapper.selectAgriculturalBaseList(agriculturalBase);
    }

    /**
     * 新增基地
     * 
     * @param agriculturalBase 基地
     * @return 结果
     */
    @Override
    public int insertAgriculturalBase(AgriculturalBase agriculturalBase)
    {
        return agriculturalBaseMapper.insertAgriculturalBase(agriculturalBase);
    }

    /**
     * 修改基地
     * 
     * @param agriculturalBase 基地
     * @return 结果
     */
    @Override
    public int updateAgriculturalBase(AgriculturalBase agriculturalBase)
    {
        return agriculturalBaseMapper.updateAgriculturalBase(agriculturalBase);
    }

    /**
     * 批量删除基地
     * 
     * @param ids 需要删除的基地主键
     * @return 结果
     */
    @Override
    public int deleteAgriculturalBaseByIds(Long[] ids)
    {
        return agriculturalBaseMapper.deleteAgriculturalBaseByIds(ids);
    }

    /**
     * 删除基地信息
     * 
     * @param id 基地主键
     * @return 结果
     */
    @Override
    public int deleteAgriculturalBaseById(Long id)
    {
        return agriculturalBaseMapper.deleteAgriculturalBaseById(id);
    }
}
