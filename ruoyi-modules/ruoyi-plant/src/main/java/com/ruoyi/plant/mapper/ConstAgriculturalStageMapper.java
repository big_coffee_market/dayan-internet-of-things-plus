package com.ruoyi.plant.mapper;

import java.util.List;
import com.ruoyi.plant.domain.ConstAgriculturalStage;

/**
 * 农事阶段Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface ConstAgriculturalStageMapper 
{
    /**
     * 查询农事阶段
     * 
     * @param id 农事阶段主键
     * @return 农事阶段
     */
    public ConstAgriculturalStage selectConstAgriculturalStageById(Long id);

    /**
     * 查询农事阶段列表
     * 
     * @param constAgriculturalStage 农事阶段
     * @return 农事阶段集合
     */
    public List<ConstAgriculturalStage> selectConstAgriculturalStageList(ConstAgriculturalStage constAgriculturalStage);

    /**
     * 新增农事阶段
     * 
     * @param constAgriculturalStage 农事阶段
     * @return 结果
     */
    public int insertConstAgriculturalStage(ConstAgriculturalStage constAgriculturalStage);

    /**
     * 修改农事阶段
     * 
     * @param constAgriculturalStage 农事阶段
     * @return 结果
     */
    public int updateConstAgriculturalStage(ConstAgriculturalStage constAgriculturalStage);

    /**
     * 删除农事阶段
     * 
     * @param id 农事阶段主键
     * @return 结果
     */
    public int deleteConstAgriculturalStageById(Long id);

    /**
     * 批量删除农事阶段
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteConstAgriculturalStageByIds(Long[] ids);
}
