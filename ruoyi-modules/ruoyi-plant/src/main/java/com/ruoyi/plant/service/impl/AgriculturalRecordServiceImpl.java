package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.AgriculturalRecordMapper;
import com.ruoyi.plant.domain.AgriculturalRecord;
import com.ruoyi.plant.service.IAgriculturalRecordService;

/**
 * 农事记录Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class AgriculturalRecordServiceImpl implements IAgriculturalRecordService
{
    @Autowired
    private AgriculturalRecordMapper agriculturalRecordMapper;

    /**
     * 查询农事记录
     *
     * @param id 农事记录主键
     * @return 农事记录
     */
    @Override
    public AgriculturalRecord selectAgriculturalRecordById(Long id)
    {
        return agriculturalRecordMapper.selectAgriculturalRecordById(id);
    }

    /**
     * 查询农事记录列表
     *
     * @param agriculturalRecord 农事记录
     * @return 农事记录
     */
    @Override
    public List<AgriculturalRecord> selectAgriculturalRecordList(AgriculturalRecord agriculturalRecord)
    {
        return agriculturalRecordMapper.selectAgriculturalRecordList(agriculturalRecord);
    }

    /**
     * 新增农事记录
     *
     * @param agriculturalRecord 农事记录
     * @return 结果
     */
    @Override
    public int insertAgriculturalRecord(AgriculturalRecord agriculturalRecord)
    {
        return agriculturalRecordMapper.insertAgriculturalRecord(agriculturalRecord);
    }

    /**
     * 修改农事记录
     *
     * @param agriculturalRecord 农事记录
     * @return 结果
     */
    @Override
    public int updateAgriculturalRecord(AgriculturalRecord agriculturalRecord)
    {
        return agriculturalRecordMapper.updateAgriculturalRecord(agriculturalRecord);
    }

    /**
     * 批量删除农事记录
     *
     * @param ids 需要删除的农事记录主键
     * @return 结果
     */
    @Override
    public int deleteAgriculturalRecordByIds(Long[] ids)
    {
        return agriculturalRecordMapper.deleteAgriculturalRecordByIds(ids);
    }

    /**
     * 删除农事记录信息
     *
     * @param id 农事记录主键
     * @return 结果
     */
    @Override
    public int deleteAgriculturalRecordById(Long id)
    {
        return agriculturalRecordMapper.deleteAgriculturalRecordById(id);
    }
}
