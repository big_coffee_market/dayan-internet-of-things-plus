package com.ruoyi.plant.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 农作物对象 agricultural_crop
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public class AgriculturalCrop extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 类别 */
    @Excel(name = "类别")
    private Long resourceSeedId;

    /** 别名 */
    @Excel(name = "别名")
    private String alias;

    /** 图像 */
    @Excel(name = "图像")
    private String image;

    /** 所含阶段 */
    @Excel(name = "所含阶段")
    private String stage;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setResourceSeedId(Long resourceSeedId) 
    {
        this.resourceSeedId = resourceSeedId;
    }

    public Long getResourceSeedId() 
    {
        return resourceSeedId;
    }
    public void setAlias(String alias) 
    {
        this.alias = alias;
    }

    public String getAlias() 
    {
        return alias;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setStage(String stage) 
    {
        this.stage = stage;
    }

    public String getStage() 
    {
        return stage;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("resourceSeedId", getResourceSeedId())
            .append("alias", getAlias())
            .append("image", getImage())
            .append("stage", getStage())
            .append("remarks", getRemarks())
            .toString();
    }
}
