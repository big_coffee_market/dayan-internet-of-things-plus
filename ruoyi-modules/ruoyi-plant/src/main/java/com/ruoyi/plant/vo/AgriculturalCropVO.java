package com.ruoyi.plant.vo;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 农作物对象 agricultural_crop
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class AgriculturalCropVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 类别 */
    @Excel(name = "类别")
    private String resourceSeedName;

    /** 别名 */
    @Excel(name = "别名")
    private String alias;

    /** 图像 */
    @Excel(name = "图像")
    private String image;

    /** 所含阶段 */
    @Excel(name = "所含阶段")
    private String stage;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;


}
