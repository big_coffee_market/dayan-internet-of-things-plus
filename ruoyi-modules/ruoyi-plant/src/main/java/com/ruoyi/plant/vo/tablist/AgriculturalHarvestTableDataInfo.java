package com.ruoyi.plant.vo.tablist;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.api.domain.abs.IDropValue;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 采收管理对象 agricultural_harvest
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class AgriculturalHarvestTableDataInfo extends TableDataInfo
{


    /**
     * 农事计划列表
     */
    List<? extends IDropValue> planDropList;


}
