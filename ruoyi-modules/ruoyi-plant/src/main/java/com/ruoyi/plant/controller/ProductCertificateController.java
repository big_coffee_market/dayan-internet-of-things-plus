package com.ruoyi.plant.controller;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.nacos.shaded.org.checkerframework.checker.units.qual.C;
import com.ruoyi.plant.domain.ConstCertificateStage;
import com.ruoyi.plant.domain.ConstProductSupplier;
import com.ruoyi.plant.service.impl.ConstCertificateStageServiceImpl;
import com.ruoyi.plant.service.impl.ConstProductSupplierServiceImpl;
import com.ruoyi.plant.vo.ProductCertificateVO;
import com.ruoyi.plant.vo.tablist.ProductCertificateTableDataInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.ProductCertificate;
import com.ruoyi.plant.service.IProductCertificateService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 产品证书Controller
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/product_certificate")
public class ProductCertificateController extends BaseController
{
    @Autowired
    private IProductCertificateService productCertificateService;

    /**
     * 供货商服务
     */
    @Autowired
    ConstProductSupplierServiceImpl productSupplierService;

    /**
     * 证书服务
     */
    @Autowired
    ConstCertificateStageServiceImpl constCertificateStageService;


    /**
     * 查询产品证书列表
     */
    @RequiresPermissions("plant:product_certificate:list")
    @GetMapping("/list")
    public TableDataInfo list(ProductCertificate productCertificate)
    {
        startPage();
        List<ProductCertificate> list = productCertificateService.selectProductCertificateList(productCertificate);

        List<ProductCertificateVO> voList = new ArrayList<>();

        for(ProductCertificate item : list) {

            ProductCertificateVO vo = new ProductCertificateVO();
            BeanUtils.copyProperties(item,vo);

            ConstCertificateStage constCertificateStage = constCertificateStageService.selectConstCertificateStageById(item.getType());
            if(constCertificateStage != null) {
                vo.setCertificate(constCertificateStage.getName());
            }

            ConstProductSupplier productSupplier = productSupplierService.selectConstProductSupplierById(item.getSupplierId());
            if(productSupplier != null) {
                vo.setSupplierName(productSupplier.getName());
            }

            voList.add(vo);
        }

        ProductCertificateTableDataInfo productCertificateTableDataInfo = new ProductCertificateTableDataInfo();

        ConstProductSupplier querySupplier = new ConstProductSupplier();
        List<ConstProductSupplier> constProductSuppliers = productSupplierService.selectConstProductSupplierList(querySupplier);
        productCertificateTableDataInfo.setSupplierDropList(constProductSuppliers);


        ConstCertificateStage queryCertificate = new ConstCertificateStage();
        List<ConstCertificateStage> certificateStages = constCertificateStageService.selectConstCertificateStageList(queryCertificate);
        productCertificateTableDataInfo.setCertificateDropList(certificateStages);


        return packVOList(productCertificateTableDataInfo,voList);
    }

    /**
     * 导出产品证书列表
     */
    @RequiresPermissions("plant:product_certificate:export")
    @Log(title = "产品证书", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProductCertificate productCertificate)
    {
        List<ProductCertificate> list = productCertificateService.selectProductCertificateList(productCertificate);
        ExcelUtil<ProductCertificate> util = new ExcelUtil<ProductCertificate>(ProductCertificate.class);
        util.exportExcel(response, list, "产品证书数据");
    }

    /**
     * 获取产品证书详细信息
     */
    @RequiresPermissions("plant:product_certificate:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(productCertificateService.selectProductCertificateById(id));
    }

    /**
     * 新增产品证书
     */
    @RequiresPermissions("plant:product_certificate:add")
    @Log(title = "产品证书", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductCertificate productCertificate)
    {
        return toAjax(productCertificateService.insertProductCertificate(productCertificate));
    }

    /**
     * 修改产品证书
     */
    @RequiresPermissions("plant:product_certificate:edit")
    @Log(title = "产品证书", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductCertificate productCertificate)
    {
        return toAjax(productCertificateService.updateProductCertificate(productCertificate));
    }

    /**
     * 删除产品证书
     */
    @RequiresPermissions("plant:product_certificate:remove")
    @Log(title = "产品证书", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(productCertificateService.deleteProductCertificateByIds(ids));
    }
}
