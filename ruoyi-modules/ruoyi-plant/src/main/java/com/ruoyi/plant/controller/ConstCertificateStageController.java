package com.ruoyi.plant.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.ConstCertificateStage;
import com.ruoyi.plant.service.IConstCertificateStageService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 证书类型Controller
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/certificate_stage")
public class ConstCertificateStageController extends BaseController
{
    @Autowired
    private IConstCertificateStageService constCertificateStageService;

    /**
     * 查询证书类型列表
     */
    @RequiresPermissions("plant:certificate_stage:list")
    @GetMapping("/list")
    public TableDataInfo list(ConstCertificateStage constCertificateStage)
    {
        startPage();
        List<ConstCertificateStage> list = constCertificateStageService.selectConstCertificateStageList(constCertificateStage);
        return getDataTable(list);
    }

    /**
     * 导出证书类型列表
     */
    @RequiresPermissions("plant:certificate_stage:export")
    @Log(title = "证书类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ConstCertificateStage constCertificateStage)
    {
        List<ConstCertificateStage> list = constCertificateStageService.selectConstCertificateStageList(constCertificateStage);
        ExcelUtil<ConstCertificateStage> util = new ExcelUtil<ConstCertificateStage>(ConstCertificateStage.class);
        util.exportExcel(response, list, "证书类型数据");
    }

    /**
     * 获取证书类型详细信息
     */
    @RequiresPermissions("plant:certificate_stage:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(constCertificateStageService.selectConstCertificateStageById(id));
    }

    /**
     * 新增证书类型
     */
    @RequiresPermissions("plant:certificate_stage:add")
    @Log(title = "证书类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ConstCertificateStage constCertificateStage)
    {
        return toAjax(constCertificateStageService.insertConstCertificateStage(constCertificateStage));
    }

    /**
     * 修改证书类型
     */
    @RequiresPermissions("plant:certificate_stage:edit")
    @Log(title = "证书类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ConstCertificateStage constCertificateStage)
    {
        return toAjax(constCertificateStageService.updateConstCertificateStage(constCertificateStage));
    }

    /**
     * 删除证书类型
     */
    @RequiresPermissions("plant:certificate_stage:remove")
    @Log(title = "证书类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(constCertificateStageService.deleteConstCertificateStageByIds(ids));
    }
}
