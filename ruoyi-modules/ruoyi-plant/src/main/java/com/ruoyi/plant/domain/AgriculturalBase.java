package com.ruoyi.plant.domain;

import com.ruoyi.system.api.domain.abs.IDropValue;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 基地对象 agricultural_base
 *
 * @author ruoyi
 * @date 2023-02-21
 */
public class AgriculturalBase extends BaseEntity implements IDropValue
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 责任人 */
    @Excel(name = "责任人")
    private Long usrId;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUsrId(Long usrId)
    {
        this.usrId = usrId;
    }

    public Long getUsrId()
    {
        return usrId;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("usrId", getUsrId())
            .append("name", getName())
            .append("remark", getRemark())
            .toString();
    }
}
