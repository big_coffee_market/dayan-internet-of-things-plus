package com.ruoyi.plant.vo.tablist;

import com.ruoyi.common.core.web.domain.BaseEntity;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.api.domain.abs.IDropValue;
import lombok.Data;

import java.util.List;

/**
 * 基地对象 agricultural_base
 *
 * @author ruoyi
 * @date 2023-02-21
 */
@Data
public class AgriculturalBaseTableDataInfo extends TableDataInfo
{
    /**
     * 种子选择列表
     */
    List<? extends IDropValue> usrDropList;



}
