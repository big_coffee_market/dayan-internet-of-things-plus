package com.ruoyi.plant.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.ConstResourceStage;
import com.ruoyi.plant.service.IConstResourceStageService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 农资类型Controller
 *
 * @author ruoyi
 * @date 2023-02-22
 */
@RestController
@RequestMapping("/const_resource_stage")
public class ConstResourceStageController extends BaseController
{
    @Autowired
    private IConstResourceStageService constResourceStageService;

    /**
     * 查询农资类型列表
     */
    @RequiresPermissions("plant:const_resource_stage:list")
    @GetMapping("/list")
    public TableDataInfo list(ConstResourceStage constResourceStage)
    {
        startPage();
        List<ConstResourceStage> list = constResourceStageService.selectConstResourceStageList(constResourceStage);
        return getDataTable(list);
    }

    /**
     * 导出农资类型列表
     */
    @RequiresPermissions("plant:const_resource_stage:export")
    @Log(title = "农资类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ConstResourceStage constResourceStage)
    {
        List<ConstResourceStage> list = constResourceStageService.selectConstResourceStageList(constResourceStage);
        ExcelUtil<ConstResourceStage> util = new ExcelUtil<ConstResourceStage>(ConstResourceStage.class);
        util.exportExcel(response, list, "农资类型数据");
    }

    /**
     * 获取农资类型详细信息
     */
    @RequiresPermissions("plant:const_resource_stage:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(constResourceStageService.selectConstResourceStageById(id));
    }

    /**
     * 新增农资类型
     */
    @RequiresPermissions("plant:const_resource_stage:add")
    @Log(title = "农资类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ConstResourceStage constResourceStage)
    {
        return toAjax(constResourceStageService.insertConstResourceStage(constResourceStage));
    }

    /**
     * 修改农资类型
     */
    @RequiresPermissions("plant:const_resource_stage:edit")
    @Log(title = "农资类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ConstResourceStage constResourceStage)
    {
        return toAjax(constResourceStageService.updateConstResourceStage(constResourceStage));
    }

    /**
     * 删除农资类型
     */
    @RequiresPermissions("plant:const_resource_stage:remove")
    @Log(title = "农资类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(constResourceStageService.deleteConstResourceStageByIds(ids));
    }
}
