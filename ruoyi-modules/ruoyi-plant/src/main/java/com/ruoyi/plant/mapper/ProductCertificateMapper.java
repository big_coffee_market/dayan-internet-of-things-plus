package com.ruoyi.plant.mapper;

import java.util.List;
import com.ruoyi.plant.domain.ProductCertificate;

/**
 * 产品证书Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface ProductCertificateMapper 
{
    /**
     * 查询产品证书
     * 
     * @param id 产品证书主键
     * @return 产品证书
     */
    public ProductCertificate selectProductCertificateById(Long id);

    /**
     * 查询产品证书列表
     * 
     * @param productCertificate 产品证书
     * @return 产品证书集合
     */
    public List<ProductCertificate> selectProductCertificateList(ProductCertificate productCertificate);

    /**
     * 新增产品证书
     * 
     * @param productCertificate 产品证书
     * @return 结果
     */
    public int insertProductCertificate(ProductCertificate productCertificate);

    /**
     * 修改产品证书
     * 
     * @param productCertificate 产品证书
     * @return 结果
     */
    public int updateProductCertificate(ProductCertificate productCertificate);

    /**
     * 删除产品证书
     * 
     * @param id 产品证书主键
     * @return 结果
     */
    public int deleteProductCertificateById(Long id);

    /**
     * 批量删除产品证书
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductCertificateByIds(Long[] ids);
}
