package com.ruoyi.plant.controller;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.ruoyi.plant.domain.AgriculturalHarvest;
import com.ruoyi.plant.domain.ConstProductTestStage;
import com.ruoyi.plant.service.impl.AgriculturalHarvestServiceImpl;
import com.ruoyi.plant.service.impl.ConstProductTestStageServiceImpl;
import com.ruoyi.plant.vo.ProductTestingVO;
import com.ruoyi.plant.vo.tablist.ProductTestingTableDataInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.ProductTesting;
import com.ruoyi.plant.service.IProductTestingService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 产品检测Controller
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/product_testing")
public class ProductTestingController extends BaseController
{
    @Autowired
    private IProductTestingService productTestingService;

    /**
     * 采收批次
     */
    @Autowired
    private AgriculturalHarvestServiceImpl harvestService;

    /**
     * 产品测试
     */
    @Autowired
    private ConstProductTestStageServiceImpl productTestStageService;


    /**
     * 查询产品检测列表
     */
    @RequiresPermissions("plant:product_testing:list")
    @GetMapping("/list")
    public TableDataInfo list(ProductTesting productTesting)
    {
        startPage();
        List<ProductTesting> list = productTestingService.selectProductTestingList(productTesting);
        List<ProductTestingVO> voList = new ArrayList<>();

        for(ProductTesting item: list) {
            ProductTestingVO vo = new ProductTestingVO();
            BeanUtils.copyProperties(item,vo);

            AgriculturalHarvest agriculturalHarvest = harvestService.selectAgriculturalHarvestById(item.getHarvestId());
            if(agriculturalHarvest != null) {
                vo.setHarvestName(agriculturalHarvest.getName());
            }

            ConstProductTestStage constProductTestStage = productTestStageService.selectConstProductTestStageById(item.getType());
            if(constProductTestStage != null) {
                vo.setType(constProductTestStage.getName());
            }

            voList.add(vo);
        }

        ProductTestingTableDataInfo productTestingTableDataInfo = new ProductTestingTableDataInfo();

        AgriculturalHarvest queryHarvest = new AgriculturalHarvest();
        List<AgriculturalHarvest> agriculturalHarvestList = harvestService.selectAgriculturalHarvestList(queryHarvest);
        productTestingTableDataInfo.setHarvestDropList(agriculturalHarvestList);

        ConstProductTestStage queryStage = new ConstProductTestStage();
        List<ConstProductTestStage> constProductTestStageList  = productTestStageService.selectConstProductTestStageList(queryStage);
        productTestingTableDataInfo.setTypeDropList(constProductTestStageList);



        return packVOList(productTestingTableDataInfo,voList);
    }

    /**
     * 导出产品检测列表
     */
    @RequiresPermissions("plant:product_testing:export")
    @Log(title = "产品检测", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProductTesting productTesting)
    {
        List<ProductTesting> list = productTestingService.selectProductTestingList(productTesting);
        ExcelUtil<ProductTesting> util = new ExcelUtil<ProductTesting>(ProductTesting.class);
        util.exportExcel(response, list, "产品检测数据");
    }

    /**
     * 获取产品检测详细信息
     */
    @RequiresPermissions("plant:product_testing:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(productTestingService.selectProductTestingById(id));
    }

    /**
     * 新增产品检测
     */
    @RequiresPermissions("plant:product_testing:add")
    @Log(title = "产品检测", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductTesting productTesting)
    {
        return toAjax(productTestingService.insertProductTesting(productTesting));
    }

    /**
     * 修改产品检测
     */
    @RequiresPermissions("plant:product_testing:edit")
    @Log(title = "产品检测", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductTesting productTesting)
    {
        return toAjax(productTestingService.updateProductTesting(productTesting));
    }

    /**
     * 删除产品检测
     */
    @RequiresPermissions("plant:product_testing:remove")
    @Log(title = "产品检测", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(productTestingService.deleteProductTestingByIds(ids));
    }
}
