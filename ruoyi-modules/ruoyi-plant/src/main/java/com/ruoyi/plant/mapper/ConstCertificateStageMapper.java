package com.ruoyi.plant.mapper;

import java.util.List;
import com.ruoyi.plant.domain.ConstCertificateStage;

/**
 * 证书类型Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface ConstCertificateStageMapper 
{
    /**
     * 查询证书类型
     * 
     * @param id 证书类型主键
     * @return 证书类型
     */
    public ConstCertificateStage selectConstCertificateStageById(Long id);

    /**
     * 查询证书类型列表
     * 
     * @param constCertificateStage 证书类型
     * @return 证书类型集合
     */
    public List<ConstCertificateStage> selectConstCertificateStageList(ConstCertificateStage constCertificateStage);

    /**
     * 新增证书类型
     * 
     * @param constCertificateStage 证书类型
     * @return 结果
     */
    public int insertConstCertificateStage(ConstCertificateStage constCertificateStage);

    /**
     * 修改证书类型
     * 
     * @param constCertificateStage 证书类型
     * @return 结果
     */
    public int updateConstCertificateStage(ConstCertificateStage constCertificateStage);

    /**
     * 删除证书类型
     * 
     * @param id 证书类型主键
     * @return 结果
     */
    public int deleteConstCertificateStageById(Long id);

    /**
     * 批量删除证书类型
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteConstCertificateStageByIds(Long[] ids);
}
