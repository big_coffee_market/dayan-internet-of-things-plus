package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.ConstAgriculturalStageMapper;
import com.ruoyi.plant.domain.ConstAgriculturalStage;
import com.ruoyi.plant.service.IConstAgriculturalStageService;

/**
 * 农事阶段Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class ConstAgriculturalStageServiceImpl implements IConstAgriculturalStageService
{
    @Autowired
    private ConstAgriculturalStageMapper constAgriculturalStageMapper;

    /**
     * 查询农事阶段
     *
     * @param id 农事阶段主键
     * @return 农事阶段
     */
    @Override
    public ConstAgriculturalStage selectConstAgriculturalStageById(Long id)
    {
        return constAgriculturalStageMapper.selectConstAgriculturalStageById(id);
    }

    /**
     * 查询农事阶段列表
     *
     * @param constAgriculturalStage 农事阶段
     * @return 农事阶段
     */
    @Override
    public List<ConstAgriculturalStage> selectConstAgriculturalStageList(ConstAgriculturalStage constAgriculturalStage)
    {
        return constAgriculturalStageMapper.selectConstAgriculturalStageList(constAgriculturalStage);
    }

    /**
     * 新增农事阶段
     *
     * @param constAgriculturalStage 农事阶段
     * @return 结果
     */
    @Override
    public int insertConstAgriculturalStage(ConstAgriculturalStage constAgriculturalStage)
    {
        return constAgriculturalStageMapper.insertConstAgriculturalStage(constAgriculturalStage);
    }

    /**
     * 修改农事阶段
     *
     * @param constAgriculturalStage 农事阶段
     * @return 结果
     */
    @Override
    public int updateConstAgriculturalStage(ConstAgriculturalStage constAgriculturalStage)
    {
        return constAgriculturalStageMapper.updateConstAgriculturalStage(constAgriculturalStage);
    }

    /**
     * 批量删除农事阶段
     *
     * @param ids 需要删除的农事阶段主键
     * @return 结果
     */
    @Override
    public int deleteConstAgriculturalStageByIds(Long[] ids)
    {
        return constAgriculturalStageMapper.deleteConstAgriculturalStageByIds(ids);
    }

    /**
     * 删除农事阶段信息
     *
     * @param id 农事阶段主键
     * @return 结果
     */
    @Override
    public int deleteConstAgriculturalStageById(Long id)
    {
        return constAgriculturalStageMapper.deleteConstAgriculturalStageById(id);
    }
}
