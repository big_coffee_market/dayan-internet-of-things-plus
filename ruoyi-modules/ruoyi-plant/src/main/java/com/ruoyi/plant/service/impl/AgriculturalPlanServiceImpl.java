package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.AgriculturalPlanMapper;
import com.ruoyi.plant.domain.AgriculturalPlan;
import com.ruoyi.plant.service.IAgriculturalPlanService;

/**
 * 农事计划Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class AgriculturalPlanServiceImpl implements IAgriculturalPlanService
{
    @Autowired
    private AgriculturalPlanMapper agriculturalPlanMapper;

    /**
     * 查询农事计划
     *
     * @param id 农事计划主键
     * @return 农事计划
     */
    @Override
    public AgriculturalPlan selectAgriculturalPlanById(Long id)
    {
        return agriculturalPlanMapper.selectAgriculturalPlanById(id);
    }

    /**
     * 查询农事计划列表
     *
     * @param agriculturalPlan 农事计划
     * @return 农事计划
     */
    @Override
    public List<AgriculturalPlan> selectAgriculturalPlanList(AgriculturalPlan agriculturalPlan)
    {
        return agriculturalPlanMapper.selectAgriculturalPlanList(agriculturalPlan);
    }

    /**
     * 新增农事计划
     *
     * @param agriculturalPlan 农事计划
     * @return 结果
     */
    @Override
    public int insertAgriculturalPlan(AgriculturalPlan agriculturalPlan)
    {
        return agriculturalPlanMapper.insertAgriculturalPlan(agriculturalPlan);
    }

    /**
     * 修改农事计划
     *
     * @param agriculturalPlan 农事计划
     * @return 结果
     */
    @Override
    public int updateAgriculturalPlan(AgriculturalPlan agriculturalPlan)
    {
        return agriculturalPlanMapper.updateAgriculturalPlan(agriculturalPlan);
    }

    /**
     * 批量删除农事计划
     *
     * @param ids 需要删除的农事计划主键
     * @return 结果
     */
    @Override
    public int deleteAgriculturalPlanByIds(Long[] ids)
    {
        return agriculturalPlanMapper.deleteAgriculturalPlanByIds(ids);
    }

    /**
     * 删除农事计划信息
     *
     * @param id 农事计划主键
     * @return 结果
     */
    @Override
    public int deleteAgriculturalPlanById(Long id)
    {
        return agriculturalPlanMapper.deleteAgriculturalPlanById(id);
    }
}
