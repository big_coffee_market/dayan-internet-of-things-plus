package com.ruoyi.plant.service;

import java.util.List;
import com.ruoyi.plant.domain.ProductCertificate;

/**
 * 产品证书Service接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface IProductCertificateService 
{
    /**
     * 查询产品证书
     * 
     * @param id 产品证书主键
     * @return 产品证书
     */
    public ProductCertificate selectProductCertificateById(Long id);

    /**
     * 查询产品证书列表
     * 
     * @param productCertificate 产品证书
     * @return 产品证书集合
     */
    public List<ProductCertificate> selectProductCertificateList(ProductCertificate productCertificate);

    /**
     * 新增产品证书
     * 
     * @param productCertificate 产品证书
     * @return 结果
     */
    public int insertProductCertificate(ProductCertificate productCertificate);

    /**
     * 修改产品证书
     * 
     * @param productCertificate 产品证书
     * @return 结果
     */
    public int updateProductCertificate(ProductCertificate productCertificate);

    /**
     * 批量删除产品证书
     * 
     * @param ids 需要删除的产品证书主键集合
     * @return 结果
     */
    public int deleteProductCertificateByIds(Long[] ids);

    /**
     * 删除产品证书信息
     * 
     * @param id 产品证书主键
     * @return 结果
     */
    public int deleteProductCertificateById(Long id);
}
