package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.AgriculturalCropMapper;
import com.ruoyi.plant.domain.AgriculturalCrop;
import com.ruoyi.plant.service.IAgriculturalCropService;

/**
 * 农作物Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class AgriculturalCropServiceImpl implements IAgriculturalCropService
{
    @Autowired
    private AgriculturalCropMapper agriculturalCropMapper;

    /**
     * 查询农作物
     *
     * @param id 农作物主键
     * @return 农作物
     */
    @Override
    public AgriculturalCrop selectAgriculturalCropById(Long id)
    {
        return agriculturalCropMapper.selectAgriculturalCropById(id);
    }

    /**
     * 查询农作物列表
     *
     * @param agriculturalCrop 农作物
     * @return 农作物
     */
    @Override
    public List<AgriculturalCrop> selectAgriculturalCropList(AgriculturalCrop agriculturalCrop)
    {
        return agriculturalCropMapper.selectAgriculturalCropList(agriculturalCrop);
    }

    /**
     * 新增农作物
     *
     * @param agriculturalCrop 农作物
     * @return 结果
     */
    @Override
    public int insertAgriculturalCrop(AgriculturalCrop agriculturalCrop)
    {
        return agriculturalCropMapper.insertAgriculturalCrop(agriculturalCrop);
    }

    /**
     * 修改农作物
     *
     * @param agriculturalCrop 农作物
     * @return 结果
     */
    @Override
    public int updateAgriculturalCrop(AgriculturalCrop agriculturalCrop)
    {
        return agriculturalCropMapper.updateAgriculturalCrop(agriculturalCrop);
    }

    /**
     * 批量删除农作物
     *
     * @param ids 需要删除的农作物主键
     * @return 结果
     */
    @Override
    public int deleteAgriculturalCropByIds(Long[] ids)
    {
        return agriculturalCropMapper.deleteAgriculturalCropByIds(ids);
    }

    /**
     * 删除农作物信息
     *
     * @param id 农作物主键
     * @return 结果
     */
    @Override
    public int deleteAgriculturalCropById(Long id)
    {
        return agriculturalCropMapper.deleteAgriculturalCropById(id);
    }
}
