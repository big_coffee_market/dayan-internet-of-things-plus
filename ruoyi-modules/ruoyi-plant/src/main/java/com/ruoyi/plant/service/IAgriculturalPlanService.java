package com.ruoyi.plant.service;

import java.util.List;
import com.ruoyi.plant.domain.AgriculturalPlan;

/**
 * 农事计划Service接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface IAgriculturalPlanService 
{
    /**
     * 查询农事计划
     * 
     * @param id 农事计划主键
     * @return 农事计划
     */
    public AgriculturalPlan selectAgriculturalPlanById(Long id);

    /**
     * 查询农事计划列表
     * 
     * @param agriculturalPlan 农事计划
     * @return 农事计划集合
     */
    public List<AgriculturalPlan> selectAgriculturalPlanList(AgriculturalPlan agriculturalPlan);

    /**
     * 新增农事计划
     * 
     * @param agriculturalPlan 农事计划
     * @return 结果
     */
    public int insertAgriculturalPlan(AgriculturalPlan agriculturalPlan);

    /**
     * 修改农事计划
     * 
     * @param agriculturalPlan 农事计划
     * @return 结果
     */
    public int updateAgriculturalPlan(AgriculturalPlan agriculturalPlan);

    /**
     * 批量删除农事计划
     * 
     * @param ids 需要删除的农事计划主键集合
     * @return 结果
     */
    public int deleteAgriculturalPlanByIds(Long[] ids);

    /**
     * 删除农事计划信息
     * 
     * @param id 农事计划主键
     * @return 结果
     */
    public int deleteAgriculturalPlanById(Long id);
}
