package com.ruoyi.plant.service;

import java.util.List;
import com.ruoyi.plant.domain.ConstProductTestStage;

/**
 * 检测类型Service接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface IConstProductTestStageService 
{
    /**
     * 查询检测类型
     * 
     * @param id 检测类型主键
     * @return 检测类型
     */
    public ConstProductTestStage selectConstProductTestStageById(Long id);

    /**
     * 查询检测类型列表
     * 
     * @param constProductTestStage 检测类型
     * @return 检测类型集合
     */
    public List<ConstProductTestStage> selectConstProductTestStageList(ConstProductTestStage constProductTestStage);

    /**
     * 新增检测类型
     * 
     * @param constProductTestStage 检测类型
     * @return 结果
     */
    public int insertConstProductTestStage(ConstProductTestStage constProductTestStage);

    /**
     * 修改检测类型
     * 
     * @param constProductTestStage 检测类型
     * @return 结果
     */
    public int updateConstProductTestStage(ConstProductTestStage constProductTestStage);

    /**
     * 批量删除检测类型
     * 
     * @param ids 需要删除的检测类型主键集合
     * @return 结果
     */
    public int deleteConstProductTestStageByIds(Long[] ids);

    /**
     * 删除检测类型信息
     * 
     * @param id 检测类型主键
     * @return 结果
     */
    public int deleteConstProductTestStageById(Long id);
}
