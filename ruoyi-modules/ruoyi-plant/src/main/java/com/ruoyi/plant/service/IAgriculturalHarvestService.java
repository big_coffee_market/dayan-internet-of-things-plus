package com.ruoyi.plant.service;

import java.util.List;
import com.ruoyi.plant.domain.AgriculturalHarvest;

/**
 * 采收管理Service接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface IAgriculturalHarvestService 
{
    /**
     * 查询采收管理
     * 
     * @param id 采收管理主键
     * @return 采收管理
     */
    public AgriculturalHarvest selectAgriculturalHarvestById(Long id);

    /**
     * 查询采收管理列表
     * 
     * @param agriculturalHarvest 采收管理
     * @return 采收管理集合
     */
    public List<AgriculturalHarvest> selectAgriculturalHarvestList(AgriculturalHarvest agriculturalHarvest);

    /**
     * 新增采收管理
     * 
     * @param agriculturalHarvest 采收管理
     * @return 结果
     */
    public int insertAgriculturalHarvest(AgriculturalHarvest agriculturalHarvest);

    /**
     * 修改采收管理
     * 
     * @param agriculturalHarvest 采收管理
     * @return 结果
     */
    public int updateAgriculturalHarvest(AgriculturalHarvest agriculturalHarvest);

    /**
     * 批量删除采收管理
     * 
     * @param ids 需要删除的采收管理主键集合
     * @return 结果
     */
    public int deleteAgriculturalHarvestByIds(Long[] ids);

    /**
     * 删除采收管理信息
     * 
     * @param id 采收管理主键
     * @return 结果
     */
    public int deleteAgriculturalHarvestById(Long id);
}
