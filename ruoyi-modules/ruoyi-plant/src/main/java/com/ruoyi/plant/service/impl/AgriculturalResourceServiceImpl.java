package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.AgriculturalResourceMapper;
import com.ruoyi.plant.domain.AgriculturalResource;
import com.ruoyi.plant.service.IAgriculturalResourceService;

/**
 * 农资管理Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class AgriculturalResourceServiceImpl implements IAgriculturalResourceService
{
    @Autowired
    private AgriculturalResourceMapper agriculturalResourceMapper;

    /**
     * 查询农资管理
     *
     * @param id 农资管理主键
     * @return 农资管理
     */
    @Override
    public AgriculturalResource selectAgriculturalResourceById(Long id)
    {
        return agriculturalResourceMapper.selectAgriculturalResourceById(id);
    }

    /**
     * 查询农资管理列表
     *
     * @param agriculturalResource 农资管理
     * @return 农资管理
     */
    @Override
    public List<AgriculturalResource> selectAgriculturalResourceList(AgriculturalResource agriculturalResource)
    {
        return agriculturalResourceMapper.selectAgriculturalResourceList(agriculturalResource);
    }

    /**
     * 新增农资管理
     *
     * @param agriculturalResource 农资管理
     * @return 结果
     */
    @Override
    public int insertAgriculturalResource(AgriculturalResource agriculturalResource)
    {
        return agriculturalResourceMapper.insertAgriculturalResource(agriculturalResource);
    }

    /**
     * 修改农资管理
     *
     * @param agriculturalResource 农资管理
     * @return 结果
     */
    @Override
    public int updateAgriculturalResource(AgriculturalResource agriculturalResource)
    {
        return agriculturalResourceMapper.updateAgriculturalResource(agriculturalResource);
    }

    /**
     * 批量删除农资管理
     *
     * @param ids 需要删除的农资管理主键
     * @return 结果
     */
    @Override
    public int deleteAgriculturalResourceByIds(Long[] ids)
    {
        return agriculturalResourceMapper.deleteAgriculturalResourceByIds(ids);
    }

    /**
     * 删除农资管理信息
     *
     * @param id 农资管理主键
     * @return 结果
     */
    @Override
    public int deleteAgriculturalResourceById(Long id)
    {
        return agriculturalResourceMapper.deleteAgriculturalResourceById(id);
    }
}
