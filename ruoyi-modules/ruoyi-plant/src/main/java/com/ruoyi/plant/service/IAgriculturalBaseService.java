package com.ruoyi.plant.service;

import java.util.List;
import com.ruoyi.plant.domain.AgriculturalBase;

/**
 * 基地Service接口
 * 
 * @author ruoyi
 * @date 2023-02-21
 */
public interface IAgriculturalBaseService 
{
    /**
     * 查询基地
     * 
     * @param id 基地主键
     * @return 基地
     */
    public AgriculturalBase selectAgriculturalBaseById(Long id);

    /**
     * 查询基地列表
     * 
     * @param agriculturalBase 基地
     * @return 基地集合
     */
    public List<AgriculturalBase> selectAgriculturalBaseList(AgriculturalBase agriculturalBase);

    /**
     * 新增基地
     * 
     * @param agriculturalBase 基地
     * @return 结果
     */
    public int insertAgriculturalBase(AgriculturalBase agriculturalBase);

    /**
     * 修改基地
     * 
     * @param agriculturalBase 基地
     * @return 结果
     */
    public int updateAgriculturalBase(AgriculturalBase agriculturalBase);

    /**
     * 批量删除基地
     * 
     * @param ids 需要删除的基地主键集合
     * @return 结果
     */
    public int deleteAgriculturalBaseByIds(Long[] ids);

    /**
     * 删除基地信息
     * 
     * @param id 基地主键
     * @return 结果
     */
    public int deleteAgriculturalBaseById(Long id);
}
