package com.ruoyi.plant.remote;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.api.factory.RemoteLogFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(contextId = "sysUser", value = "127.0.0.1:8080/system", fallbackFactory = RemoteLogFallbackFactory.class)
public interface ISysUserRemote {


    /**
     * 获取用户信息
     * @param userId 用户id
     * @return
     */
    @GetMapping(value = { "/user/{userId}" })
    AjaxResult getInfo(@PathVariable(value = "userId", required = false) Long userId);

    /**
     * 获取用户列表
     * @param user 用户信息
     * @return
     */
    @PostMapping("/user/query/list")
    AjaxResult queryList(@RequestBody SysUser user);

}
