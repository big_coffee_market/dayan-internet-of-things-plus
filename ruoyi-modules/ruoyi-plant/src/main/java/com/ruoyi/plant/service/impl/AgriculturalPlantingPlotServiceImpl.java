package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.AgriculturalPlantingPlotMapper;
import com.ruoyi.plant.domain.AgriculturalPlantingPlot;
import com.ruoyi.plant.service.IAgriculturalPlantingPlotService;

/**
 * 种植地块Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class AgriculturalPlantingPlotServiceImpl implements IAgriculturalPlantingPlotService
{
    @Autowired
    private AgriculturalPlantingPlotMapper agriculturalPlantingPlotMapper;

    /**
     * 查询种植地块
     *
     * @param id 种植地块主键
     * @return 种植地块
     */
    @Override
    public AgriculturalPlantingPlot selectAgriculturalPlantingPlotById(Long id)
    {
        return agriculturalPlantingPlotMapper.selectAgriculturalPlantingPlotById(id);
    }

    /**
     * 查询种植地块列表
     *
     * @param agriculturalPlantingPlot 种植地块
     * @return 种植地块
     */
    @Override
    public List<AgriculturalPlantingPlot> selectAgriculturalPlantingPlotList(AgriculturalPlantingPlot agriculturalPlantingPlot)
    {
        return agriculturalPlantingPlotMapper.selectAgriculturalPlantingPlotList(agriculturalPlantingPlot);
    }

    /**
     * 新增种植地块
     *
     * @param agriculturalPlantingPlot 种植地块
     * @return 结果
     */
    @Override
    public int insertAgriculturalPlantingPlot(AgriculturalPlantingPlot agriculturalPlantingPlot)
    {
        return agriculturalPlantingPlotMapper.insertAgriculturalPlantingPlot(agriculturalPlantingPlot);
    }

    /**
     * 修改种植地块
     *
     * @param agriculturalPlantingPlot 种植地块
     * @return 结果
     */
    @Override
    public int updateAgriculturalPlantingPlot(AgriculturalPlantingPlot agriculturalPlantingPlot)
    {
        return agriculturalPlantingPlotMapper.updateAgriculturalPlantingPlot(agriculturalPlantingPlot);
    }

    /**
     * 批量删除种植地块
     *
     * @param ids 需要删除的种植地块主键
     * @return 结果
     */
    @Override
    public int deleteAgriculturalPlantingPlotByIds(Long[] ids)
    {
        return agriculturalPlantingPlotMapper.deleteAgriculturalPlantingPlotByIds(ids);
    }

    /**
     * 删除种植地块信息
     *
     * @param id 种植地块主键
     * @return 结果
     */
    @Override
    public int deleteAgriculturalPlantingPlotById(Long id)
    {
        return agriculturalPlantingPlotMapper.deleteAgriculturalPlantingPlotById(id);
    }
}
