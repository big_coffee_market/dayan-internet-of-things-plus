package com.ruoyi.plant.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.ConstAgriculturalStage;
import com.ruoyi.plant.service.IConstAgriculturalStageService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 农事阶段Controller
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/agricultural_stage")
public class ConstAgriculturalStageController extends BaseController
{
    @Autowired
    private IConstAgriculturalStageService constAgriculturalStageService;

    /**
     * 查询农事阶段列表
     */
    @RequiresPermissions("plant:agricultural_stage:list")
    @GetMapping("/list")
    public TableDataInfo list(ConstAgriculturalStage constAgriculturalStage)
    {
        startPage();
        List<ConstAgriculturalStage> list = constAgriculturalStageService.selectConstAgriculturalStageList(constAgriculturalStage);
        return getDataTable(list);
    }

    /**
     * 导出农事阶段列表
     */
    @RequiresPermissions("plant:agricultural_stage:export")
    @Log(title = "农事阶段", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ConstAgriculturalStage constAgriculturalStage)
    {
        List<ConstAgriculturalStage> list = constAgriculturalStageService.selectConstAgriculturalStageList(constAgriculturalStage);
        ExcelUtil<ConstAgriculturalStage> util = new ExcelUtil<ConstAgriculturalStage>(ConstAgriculturalStage.class);
        util.exportExcel(response, list, "农事阶段数据");
    }

    /**
     * 获取农事阶段详细信息
     */
    @RequiresPermissions("plant:agricultural_stage:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(constAgriculturalStageService.selectConstAgriculturalStageById(id));
    }

    /**
     * 新增农事阶段
     */
    @RequiresPermissions("plant:agricultural_stage:add")
    @Log(title = "农事阶段", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ConstAgriculturalStage constAgriculturalStage)
    {
        return toAjax(constAgriculturalStageService.insertConstAgriculturalStage(constAgriculturalStage));
    }

    /**
     * 修改农事阶段
     */
    @RequiresPermissions("plant:agricultural_stage:edit")
    @Log(title = "农事阶段", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ConstAgriculturalStage constAgriculturalStage)
    {
        return toAjax(constAgriculturalStageService.updateConstAgriculturalStage(constAgriculturalStage));
    }

    /**
     * 删除农事阶段
     */
    @RequiresPermissions("plant:agricultural_stage:remove")
    @Log(title = "农事阶段", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(constAgriculturalStageService.deleteConstAgriculturalStageByIds(ids));
    }
}
