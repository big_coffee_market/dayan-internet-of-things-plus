package com.ruoyi.plant.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.system.api.domain.abs.IDropValue;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 农资管理对象 agricultural_resource
 *
 * @author ruoyi
 * @date 2023-02-18
 */
public class AgriculturalResource extends BaseEntity implements IDropValue
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 类别 */
    @Excel(name = "类别")
    private Long classificationId;

    /** 包装规格 */
    @Excel(name = "包装规格")
    private String packingSpecification;

    /** 库存量 */
    @Excel(name = "库存量")
    private Long inventory;

    /** 生产厂家 */
    @Excel(name = "生产厂家")
    private String manufacturer;

    /** 生产日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生产日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dateOfProduction;

    /** 保质期 */
    @Excel(name = "保质期")
    private String shelfLife;

    /** 批准文号 */
    @Excel(name = "批准文号")
    private String approvalNumber;

    /** 生产许可证 */
    @Excel(name = "生产许可证")
    private String productionLicense;

    /** 销售厂商 */
    @Excel(name = "销售厂商")
    private String sellingFirm;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setClassificationId(Long classificationId)
    {
        this.classificationId = classificationId;
    }

    public Long getClassificationId()
    {
        return classificationId;
    }
    public void setPackingSpecification(String packingSpecification)
    {
        this.packingSpecification = packingSpecification;
    }

    public String getPackingSpecification()
    {
        return packingSpecification;
    }
    public void setInventory(Long inventory)
    {
        this.inventory = inventory;
    }

    public Long getInventory()
    {
        return inventory;
    }
    public void setManufacturer(String manufacturer)
    {
        this.manufacturer = manufacturer;
    }

    public String getManufacturer()
    {
        return manufacturer;
    }
    public void setDateOfProduction(Date dateOfProduction)
    {
        this.dateOfProduction = dateOfProduction;
    }

    public Date getDateOfProduction()
    {
        return dateOfProduction;
    }
    public void setShelfLife(String shelfLife)
    {
        this.shelfLife = shelfLife;
    }

    public String getShelfLife()
    {
        return shelfLife;
    }
    public void setApprovalNumber(String approvalNumber)
    {
        this.approvalNumber = approvalNumber;
    }

    public String getApprovalNumber()
    {
        return approvalNumber;
    }
    public void setProductionLicense(String productionLicense)
    {
        this.productionLicense = productionLicense;
    }

    public String getProductionLicense()
    {
        return productionLicense;
    }
    public void setSellingFirm(String sellingFirm)
    {
        this.sellingFirm = sellingFirm;
    }

    public String getSellingFirm()
    {
        return sellingFirm;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public String getRemarks()
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("classificationId", getClassificationId())
            .append("packingSpecification", getPackingSpecification())
            .append("inventory", getInventory())
            .append("manufacturer", getManufacturer())
            .append("dateOfProduction", getDateOfProduction())
            .append("shelfLife", getShelfLife())
            .append("approvalNumber", getApprovalNumber())
            .append("productionLicense", getProductionLicense())
            .append("sellingFirm", getSellingFirm())
            .append("description", getDescription())
            .append("remarks", getRemarks())
            .toString();
    }
}
