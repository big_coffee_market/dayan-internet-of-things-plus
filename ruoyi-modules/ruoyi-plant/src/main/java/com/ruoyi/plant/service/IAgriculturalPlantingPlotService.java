package com.ruoyi.plant.service;

import java.util.List;
import com.ruoyi.plant.domain.AgriculturalPlantingPlot;

/**
 * 种植地块Service接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface IAgriculturalPlantingPlotService 
{
    /**
     * 查询种植地块
     * 
     * @param id 种植地块主键
     * @return 种植地块
     */
    public AgriculturalPlantingPlot selectAgriculturalPlantingPlotById(Long id);

    /**
     * 查询种植地块列表
     * 
     * @param agriculturalPlantingPlot 种植地块
     * @return 种植地块集合
     */
    public List<AgriculturalPlantingPlot> selectAgriculturalPlantingPlotList(AgriculturalPlantingPlot agriculturalPlantingPlot);

    /**
     * 新增种植地块
     * 
     * @param agriculturalPlantingPlot 种植地块
     * @return 结果
     */
    public int insertAgriculturalPlantingPlot(AgriculturalPlantingPlot agriculturalPlantingPlot);

    /**
     * 修改种植地块
     * 
     * @param agriculturalPlantingPlot 种植地块
     * @return 结果
     */
    public int updateAgriculturalPlantingPlot(AgriculturalPlantingPlot agriculturalPlantingPlot);

    /**
     * 批量删除种植地块
     * 
     * @param ids 需要删除的种植地块主键集合
     * @return 结果
     */
    public int deleteAgriculturalPlantingPlotByIds(Long[] ids);

    /**
     * 删除种植地块信息
     * 
     * @param id 种植地块主键
     * @return 结果
     */
    public int deleteAgriculturalPlantingPlotById(Long id);
}
