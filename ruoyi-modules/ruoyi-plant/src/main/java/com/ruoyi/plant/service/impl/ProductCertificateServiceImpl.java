package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.ProductCertificateMapper;
import com.ruoyi.plant.domain.ProductCertificate;
import com.ruoyi.plant.service.IProductCertificateService;

/**
 * 产品证书Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class ProductCertificateServiceImpl implements IProductCertificateService
{
    @Autowired
    private ProductCertificateMapper productCertificateMapper;

    /**
     * 查询产品证书
     *
     * @param id 产品证书主键
     * @return 产品证书
     */
    @Override
    public ProductCertificate selectProductCertificateById(Long id)
    {
        return productCertificateMapper.selectProductCertificateById(id);
    }

    /**
     * 查询产品证书列表
     *
     * @param productCertificate 产品证书
     * @return 产品证书
     */
    @Override
    public List<ProductCertificate> selectProductCertificateList(ProductCertificate productCertificate)
    {
        return productCertificateMapper.selectProductCertificateList(productCertificate);
    }

    /**
     * 新增产品证书
     *
     * @param productCertificate 产品证书
     * @return 结果
     */
    @Override
    public int insertProductCertificate(ProductCertificate productCertificate)
    {
        return productCertificateMapper.insertProductCertificate(productCertificate);
    }

    /**
     * 修改产品证书
     *
     * @param productCertificate 产品证书
     * @return 结果
     */
    @Override
    public int updateProductCertificate(ProductCertificate productCertificate)
    {
        return productCertificateMapper.updateProductCertificate(productCertificate);
    }

    /**
     * 批量删除产品证书
     *
     * @param ids 需要删除的产品证书主键
     * @return 结果
     */
    @Override
    public int deleteProductCertificateByIds(Long[] ids)
    {
        return productCertificateMapper.deleteProductCertificateByIds(ids);
    }

    /**
     * 删除产品证书信息
     *
     * @param id 产品证书主键
     * @return 结果
     */
    @Override
    public int deleteProductCertificateById(Long id)
    {
        return productCertificateMapper.deleteProductCertificateById(id);
    }
}
