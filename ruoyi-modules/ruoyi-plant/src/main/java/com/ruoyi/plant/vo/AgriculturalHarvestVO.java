package com.ruoyi.plant.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 采收管理对象 agricultural_harvest
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class AgriculturalHarvestVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 农事计划 */
    @Excel(name = "农事计划")
    private String  planName;

    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date time;

    /** 数量 */
    @Excel(name = "数量")
    private Long quantity;

    /** 溯源码 */
    @Excel(name = "溯源码")
    private String sourceCode;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;



}
