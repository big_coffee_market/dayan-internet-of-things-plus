package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.AgriculturalHarvestMapper;
import com.ruoyi.plant.domain.AgriculturalHarvest;
import com.ruoyi.plant.service.IAgriculturalHarvestService;

/**
 * 采收管理Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class AgriculturalHarvestServiceImpl implements IAgriculturalHarvestService
{
    @Autowired
    private AgriculturalHarvestMapper agriculturalHarvestMapper;

    /**
     * 查询采收管理
     *
     * @param id 采收管理主键
     * @return 采收管理
     */
    @Override
    public AgriculturalHarvest selectAgriculturalHarvestById(Long id)
    {
        return agriculturalHarvestMapper.selectAgriculturalHarvestById(id);
    }

    /**
     * 查询采收管理列表
     *
     * @param agriculturalHarvest 采收管理
     * @return 采收管理
     */
    @Override
    public List<AgriculturalHarvest> selectAgriculturalHarvestList(AgriculturalHarvest agriculturalHarvest)
    {
        return agriculturalHarvestMapper.selectAgriculturalHarvestList(agriculturalHarvest);
    }

    /**
     * 新增采收管理
     *
     * @param agriculturalHarvest 采收管理
     * @return 结果
     */
    @Override
    public int insertAgriculturalHarvest(AgriculturalHarvest agriculturalHarvest)
    {
        return agriculturalHarvestMapper.insertAgriculturalHarvest(agriculturalHarvest);
    }

    /**
     * 修改采收管理
     *
     * @param agriculturalHarvest 采收管理
     * @return 结果
     */
    @Override
    public int updateAgriculturalHarvest(AgriculturalHarvest agriculturalHarvest)
    {
        return agriculturalHarvestMapper.updateAgriculturalHarvest(agriculturalHarvest);
    }

    /**
     * 批量删除采收管理
     *
     * @param ids 需要删除的采收管理主键
     * @return 结果
     */
    @Override
    public int deleteAgriculturalHarvestByIds(Long[] ids)
    {
        return agriculturalHarvestMapper.deleteAgriculturalHarvestByIds(ids);
    }

    /**
     * 删除采收管理信息
     *
     * @param id 采收管理主键
     * @return 结果
     */
    @Override
    public int deleteAgriculturalHarvestById(Long id)
    {
        return agriculturalHarvestMapper.deleteAgriculturalHarvestById(id);
    }
}
