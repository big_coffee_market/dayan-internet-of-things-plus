package com.ruoyi.plant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.plant.mapper.ConstCertificateStageMapper;
import com.ruoyi.plant.domain.ConstCertificateStage;
import com.ruoyi.plant.service.IConstCertificateStageService;

/**
 * 证书类型Service业务层处理
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
@Primary
public class ConstCertificateStageServiceImpl implements IConstCertificateStageService
{
    @Autowired
    private ConstCertificateStageMapper constCertificateStageMapper;

    /**
     * 查询证书类型
     *
     * @param id 证书类型主键
     * @return 证书类型
     */
    @Override
    public ConstCertificateStage selectConstCertificateStageById(Long id)
    {
        return constCertificateStageMapper.selectConstCertificateStageById(id);
    }

    /**
     * 查询证书类型列表
     *
     * @param constCertificateStage 证书类型
     * @return 证书类型
     */
    @Override
    public List<ConstCertificateStage> selectConstCertificateStageList(ConstCertificateStage constCertificateStage)
    {
        return constCertificateStageMapper.selectConstCertificateStageList(constCertificateStage);
    }

    /**
     * 新增证书类型
     *
     * @param constCertificateStage 证书类型
     * @return 结果
     */
    @Override
    public int insertConstCertificateStage(ConstCertificateStage constCertificateStage)
    {
        return constCertificateStageMapper.insertConstCertificateStage(constCertificateStage);
    }

    /**
     * 修改证书类型
     *
     * @param constCertificateStage 证书类型
     * @return 结果
     */
    @Override
    public int updateConstCertificateStage(ConstCertificateStage constCertificateStage)
    {
        return constCertificateStageMapper.updateConstCertificateStage(constCertificateStage);
    }

    /**
     * 批量删除证书类型
     *
     * @param ids 需要删除的证书类型主键
     * @return 结果
     */
    @Override
    public int deleteConstCertificateStageByIds(Long[] ids)
    {
        return constCertificateStageMapper.deleteConstCertificateStageByIds(ids);
    }

    /**
     * 删除证书类型信息
     *
     * @param id 证书类型主键
     * @return 结果
     */
    @Override
    public int deleteConstCertificateStageById(Long id)
    {
        return constCertificateStageMapper.deleteConstCertificateStageById(id);
    }
}
