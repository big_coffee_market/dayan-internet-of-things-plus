package com.ruoyi.plant.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 产品检测对象 product_testing
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public class ProductTesting extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 采收批次 */
    @Excel(name = "采收批次")
    private Long harvestId;

    /** 类型 */
    @Excel(name = "类型")
    private Long type;

    /** 机构 */
    @Excel(name = "机构")
    private String institution;

    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date time;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 结论 */
    @Excel(name = "结论")
    private String conclusion;

    /** 合格证名称 */
    @Excel(name = "合格证名称")
    private String certificateName;

    /** 证书编号 */
    @Excel(name = "证书编号")
    private String certificateNumber;

    /** 证书图片 */
    @Excel(name = "证书图片")
    private String certificatePicture;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setHarvestId(Long harvestId) 
    {
        this.harvestId = harvestId;
    }

    public Long getHarvestId() 
    {
        return harvestId;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setInstitution(String institution) 
    {
        this.institution = institution;
    }

    public String getInstitution() 
    {
        return institution;
    }
    public void setTime(Date time) 
    {
        this.time = time;
    }

    public Date getTime() 
    {
        return time;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setConclusion(String conclusion) 
    {
        this.conclusion = conclusion;
    }

    public String getConclusion() 
    {
        return conclusion;
    }
    public void setCertificateName(String certificateName) 
    {
        this.certificateName = certificateName;
    }

    public String getCertificateName() 
    {
        return certificateName;
    }
    public void setCertificateNumber(String certificateNumber) 
    {
        this.certificateNumber = certificateNumber;
    }

    public String getCertificateNumber() 
    {
        return certificateNumber;
    }
    public void setCertificatePicture(String certificatePicture) 
    {
        this.certificatePicture = certificatePicture;
    }

    public String getCertificatePicture() 
    {
        return certificatePicture;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("harvestId", getHarvestId())
            .append("type", getType())
            .append("institution", getInstitution())
            .append("time", getTime())
            .append("content", getContent())
            .append("conclusion", getConclusion())
            .append("certificateName", getCertificateName())
            .append("certificateNumber", getCertificateNumber())
            .append("certificatePicture", getCertificatePicture())
            .append("remarks", getRemarks())
            .toString();
    }
}
