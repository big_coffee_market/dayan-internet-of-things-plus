package com.ruoyi.plant.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 产品加工对象 product_processing
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class ProductProcessingVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 采收批次 */
    @Excel(name = "采收批次")
    private String harvestName;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** 数量 */
    @Excel(name = "数量")
    private Long quantity;

    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date time;

    /** 车间 */
    @Excel(name = "车间")
    private String workshop;

    /** 设备 */
    @Excel(name = "设备")
    private String equipment;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierName;

    /** 图片 */
    @Excel(name = "图片")
    private String picture;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;


}
