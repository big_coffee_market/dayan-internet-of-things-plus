package com.ruoyi.plant.service;

import java.util.List;
import com.ruoyi.plant.domain.AgriculturalResource;

/**
 * 农资管理Service接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface IAgriculturalResourceService 
{
    /**
     * 查询农资管理
     * 
     * @param id 农资管理主键
     * @return 农资管理
     */
    public AgriculturalResource selectAgriculturalResourceById(Long id);

    /**
     * 查询农资管理列表
     * 
     * @param agriculturalResource 农资管理
     * @return 农资管理集合
     */
    public List<AgriculturalResource> selectAgriculturalResourceList(AgriculturalResource agriculturalResource);

    /**
     * 新增农资管理
     * 
     * @param agriculturalResource 农资管理
     * @return 结果
     */
    public int insertAgriculturalResource(AgriculturalResource agriculturalResource);

    /**
     * 修改农资管理
     * 
     * @param agriculturalResource 农资管理
     * @return 结果
     */
    public int updateAgriculturalResource(AgriculturalResource agriculturalResource);

    /**
     * 批量删除农资管理
     * 
     * @param ids 需要删除的农资管理主键集合
     * @return 结果
     */
    public int deleteAgriculturalResourceByIds(Long[] ids);

    /**
     * 删除农资管理信息
     * 
     * @param id 农资管理主键
     * @return 结果
     */
    public int deleteAgriculturalResourceById(Long id);
}
