package com.ruoyi.plant.controller;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.nacos.shaded.org.checkerframework.checker.units.qual.A;
import com.ruoyi.plant.domain.AgriculturalPlantingPlot;
import com.ruoyi.plant.domain.AgriculturalResource;
import com.ruoyi.plant.service.IAgriculturalPlantingPlotService;
import com.ruoyi.plant.service.impl.AgriculturalPlantingPlotServiceImpl;
import com.ruoyi.plant.service.impl.AgriculturalResourceServiceImpl;
import com.ruoyi.plant.vo.AgriculturalPlanVO;
import com.ruoyi.plant.vo.tablist.AgriculturalPlanTableDataInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.AgriculturalPlan;
import com.ruoyi.plant.service.IAgriculturalPlanService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 农事计划Controller
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/agricultural_plan")
public class AgriculturalPlanController extends BaseController
{
    @Autowired
    private IAgriculturalPlanService agriculturalPlanService;

    /**
     * 地块服务
     */
    @Autowired
    private AgriculturalPlantingPlotServiceImpl agriculturalPlantingPlotService;

    /**
     * 农资服务
     */
    @Autowired
    private AgriculturalResourceServiceImpl agriculturalResourceService;

    /**
     * 查询农事计划列表
     */
    @RequiresPermissions("plant:agricultural_plan:list")
    @GetMapping("/list")
    public TableDataInfo list(AgriculturalPlan agriculturalPlan)
    {
        startPage();
        AgriculturalPlanTableDataInfo agriculturalPlanTableDataInfo = new AgriculturalPlanTableDataInfo();

        List<AgriculturalPlan> list = agriculturalPlanService.selectAgriculturalPlanList(agriculturalPlan);
        List<AgriculturalPlanVO> planVOS = new ArrayList<>();

        for(AgriculturalPlan item : list) {
            AgriculturalPlanVO agriculturalPlanVO = new AgriculturalPlanVO();
            BeanUtils.copyProperties(item,agriculturalPlanVO);

            Long plotId = item.getPlotId();
            Long seedId = item.getSeedId();

            AgriculturalPlantingPlot plantingPlot = agriculturalPlantingPlotService.selectAgriculturalPlantingPlotById(plotId);
            if(plantingPlot != null) {
                agriculturalPlanVO.setPlotName(plantingPlot.getName());
            }

            AgriculturalResource agriculturalResource =  agriculturalResourceService.selectAgriculturalResourceById(seedId);
            if(agriculturalResource != null) {
                agriculturalPlanVO.setSeedName(agriculturalResource.getName());
            }
            planVOS.add(agriculturalPlanVO);

        }

        AgriculturalPlantingPlot queryPlot = new AgriculturalPlantingPlot();
        List<AgriculturalPlantingPlot> plantingPlots =  agriculturalPlantingPlotService.selectAgriculturalPlantingPlotList(queryPlot);
        agriculturalPlanTableDataInfo.setPlotDropList(plantingPlots);

        AgriculturalResource querySeed = new AgriculturalResource();
        List<AgriculturalResource> resourceList = agriculturalResourceService.selectAgriculturalResourceList(querySeed);

        agriculturalPlanTableDataInfo.setSeedDropList(resourceList);


        return packVOList(agriculturalPlanTableDataInfo, planVOS);
    }

    /**
     * 导出农事计划列表
     */
    @RequiresPermissions("plant:agricultural_plan:export")
    @Log(title = "农事计划", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AgriculturalPlan agriculturalPlan)
    {
        List<AgriculturalPlan> list = agriculturalPlanService.selectAgriculturalPlanList(agriculturalPlan);
        ExcelUtil<AgriculturalPlan> util = new ExcelUtil<AgriculturalPlan>(AgriculturalPlan.class);
        util.exportExcel(response, list, "农事计划数据");
    }

    /**
     * 获取农事计划详细信息
     */
    @RequiresPermissions("plant:agricultural_plan:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(agriculturalPlanService.selectAgriculturalPlanById(id));
    }

    /**
     * 新增农事计划
     */
    @RequiresPermissions("plant:agricultural_plan:add")
    @Log(title = "农事计划", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AgriculturalPlan agriculturalPlan)
    {
        return toAjax(agriculturalPlanService.insertAgriculturalPlan(agriculturalPlan));
    }

    /**
     * 修改农事计划
     */
    @RequiresPermissions("plant:agricultural_plan:edit")
    @Log(title = "农事计划", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AgriculturalPlan agriculturalPlan)
    {
        return toAjax(agriculturalPlanService.updateAgriculturalPlan(agriculturalPlan));
    }

    /**
     * 删除农事计划
     */
    @RequiresPermissions("plant:agricultural_plan:remove")
    @Log(title = "农事计划", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(agriculturalPlanService.deleteAgriculturalPlanByIds(ids));
    }
}
