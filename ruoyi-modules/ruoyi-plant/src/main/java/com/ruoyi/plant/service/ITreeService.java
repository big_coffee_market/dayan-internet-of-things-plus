package com.ruoyi.plant.service;

import com.ruoyi.plant.vo.TreeVO;

import java.util.List;

public interface ITreeService<T> {


    /**
     * 获取树 VO
     * @return
     */
    TreeVO takeTree();

    /**
     * 获取节点数据
     * @param nodeId 节点id
     * @return
     */
    List<TreeVO.Node> takeNode(Long nodeId);

    /**
     * 增加节点
     * @param nodeId 节点id
     * @param model 模型
     * @return
     */
    Boolean addNode(Long nodeId,T model);

    /**
     * 删除对应得节点
     * @param nodeId
     * @return
     */
    Boolean remove(Long nodeId);


}
