package com.ruoyi.plant.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.system.api.domain.abs.IDropValue;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 采收管理对象 agricultural_harvest
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class AgriculturalHarvest extends BaseEntity implements IDropValue
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /**
     * 名称
     */
    private String name;

    /** 农事计划 */
    @Excel(name = "农事计划")
    private Long planId;

    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date time;

    /** 数量 */
    @Excel(name = "数量")
    private Long quantity;

    /** 溯源码 */
    @Excel(name = "溯源码")
    private String sourceCode;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;


}
