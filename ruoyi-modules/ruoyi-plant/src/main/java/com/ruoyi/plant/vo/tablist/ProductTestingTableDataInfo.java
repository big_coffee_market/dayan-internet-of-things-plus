package com.ruoyi.plant.vo.tablist;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.api.domain.abs.IDropValue;
import lombok.Data;

import java.util.List;


/**
 * 产品检测对象 product_testing
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class ProductTestingTableDataInfo extends TableDataInfo
{


    /** 采收批次 */
    @Excel(name = "采收批次")
    private List<? extends IDropValue> harvestDropList;

    /** 类型 */
    @Excel(name = "类型")
    private List<? extends IDropValue> typeDropList;



}
