package com.ruoyi.plant.mapper;

import java.util.List;
import com.ruoyi.plant.domain.ProductProcessing;

/**
 * 产品加工Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface ProductProcessingMapper 
{
    /**
     * 查询产品加工
     * 
     * @param id 产品加工主键
     * @return 产品加工
     */
    public ProductProcessing selectProductProcessingById(Long id);

    /**
     * 查询产品加工列表
     * 
     * @param productProcessing 产品加工
     * @return 产品加工集合
     */
    public List<ProductProcessing> selectProductProcessingList(ProductProcessing productProcessing);

    /**
     * 新增产品加工
     * 
     * @param productProcessing 产品加工
     * @return 结果
     */
    public int insertProductProcessing(ProductProcessing productProcessing);

    /**
     * 修改产品加工
     * 
     * @param productProcessing 产品加工
     * @return 结果
     */
    public int updateProductProcessing(ProductProcessing productProcessing);

    /**
     * 删除产品加工
     * 
     * @param id 产品加工主键
     * @return 结果
     */
    public int deleteProductProcessingById(Long id);

    /**
     * 批量删除产品加工
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductProcessingByIds(Long[] ids);
}
