package com.ruoyi.plant.controller;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.plant.domain.AgriculturalPlantingPlot;
import com.ruoyi.plant.domain.ConstAgriculturalStage;
import com.ruoyi.plant.service.impl.AgriculturalPlantingPlotServiceImpl;
import com.ruoyi.plant.service.impl.ConstAgriculturalStageServiceImpl;
import com.ruoyi.plant.vo.AgriculturalRecordVO;
import com.ruoyi.plant.vo.tablist.AgriculturalRecordTableDataInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.AgriculturalRecord;
import com.ruoyi.plant.service.IAgriculturalRecordService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 农事记录Controller
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/agricultural_record")
public class AgriculturalRecordController extends BaseController
{
    @Autowired
    private IAgriculturalRecordService agriculturalRecordService;

    /**
     * 农事阶段
     */
    @Autowired
    private ConstAgriculturalStageServiceImpl agriculturalStageService;

    /**
     * 种植地块
     */
    @Autowired
    private AgriculturalPlantingPlotServiceImpl agriculturalPlantingPlotService;

    /**
     * 查询农事记录列表
     */
    @RequiresPermissions("plant:agricultural_record:list")
    @GetMapping("/list")
    public TableDataInfo list(AgriculturalRecord agriculturalRecord)
    {
        startPage();
        List<AgriculturalRecord> list = agriculturalRecordService.selectAgriculturalRecordList(agriculturalRecord);
        List<AgriculturalRecordVO> voList = new ArrayList<>();
        for(AgriculturalRecord item : list) {
            AgriculturalRecordVO vo = new AgriculturalRecordVO();
            BeanUtils.copyProperties(item,vo);

            AgriculturalPlantingPlot plantingPlot = agriculturalPlantingPlotService.selectAgriculturalPlantingPlotById(item.getPlanId());
            if(plantingPlot != null) {
                vo.setPlanName(plantingPlot.getName());
            }

           ConstAgriculturalStage constAgriculturalStage = agriculturalStageService.selectConstAgriculturalStageById(item.getAgriculturalStageId());
           if(constAgriculturalStage != null) {
                vo.setAgriculturalStageName(constAgriculturalStage.getName());
            }

           voList.add(vo);
        }

        AgriculturalRecordTableDataInfo recordTableDataInfo = new AgriculturalRecordTableDataInfo();

        ConstAgriculturalStage constAgriculturalStage = new ConstAgriculturalStage();
        List<ConstAgriculturalStage> constAgriculturalStages = agriculturalStageService.selectConstAgriculturalStageList(constAgriculturalStage);
        recordTableDataInfo.setAgriculturalDropList(constAgriculturalStages);

        AgriculturalPlantingPlot queryPlot = new AgriculturalPlantingPlot();
        List<AgriculturalPlantingPlot> plantingPlots = agriculturalPlantingPlotService.selectAgriculturalPlantingPlotList(queryPlot);
        recordTableDataInfo.setPlanDropList(plantingPlots);


        return packVOList(recordTableDataInfo,voList);
    }

    /**
     * 导出农事记录列表
     */
    @RequiresPermissions("plant:agricultural_record:export")
    @Log(title = "农事记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AgriculturalRecord agriculturalRecord)
    {
        List<AgriculturalRecord> list = agriculturalRecordService.selectAgriculturalRecordList(agriculturalRecord);
        ExcelUtil<AgriculturalRecord> util = new ExcelUtil<AgriculturalRecord>(AgriculturalRecord.class);
        util.exportExcel(response, list, "农事记录数据");
    }

    /**
     * 获取农事记录详细信息
     */
    @RequiresPermissions("plant:agricultural_record:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(agriculturalRecordService.selectAgriculturalRecordById(id));
    }

    /**
     * 新增农事记录
     */
    @RequiresPermissions("plant:agricultural_record:add")
    @Log(title = "农事记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AgriculturalRecord agriculturalRecord)
    {
        return toAjax(agriculturalRecordService.insertAgriculturalRecord(agriculturalRecord));
    }

    /**
     * 修改农事记录
     */
    @RequiresPermissions("plant:agricultural_record:edit")
    @Log(title = "农事记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AgriculturalRecord agriculturalRecord)
    {
        return toAjax(agriculturalRecordService.updateAgriculturalRecord(agriculturalRecord));
    }

    /**
     * 删除农事记录
     */
    @RequiresPermissions("plant:agricultural_record:remove")
    @Log(title = "农事记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(agriculturalRecordService.deleteAgriculturalRecordByIds(ids));
    }
}
