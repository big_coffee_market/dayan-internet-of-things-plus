package com.ruoyi.plant.vo;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 种植地块对象 agricultural_planting_plot
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class AgriculturalPlantingPlotVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 基地 */
    @Excel(name = "基地")
    private String baseName;

    /** 责任人 */
    @Excel(name = "责任人")
    private String responsiblePerson;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String telPhone;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;




}
