package com.ruoyi.plant.controller;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.ruoyi.plant.domain.AgriculturalBase;
import com.ruoyi.plant.remote.ISysUserRemote;
import com.ruoyi.plant.service.IAgriculturalBaseService;
import com.ruoyi.plant.vo.AgriculturalPlantingPlotVO;
import com.ruoyi.plant.vo.tablist.AgriculturalPlantingPlotTableDataInfo;
import com.ruoyi.system.api.domain.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.AgriculturalPlantingPlot;
import com.ruoyi.plant.service.IAgriculturalPlantingPlotService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 种植地块Controller
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Slf4j
@RestController
@RequestMapping("/agricultural_planting_plot")
public class AgriculturalPlantingPlotController extends BaseController
{
    @Autowired
    private IAgriculturalPlantingPlotService agriculturalPlantingPlotService;

    /**
     * 基地服务
     */
    @Autowired
    private IAgriculturalBaseService agriculturalBaseService;


    /**
     * 用户服务
     */
    @Autowired
    private ISysUserRemote sysUserRemote;


    /**
     * 查询种植地块列表
     */
    @RequiresPermissions("plant:agricultural_planting_plot:list")
    @GetMapping("/list")
    public TableDataInfo list(AgriculturalPlantingPlot agriculturalPlantingPlot)
    {
        startPage();
        List<AgriculturalPlantingPlot> list = agriculturalPlantingPlotService.selectAgriculturalPlantingPlotList(agriculturalPlantingPlot);
        AgriculturalPlantingPlotTableDataInfo plotTableDataInfo = new AgriculturalPlantingPlotTableDataInfo();
        List<AgriculturalPlantingPlotVO> voList = new ArrayList<>();
        for(AgriculturalPlantingPlot item : list) {

            AgriculturalPlantingPlotVO vo = new AgriculturalPlantingPlotVO();
            BeanUtils.copyProperties(item,vo);
            AgriculturalBase agriculturalBase = agriculturalBaseService.selectAgriculturalBaseById(item.getBaseId());
            if(agriculturalBase != null) {
                vo.setBaseName(agriculturalBase.getName());
            }

            SysUser sysUser = (SysUser) sysUserRemote.getInfo(item.getUsrId()).get(AjaxResult.DATA_TAG);
            if(sysUser != null) {
                vo.setResponsiblePerson(sysUser.getUserName());
            }
            voList.add(vo);
        }

        SysUser queryUser = new SysUser();
        List<SysUser> sysUserList = (List<SysUser>) sysUserRemote.queryList(queryUser).get(AjaxResult.DATA_TAG);
        plotTableDataInfo.setUsrDropList(sysUserList);

        AgriculturalBase queryBase = new AgriculturalBase();
        List<AgriculturalBase> baseList = agriculturalBaseService.selectAgriculturalBaseList(queryBase);
        plotTableDataInfo.setBaseDropList(baseList);

        return packVOList(plotTableDataInfo,voList);
    }

    /**
     * 导出种植地块列表
     */
    @RequiresPermissions("plant:agricultural_planting_plot:export")
    @Log(title = "种植地块", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AgriculturalPlantingPlot agriculturalPlantingPlot)
    {
        List<AgriculturalPlantingPlot> list = agriculturalPlantingPlotService.selectAgriculturalPlantingPlotList(agriculturalPlantingPlot);
        ExcelUtil<AgriculturalPlantingPlot> util = new ExcelUtil<AgriculturalPlantingPlot>(AgriculturalPlantingPlot.class);
        util.exportExcel(response, list, "种植地块数据");
    }

    /**
     * 获取种植地块详细信息
     */
    @RequiresPermissions("plant:agricultural_planting_plot:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(agriculturalPlantingPlotService.selectAgriculturalPlantingPlotById(id));
    }

    /**
     * 新增种植地块
     */
    @RequiresPermissions("plant:agricultural_planting_plot:add")
    @Log(title = "种植地块", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AgriculturalPlantingPlot agriculturalPlantingPlot)
    {
        log.info("post:" + JSON.toJSONString(agriculturalPlantingPlot));
        return toAjax(agriculturalPlantingPlotService.insertAgriculturalPlantingPlot(agriculturalPlantingPlot));
    }

    /**
     * 修改种植地块
     */
    @RequiresPermissions("plant:agricultural_planting_plot:edit")
    @Log(title = "种植地块", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AgriculturalPlantingPlot agriculturalPlantingPlot)
    {
        return toAjax(agriculturalPlantingPlotService.updateAgriculturalPlantingPlot(agriculturalPlantingPlot));
    }

    /**
     * 删除种植地块
     */
    @RequiresPermissions("plant:agricultural_planting_plot:remove")
    @Log(title = "种植地块", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(agriculturalPlantingPlotService.deleteAgriculturalPlantingPlotByIds(ids));
    }
}
