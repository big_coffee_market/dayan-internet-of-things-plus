package com.ruoyi.plant.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.ConstProcessingStage;
import com.ruoyi.plant.service.IConstProcessingStageService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 加工类型Controller
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/processing_stage")
public class ConstProcessingStageController extends BaseController
{
    @Autowired
    private IConstProcessingStageService constProcessingStageService;

    /**
     * 查询加工类型列表
     */
    @RequiresPermissions("plant:processing_stage:list")
    @GetMapping("/list")
    public TableDataInfo list(ConstProcessingStage constProcessingStage)
    {
        startPage();
        List<ConstProcessingStage> list = constProcessingStageService.selectConstProcessingStageList(constProcessingStage);
        return getDataTable(list);
    }

    /**
     * 导出加工类型列表
     */
    @RequiresPermissions("plant:processing_stage:export")
    @Log(title = "加工类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ConstProcessingStage constProcessingStage)
    {
        List<ConstProcessingStage> list = constProcessingStageService.selectConstProcessingStageList(constProcessingStage);
        ExcelUtil<ConstProcessingStage> util = new ExcelUtil<ConstProcessingStage>(ConstProcessingStage.class);
        util.exportExcel(response, list, "加工类型数据");
    }

    /**
     * 获取加工类型详细信息
     */
    @RequiresPermissions("plant:processing_stage:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(constProcessingStageService.selectConstProcessingStageById(id));
    }

    /**
     * 新增加工类型
     */
    @RequiresPermissions("plant:processing_stage:add")
    @Log(title = "加工类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ConstProcessingStage constProcessingStage)
    {
        return toAjax(constProcessingStageService.insertConstProcessingStage(constProcessingStage));
    }

    /**
     * 修改加工类型
     */
    @RequiresPermissions("plant:processing_stage:edit")
    @Log(title = "加工类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ConstProcessingStage constProcessingStage)
    {
        return toAjax(constProcessingStageService.updateConstProcessingStage(constProcessingStage));
    }

    /**
     * 删除加工类型
     */
    @RequiresPermissions("plant:processing_stage:remove")
    @Log(title = "加工类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(constProcessingStageService.deleteConstProcessingStageByIds(ids));
    }
}
