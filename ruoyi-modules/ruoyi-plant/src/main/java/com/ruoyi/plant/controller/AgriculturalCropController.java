package com.ruoyi.plant.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.plant.domain.AgriculturalResource;
import com.ruoyi.plant.service.impl.AgriculturalResourceServiceImpl;
import com.ruoyi.plant.vo.AgriculturalCropVO;
import com.ruoyi.plant.vo.tablist.AgriculturalCropTableDataInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.plant.domain.AgriculturalCrop;
import com.ruoyi.plant.service.IAgriculturalCropService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 农作物Controller
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/agricultural_crop")
public class AgriculturalCropController extends BaseController
{
    /**
     * 农资植物
     */
    @Autowired
    private IAgriculturalCropService agriculturalCropService;

    /**
     * 农资服务
     */
    @Autowired
    private AgriculturalResourceServiceImpl agriculturalResourceService;


    /**
     * 查询农作物列表
     */
    @RequiresPermissions("plant:agricultural_crop:list")
    @GetMapping("/list")
    public TableDataInfo list(AgriculturalCrop agriculturalCrop)
    {
        startPage();
        List<AgriculturalCrop> list = agriculturalCropService.selectAgriculturalCropList(agriculturalCrop);
        List<AgriculturalCropVO> voList = new ArrayList<>();
        for(AgriculturalCrop item: list) {
            AgriculturalCropVO  agriculturalCropVO = new AgriculturalCropVO();
            BeanUtils.copyProperties(item,agriculturalCropVO);
            AgriculturalResource agriculturalResource = agriculturalResourceService.selectAgriculturalResourceById(item.getResourceSeedId());
            if(agriculturalResource != null) {
                agriculturalCropVO.setName(agriculturalResource.getName());
            }
            voList.add(agriculturalCropVO);
        }

        AgriculturalCropTableDataInfo agriculturalCropTableDataInfo = new AgriculturalCropTableDataInfo();

        AgriculturalResource query = new AgriculturalResource();
        List<AgriculturalResource> resourceList = agriculturalResourceService.selectAgriculturalResourceList(query);

        agriculturalCropTableDataInfo.setSeedDropList(resourceList);

        return packVOList(agriculturalCropTableDataInfo,voList);
    }

    /**
     * 导出农作物列表
     */
    @RequiresPermissions("plant:agricultural_crop:export")
    @Log(title = "农作物", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AgriculturalCrop agriculturalCrop)
    {
        List<AgriculturalCrop> list = agriculturalCropService.selectAgriculturalCropList(agriculturalCrop);
        ExcelUtil<AgriculturalCrop> util = new ExcelUtil<AgriculturalCrop>(AgriculturalCrop.class);
        util.exportExcel(response, list, "农作物数据");
    }

    /**
     * 获取农作物详细信息
     */
    @RequiresPermissions("plant:agricultural_crop:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(agriculturalCropService.selectAgriculturalCropById(id));
    }

    /**
     * 新增农作物
     */
    @RequiresPermissions("plant:agricultural_crop:add")
    @Log(title = "农作物", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AgriculturalCrop agriculturalCrop)
    {
        return toAjax(agriculturalCropService.insertAgriculturalCrop(agriculturalCrop));
    }

    /**
     * 修改农作物
     */
    @RequiresPermissions("plant:agricultural_crop:edit")
    @Log(title = "农作物", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AgriculturalCrop agriculturalCrop)
    {
        return toAjax(agriculturalCropService.updateAgriculturalCrop(agriculturalCrop));
    }

    /**
     * 删除农作物
     */
    @RequiresPermissions("plant:agricultural_crop:remove")
    @Log(title = "农作物", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(agriculturalCropService.deleteAgriculturalCropByIds(ids));
    }
}
