package com.ruoyi.plant.vo;

import lombok.Data;

import java.util.List;

@Data
public class TreeVO {



    /**
     * 数组
     */
    List<Node> data;




    @Data
    public static class Node {

        /**
         * 身份标识
         */
        Long id;

        /**
         * 标签
         */
        String label;

        /**
         * 数据
         */
        List<Node> children;

    }




}
