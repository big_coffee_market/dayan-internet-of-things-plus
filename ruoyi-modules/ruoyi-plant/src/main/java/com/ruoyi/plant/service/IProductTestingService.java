package com.ruoyi.plant.service;

import java.util.List;
import com.ruoyi.plant.domain.ProductTesting;

/**
 * 产品检测Service接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface IProductTestingService 
{
    /**
     * 查询产品检测
     * 
     * @param id 产品检测主键
     * @return 产品检测
     */
    public ProductTesting selectProductTestingById(Long id);

    /**
     * 查询产品检测列表
     * 
     * @param productTesting 产品检测
     * @return 产品检测集合
     */
    public List<ProductTesting> selectProductTestingList(ProductTesting productTesting);

    /**
     * 新增产品检测
     * 
     * @param productTesting 产品检测
     * @return 结果
     */
    public int insertProductTesting(ProductTesting productTesting);

    /**
     * 修改产品检测
     * 
     * @param productTesting 产品检测
     * @return 结果
     */
    public int updateProductTesting(ProductTesting productTesting);

    /**
     * 批量删除产品检测
     * 
     * @param ids 需要删除的产品检测主键集合
     * @return 结果
     */
    public int deleteProductTestingByIds(Long[] ids);

    /**
     * 删除产品检测信息
     * 
     * @param id 产品检测主键
     * @return 结果
     */
    public int deleteProductTestingById(Long id);
}
