package com.ruoyi.plant.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 农资管理对象 agricultural_resource
 *
 * @author ruoyi
 * @date 2023-02-18
 */
@Data
public class AgriculturalResourceVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 类别 */
    @Excel(name = "类别")
    private String classificationName;

    /** 包装规格 */
    @Excel(name = "包装规格")
    private String packingSpecification;

    /** 库存量 */
    @Excel(name = "库存量")
    private Long inventory;

    /** 生产厂家 */
    @Excel(name = "生产厂家")
    private String manufacturer;

    /** 生产日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生产日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dateOfProduction;

    /** 保质期 */
    @Excel(name = "保质期")
    private String shelfLife;

    /** 批准文号 */
    @Excel(name = "批准文号")
    private String approvalNumber;

    /** 生产许可证 */
    @Excel(name = "生产许可证")
    private String productionLicense;

    /** 销售厂商 */
    @Excel(name = "销售厂商")
    private String sellingFirm;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;


}
