package com.ruoyi.plant.vo;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import com.ruoyi.system.api.domain.abs.IDropValue;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 基地对象 agricultural_base
 *
 * @author ruoyi
 * @date 2023-02-21
 */
@Data
public class AgriculturalBaseVO extends BaseEntity implements IDropValue
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 责任人 */
    @Excel(name = "责任人")
    private String usrName;



}
