package com.ruoyi.system.controller;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.api.domain.SysUser;

import com.ruoyi.system.controller.feign.provider.ISysPermissionFeign;
import com.ruoyi.system.service.impl.SysPermissionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/permission")
public class SysPermissionController implements ISysPermissionFeign {

    @Autowired
    SysPermissionServiceImpl sysPermissionService;


    @Override
    public AjaxResult getRolePermission(SysUser user) {
        return AjaxResult.success(sysPermissionService.getRolePermission(user));
    }


    @Override
    public AjaxResult getMenuPermission(SysUser user) {

        return AjaxResult.success(sysPermissionService.getMenuPermission(user));
    }


}
