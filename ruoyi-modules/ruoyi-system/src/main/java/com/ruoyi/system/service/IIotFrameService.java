package com.ruoyi.system.service;


import com.ruoyi.system.domain.IotFrame;

import java.util.List;

/**
 * 厂家协议Service接口
 * 
 * @author ruoyi
 * @date 2022-12-02
 */
public interface IIotFrameService 
{
    /**
     * 查询厂家协议
     * 
     * @param id 厂家协议主键
     * @return 厂家协议
     */
    public IotFrame selectIotFrameById(Long id);

    /**
     * 查询厂家协议列表
     * 
     * @param iotFrame 厂家协议
     * @return 厂家协议集合
     */
    public List<IotFrame> selectIotFrameList(IotFrame iotFrame);

    /**
     * 新增厂家协议
     * 
     * @param iotFrame 厂家协议
     * @return 结果
     */
    public int insertIotFrame(IotFrame iotFrame);

    /**
     * 修改厂家协议
     * 
     * @param iotFrame 厂家协议
     * @return 结果
     */
    public int updateIotFrame(IotFrame iotFrame);

    /**
     * 批量删除厂家协议
     * 
     * @param ids 需要删除的厂家协议主键集合
     * @return 结果
     */
    public int deleteIotFrameByIds(Long[] ids);

    /**
     * 删除厂家协议信息
     * 
     * @param id 厂家协议主键
     * @return 结果
     */
    public int deleteIotFrameById(Long id);
}
