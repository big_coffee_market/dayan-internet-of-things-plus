package com.ruoyi.file.config;

import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MinioClientConfig {

    @Value("${minio.url}")
    private String url;

    @Value("${minio.access-key}")
    private String accessKey;

    @Value("${minio.secret-key}")
    private String secretKey;



    /**
     * 文件存储客户端
     */
    @Bean
    public MinioClient createMinio() {
        System.out.println("minio url:" + url + "accessKey:" + accessKey + "secretKey:" + secretKey);
        MinioClient miniClient = MinioClient.builder().endpoint(url) //本地ip+端口号
                .credentials(accessKey, secretKey)  //用户名和密码
                .build();
        return miniClient;
    }







}
