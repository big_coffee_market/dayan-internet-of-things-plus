package com.ruoyi.file.controller;

import com.ruoyi.common.core.utils.file.FileUtils;
import com.ruoyi.file.minio.MinioClientProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.domain.SysFile;

import java.io.File;
import java.io.FileOutputStream;

/**
 * 文件请求处理
 *
 * @author ruoyi
 */
@RestController
public class SysFileController
{
    private static final Logger log = LoggerFactory.getLogger(SysFileController.class);

    @Autowired
    private MinioClientProxy minioClientProxy;

    /**
     * 文件上传请求
     */
    @PostMapping("upload")
    public R<SysFile> upload(MultipartFile file)
    {
        try
        {
            File tempFile = new File(file.getOriginalFilename());
            if(!tempFile.exists()) {
                tempFile.createNewFile();
            }
            FileOutputStream tempStream = new FileOutputStream(tempFile);
            tempStream.write(file.getBytes());
            tempStream.close();
            // 上传并返回访问地址
            String url = minioClientProxy.upFile(tempFile);
            SysFile sysFile = new SysFile();
            sysFile.setName(FileUtils.getName(url));
            sysFile.setUrl(url);
            org.apache.commons.io.FileUtils.delete(tempFile);
            return R.ok(sysFile);
        }
        catch (Exception e)
        {
            log.error("上传文件失败", e);
            return R.fail(e.getMessage());
        }
    }
}
