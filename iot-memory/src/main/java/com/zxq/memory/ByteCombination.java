package com.zxq.memory;




import java.nio.ByteBuffer;
import java.util.List;

public class ByteCombination {

    /**
     * 合并byte[] 数组
     * @param byteArray byte数组
     * @return 返回合并后的byte[]数组
     */
    public static byte[] combinationContent(byte[]... byteArray) {
        int len = 0;
        for(byte[] buff:byteArray)
        {
            len += buff.length;
        }
        ByteBuffer byteBuffer =  ByteBuffer.wrap(ByteArrayPools.getBytes(len + "")); //ByteBuffer.allocate(len);
        for(byte[] buff:byteArray)
        {
            byteBuffer.put(buff);
        }
        byte[] buff = byteBuffer.array();
        return buff;
    }

    /**
     * bytes动态数组，提供给注解使用的
     * @param byteArray bytes数组
     * @return
     */
    public static byte[] combinationContent(List<byte[]> byteArray) {
        int len = 0;
        for(byte[] buff:byteArray)
        {
            len += buff.length;
        }
        ByteBuffer byteBuffer =  ByteBuffer.wrap(ByteArrayPools.getBytes(len + "")); //ByteBuffer.allocate(len);
        for(byte[] buff:byteArray)
        {
            byteBuffer.put(buff);
        }
        byte[] buff = byteBuffer.array();
        return buff;
    }



//    public static void main(String[] args) {
//       byte[] msg = ByteCombination.combinationContent(new byte[]{1,2,3,4},new byte[]{5,6,7,8});
//       log.info("hex:" + HexBin.encode(msg));
//    }







}